/*
** connection.c for zappy in /home/gressi_b/zappy/connection
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Tue May 15 17:16:54 2012 gressi_b
** Last update Sun Jul 15 18:56:03 2012 benjamin bourdin
*/

#include	"connect.h"

int			connect_set_server(t_connect *this, int queue_length)
{
  struct protoent	*protocol;

  if (!(protocol = getprotobyname(TCP)))
    return (ret_perror("getprotobyname(3)"));
  if ((this->fd = socket(AF_INET, SOCK_STREAM, protocol->p_proto)) == -1)
    return (ret_perror("socket(2)"));
  if (bind(this->fd, (struct sockaddr*) &(this->sock_addr),
	   sizeof(this->sock_addr)))
    {
      close(this->fd);
      return (ret_perror("bind(2)"));
    }
  if (listen(this->fd, queue_length))
    {
      close(this->fd);
      return (ret_perror("listen(2)"));
    }
  return (0);
}

int		connect_accept(t_connect *this, t_connect *client)
{
  socklen_t	size;

  size = sizeof(client->sock_addr);
  if ((client->fd = accept(this->fd, (struct sockaddr *) &client->sock_addr,
			   &size)) < 0)
    return (ret_perror("accept(2)"));
  return (client->fd);
}

int		connect_connect(t_connect *this)
{
  return (connect(this->fd, (struct sockaddr *) &(this->sock_addr),
		  sizeof(this->sock_addr)));
}

int		connect_destroy(t_connect *this)
{
  return (close(this->fd));
}

int		connect_init(t_connect *this, char *addr, int port)
{
  this->sock_addr.sin_family = AF_INET;
  this->sock_addr.sin_port = htons(port);
  if (!inet_aton(addr, &this->sock_addr.sin_addr))
    return (ret_perror("inet_aton(3)"));
  this->fd = -1;
  this->accept = &connect_accept;
  this->connect = &connect_connect;
  this->destroy = &connect_destroy;
  this->recv = &connect_recv;
  this->send = &connect_send;
  this->set_server = &connect_set_server;
  return (0);
}
