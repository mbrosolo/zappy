/*
** connect.h for connect in /home/brosol_m/docs/systeme_unix/zappy/lib/connect/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Mon Jun 18 16:23:18 2012 matthias brosolo
** Last update Mon Jun 18 16:23:18 2012 matthias brosolo
*/

#ifndef	__CONNECT_H__
# define	__CONNECT_H__

# define	_BSD_SOURCE

# include	<arpa/inet.h>
# include	<netdb.h>
# include	<netinet/in.h>
# include	<sys/socket.h>
# include	<sys/types.h>
# include	<unistd.h>
# include	"../../utils/includes/utils.h"
/*
** DEFINES
*/
# define	TCP		"TCP"

/*
** STRUCTURES
*/
typedef	struct		s_connect
{
  int			fd;
  int			port;
  struct sockaddr_in	sock_addr;
  int			(*accept)(struct s_connect *this,
				  struct s_connect *client);
  int			(*connect)(struct s_connect *this);
  int			(*destroy)(struct s_connect *this);
  ssize_t		(*recv)(struct s_connect *this, void *buffer,
				size_t size);
  ssize_t		(*send)(struct s_connect *this, void *buffer,
				size_t size);
  int			(*set_server)(struct s_connect *this,
				      int queue_length);
}			t_connect;

int		connect_accept(t_connect *this, t_connect *client);
int		connect_connect(t_connect *this);
int		connect_destroy(t_connect *this);
int		connect_init(t_connect *this, char *addr, int port);
int		connect_set_server(t_connect *this, int queue_length);
ssize_t		connect_send(t_connect *this, void *data, size_t size);
ssize_t		connect_recv(t_connect *this, void *data, size_t size);

#endif
