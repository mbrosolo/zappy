/*
** transfert.c for connect in /home/brosol_m/docs/systeme_unix/zappy/lib/connect/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Mon Jun 18 17:29:23 2012 matthias brosolo
** Last update Mon Jun 18 17:29:23 2012 matthias brosolo
*/

#include	"connect.h"

ssize_t			connect_send(t_connect *this, void *buffer, size_t size)
{
  return (send(this->fd, buffer, size, 0));
}

ssize_t			connect_recv(t_connect *this, void *buffer, size_t size)
{
  return (recv(this->fd, buffer, size, 0));
}
