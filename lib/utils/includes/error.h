/*
** error.h for utils in lib/utils/includes
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Mar 29 22:32:45 2012 matthias brosolo
** Last update Thu Mar 29 22:32:45 2012 matthias brosolo
*/

#ifndef	__ERROR_H__
# define	__ERROR_H__

# include	<stdio.h>

/*
** error.c
*/
int	ret_perror(char *function);
int	ret_error(char *error);

#endif
