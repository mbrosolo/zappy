/*
** error.c for utils in lib/utils/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed Mar  7 09:51:20 2012 matthias brosolo
** Last update Thu Mar  8 10:49:06 2012 matthias brosolo
*/

#include	"error.h"

int	ret_perror(char *function)
{
  perror(function);
  return (1);
}

int	ret_error(char *error)
{
  fprintf(stderr, "%s\n", error);
  return (1);
}
