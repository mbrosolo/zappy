/*
** warn.h for warn in /home/gressi_b/ftrace/lib/libwarn
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Tue Jun 12 14:21:29 2012 gressi_b
** Last update Tue Jun 12 14:21:29 2012 gressi_b
*/

#ifndef	__LIB_WARN_H__
#define	__LIB_WARN_H__

void	*wmalloc(size_t size);
int	wopen(char const *pathname, int flags);
int	wclose(int fd);

#endif
