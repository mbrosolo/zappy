/*
** wopen.c for wopen in /home/gressi_b/ftrace/lib/libwarn
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Tue Jun 12 14:01:47 2012 gressi_b
** Last update Tue Jun 12 14:01:47 2012 gressi_b
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <err.h>

int	wopen(char const *pathname, int flags)
{
  int	fd;

  fd = open(pathname, flags);
  if (fd == -1)
    warn("could not open [%s]", pathname);
  return (fd);
}
