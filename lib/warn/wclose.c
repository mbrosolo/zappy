/*
** wclose.c for wclose in /home/gressi_b/ftrace/lib/libwarn
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Tue Jun 12 14:04:02 2012 gressi_b
** Last update Tue Jun 12 14:04:02 2012 gressi_b
*/

#include <unistd.h>
#include <err.h>

int	wclose(int fd)
{
  int	r;

  r = close(fd);
  if (r == -1)
    warn("invalid closure of fd %d", fd);
  return (r);
}
