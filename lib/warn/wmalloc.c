/*
** wmalloc.c for wmalloc in /home/gressi_b/ftrace/lib/libwarn
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Tue Jun 12 13:54:10 2012 gressi_b
** Last update Tue Jun 12 13:54:10 2012 gressi_b
*/

#include <stdlib.h>
#include <err.h>

void	*wmalloc(size_t size)
{
  void	*p;

  p = malloc(size);
  if (p == NULL)
    warn("malloc(3)");
  return (p);
}
