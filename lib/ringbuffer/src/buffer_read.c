/*
** buffer_read.c for buffer in /afs/epitech.net/users/epitech_2015/bourdi_b/public/irc/lib/ringbuffer
**
** Made by Ga
** Login   <ga@epitech.net>
**
** Started on  Tue Apr 17 16:31:14 2012 Ga
** Last update Sun Jul 15 19:09:37 2012 benjamin bourdin
*/

#include	"ringbuffer.h"

int		is_eor(t_ref_state *rstate, t_ref_state *wstate)
{
  if (rstate->linkpos.current(&rstate->linkpos)
      == wstate->linkpos.current(&wstate->linkpos)
      && rstate->bufpos == wstate->bufpos)
    return (1);
  return (0);
}

void		update_state(t_buffer *b)
{
  t_ref_state	*ref;

  ref = &b->rstate;
  if ((ref->bufpos + 1 == b->sizebuf))
    {
      ref->bufpos = 0;
      if (ref->linkpos.current(&ref->linkpos)
	  != b->wstate.linkpos.current(&b->wstate.linkpos))
	ref->linkpos.remove(&ref->linkpos, &free);
      else
	ref->linkpos.next(&ref->linkpos);
      if (ref->linkpos.current(&ref->linkpos)
      	  == b->bufflist.end(&b->bufflist))
      	ref->linkpos.next(&ref->linkpos);
   }
  else
    ref->bufpos++;
}

int		get_circ(t_buffer *buffer, char *set)
{
  char		*data;

  if (!(buffer->bufflist.size(&buffer->bufflist))
      || is_eor(&buffer->rstate, &buffer->wstate))
    return (END_OF_BUFFER);
  data = (char *) buffer->rstate.linkpos.data;
  *set = (char) data[buffer->rstate.bufpos];
  update_state(buffer);
  return (1);
}

int		rewind_buffer(t_buffer *buffer)
{
  if (buffer->bufflist.size(&buffer->bufflist))
    {
      buffer->bufflist.begin(&buffer->bufflist, &buffer->rstate.linkpos);
      buffer->rstate.bufpos = 0;
      return (1);
    }
  else
    return (-1);
}
