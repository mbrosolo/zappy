/*
** buffer.c for ringbuffer in ringbuffer/src
**
** Made by gaetan senn
** Login   <ga@epitech.net>
**
** Started on  Tue Apr 17 16:37:59 2012 gaetan senn
** Last update Sun Apr 22 13:41:16 2012 benjamin bourdin
*/

#include	"ringbuffer.h"

int	create_ringbuffer(t_buffer *buffer, size_t size)
{
  if (create_list(&buffer->bufflist))
    return (1);
  buffer->rstate.bufpos = 0;
  buffer->bufflist.begin(&buffer->bufflist, &buffer->rstate.linkpos);
  buffer->wstate.bufpos = 0;
  buffer->bufflist.begin(&buffer->bufflist, &buffer->wstate.linkpos);
  buffer->sizebuf = (size ? size : BUFSIZE);
  return (0);
}

void	cleaner(void *a)
{
  free(a);
}

void	delete_ringbuffer(t_buffer *b)
{
  b->bufflist.delete(&b->bufflist, &free);
}
