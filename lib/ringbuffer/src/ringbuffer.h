/*
** ringbuffer.h for ringbuffer in ringbuffer/src
**
** Made by gaetan senn
** Login   <ga@epitech.net>
**
** Started on  Tue Apr 17 16:21:43 2012 gaetan senn
** Last update Sat Apr 21 14:55:19 2012 benjamin bourdin
*/

#ifndef __RINGBUFFER__
# define	__RINGBUFFER__

# include	<string.h>
# include	"../../object_list/src/list.h"
# include	"../../warn/warn.h"

# define	BUFSIZE 1024
# define	END_OF_BUFFER -2

typedef struct		s_list_data
{
  char			buf[BUFSIZE];
}			t_list_data;

typedef struct		s_ref_state
{
  size_t		bufpos;
  t_list_iterator	linkpos;
}			t_ref_state;

typedef struct	s_buffer
{
  size_t	sizebuf;
  t_list	bufflist;
  t_ref_state	rstate;
  t_ref_state	wstate;
}		t_buffer;

/*
** Functions in ringbuffer.c
*/
int		create_ringbuffer(t_buffer *, size_t);
void		cleaner(void *);
void		delete_ringbuffer(t_buffer *);

/*
** Functions in buffer_read.c
*/
int		is_eor(t_ref_state *rstate, t_ref_state *wstate);
int		remove_it(void *, void *);
void		update_state(t_buffer *buffer);
int		get_circ(t_buffer *buffer, char *data);
int		rewind_buffer(t_buffer *buffer);

/*
** Functions in buffer_write.c
*/
char		add_data(t_buffer *, char);
char		put_circ(t_buffer *, char);
char		string_circ(t_buffer *, char const *, size_t);

#endif
