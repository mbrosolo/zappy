/*
** buffer_write.c for buffer_write in /home/ga/Documents/Projet/irc/lib/liblist/ringbuffer
**
** Made by Ga
** Login   <ga@epitech.net>
**
** Started on  Wed Apr 18 14:01:56 2012 Ga
** Last update Sun Jul 15 19:10:11 2012 benjamin bourdin
*/

#include	"ringbuffer.h"

char		add_new_node(t_buffer *buffer, t_ref_state *ref)
{
  void		*tmp;

  if (ref->linkpos.list->next ==
      buffer->bufflist.end(&buffer->bufflist))
    ref->linkpos.next(&ref->linkpos);
  if (ref->linkpos.list->next
      == buffer->rstate.linkpos.current(&buffer->rstate.linkpos))
    {
      if (!(tmp = wmalloc(buffer->sizebuf)))
	return (-1);
      ref->linkpos.insert_next(&ref->linkpos, tmp);
      memset(ref->linkpos.data, 0, buffer->sizebuf);
    }
  else
    ref->linkpos.next(&ref->linkpos);
  ref->bufpos = 0;
  return (0);
}
char		add_data(t_buffer *buffer, char c)
{
  size_t	sizebuf;
  t_ref_state	*ref;

  sizebuf = buffer->sizebuf;
  ref = &buffer->wstate;
  ((char *)ref->linkpos.data)[ref->bufpos] = c;
  if (ref->bufpos + 1 == sizebuf)
    {
      if (add_new_node(buffer, ref) == -1)
	return (-1);
    }
  else
    ref->bufpos++;
  return (0);
}

char		put_circ(t_buffer *buffer, char set)
{
  void		*tmp;

  if (!(buffer->bufflist.size(&buffer->bufflist)))
    {
      if (!(tmp = wmalloc(buffer->sizebuf)))
	return (-1);
      buffer->bufflist.push_back(&buffer->bufflist, tmp);
      buffer->rstate.bufpos = 0;
      buffer->wstate.bufpos = 0;
      buffer->bufflist.begin(&buffer->bufflist, &buffer->rstate.linkpos);
      buffer->bufflist.begin(&buffer->bufflist, &buffer->wstate.linkpos);
      memset(buffer->wstate.linkpos.data, 0, buffer->sizebuf);
    }
  return (add_data(buffer, set));
}

char		string_circ(t_buffer *buffer, char const *str, size_t size)
{
  size_t	i;

  i = 0;
  while (i < size)
    {
      if (put_circ(buffer, str[i]) == -1)
	return (-1);
      ++i;
    }
  return (0);
}
