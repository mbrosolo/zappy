/*
** double_clist_access.c for object_list in object_list/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri May 18 16:04:34 2012 matthias brosolo
** Last update Fri May 18 16:04:34 2012 matthias brosolo
*/

#include	"double_clist.h"

t_double_clist		*create_node_double_clist()
{
  t_double_clist	*a;

  a = wmalloc(sizeof(*a));
  if (!a)
    return (NULL);
  a->next = a;
  a->prev = a;
  a->data = (void *)FIRST_NODE;
  return (a);
}

t_double_clist		*create_link_double_clist(void *data)
{
  t_double_clist	*a;

  a = wmalloc(sizeof(*a));
  if (!a)
    return (NULL);
  a->data = data;
  a->next = NULL;
  a->prev = NULL;
  return (a);
}
