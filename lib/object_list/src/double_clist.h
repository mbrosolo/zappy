/*
** double_clist.h for object_list in object_list/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri May 25 12:18:24 2012 matthias brosolo
** Last update Fri May 25 12:18:24 2012 matthias brosolo
*/

#ifndef	__DOUBLE_LINK_CIRCULAR_LIST__
# define	__DOUBLE_LINK_CIRCULAR_LIST__

# include	<stdlib.h>
# include	"../../warn/warn.h"

/*
** DEFINES
*/
# define	FIRST_NODE	0x5A4D

/*
** STRUCTURES
*/
typedef	struct		s_double_clist
{
  void			*data;
  struct s_double_clist	*next;
  struct s_double_clist	*prev;
}			t_double_clist;

/*
** PROTOTYPES
*/
/*
** double_clist_access.c
*/
t_double_clist		*create_node_double_clist();
t_double_clist		*create_link_double_clist(void *data);

/*
** double_clist_modifiers.c
*/
t_double_clist		*free_node_double_clist(t_double_clist *it,
						void (*f)(void *p));
t_double_clist		*push_back_double_clist(t_double_clist *list,
						void *data);
t_double_clist		*push_front_double_clist(t_double_clist *list,
						 void *data);

/*
** double_clist_insert.c
*/
t_double_clist		*insert_prev(t_double_clist *list, void *data);
t_double_clist		*insert_next(t_double_clist *list, void *data);
t_double_clist		*insert_at_pos(t_double_clist *list,
				       size_t pos, void *data);

#endif
