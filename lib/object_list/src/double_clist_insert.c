/*
** double_clist_insert.c for object_list in object_list/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri May 25 12:00:57 2012 matthias brosolo
** Last update Fri May 25 12:00:57 2012 matthias brosolo
*/

#include	"double_clist.h"

t_double_clist	*insert_prev(t_double_clist *list, void *data)
{
  return (push_back_double_clist(list, data));
}

t_double_clist	*insert_next(t_double_clist *list, void *data)
{
  return (push_front_double_clist(list, data));
}

t_double_clist	*insert_at_pos(t_double_clist *node, size_t pos, void *data)
{
  size_t	i;

  i = 0;
  node = node->next;
  while (i++ < pos && node->data != (void *) FIRST_NODE)
    node = node->next;
  return (push_back_double_clist(node, data));
}
