/*
** double_clist_modifiers.c for object_list in object_list/src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Fri May 25 11:57:24 2012 matthias brosolo
** Last update Sun Jul 15 18:57:12 2012 benjamin bourdin
*/

#include	<unistd.h>
#include	"double_clist.h"

static void	first_link(t_double_clist *it, t_double_clist *tmp)
{
  it->next = tmp;
  it->prev = tmp;
  tmp->next = it;
  tmp->prev = it;
}

t_double_clist		*push_back_double_clist(t_double_clist *node,
						void *data)
{
  t_double_clist	*it;
  t_double_clist	*tmp;

  if (!(tmp = create_link_double_clist(data)))
    return (NULL);
  it = node;
  if (it->prev == it)
    first_link(it, tmp);
  else
    {
      it->prev->next = tmp;
      tmp->prev = it->prev;
      tmp->next = it;
      it->prev = tmp;
    }
  return (tmp);
}

t_double_clist		*push_front_double_clist(t_double_clist *node,
						 void *data)
{
  t_double_clist	*it;
  t_double_clist	*tmp;

  if (!(tmp = create_link_double_clist(data)))
    return (NULL);
  it = node;
  if (it->next == it)
    first_link(it, tmp);
  else
    {
      it->next->prev = tmp;
      tmp->prev = it;
      tmp->next = it->next;
      it->next = tmp;
    }
  return (tmp);
}

t_double_clist		*free_node_double_clist(t_double_clist *it,
						void (*f)(void *p))
{
  t_double_clist	*tmp;

  tmp = it->next;
  if (it->prev != it)
    {
      it->prev->next = it->next;
      it->next->prev = it->prev;
    }
  if (f)
    f(it->data);
  free(it);
  return (tmp);
}
