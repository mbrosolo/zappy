/*
** list.h for object_list in object_list/src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Wed May 16 10:29:12 2012 matthias brosolo
** Last update Sun Jul 15 19:07:31 2012 benjamin bourdin
*/

#ifndef	__LIST_H__
# define	__LIST_H__

# include	"double_clist.h"

typedef	enum	e_bool
  {
    FALSE = 0,
    TRUE
  }		t_bool;

typedef	int	(*t_pred)(void *data, void *cmp);

typedef	struct		s_list_iterator
{
  void			*data;
  size_t		*count;
  t_double_clist	*list;
  void			*(*current)(struct s_list_iterator *it);
  void			(*next)(struct s_list_iterator *it);
  void			(*prev)(struct s_list_iterator *it);
  void			*(*insert)(struct s_list_iterator *it, void *data);
  void			*(*insert_next)(struct s_list_iterator *it, void *data);
  void			*(*remove)(struct s_list_iterator *it, void (*f)(void *p));
}			t_list_iterator;

typedef	struct		s_list
{
  size_t		count;
  t_double_clist	*node;
  void			*(*back)(struct s_list *list);
  void			*(*front)(struct s_list *list);
  void			*(*push_back)(struct s_list *list, void *data);
  void			*(*push_front)(struct s_list *list, void *data);
  void			*(*pop_back)(struct s_list *list);
  void			*(*pop_front)(struct s_list *list);
  void			*(*end)(struct s_list *list);
  void			*(*find)(struct s_list *list, t_pred p, void *data);
  void			(*begin)(struct s_list *list, t_list_iterator *it);
  void			(*clear)(struct s_list *list, void (*f)(void *p));
  void			*(*insert)(struct s_list *list, void *data);
  size_t		(*size)(struct s_list *list);
  void			*(*insert_pred)(struct s_list *list, void *data,
					t_pred p, void *cmp);
  void			*(*insert_at)(struct s_list *list, size_t pos,
				      void *data);
  void			(*delete)(struct s_list *list, void (*f)(void *data));
  void			(*remove)(struct s_list *list, void *data,
				  void (*free)(void *));
  void			(*remove_if)(struct s_list *list, t_pred p,
				     void *data, void (*free)(void *));
}			t_list;

/*
** PROTOTYPES
*/
/*
** iterator.c
*/
void			init_iterator(t_list_iterator *it,
				      t_double_clist *node);

/*
** list.c
*/
t_bool			create_list(t_list *c);

/*
** iterator_move.c
*/
void	list_iterator_next(t_list_iterator *it);
void	list_iterator_prev(t_list_iterator *it);

void		*list_pop_back(t_list *c);
void		*list_pop_front(t_list *c);
void		*list_push_front(t_list *c, void *data);
void		*list_push_back(t_list *c, void *data);

void		*list_back(t_list *c);
void		*list_front(t_list *c);
void		list_begin(t_list *list, t_list_iterator *it);
void		*list_end(t_list *c);
void		*list_find(t_list *c, t_pred p, void *data);

void		list_clear(t_list *c, void (*f)(void *));
void		list_remove_if(t_list *c, t_pred p, void *data,
			       void (*free)(void *));
void		list_remove(t_list *c, void *data, void (*free)(void *));
void		list_delete(t_list *c, void (*f)(void *d));

#endif	/* __LIST_H__ */
