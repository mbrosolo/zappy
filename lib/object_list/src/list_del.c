/*
** list_del.c for  in /home/bourdi_b/zappy/lib/object_list/src
**
** Made by benjamin bourdin
** Login   <bourdi_b@epitech.net>
**
** Started on  Sun Jul 15 19:04:46 2012 benjamin bourdin
** Last update Sun Jul 15 19:07:32 2012 benjamin bourdin
*/

#include	"list.h"

void		list_delete(t_list *c, void (*f)(void *d))
{
  c->clear(c, f);
  free(c->node);
}

void		list_remove(t_list *c, void *data, void (*free)(void *))
{
  t_list_iterator	it;

  c->begin(c, &it);
  while (it.current(&it) != c->end(c))
    {
      if (it.data == data)
	{
	  it.remove(&it, free);
	  return;
	}
      it.next(&it);
    }
}

void		list_remove_if(t_list *c, t_pred p, void *data,
			       void (*free)(void *))
{
  t_list_iterator	it;

  c->begin(c, &it);
  while (it.current(&it) != c->end(c))
    {
      if (p(it.data, data))
	{
	  it.remove(&it, free);
	}
      else
	it.next(&it);
    }
}

void		list_clear(t_list *c, void (*f)(void *))
{
  t_list_iterator	it;

  c->begin(c, &it);
  while (it.current(&it) != c->end(c))
    it.remove(&it, f);
  c->count = 0;
}
