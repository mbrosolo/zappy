/*
** iterator_move.c for  in /home/bourdi_b/zappy/lib/object_list/src
**
** Made by benjamin bourdin
** Login   <bourdi_b@epitech.net>
**
** Started on  Sun Jul 15 19:00:15 2012 benjamin bourdin
** Last update Sun Jul 15 19:01:01 2012 benjamin bourdin
*/

#include	"list.h"

void	list_iterator_next(t_list_iterator *it)
{
  it->list = it->list->next;
  it->data = it->list->data;
}

void	list_iterator_prev(t_list_iterator *it)
{
  it->list = it->list->prev;
  it->data = it->list->data;
}
