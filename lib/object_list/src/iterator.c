/*
** iterator.c for list in object_list/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed May 16 15:05:23 2012 matthias brosolo
** Last update Sun Jul 15 19:01:02 2012 benjamin bourdin
*/

#include	"list.h"

static void	*list_iterator_remove(t_list_iterator *it, void (*f)(void *d))
{
  void		*data;

  data = it->data;
  it->list = free_node_double_clist(it->list, f);
  it->data = it->list->data;
  --(*(it->count));
  return (data);
}

static void		*list_iterator_current(t_list_iterator *it)
{
  return (it->list);
}

static void		*list_iterator_insert(t_list_iterator *it, void *data)
{
  t_double_clist	*tmp;

  if ((tmp = push_front_double_clist(it->list, data)))
    ++(*(it->count));
  return (tmp);
}

static void		*list_iterator_insert_next(t_list_iterator *it,
						   void *data)
{
  t_double_clist	*tmp;

  if ((tmp = insert_next(it->list, data)))
    {
      ++(*(it->count));
      it->next(it);
    }
  return (tmp);
}

void		init_iterator(t_list_iterator *it, t_double_clist *node)
{
  it->list = node->next;
  it->data = it->list->data;
  it->next = &list_iterator_next;
  it->prev = &list_iterator_prev;
  it->remove = &list_iterator_remove;
  it->current = &list_iterator_current;
  it->insert = &list_iterator_insert;
  it->insert_next = &list_iterator_insert_next;
}
