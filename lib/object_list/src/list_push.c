/*
** list_push.c for  in /home/bourdi_b/zappy/lib/object_list/src
**
** Made by benjamin bourdin
** Login   <bourdi_b@epitech.net>
**
** Started on  Sun Jul 15 19:01:54 2012 benjamin bourdin
** Last update Sun Jul 15 19:02:41 2012 benjamin bourdin
*/

#include	"list.h"

void		*list_push_back(t_list *c, void *data)
{
  t_double_clist	*tmp;

  data = ((tmp = push_back_double_clist(c->node, data)) ? tmp->data : NULL);
  if (tmp)
    c->count++;
  return (data);
}

void		*list_push_front(t_list *c, void *data)
{
  t_double_clist	*tmp;

  data = ((tmp = push_front_double_clist(c->node, data)) ? tmp->data : NULL);
  if (tmp)
    c->count++;
  return (data);
}

void		*list_pop_front(t_list *c)
{
  void			*data;
  t_double_clist	*tmp;

  data = ((tmp = list_front(c)) ? tmp->data : NULL);
  if (tmp)
    {
      c->count--;
      free_node_double_clist(tmp, NULL);
    }
  return (data);
}

void		*list_pop_back(t_list *c)
{
  void			*data;
  t_double_clist	*tmp;

  data = ((tmp = list_back(c)) ? tmp->data : NULL);
  if (tmp)
    {
      c->count--;
      free_node_double_clist(tmp, NULL);
    }
  return (data);
}
