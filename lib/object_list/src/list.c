/*
** list.c for object_list in object_list/src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Wed May 16 13:09:54 2012 matthias brosolo
** Last update Sun Jul 15 19:07:33 2012 benjamin bourdin
*/

#include	"list.h"

static void		*list_insert_pred(t_list *c, void *data, t_pred p,
					  void *cmp)
{
  t_double_clist	*tmp;

  tmp = c->node->next;
  while (tmp->data != (void *) FIRST_NODE)
    {
      if (p(tmp->data, cmp))
	{
	  data = ((tmp = push_back_double_clist(tmp, data)) ? tmp->data : NULL);
	  if (tmp)
	    c->count++;
	  return (data);
	}
      tmp = tmp->next;
    }
  data = ((tmp = push_back_double_clist(tmp, data)) ? tmp->data : NULL);
  if (tmp)
    c->count++;
  return (data);
}

static void		*list_insert_at(t_list *c, size_t pos, void *data)
{
  t_double_clist	*tmp;

  if ((tmp = insert_at_pos(c->node, pos, data)))
    c->count++;
  return ((tmp ? tmp->data : NULL));
}

static size_t		list_size(t_list *c)
{
  return (c->count);
}

t_bool			create_list(t_list *c)
{
  if (!(c->node = create_node_double_clist()))
    return (1);
  c->count = 0;
  c->back = &list_back;
  c->front = &list_front;
  c->push_back = &list_push_back;
  c->push_front = &list_push_front;
  c->pop_back = &list_pop_back;
  c->pop_front = &list_pop_front;
  c->end = &list_end;
  c->find = &list_find;
  c->begin = &list_begin;
  c->clear = &list_clear;
  c->size = &list_size;
  c->insert_at = &list_insert_at;
  c->insert_pred = &list_insert_pred;
  c->delete = &list_delete;
  c->remove = &list_remove;
  c->remove_if = &list_remove_if;
  return (0);
}
