/*
** list_access.c for  in /home/bourdi_b/zappy/lib/object_list/src
** 
** Made by benjamin bourdin
** Login   <bourdi_b@epitech.net>
** 
** Started on  Sun Jul 15 19:03:02 2012 benjamin bourdin
** Last update Sun Jul 15 19:03:51 2012 benjamin bourdin
*/

#include	"list.h"

void		*list_back(t_list *c)
{
  return (c->node->prev != c->node ? c->node->prev->data : NULL);
}

void		*list_front(t_list *c)
{
  return (c->node->next != c->node ? c->node->next->data : NULL);
}

void		list_begin(t_list *list, t_list_iterator *it)
{
  init_iterator(it, list->node);
  it->count = &list->count;
}

void		*list_end(t_list *c)
{
  return (c->node);
}

void		*list_find(t_list *c, t_pred p, void *data)
{
  t_list_iterator	it;

  c->begin(c, &it);
  while (it.current(&it) != c->end(c))
    {
      if (p(it.data, data))
	return (it.data);
      it.next(&it);
    }
  return (NULL);
}
