//
// Edge.hpp for graph in /home/gressi_b/zappy/lib/graph
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Jun  6 16:56:04 2012 gressi_b
// Last update Wed Jun  6 16:56:04 2012 gressi_b
//

#ifndef	__LIB_EDGE_HPP__
#define	__LIB_EDGE_HPP__

#include <vector>

#include "Vertex.hpp"
#include "Graph.hpp"

namespace gr
{
  template<class V, class E> class TGraph;
  template<class V, class E> class TVertex;

  template<class V, class E>
  class TEdge
  {
  public:
    typedef TGraph<V, E>	Graph;
    typedef TVertex<V, E>	Vertex;
    typedef TEdge<V, E>		Edge;

  private:
    E		_label;
    Vertex*	_outV;
    Vertex*	_inV;
    Graph*	_g;

  private:
    TEdge();
    TEdge(Edge const&);
    Edge&	operator=(Edge const&);

  public:
    TEdge(Graph* g, Vertex* outV, Vertex* inV, E const& label)
      : _label(label), _outV(outV), _inV(inV), _g(g)
    {}

    ~TEdge() {}

    E
    getLabel() const
    {
      return this->_label;
    }

    Vertex*
    getInV() const
    {
      return this->_inV;
    }

    Vertex*
    getOutV() const
    {
      return this->_outV;
    }
  };
}

#endif
