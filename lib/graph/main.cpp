//
// main.cpp for test in /home/gressi_b/zappy/lib/graph
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Jun  6 16:28:19 2012 gressi_b
// Last update Wed Jun  6 16:28:19 2012 gressi_b
//

#include <iostream>
#include <string>
#include "Graph.hpp"
#include "Vector.hpp"

typedef gr::TGraph<void*, int>	Graph;

#define NB_V	128

static void
test1()
{
  Graph		g;
  Graph::Vertex*	v1;
  Graph::Vertex*	v2;
  Graph::Edge*		e;

  v1 = g.vertex(0);
  v2 = g.vertex(0);
  e = g.edge(v1, v2, 0);
  g.remove(e);
  g.remove(v1);
  g.remove(v2);
}

static void
test2()
{
  Graph			g;
  Graph::Vertex*	v[NB_V];

  for (int i = 0; i < NB_V; ++i)
    v[i] = g.vertex(0);
  for (int i = 0; i < NB_V - 1; ++i)
    g.edge(v[i], v[i + 1], 0);
  for (int i = 0; i < NB_V - 2; ++i)
    g.edge(v[i], v[i + 2], 0);
  for (int i = 0; i < NB_V - 3; ++i)
    g.edge(v[i], v[i + 3], 0);
}

static void
test3()
{
  Graph			g;
  Graph::Vertex*	v[NB_V];

  for (int i = 0; i < NB_V; ++i)
    v[i] = g.vertex(0);
  for (int i = 0; i < NB_V - 1; ++i)
    g.edge(v[i], v[i + 1], 0);
  for (int i = 0; i < NB_V - 2; ++i)
    g.edge(v[i], v[i + 2], 0);
  for (int i = 0; i < NB_V - 3; ++i)
    g.edge(v[i], v[i + 3], 0);
  g.remove(v[0]);
  g.remove(v[1]);
  g.remove(v[2]);
}

int	main()
{
  gr::Vector<int>		v;
  gr::Vector<int>::iterator	it;

  test1();
  test2();
  test3();
  for (int i = 0; i < 10; i++)
    v.push_back(i * 2);
  for (it = v.begin(); it != v.end(); it++)
    std::cout << "i: " << *it << std::endl;
  std::cout << "i: " << *v.find(8) << std::endl;
  return (0);
}
