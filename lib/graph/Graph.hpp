//
// Graph.hh for graph in /home/gressi_b/zappy/lib/graph
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Jun  6 16:02:27 2012 gressi_b
// Last update Wed Jun  6 16:02:27 2012 gressi_b
//

#ifndef	__LIB_GRAPH_HPP__
#define	__LIB_GRAPH_HPP__

#include <vector>
#include <list>
#include <algorithm>
#include <cstddef>

#include "Vertex.hpp"
#include "Edge.hpp"
#include "Vector.hpp"

namespace gr
{
  template<typename T>
  void
  deleter(T& v)
  {
    delete v;
  }

  template<class V, class E>
  class TGraph
  {
  public:
    typedef TVertex<V, E>		Vertex;
    typedef TEdge<V, E>			Edge;
    typedef Vector<Vertex*>		vertex_list_t;
    typedef Vector<Edge*>		edge_list_t;

  private:
    vertex_list_t	_vertex;
    edge_list_t		_edge;

  public:
    TGraph()
      : _vertex(), _edge()
    {}

    ~TGraph()
    {
      this->_vertex.clear(&deleter<Vertex*>);
      this->_edge.clear(&deleter<Edge*>);
    }

    Edge*
    findE(E const& label)
    {
      return this->findE(label, this->_edge.begin());
    }

    Edge*
    findE(E const& label, typename edge_list_t::iterator it)
    {
      typename edge_list_t::iterator	end = this->_edge.end();

      for (; it != end; it++)
	{
	  if ((*it)->getLabel() == label)
	    return *it;
	}
      return 0;
    }

    Vertex*
    findV(V const& data)
    {
      return this->findV(data, this->_vertex.begin());
    }

    Vertex*
    findV(V const& data, typename vertex_list_t::iterator it)
    {
      typename vertex_list_t::iterator	end = this->_vertex.end();

      for (; it != end; it++)
	{
	  if ((*it)->getData() == data)
	    return *it;
	}
      return 0;
    }

    Vertex*
    vertex(V const& data)
    {
      Vertex*	v = new Vertex(this, data);

      this->_vertex.push_back(v);
      return v;
    }

    Edge*
    edge(Vertex* outV, Vertex* inV, E label)
    {
      Edge*	e = new Edge(this, outV, inV, label);

      outV->addOutE(e);
      inV->addInE(e);
      this->_edge.push_back(e);
      return e;
    }

    void
    remove(Edge* e)
    {
      if (this->_edge.has(e))
	{
	  this->_edge.remove(e);
      	  e->getOutV()->removeOutE(e);
      	  e->getInV()->removeInE(e);
      	  delete e;
	}
    }

    void
    remove(Vertex* v)
    {
      if (this->_vertex.has(v))
	{
	  this->_vertex.remove(v);
	  v->removeAllEdges();
	  delete v;
	}
    }

    size_t
    getNbVertices() const
    {
      return this->_vertex.size();
    }

    size_t
    getNbEdges() const
    {
      return this->_edge.size();
    }
  };
}

#endif
