//
// Vertex.hh for graph in /home/gressi_b/Epitech/B4/zappy/lib/graph
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Jun  6 16:17:30 2012 gressi_b
// Last update Wed Jun 13 14:14:32 2012 benjamin bourdin
//

#ifndef	__LIB_VERTEX_HPP__
#define	__LIB_VERTEX_HPP__

#include <vector>
#include <list>
#include <algorithm>

#include "Edge.hpp"
#include "Graph.hpp"
#include "Vector.hpp"

namespace gr
{
  template<class V, class E>
  class TGraph;

  template<class V, class E>
  class TVertex
  {
  public:
    typedef TGraph<V, E>		Graph;
    typedef TVertex<V, E>		Vertex;
    typedef TEdge<V, E>			Edge;
    typedef Vector<Edge*>		edge_list_t;

  private:
    V			_data;
    edge_list_t		_inE;
    edge_list_t		_outE;
    Graph*		_g;

  private:
    TVertex();
    TVertex(Vertex const&);
    Vertex&	operator=(Vertex const&);

  public:
    TVertex(Graph* g, V const& data)
      : _data(data), _inE(), _outE(), _g(g)
    {}

    ~TVertex() {}

    void
    addOutE(Edge* e)
    {
      this->_outE.push_back(e);
    }

    void
    addInE(Edge* e)
    {
      this->_inE.push_back(e);
    }

    void
    removeOutE(Edge* e)
    {
      this->_outE.remove(e);
    }

    void
    removeInE(Edge* e)
    {
      this->_inE.remove(e);
    }

    void
    removeAllEdges()
    {
      while (this->_inE.empty() == false)
	this->_g->remove(this->_inE.back());
      while (this->_outE.empty() == false)
	this->_g->remove(this->_outE.back());
    }

    V
    getData() const
    {
      return this->_data;
    }

    Edge*
    findOutE(E const& label)
    {
      return this->findOutE(label, this->_outE.begin());
    }

    Edge*
    findOutE(E const& label, typename edge_list_t::iterator it)
    {
      typename edge_list_t::iterator	end = this->_outE.end();

      for (; it != end; it++)
	{
	  if ((*it)->getLabel() == label)
	    return *it;
	}
      return 0;
    }

    edge_list_t const&
    getOutE() const
    {
      return this->_outE;
    }

    edge_list_t&
    getOutE()
    {
      return this->_outE;
    }

    edge_list_t const&
    getInE() const
    {
      return this->_inE;
    }

    edge_list_t&
    getInE()
    {
      return this->_inE;
    }
  };
}

#endif
