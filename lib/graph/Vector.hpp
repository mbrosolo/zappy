//
// List.hpp for graph in /home/gressi_b/zappy/lib/graph
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Fri Jun 15 16:07:50 2012 gressi_b
// Last update Fri Jun 15 16:07:50 2012 gressi_b
//

#ifndef	__LIB_VECTOR_HPP__
#define	__LIB_VECTOR_HPP__

namespace gr
{
  template<typename T>
  class Vector
  {
  private:
    struct Link
    {
    public:
      T		value;
      Link*	prev;
      Link*	next;

      Link(T const& e)
	: value(e), prev(0), next(0)
      {}

      Link(Link const& ln)
	: value(ln.elen), prev(ln.prev), next(ln.next)
      {}

      Link&	operator=(Link const& ln)
      {
	this->value = ln.value;
	this->prev = ln.prev;
	this->next = ln.next;
      }

      ~Link()
      {}
    };

  public:
    class iterator
    {
    private:
      Link*		_p;

    public:
      iterator(Link* p = 0)
	: _p(p)
      {}

      iterator(iterator const& it)
	: _p(it._p)
      {}

      iterator&
      operator=(iterator const& it)
      {
	this->_p = it._p;
	return *this;
      }

      ~iterator() {}

      bool
      operator==(iterator const& it) const
      {
	return (this->_p == it._p);
      }

      bool
      operator!=(iterator const& it) const
      {
	return (this->_p != it._p);
      }

      iterator
      operator++(int)
      {
	if (this->_p)
	  this->_p = this->_p->next;
	return *this;
      }

      T&
      operator*() const
      {
	return this->_p->value;
      }
    };

  private:
    Link*	_head;
    Link*	_tail;
    size_t	_size;

  private:
    Vector(Vector const&);
    Vector&	operator=(Vector const&);

  public:
    Vector()
      : _head(0), _tail(0), _size(0)
    {
    }

    ~Vector()
    {
      this->clear();
    }

    iterator
    begin()
    {
      return iterator(this->_head);
    }

    iterator
    end()
    {
      return iterator(0);
    }

    iterator
    find(T const& e)
    {
      Link*	p = this->_head;

      while (p)
	{
	  if (p->value == e)
	    return iterator(p);
	  p = p->next;
	}
      return this->end();
    }

    void
    push_back(T const& e)
    {
      Link*	nl = new Link(e);

      nl->prev = this->_tail;
      if (this->_tail)
	this->_tail->next = nl;
      else
	this->_head = nl;
      this->_tail = nl;
      ++this->_size;
    }

    void
    pop_back()
    {
      Link*	old = this->_tail;

      this->_tail = old->prev;
      if (this->_tail)
	this->_tail->next = 0;
      else
	this->_head = 0;
      delete old;
      --this->_size;
    }

    void
    clear()
    {
      Link*	p;
      Link*	save;

      p = this->_head;
      while (p)
	{
	  save = p;
	  p = p->next;
	  delete save;
	}
      this->_head = 0;
      this->_tail = 0;
      this->_size = 0;
    }

    template<typename V>
    void
    clear(void (*deleter)(V&))
    {
      Link*	p;
      Link*	save;

      p = this->_head;
      while (p)
	{
	  save = p;
	  p = p->next;
	  deleter(save->value);
	  delete save;
	}
      this->_head = 0;
      this->_tail = 0;
      this->_size = 0;
    }

    void
    remove(T const& v)
    {
      Link*	p;
      int	i;

      p = this->_head;
      i = 0;
      while (p && p->value != v)
	{
	  p = p->next;
	  ++i;
	}
      if (p->prev)
	p->prev->next = p->next;
      else
	this->_head = p->next;
      if (p->next)
	p->next->prev = p->prev;
      else
	this->_tail = p->prev;
      delete p;
      --this->_size;
    }

    bool
    has(T const& v) const
    {
      Link*	p;

      p = this->_head;
      while (p)
	{
	  if (p->value == v)
	    return true;
	  p = p->next;
	}
      return false;
    }

    T&
    operator[](size_t n)
    {
      return this->at(n);
    }

    T const&
    operator[](size_t n) const
    {
      return this->at(n);
    }

    T&
    at(size_t n)
    {
      Link*	p;

      p = this->_head;
      while (n > 0)
	{
	  p = p->next;
	  --n;
	}
      if (p)
	return p->value;
      return this->back();
    }

    T const&
    at(size_t n) const
    {
      Link*	p;

      p = this->_head;
      while (n > 0)
	{
	  p = p->next;
	  --n;
	}
      if (p)
	return p->value;
      return this->back();
    }

    T&
    front()
    {
      return this->_head->value;
    }

    T const&
    front() const
    {
      return this->_head->value;
    }

    T&
    back()
    {
      return this->_tail->value;
    }

    T const&
    back() const
    {
      return this->_tail->value;
    }

    bool
    empty() const
    {
      return !this->_size;
    }

    size_t
    size() const
    {
      return this->_size;
    }
  };
}

#endif
