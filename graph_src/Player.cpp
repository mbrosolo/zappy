//
// Player.cpp for Zappy in /home/sarglen/zappy/graph_src
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu Jun  7 11:46:57 2012 anna texier
// Last update Sat Jul 14 10:21:45 2012 anna texier
//

#include	"Player.hh"

Player::Player(size_t x, size_t y, eOrient orient, ushort_t level, std::string team,
	       irr::video::SColor &color)
  : _dragon(NULL),
    _invent(), _x(x), _y(y), _team(team), _orient(orient),
    _level(level)
{
  _invent[Stone::NOURRITURE] = 0;
  _invent[Stone::LINEMATE] = 0;
  _invent[Stone::DERAUMERE] = 0;
  _invent[Stone::SIBUR] = 0;
  _invent[Stone::MENDIANE] = 0;
  _invent[Stone::PHIRAS] = 0;
  _invent[Stone::THYSTAME] = 0;

  _dragon = Window::getInstance().getSceneManager()
    ->addAnimatedMeshSceneNode(Window::getInstance().getDragon(), NULL, 1,
			       irr::core::vector3df(_x, 1.0f, _y), // position
			       irr::core::vector3df(0, 0, 0), // rotation
			       irr::core::vector3df(0.06f, 0.06f, 0.06f)); // scale
  _dragon->setMD2Animation(irr::scene::EMAT_STAND);
  _dragon->setMaterialFlag(irr::video::EMF_LIGHTING, true);

  _dragon->getMaterial(0).EmissiveColor = color;
  orientPlayer();
}

Player::~Player()
{
  Window::getInstance().unsetNode(_dragon);
  if (_dragon)
    _dragon->remove();
}

irr::scene::ISceneNode	*Player::getNode() const
{
  return _dragon;
}

void	Player::move(size_t x, size_t y, eOrient orient)
{
  _x = x;
  _y = y;
  _orient = orient;

  _dragon->setPosition(irr::core::vector3df(x, _dragon->getPosition().Y, y));
  orientPlayer();
}

void	Player::setLevel(ushort_t level)
{
  _level = level;
}

ushort_t	Player::getLevel() const
{
  return _level;
}

void	Player::setInvent(size_t n, size_t l, size_t d, size_t s, size_t m, size_t p, size_t t)
{
  _invent[Stone::NOURRITURE] = n;
  _invent[Stone::LINEMATE] = l;
  _invent[Stone::DERAUMERE] = d;
  _invent[Stone::SIBUR] = s;
  _invent[Stone::MENDIANE] = m;
  _invent[Stone::PHIRAS] = p;
  _invent[Stone::THYSTAME] = t;
}

size_t	Player::getX() const
{
  return _x;
}

size_t	Player::getY() const
{
  return _y;
}

size_t	Player::getNourriture()
{
  return _invent[Stone::NOURRITURE];
}

size_t	Player::getLinemate()
{
  return _invent[Stone::LINEMATE];
}

size_t	Player::getDeraumere()
{
  return _invent[Stone::DERAUMERE];
}

size_t	Player::getSibur()
{
  return _invent[Stone::SIBUR];
}

size_t	Player::getMendiane()
{
  return _invent[Stone::MENDIANE];
}

size_t	Player::getPhiras()
{
  return _invent[Stone::PHIRAS];
}

size_t	Player::getThystame()
{
  return _invent[Stone::THYSTAME];
}

void	Player::orientPlayer()
{
  irr::core::vector3df r = _dragon->getRotation();
  switch (_orient)
    {
    case NORD:
      r.Y = 180; break;
    case EST:
      r.Y = 90; break;
    case SUD:
      r.Y = 0; break;
    case OUEST:
      r.Y = 270; break;
    }
  _dragon->setRotation(r);
}

std::string const	&Player::getTeam() const
{
  return (_team);
}
