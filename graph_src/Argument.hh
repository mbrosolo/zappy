//
// Argument.hh for zappy in /home/gressi_b/Epitech/B4/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Tue Jun 19 18:08:08 2012 gressi_b
// Last update Sun Jul 15 17:43:38 2012 gaetan senn
//

#ifndef	__ARGUMENT_HH__
#define	__ARGUMENT_HH__

/**
 * @author Benjamin Gressier
 * @file Argument.hh
 */

#include <vector>
#include "BaseError.hpp"

#define USAGE "./graphic -p [port] -h [nom de la machine]"

/*!
 * @class Argument
 * @brief A class to get arguments
 */
class Argument : public BaseError<std::string>
{
private:
  int		_port; /*!< chosen port  */
  std::string	_hostName; /*!<  chosen hostname */
  std::string	_song; /*!<  chosen song */

private:

public:
  /*!
   * @brief Argument ctor
   */
  Argument();
  /*!
   * @brief Argument dtor
   */
  ~Argument() {}
  /*!
   * @brief check if it is an option
   */
  bool		_isOption(std::string const& str) const;
  /*!
   * @brief check arguments
   */
  bool		_checkArgs(std::string const& progname);
  /*!
   * @brief arguments parsing
   */
  bool		parse(int ac, char **av);
  /*!
   * @brief get hostname
   */
  std::string const&	getHostName() const; 
  /*!
   * @brief get port
   */
  int			getPort() const;
  /*!
   * @brief get chosen song
   */
  std::string const &	getSong() const;
};

#endif
