//
// main.cpp for  in /home/sarglen/zappy/graph_client_src
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Mon Jun  4 17:56:34 2012 anna texier
// Last update Sun Jul 15 20:58:52 2012 gaetan senn
//

#include	<stdlib.h>
#include	<stdio.h>
#include	"Display.hh"
#include	"Argument.hh"
#include	"Window.hh"
#include	"ExceptionInterpret.hh"
#include	"ExceptionQuit.hh"
#include	"LoadSound.hh"

#include	<unistd.h>

int		main(int ac, char **av)
{
  Argument	args;

  if (!args.parse(ac, av))
    {
      std::cerr << args.getError() << std::endl;
      std::cerr << "Usage: " << USAGE << std::endl;
      return (1);
    }

  Display d(args.getSong());
  try
    {
      Window::getInstance();
      d.launch(args.getPort(), args.getHostName());
    }
  catch (ExceptionSocket &e) {
    std::cerr << e.what() << std::endl;
    return (1);
  }
  catch (ExceptionQuit &e) {
    std::cerr << e.what() << std::endl;
    return (1);
  }
  while (d.run_display())
    {
      try
	{
	  d.start();
	}
      catch (ExceptionInterpret &e) {
	std::cerr << "error : " << e.what() << std::endl;
      }
      catch (ExceptionQuit &e) {
	std::cerr << e.what() << std::endl;
	return (1);
      }
      catch (...) {
	std::cerr << "Unexepected error occured." << std::endl;
	return (1);
      }
      d.draw();
    }
  return (0);
}
