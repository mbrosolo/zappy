//
// Display.cpp for Zappy in /home/sarglen/zappy/graph_src
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu Jun  7 10:10:42 2012 anna texier
// Last update Sun Jul 15 23:06:04 2012 anna texier
//

#include	<sstream>
#include	"Display.hh"
#include	"ExceptionDisplay.hh"

Display::Display(std::string const & song)
  : AInterpret<Display, Display>(this, this),
    _board(), _players(), _eggs(), _teams(),
    _pNodes(), _eNodes(), _teamBox(NULL), _updateTeams(true), _cameraP(NULL),
    _updateNode(NULL),
    _refcmd()
{
  _refcmd["msz"] = &Display::msz;
  _refcmd["bct"] = &Display::bct;
  _refcmd["tna"] = &Display::tna;
  _refcmd["pnw"] = &Display::pnw;
  _refcmd["ppo"] = &Display::ppo;
  _refcmd["plv"] = &Display::plv;
  _refcmd["pin"] = &Display::pin;
  _refcmd["pex"] = &Display::pex;
  _refcmd["pbc"] = &Display::pbc;
  _refcmd["pic"] = &Display::pic;
  _refcmd["pie"] = &Display::pie;
  _refcmd["pfk"] = &Display::pfk;
  _refcmd["pdr"] = &Display::pdr;
  _refcmd["pgt"] = &Display::pgt;
  _refcmd["pdi"] = &Display::pdi;
  _refcmd["enw"] = &Display::enw;
  _refcmd["eht"] = &Display::eht;
  _refcmd["ebo"] = &Display::ebo;
  _refcmd["edi"] = &Display::edi;
  _refcmd["sgt"] = &Display::sgt;
  _refcmd["seg"] = &Display::seg;
  _refcmd["smg"] = &Display::smg;
  _refcmd["suc"] = &Display::suc;
  _refcmd["sbp"] = &Display::sbp;

  _soundManager.addEntry("SOUNDTRACK", song.c_str(), sound::MUSIC);
  _soundManager.addEntry("DEAD", "./sounds/Dead.mp3", sound::MUSIC);
  _soundManager.addEntry("HATCH", "./sounds/Hatching.mp3", sound::MUSIC);
  _soundManager.addEntry("BROADCAST", "./sounds/Broadcast.mp3", sound::MUSIC);
  _soundManager.addEntry("INCANTATION", "./sounds/Incantation.wav", sound::MUSIC);
  _soundManager.loadSounds();
  _soundManager.play("SOUNDTRACK");
  _soundManager.setLoop("SOUNDTRACK", -1);
}

Display::~Display()
{
  for (players_t::iterator it = _players.begin() ; it != _players.end() ; ++it)
    delete it->second;
  for (eggs_t::iterator it = _eggs.begin() ; it != _eggs.end() ; ++it)
    delete it->second;
}

bool	Display::run_display()
{
  if (!Window::getInstance().stop())
    return Window::getInstance().getDevice()->run();
  return false;
}

void	Display::copyStr(std::wstring &text, const std::string &str)
{
  std::wstring	tmp(str.length(), L' ');

  std::copy(str.begin(), str.end(), tmp.begin());
  text.append(tmp);
}

void	Display::draw()
{
  irr::video::SColor color(255, 160, 140, 255);

  if (_updateTeams)
    {
      _updateTeams = false;

      std::wstring	text(L"Teams\n");

      for (teams_t::iterator it = _teams.begin() ; it != _teams.end() ;)
	{
	  copyStr(text, it->first);
	  text.append(L" : ");
	  EventManager::append(text, it->second.first);
	  ++it;
	  if (it == _teams.end())
	    text.append(L"\n");
	  else
	    text.append(L" - ");
	}

      if (_teamBox)
	_teamBox->remove();
      _teamBox = Window::getInstance().getGUI()->
	addStaticText(text.c_str(),
		      irr::core::rect<irr::s32>(0,0,WINX,60),true,true,0,-1,true);
      _teamBox->setOverrideFont(Window::getInstance().getFont());
    }

  Window::getInstance().getDriver()->beginScene(true, true, color);
  Window::getInstance().update(*this);
  Window::getInstance().getSceneManager()->drawAll();
  Window::getInstance().getGUI()->drawAll();
  Window::getInstance().getDriver()->endScene();
}

void	Display::setCameraP(Player *player)
{
  _cameraP = player;
}

void	Display::unsetCameraP()
{
  _cameraP = NULL;
}

Player	*Display::getPlayerNode(irr::scene::ISceneNode *node)
{
  if (_pNodes.find(node) == _pNodes.end())
    return NULL;
  return _pNodes[node];
}

Square	*Display::getSquareNode(irr::scene::ISceneNode *node)
{
  return _board.getSquareNode(node);
}

irr::scene::ISceneNode	*Display::getUpdateNode() const
{
  return _updateNode;
}

void	Display::unsetNode()
{
  _updateNode = NULL;
}

bool	Display::checkCommand(std::queue<Token> *q)
{
  refcmd::iterator			it;
  std::string				cmdname;

  cmdname = q->front().getData();
  if ((it = _refcmd.find(cmdname)) != _refcmd.end())
    {
      try
	{
	  (this->*(it->second))(q);
	  delete q;
	  return true;
	}
      catch (ExceptionDisplay const e)
	{
	  std::cerr << "Error type >" << e.what() << " and " << e.where() << std::endl;
	  std::cerr << "Error catched ExceptionDisplay Call" << std::endl;
	  return false;
	}
    }
  return false;
}

void	Display::pop(tokens_t &q, bool check)
{
  if (q.empty())
    throw (ExceptionDisplay("Can't pop Queue empty"));
  q.pop();
  if (check && q.empty())
    throw (ExceptionDisplay("Token Queue isnt complete"));
}

size_t	Display::getSizeT(tokens_t &q, bool toPop, bool check)
{
  std::stringstream	tmp;
  size_t		nbr;

  if (toPop)
    pop(q, check);
  if (q.front().getType() != Token::BLANK)
    throw (ExceptionDisplay("Invalid type for token (should be space)"));
  pop(q, check);
  if (q.front().getType() != Token::UNSIGNED_INT)
    throw (ExceptionDisplay("Invalid type for token (should be unsigned int)"));
  tmp << q.front().getData();
  tmp >> nbr;
  return nbr;
}

std::string	Display::getStr(tokens_t &q, bool toPop, bool check)
{
  if (toPop)
    pop(q, check);
  if (q.front().getType() != Token::BLANK)
    throw (ExceptionDisplay("Invalid type for token (should be space)"));
  pop(q, check);
  if (q.front().getType() != Token::STRING)
    throw (ExceptionDisplay("Invalid type for token (should be string)"));
  return q.front().getData();
}

// Map size
void	Display::msz(tokens_t *q)
{
  size_t x = getSizeT(*q, true, true);
  size_t y = getSizeT(*q, true, true);

  pop(*q, false);

  _board.setX(x);
  _board.setY(y);
  _board.draw();
}

// Squares inventory
void	Display::bct(tokens_t *q)
{
  size_t x = getSizeT(*q, true, true);
  size_t y = getSizeT(*q, true, true);

  Square *sq = _board.getSquare(x, y);

  size_t n = getSizeT(*q, true, true);
  size_t l = getSizeT(*q, true, true);
  size_t d = getSizeT(*q, true, true);
  size_t s = getSizeT(*q, true, true);
  size_t m = getSizeT(*q, true, true);
  size_t p = getSizeT(*q, true, true);
  size_t t = getSizeT(*q, true, true);

  pop(*q, false);

  sq->setStone(Stone::NOURRITURE, n);
  sq->setStone(Stone::LINEMATE, l);
  sq->setStone(Stone::DERAUMERE, d);
  sq->setStone(Stone::SIBUR, s);
  sq->setStone(Stone::MENDIANE, m);
  sq->setStone(Stone::PHIRAS, p);
  sq->setStone(Stone::THYSTAME, t);

  _updateNode = sq->getNode();
  Window::getInstance().update(*this);
}

// Teamname
void	Display::tna(tokens_t *q)
{
  int	r;
  int	g;
  int	b;

  r = rand() % 255;
  g = rand() % 255;
  b = rand() % 255;

  irr::video::SColor color(255, r, g, b);

  std::pair<size_t, irr::video::SColor>  p = std::make_pair(0, color);
  _teams[getStr(*q, true, true)] = p;
  _updateTeams = true;

  pop(*q, false);
}

// New player
void	Display::pnw(tokens_t *q)
{
  size_t n = getSizeT(*q, true, true);
  size_t x = getSizeT(*q, true, true);
  size_t y = getSizeT(*q, true, true);
  size_t o = getSizeT(*q, true, true);
  size_t l = getSizeT(*q, true, true);

  std::string t = getStr(*q, true, true);

  pop(*q, false);

  _players[n] =
    new Player(x, y, static_cast<Player::eOrient>(o), l, t, _teams[t].second);

  (_teams[t].first)++;
  _updateTeams = true;

  _pNodes[_players[n]->getNode()] = _players[n];
}

// Players position
void	Display::ppo(tokens_t *q)
{
  size_t n = getSizeT(*q, true, true);
  size_t x = getSizeT(*q, true, true);
  size_t y = getSizeT(*q, true, true);
  size_t o = getSizeT(*q, true, true);

  pop(*q, false);

  Player *p = _players[n];
  p->move(x, y, static_cast<Player::eOrient>(o));

  if (_cameraP == p)
    {
      irr::core::vector3df v(p->getX() + 10.f, 10.f, p->getY() + 10.f);
      irr::core::vector3df t(p->getX(), 1.f, p->getY());

      Window::getInstance().getCamera()->setPosition(v);
      Window::getInstance().getCamera()->setTarget(t);
    }
}

// Players level
void	Display::plv(tokens_t *q)
{
  size_t n = getSizeT(*q, true, true);
  size_t l = getSizeT(*q, true, true);

  pop(*q, false);

  _players[n]->setLevel(l);
}

// Players inventory
void	Display::pin(tokens_t *q)
{
  size_t j = getSizeT(*q, true, true);

  getSizeT(*q, true, true); // x
  getSizeT(*q, true, true); // y

  size_t n = getSizeT(*q, true, true);
  size_t l = getSizeT(*q, true, true);
  size_t d = getSizeT(*q, true, true);
  size_t s = getSizeT(*q, true, true);
  size_t m = getSizeT(*q, true, true);
  size_t p = getSizeT(*q, true, true);
  size_t t = getSizeT(*q, true, true);

  pop(*q, false);

  _players[j]->setInvent(n, l, d, s, m, p, t);
  _updateNode = _players[j]->getNode();
  Window::getInstance().update(*this);
}

void	Display::pex(tokens_t *q)
{
  getSizeT(*q, true, true);

  pop(*q, false);
}

void	Display::pbc(tokens_t *q)
{
  getSizeT(*q, true, true);
  getStr(*q, true, true);

  pop(*q, false);

  _soundManager.play("BROADCAST");
}

void	Display::pic(tokens_t *q)
{
  size_t x = getSizeT(*q, true, true);
  size_t y = getSizeT(*q, true, true);
  size_t l = getSizeT(*q, true, true);

  pop(*q, true);
  while (!q->empty())
    {
      size_t j;

      j = getSizeT(*q, false, false);
      _players[j]->setLevel(l);
      pop(*q, false);
    }

  Square *s = _board.getSquare(x, y);
  s->startIncant();

  _soundManager.play("INCANTATION");
}

void	Display::pie(tokens_t *q)
{
  size_t x = getSizeT(*q, true, true);
  size_t y = getSizeT(*q, true, true);
  getSizeT(*q, true, true);

  pop(*q, false);

  Square *s = _board.getSquare(x, y);
  s->endIncant();
}

void	Display::pfk(tokens_t *q)
{
  getSizeT(*q, true, true);

  pop(*q, false);
}

void	Display::pdr(tokens_t *q)
{
  getSizeT(*q, true, true);
  getSizeT(*q, true, true);

  pop(*q, false);
}

void	Display::pgt(tokens_t *q)
{
  getSizeT(*q, true, true);
  getSizeT(*q, true, true);

  pop(*q, false);
}

void	Display::pdi(tokens_t *q)
{
  players_t::iterator	it;
  size_t		n = getSizeT(*q, true, true);

  pop(*q, false);

  it = _players.find(n);
  if (it != _players.end())
    {
      _soundManager.play("DEAD");

      (_teams[it->second->getTeam()].first)--;
      _updateTeams = true;

      delete it->second;
      _players.erase(it);
    }
}

void	Display::enw(tokens_t *q)
{
  size_t e = getSizeT(*q, true, true);
  getSizeT(*q, true, true);
  size_t x = getSizeT(*q, true, true);
  size_t y = getSizeT(*q, true, true);

  pop(*q, false);

  _eggs[e] = new Egg(x, y);
}

void	Display::eht(tokens_t *q)
{
  getSizeT(*q, true, true);

  pop(*q, false);

  _soundManager.play("HATCH");
}

void	Display::ebo(tokens_t *q)
{
  eggs_t::iterator	it;
  size_t		e = getSizeT(*q, true, true);

  pop(*q, false);

  it = _eggs.find(e);
  if (it != _eggs.end())
    {
      delete it->second;
      _eggs.erase(it);
    }
}

void	Display::edi(tokens_t *q)
{
  eggs_t::iterator	it;
  size_t		e = getSizeT(*q, true, true);

  pop(*q, false);

  _soundManager.play("DEAD");
  it = _eggs.find(e);
  if (it != _eggs.end())
    {
      delete it->second;
      _eggs.erase(it);
    }
}

void	Display::sgt(tokens_t *q)
{
  size_t time = getSizeT(*q, true, true);

  pop(*q, false);

  _time = time;
}

void	Display::seg(tokens_t *q)
{
  const std::string winner = getStr(*q, true, true);

  pop(*q, false);

  irr::gui::IGUIStaticText	*box;
  std::wstring			text;

  std::cout << "-------------------- FIN DU JEU ----------------------------" << std::endl;

  text.append(L"End of the game.\nThe winner is team ");
  copyStr(text, winner);
  text.append(L" !");

  box = Window::getInstance().getGUI()->
    addStaticText(text.c_str(),
		  irr::core::rect<irr::s32>(0,0,WINX,60), true, true, 0, -1, true);
  box->setOverrideFont(Window::getInstance().getFont());
  sleep(1);
  box->remove();
}

void	Display::smg(tokens_t *q)
{
  const std::string msg = getStr(*q, true, true);

  pop(*q, false);
}

void	Display::suc(tokens_t *q)
{
  pop(*q, false);
}

void	Display::sbp(tokens_t *q)
{
  pop(*q, false);
}

void	Display::launch(int port, std::string const &hostname)
{
  InterpretConnection<Display>		parsingconnection(*this);

  this->_socket.createSocket(AF_INET, SOCK_STREAM, Socket::TCP);
  this->_socket.connection(AF_INET, port, hostname);
  this->_select.addFd(_socket.getSocket(),
		      &Display::readServer, &Display::writeServer, NULL);
		      parsingconnection.start();
}

void	Display::start()
{
  this->run(true);
}
