//
// Board.cpp for Zappy in /home/sarglen/zappy/graph_src
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Wed Jun  6 14:22:40 2012 anna texier
// Last update Sun Jul 15 15:18:59 2012 anna texier
//

#include	"Board.hh"
#include	"ExceptionDisplay.hh"
#include	"Window.hh"
#include	<algorithm>

namespace board_utils
{
  void _rm_square(Square *it)
  {
    delete it;
  }

}

Board::Board(size_t x, size_t y)
  : _x(x), _y(y), _squares(), _sNodes()
{

}

Board::~Board()
{
  std::for_each(_squares.begin(), _squares.end(), &board_utils::_rm_square);
}

void	Board::setX(const size_t x)
{
  _x = x;
}

void	Board::setY(const size_t y)
{
  _y = y;
}

void	Board::draw()
{
  Square	*s;

  for (size_t y = 0 ; y < _y ; y++)
    for (size_t x = 0 ; x < _x ; x++)
      {
	s = new Square(x, y);
	_squares.push_back(s);
	_sNodes[_squares.back()->getNode()] = _squares.back();
      }
  Window::getInstance().getCamera()->
    setPosition(irr::core::vector3df((_x / 2.f) + 10.f, 20.f, (_y / 2.f) + 10.f));
  Window::getInstance().getCamera()->
    setTarget(irr::core::vector3df(_x / 2.f, 0.f, _y / 2.f));
}

Square	 *Board::getSquare(const size_t x, const size_t y) const
{
  if (x > _x || y > _y)
    throw (ExceptionDisplay("Trying to get inexistant square"));
  return _squares[x + _x * y];
}

Square	*Board::getSquareNode(irr::scene::ISceneNode *node)
{
  if (_sNodes.find(node) == _sNodes.end())
    return NULL;
  return _sNodes[node];
}
