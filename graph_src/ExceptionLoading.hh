//
// ExceptionLoading.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:59:48 2012 benjamin bourdin
// Last update Fri Jul 13 20:42:10 2012 benjamin bourdin
//

#ifndef __EXCEPTION_LOADING_HH__
#define __EXCEPTION_LOADING_HH__

/**
 * @author bourdi_b
 * @file ExceptionLoading.hh
 */

#include	<string>
#include	"ExceptionRuntime.hh"

/*!
 * @class ExceptionLoading
 * @brief Exception class for the Loading
 */
class ExceptionLoading : public ExceptionRuntime
{
public:
  /*!
   * @brief Constructor of ExceptionLoading
   * @param what The description of the error
   * @param where Where is the error
   */
  ExceptionLoading(std::string const &what = "", std::string const &where = "") throw();
  /*!
   * @brief Destructor of ExceptionLoading
   */
  virtual ~ExceptionLoading() throw();
};

#endif
