//
// Square.cpp for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu Jun  7 10:39:32 2012 anna texier
// Last update Sun Jul 15 09:51:58 2012 anna texier
//

#include	"Square.hh"

Square::Square(size_t x, size_t y)
  : _particles(NULL), _cube(NULL),
    _x(x), _y(y), _incant(false)
{
  _cube = Window::getInstance().getSceneManager()
    ->addCubeSceneNode(1.f, NULL, -1, irr::core::vector3df(_x, 0.f, _y));
  _cube->setMaterialFlag(irr::video::EMF_LIGHTING, false);
  _cube->setMaterialTexture(0, Window::getInstance().getFloor());
}

Square::~Square()
{
  Window::getInstance().unsetNode(_cube);
  Window::getInstance().unsetNode(_particles);

  if (_stones[Stone::NOURRITURE].first != NULL)
    delete _stones[Stone::NOURRITURE].first;
  if (_stones[Stone::LINEMATE].first != NULL)
    delete _stones[Stone::LINEMATE].first;
  if (_stones[Stone::DERAUMERE].first != NULL)
    delete _stones[Stone::DERAUMERE].first;
  if (_stones[Stone::SIBUR].first != NULL)
    delete _stones[Stone::SIBUR].first;
  if (_stones[Stone::MENDIANE].first != NULL)
    delete _stones[Stone::MENDIANE].first;
  if (_stones[Stone::PHIRAS].first != NULL)
    delete _stones[Stone::PHIRAS].first;
  if (_stones[Stone::THYSTAME].first != NULL)
    delete _stones[Stone::THYSTAME].first;

  if (_cube)
    _cube->remove();
  if (_particles)
    _particles->remove();
}

void	Square::setStone(Stone::eType type, size_t quantity)
{
  _stones[type].second = quantity;
  if (quantity > 0 && _stones[type].first == NULL)
    _stones[type].first = new Stone(_x, _y, type);
  else if (quantity == 0 && _stones[type].first != NULL)
    {
      delete _stones[type].first;
      _stones[type].first = NULL;
    }
}

irr::scene::ISceneNode	*Square::getNode() const
{
  return _cube;
}

void	Square::startIncant()
{
  _incant = true;

  if (!_particles)
    {
      _particles = Window::getInstance().getSceneManager()
	->addParticleSystemSceneNode(false);

      irr::scene::IParticleEmitter* emitter = _particles->
	createBoxEmitter(irr::core::aabbox3d<irr::f32>(_x - 0.5f, 2.f, _y - 0.5f,
						       _x + 0.5f, 3.f, _y + 0.5f),
			 // T minx, T miny, T minz, T maxx, T maxy, T maxz
			 irr::core::vector3df(0.0f, 0.06f, 0.0f), // direction de diffusion
			 80,200, // nb particules emises a la sec min / max
			 irr::video::SColor(0,0,0,0), // couleur la plus sombre
			 irr::video::SColor(0,255,255,255), // couleur la plus claire
			 600, 1000, // duree de vie min / max
			 10, // angle max d'ecart / direction prevue
			 irr::core::dimension2df(0.08f, 0.08f), // taille minimum
			 irr::core::dimension2df(0.3f, 0.3f)); // taille maximum

      _particles->setEmitter(emitter);
      emitter->drop();

      _particles->setMaterialFlag(irr::video::EMF_LIGHTING, false);
      _particles->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
      _particles->setMaterialTexture(0, Window::getInstance().getFire());
      _particles->setMaterialType(irr::video::EMT_TRANSPARENT_VERTEX_ALPHA);

      irr::scene::IParticleAffector* affector =
	_particles->createFadeOutParticleAffector(irr::video::SColor(0, 0, 0, 0), 1);
      _particles->addAffector(affector);
      affector->drop();
    }
}

void	Square::endIncant()
{
  Window::getInstance().unsetNode(_particles);

  _incant = false;

  if (_particles)
    _particles->remove();

  _particles = NULL;
}

size_t	Square::getNourriture()
{
  return _stones[Stone::NOURRITURE].second;
}

size_t	Square::getLinemate()
{
  return _stones[Stone::LINEMATE].second;
}

size_t	Square::getDeraumere()
{
  return _stones[Stone::DERAUMERE].second;
}

size_t	Square::getSibur()
{
  return _stones[Stone::SIBUR].second;
}

size_t	Square::getMendiane()
{
  return _stones[Stone::MENDIANE].second;
}

size_t	Square::getPhiras()
{
  return _stones[Stone::PHIRAS].second;
}

size_t	Square::getThystame()
{
  return _stones[Stone::THYSTAME].second;
}
