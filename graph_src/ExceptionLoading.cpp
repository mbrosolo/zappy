//
// ExceptionLoading.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 07:02:02 2012 benjamin bourdin
// Last update Wed May 23 07:06:05 2012 benjamin bourdin
//

#include	"ExceptionLoading.hh"

ExceptionLoading::ExceptionLoading(std::string const &what, std::string const &where) throw()
: ExceptionRuntime(what, where)
{
}

ExceptionLoading::~ExceptionLoading() throw()
{
}
