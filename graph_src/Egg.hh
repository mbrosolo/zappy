//
// Egg.hh for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu Jun  7 11:44:31 2012 anna texier
// Last update Sat Jul 14 10:09:53 2012 tosoni_t
//

#ifndef	__EGG_H_
#define	__EGG_H_

/**
 * @author Anna Texier
 * @file Egg.hh
 */

#include	"Player.hh"

/*!
 * @class Egg
 * @brief The Eggs
 */
class Egg
{
private:
  irr::scene::IMeshSceneNode *	_egg; /*!< Mesh for the egg */

  size_t	_x; /*!< X position */
  size_t	_y; /*!< Y position */
  // on stocke le player qui l'a fait ? ca sert a rien, si ?

public:
  /*!
   * @brief Constructor of the egg, creates the node
   * @param x X position
   * @param y Y position
   */
  Egg(size_t, size_t);
  /*!
   * @brief Destructor of the egg, destroys the node
   */
  ~Egg();
  /*!
   * @brief Cpy ctor
   */
  Egg(const Egg &);
  /*!
   * @brief Operator =
   */
  Egg &operator=(const Egg &);
};

#endif
