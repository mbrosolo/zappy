//
// EventManager.cpp for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Mon Jun 25 10:40:51 2012 anna texier
// Last update Sun Jul 15 13:20:15 2012 anna texier
//

#include	"EventManager.hh"
#include	"Window.hh"
#include	"Display.hh"

EventManager::EventManager()
  : IEventReceiver(),
    _stop(false), _update(false), _camera(false), _fPlayer(false),
    _node(NULL), _box(NULL),
    _LMouse(false), _RMouse(false), _MMouse(false)
{

}

EventManager::~EventManager()
{

}

bool	EventManager::OnEvent(const irr::SEvent &event)
{
  if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
    {
      if (event.MouseInput.Event == irr::EMIE_LMOUSE_PRESSED_DOWN)
	{
	  _LMouse = true;
	  _pos.X = event.MouseInput.X;
	  _pos.Y = event.MouseInput.Y;
	}
      else if (event.MouseInput.Event == irr::EMIE_RMOUSE_PRESSED_DOWN)
	{
	  _RMouse = true;
	  _pos.X = event.MouseInput.X;
	  _pos.Y = event.MouseInput.Y;
	}
      else if (event.MouseInput.Event == irr::EMIE_MMOUSE_PRESSED_DOWN)
	{
	  irr::core::line3df ray = Window::getInstance().getSceneManager()
	    ->getSceneCollisionManager()->getRayFromScreenCoordinates
	    (Window::getInstance().getDevice()->getCursorControl()->getPosition());

	  _fPlayer = false;
	  if (_node)
	    {
	      _camera = true;
	      _node->setDebugDataVisible(irr::scene::EDS_OFF);
	      _node = NULL;
	    }
	  if (_box)
	    {
	      _box->remove();
	      _box = NULL;
	    }
	  _node = Window::getInstance().getSceneManager()
	    ->getSceneCollisionManager()->getSceneNodeFromRayBB(ray,0);
	  _update = true;
	}
      else if (event.MouseInput.Event == irr::EMIE_LMOUSE_LEFT_UP)
	_LMouse = false;
      else if (event.MouseInput.Event == irr::EMIE_RMOUSE_LEFT_UP)
	_RMouse = false;
      else if (event.MouseInput.Event == irr::EMIE_MOUSE_MOVED)
	{
	  if(_LMouse)
	    {
	      irr::core::vector3df rTarget =
		Window::getInstance().getCamera()->getTarget() -
		Window::getInstance().getCamera()->getPosition();

	      rTarget.normalize();

	      irr::core::vector3df upVector =
		Window::getInstance().getCamera()->getUpVector();
	      irr::core::vector3df tvectX = rTarget.crossProduct(upVector);

	      tvectX.normalize();

	      irr::core::vector3df tvectY = tvectX.crossProduct(rTarget);

	      tvectY.normalize();
	      upVector = tvectY;
	      rTarget +=  tvectX * (event.MouseInput.X - _pos.X) * 0.002f +
		tvectY * (event.MouseInput.Y - _pos.Y) * 0.002f;
	      Window::getInstance().getCamera()
		->setTarget(Window::getInstance().getCamera()
			    ->getPosition() + rTarget);
	      Window::getInstance().getCamera()->setUpVector(upVector);

	      _pos.X = event.MouseInput.X;
	      _pos.Y = event.MouseInput.Y;
	    }
	  if (_RMouse)
	    {
	      irr::core::vector3df pos =
		Window::getInstance().getCamera()->getPosition();
	      irr::core::vector3df target =
		Window::getInstance().getCamera()->getTarget();
	      irr::core::vector3df translate =
		Window::getInstance().getCamera()->getTarget();
	      irr::core::vector3df upVector =
		Window::getInstance().getCamera()->getUpVector();
	      irr::core::vector3df tvectX = target - pos;

	      tvectX = tvectX.crossProduct(upVector);
	      tvectX.normalize();

	      irr::core::vector3df tvectY = tvectX.crossProduct(target - pos);

	      tvectY.normalize();
	      translate +=  tvectX * (event.MouseInput.X - _pos.X) * 0.1f +
	      	tvectY * (event.MouseInput.Y - _pos.Y) * 0.1f;
	      pos += tvectX * (event.MouseInput.X - _pos.X) * 0.1f +
	      	tvectY * (event.MouseInput.Y - _pos.Y) * 0.1f;

	      Window::getInstance().getCamera()->setTarget(translate);
	      Window::getInstance().getCamera()->setPosition(pos);

	      _pos.X = event.MouseInput.X;
	      _pos.Y = event.MouseInput.Y;
	    }
	}
      else if (event.MouseInput.Event == irr::EMIE_MOUSE_WHEEL)
	{
	  irr::core::vector3df pos = Window::getInstance().getCamera()->getPosition();
	  irr::core::vector3df tar = Window::getInstance().getCamera()->getTarget();
	  irr::core::vector3df relative = tar - pos;

	  relative.normalize();
	  Window::getInstance().getCamera()
	    ->setTarget(pos + relative * (1 + 3.0f * event.MouseInput.Wheel));
	  Window::getInstance().getCamera()
	    ->setPosition(pos + 3.0f * relative * event.MouseInput.Wheel);
	}
      else
	return false;
      return true;
    }
  else if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
      if (event.KeyInput.Key == irr::KEY_ESCAPE)
	_stop = true;
      else if (event.KeyInput.Key == irr::KEY_KEY_U)
	{
	  _fPlayer = false;
	  _camera = true;
	  if (_node)
	    {
	      _node->setDebugDataVisible(irr::scene::EDS_OFF);
	      _node = NULL;
	    }
	  if (_box)
	    {
	      _box->remove();
	      _box = NULL;
	    }
	}
      else if (event.KeyInput.Key == irr::KEY_LEFT)
	{
	  irr::core::vector3df pos = Window::getInstance().getCamera()->getPosition();

	  pos.Z += 5.f;
	  Window::getInstance().getCamera()->setPosition(pos);
	}
      else if (event.KeyInput.Key == irr::KEY_UP)
	{
	  irr::core::vector3df pos = Window::getInstance().getCamera()->getPosition();

	  pos.X -= 5.f;
	  Window::getInstance().getCamera()->setPosition(pos);
	}
      else if (event.KeyInput.Key == irr::KEY_DOWN)
	{
	  irr::core::vector3df pos = Window::getInstance().getCamera()->getPosition();

	  pos.X += 5.f;
	  Window::getInstance().getCamera()->setPosition(pos);
	}
      else if (event.KeyInput.Key == irr::KEY_RIGHT)
	{
	  irr::core::vector3df pos = Window::getInstance().getCamera()->getPosition();

	  pos.Z -= 5.f;
	  Window::getInstance().getCamera()->setPosition(pos);
	}
      else if (event.KeyInput.Key == irr::KEY_KEY_F &&
	       event.KeyInput.PressedDown)
	{
	  if (_fPlayer)
	    {
	      _camera = true;
	      _fPlayer = false;
	    }
	  else
	    _fPlayer = true;
	  _update = true;
	}
      else
	return false;
      return true;
    }
  return false;
}

bool	EventManager::stop() const
{
  return _stop;
}

void	EventManager::unsetNode(irr::scene::ISceneNode *node)
{
  if (node && (node == _node))
    _node = NULL;
}

void	EventManager::append(std::wstring &text, size_t n)
{
  std::wstringstream truc;
  std::wstring	tmp;

  truc << n;
  truc >> tmp;
  text.append(tmp);
}

void	EventManager::update(Display &d)
{
  Player	*p;

  if (_camera)
    {
      _camera = false;
      d.unsetCameraP();
    }
  if (_fPlayer && _update && (p = d.getPlayerNode(_node)))
    {
      d.setCameraP(p);

      irr::core::vector3df v(p->getX() + 10.f, 10.f, p->getY() + 10.f);
      irr::core::vector3df t(p->getX(), 1.f, p->getY());

      Window::getInstance().getCamera()->setPosition(v);
      Window::getInstance().getCamera()->setTarget(t);
    }
  if (_update == false && _node != NULL && d.getUpdateNode() == _node)
    {
      _update = true;
      if (_box)
  	{
  	  _box->remove();
  	  _box = NULL;
  	}
    }
  if (_update || (_node != NULL && d.getUpdateNode() == _node))
    {
      _update = false;

      if (_box)
      	{
      	  _box->remove();
      	  _box = NULL;
      	}

      std::wstring	text;
      Square		*s;

      p = NULL;
      s = NULL;

      if ((p = d.getPlayerNode(_node)))
	{
	  text.append(L"\n\n   Niveau du joueur : ");
	  append(text, p->getLevel());
	  text.append(L"\n\n   Inventaire du joueur\n");
	  text.append(L"\n    Nourriture : ");
	  append(text, p->getNourriture());
	  text.append(L"\n    Linemate : ");
	  append(text, p->getLinemate());
	  text.append(L"\n    Deraumere : ");
	  append(text, p->getDeraumere());
	  text.append(L"\n    Sibur : ");
	  append(text, p->getSibur());
	  text.append(L"\n    Mendiane : ");
	  append(text, p->getMendiane());
	  text.append(L"\n    Phiras : ");
	  append(text, p->getPhiras());
	  text.append(L"\n    Thystame : ");
	  append(text, p->getThystame());
	}
      else if ((s = d.getSquareNode(_node)))
      	{
	  text.append(L"\n\n   Inventaire de la case\n");
	  text.append(L"\n    Nourriture : ");
	  append(text, s->getNourriture());
	  text.append(L"\n    Linemate : ");
	  append(text, s->getLinemate());
	  text.append(L"\n    Deraumere : ");
	  append(text, s->getDeraumere());
	  text.append(L"\n    Sibur : ");
	  append(text, s->getSibur());
	  text.append(L"\n    Mendiane : ");
	  append(text, s->getMendiane());
	  text.append(L"\n    Phiras : ");
	  append(text, s->getPhiras());
	  text.append(L"\n    Thystame : ");
	  append(text, s->getThystame());
      	}
      if (s || p)
	{
	  _node->setDebugDataVisible(irr::scene::EDS_BBOX);
	  _box = Window::getInstance().getGUI()->
	    addStaticText(text.c_str(),
			  irr::core::rect<irr::s32>(WINX - (WINX / 5), 60, WINX, 300),
			  true, true, 0, -1, true);
	  _box->setOverrideFont(Window::getInstance().getFont());
	}
      d.unsetNode();
    }
}
