//
// Stone.hh for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Wed Jun  6 18:56:18 2012 anna texier
// Last update Sat Jul 14 10:29:09 2012 tosoni_t
//

#ifndef	__STONE_H_
#define	__STONE_H_

/**
 * @author Anna Texier
 * @file Stone.hh
 */

#include	<map>
#include	"Window.hh"

/*!
 * @class Stone
 * @brief Represents the ressources on the board
 */
class Stone
{
public:
  /*!
   * @enum eType
   * @brief Type of the ressource
   */
  enum	eType
    {
      NOURRITURE = 0,
      LINEMATE,
      DERAUMERE, // emeraude
      SIBUR, // rubis
      MENDIANE, // diamant ?
      PHIRAS, // saphir
      THYSTAME // amethyste
    }; // utlise dans Stone.cpp et Player.cpp

private:
  /*!
   * @brief Defines a pointer to create the corresponding ressource
   */
  typedef	void (Stone::*ptr_t)(void);

  float		_x; /*!< X position */
  float		_y; /*!< Y position */
  eType		_type; /*!< Ressource type */

  irr::scene::IMeshSceneNode *	_sphere; /*!< Irrlicht Mesh */

  std::map<eType, ptr_t>	_create; /*!< Pointer map to create the mesh */

  /*!
   * @brief Creates Nourriture
   */
  void	createNourriture();
  /*!
   * @brief Creates Linemate
   */
  void	createLinemate();
  /*!
   * @brief Creates Deraumere
   */
  void	createDeraumere();
  /*!
   * @brief Creates Sibur
   */
  void	createSibur();
  /*!
   * @brief Creates Mendiane
   */
  void	createMendiane();
  /*!
   * @brief Creates Phiras
   */
  void	createPhiras();
  /*!
   * @brief Creates Thystame
   */
  void	createThystame();

public:
  /*!
   * @brief Constructor
   * @param x X position
   * @param y Y position
   * @param type Type of the ressource, default value = NOURRITURE
   */
  Stone(float, float, eType = NOURRITURE);
  /*!
   * @brief Destructor
   */
  ~Stone();
  /*!
   * @brief Copy Ctor
   */
  Stone(const Stone &);
  /*!
   * @brief operator = 
   */
  Stone &operator=(const Stone &);

  /*!
   * @brief Gets the type of the ressource
   * @return Returns the type of the ressource
   */
  eType		getType() const;
};

#endif
