//
// Player.hh for Zappy in /home/sarglen/zappy/graph_src
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Thu Jun  7 11:46:49 2012 anna texier
// Last update Sat Jul 14 11:04:53 2012 anna texier
//

#ifndef	__PLAYER_H_
#define	__PLAYER_H_

/**
 * @author Anna Texier
 * @file Player.hh
 */

#include	<string>
#include	"Stone.hh"

/*!
 * @brief Defines the type for the players levels
 */
typedef	unsigned short	ushort_t;

/*!
 * @class Player
 * @brief Players caracteristics
 */
class Player
{
public:
  /*!
   * @enum eOrient
   * @brief Orientation of the player
   */
  enum	eOrient
    {
      NORD = 1,
      EST,
      SUD,
      OUEST
    };

private:
  /*!
   * @brief Defines the type for the players inventory
   */
  typedef	std::map<Stone::eType, size_t>	invent_t;

  irr::scene::IAnimatedMeshSceneNode *		_dragon; /*!< Mesh for the player */

  invent_t	_invent; /*!< Players inventory */
  size_t	_x; /*!< X position */
  size_t	_y; /*!< Y position */
  std::string	_team; /*!< Players teamname */
  eOrient	_orient; /*!< Players orientation */
  ushort_t	_level; /*!< Players level */

public:
  /*!
   * @brief Constructor, creates the mesh and the inventory
   * @param x X position
   * @param y Y position
   * @param orient Orientation of the player
   * @param level Level of the player
   * @param team Teamname of the player
   * @param color Color of the team
   */
  Player(size_t, size_t, eOrient, ushort_t, std::string, irr::video::SColor &);
  /*!
   * @brief Destructor
   */
  ~Player();
  /*!
   * @brief Copy Ctor
   */
  Player(const Player &);
  /*!
   * @brief op =
   */
  Player &operator=(const Player &);

  /*!
   * @brief Gets the pointer for the mesh
   * @return Returns the pointer for the mesh
   */
  irr::scene::ISceneNode	*getNode() const;
  /*!
   * @brief Moves the player on the board
   * @param x X position
   * @param y Y position
   * @param orient New orientation for the player
   */
  void		move(size_t, size_t, eOrient);

  /*!
   * @brief Sets the players level
   * @param level New level for the player
   */
  void		setLevel(ushort_t);
  /*!
   * @brief Gets the players level
   * @return The level
   */
  ushort_t	getLevel() const;

  /*!
   * @brief Sets the inventory
   * @param n Number of Nourriture
   * @param l Number of Linemate
   * @param d Number of Deraumere
   * @param s Number of Sibur
   * @param m Number of Mendiane
   * @param p Number of Phiras
   * @param t Number of Thystame
   */
  void		setInvent(size_t, size_t, size_t, size_t, size_t, size_t, size_t);

  /*!
   * @brief Gets the X position
   * @return Returns the position
   */
  size_t	getX() const;
  /*!
   * @brief Gets the Y position
   * @return Returns the position
   */
  size_t	getY() const;

  /*!
   * @brief Gets the quantity of Nourriture
   * @return Returns the quantity
   */
  size_t	getNourriture();
  /*!
   * @brief Gets the quantity of Linemate
   * @return Returns the quantity
   */
  size_t	getLinemate();
  /*!
   * @brief Gets the quantity of Deraumere
   * @return Returns the quantity
   */
  size_t	getDeraumere();
  /*!
   * @brief Gets the quantity of Sibur
   * @return Returns the quantity
   */
  size_t	getSibur();
  /*!
   * @brief Gets the quantity of Mendiane
   * @return Returns the quantity
   */
  size_t	getMendiane();
  /*!
   * @brief Gets the quantity of Phiras
   * @return Returns the quantity
   */
  size_t	getPhiras();
  /*!
   * @brief Gets the quantity of Thystame
   * @return Returns the quantity
   */
  size_t	getThystame();
  /*!
   * @brief Set the rotation to the dragon
   */
  void		orientPlayer();
  /*!
   * @brief
   */
  std::string const	&getTeam() const;
};

#endif
