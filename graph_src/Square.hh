//
// Square.hh for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu Jun  7 10:34:42 2012 anna texier
// Last update Sat Jul 14 11:04:45 2012 anna texier
//

#ifndef	__SQUARE_H_
#define	__SQUARE_H_

/**
 * @author Anna Texier
 * @file Square.hh
 */

#include	<map>
#include	"Window.hh"
#include	"Stone.hh"

/*!
 * @class Square
 * @brief Contains all the ressources in the same place on the board
 */
class Square
{
public:
  /*!
   * @brief Defines the type for the stones list
   */
  typedef	std::map<Stone::eType, std::pair<Stone *, size_t> >	stones_t;

private:
  irr::scene::IParticleSystemSceneNode *	_particles; /*!< Incant mesh */
  irr::scene::IMeshSceneNode *			_cube; /*!< Mesh for the square */

  size_t	_x; /*!< X position */
  size_t	_y; /*!< Y position */
  stones_t	_stones; /*!< Stones list and quantities */
  bool		_incant; /*!< Boolean to know if an incantation is being done */

public:
  /*!
   * @brief Constructor, creates the Mesh
   * @param x X position
   * @param y Y position
   */
  Square(size_t, size_t);
  /*!
   * @brief Destructor
   */
  ~Square();
  /*!
   * @brief Ctor cpy
   */
  Square(const Square &);
  /*!
   * @brief op =
   */
  Square &operator=(const Square &);

  /*!
   * @brief Adds a stone to the list
   * @param type Stone type
   * @param quantity to add, default value = 1
   */
  void	setStone(Stone::eType, size_t = 1);

  /*!
   * @brief Gets the pointer for the mesh
   * @return Returns the pointer for the mesh
   */
  irr::scene::ISceneNode	*getNode() const;

  /*!
   * @brief Starts the incantation
   */
  void		startIncant();
  /*!
   * @brief Ends the incantation
   */
  void		endIncant();

  /*!
   * @brief Gets the quantity of Nourriture
   * @return Returns the quantity
   */
  size_t	getNourriture();
  /*!
   * @brief Gets the quantity of Linemate
   * @return Returns the quantity
   */
  size_t	getLinemate();
  /*!
   * @brief Gets the quantity of Deraumere
   * @return Returns the quantity
   */
  size_t	getDeraumere();
  /*!
   * @brief Gets the quantity of Sibur
   * @return Returns the quantity
   */
  size_t	getSibur();
  /*!
   * @brief Gets the quantity of Mendiane
   * @return Returns the quantity
   */
  size_t	getMendiane();
  /*!
   * @brief Gets the quantity of Phiras
   * @return Returns the quantity
   */
  size_t	getPhiras();
  /*!
   * @brief Gets the quantity of Thystame
   * @return Returns the quantity
   */
  size_t	getThystame();
};

#endif
