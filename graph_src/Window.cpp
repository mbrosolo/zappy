//
// Window.cpp for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Wed Jun  6 15:27:39 2012 anna texier
// Last update Fri Jul 13 21:12:11 2012 anna texier
//

#include	"Window.hh"
#include	"Display.hh"
#include	"ExceptionQuit.hh"

Window::Window()
  : _device(irr::createDevice(irr::video::EDT_OPENGL,
			      irr::core::dimension2d<irr::u32>(WINX, WINY), 32,
			      false, true, false, 0)),
    _driver(_device->getVideoDriver()),
    _sceneManager(_device->getSceneManager()),
    _camera(_sceneManager->addCameraSceneNode()),
    _GUI(_device->getGUIEnvironment()),
    _event(),
    _font(_GUI->getFont("ressources/MyFont.png")),
    _dragon(_sceneManager->getMesh("ressources/DRAGON.md2")),
    _egg(_sceneManager->getMesh("ressources/egg.3ds")),
    _floor(_driver->getTexture("ressources/floor.bmp")),
    _fire(_driver->getTexture("ressources/fire.bmp")),
    _apple(_driver->getTexture("ressources/apple.jpg"))
{
  if (!_egg || !_dragon)
    throw(ExceptionQuit("Some important meshes couldn't be loaded"));
  _device->getCursorControl()->setVisible(true);
  _device->setWindowCaption(L"Zappy");
  _device->setEventReceiver(&_event);
}

Window::~Window()
{
  _device->drop();
}

Window	&Window::getInstance()
{
  static Window	window;
  return window;
}

bool	Window::stop() const
{
  return _event.stop();
}

void	Window::unsetNode(irr::scene::ISceneNode *node)
{
  _event.unsetNode(node);
}

void	Window::update(Display &d)
{
  _event.update(d);
}

irr::IrrlichtDevice	*Window::getDevice() const
{
  return _device;
}

irr::video::IVideoDriver	*Window::getDriver() const
{
  return _driver;
}

irr::scene::ISceneManager	*Window::getSceneManager() const
{
  return _sceneManager;
}

irr::scene::ICameraSceneNode	*Window::getCamera() const
{
  return _camera;
}

irr::gui::IGUIEnvironment	*Window::getGUI() const
{
  return _GUI;
}

irr::gui::IGUIFont	*Window::getFont() const
{
  return _font;
}

irr::scene::IAnimatedMesh	*Window::getDragon() const
{
  return _dragon;
}

irr::scene::IMesh	*Window::getEgg() const
{
  return _egg;
}

irr::video::ITexture	*Window::getFloor() const
{
  return _floor;
}

irr::video::ITexture	*Window::getFire() const
{
  return _fire;
}

irr::video::ITexture	*Window::getApple() const
{
  return _apple;
}
