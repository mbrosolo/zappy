//
// ExceptionQuit.cpp for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Fri Jul 13 14:03:18 2012 anna texier
// Last update Fri Jul 13 14:24:50 2012 anna texier
//

#include	"ExceptionQuit.hh"

ExceptionQuit::ExceptionQuit(std::string const &what,
			     std::string const &where) throw()
  : ExceptionRuntime(what, where)
{

}

ExceptionQuit::~ExceptionQuit() throw()
{

}
