//
// EventManager.hh for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Mon Jun 25 10:40:39 2012 anna texier
// Last update Sun Jul 15 11:25:33 2012 anna texier
//

#ifndef	__ENVENT_MANAGER_H_
#define	__ENVENT_MANAGER_H_

/**
 * @author Anna Texier
 * @file EventManager.hh
 */

#include	<iostream>
#include	<sstream>
#include	"irrlicht.h"

/*!
 * @class Display
 * @brief Predefinition of the Display class
 */
class Display;

/*!
 * @class EventManager
 * @brief Manages the events in the Window
 */
class EventManager : public irr::IEventReceiver
{
private:
  bool				_stop; /*!< To know if the programm has to stop */
  bool				_update; /*!< To know if its necessary to update */
  bool				_camera; /*!< If needs to update the camera */
  bool				_fPlayer; /*! If wants to follow the player */
  irr::scene::ISceneNode *	_node; /*!< Last selected node (NULL if none) */
  irr::gui::IGUIStaticText *	_box; /*!< Static text box to show inventary */

  bool				_LMouse; /*! Left Mouse Button is pressed */
  bool				_RMouse; /*! Right Mouse Button is pressed */
  bool				_MMouse; /*! Middle Mouse Button is pressed */

  irr::core::vector3df		_pos; /*! Position of the Mouse */

public:
  /*!
   * @brief Constructor
   */
  EventManager();
  /*!
   * @brief Destructor
   */
  ~EventManager();
  /*!
   * @brief Copy Constructor
   */
  EventManager(const EventManager &);
  /*!
   * @brief op =
   */
  EventManager &operator=(const EventManager &);

  /*!
   * @brief Override of the OnEvent function from IEventReceiver
   * @return If the key has been treated returns true, else false
   */
  virtual bool	OnEvent(const irr::SEvent &);
  /*!
   * @brief To know if the programm has to stop
   * @return Returns the state of the boolean _stop
   */
  bool		stop() const;

  /*!
   * @brief Sets _node to NULL
   * @param node The node to check and unset
   */
  void		unsetNode(irr::scene::ISceneNode *);

  /*!
   * @brief Appends a number to the string
   * @param text String to append the text to
   * @param n Number to append to the text
   */
  static void	append(std::wstring &, size_t);

  /*!
   * @brief Updates everything
   * @param d Display
   */
  void		update(Display &);
};

#endif
