//
// BaseError.hpp for zappy in /home/tosoni_t/BitBucket/zappy
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Sat Jul 14 09:52:00 2012 tosoni_t
// Last update Sat Jul 14 09:53:50 2012 tosoni_t
//

#ifndef	__BASE_ERROR_HPP__
#define	__BASE_ERROR_HPP__

#include <string>

/**
 * @author Benjamin Gressier
 * @file BaseError.hh
 */

// This class is intented to be ONLY used as a base class
// It should never be instanciated

/*!
 * @class BaseError
 * @brief 
 */
template<typename T>
class BaseError
{
protected:
  mutable T	_error; /*!< */

protected:
  /*!
   * @brief
   */
  BaseError(T err = 0)
    : _error(err)
  {
  }

public:
  /*!
   * @brief
   */
  virtual ~BaseError() {}

  /*!
   * @brief
   */
  T		getError() const
  {
    return (this->_error);
  }
};

/*!
 * @class BaseError
 * @brief 
 */
template<>
class BaseError<std::string>
{
protected:
  mutable std::string		_error; /*!< */

public:
  /*!
   * @brief
   */
  BaseError(std::string const& err = "")
    : _error(err)
  {
  }

  /*!
   * @brief
   */
  virtual ~BaseError() {}

  /*!
   * @brief
   */
  std::string const&	getError() const
  {
    return (this->_error);
  }
};

#endif
