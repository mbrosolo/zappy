//
// Stone.cpp for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Wed Jun  6 18:56:22 2012 anna texier
// Last update Fri Jul 13 15:18:07 2012 anna texier
//

#include	"Stone.hh"
#include	"ExceptionDisplay.hh"

Stone::Stone(float x, float y, eType type)
  : _x(x), _y(y), _type(type), _sphere(NULL), _create()
{
  _create[NOURRITURE] = &Stone::createNourriture;
  _create[LINEMATE] = &Stone::createLinemate;
  _create[DERAUMERE] = &Stone::createDeraumere;
  _create[SIBUR] = &Stone::createSibur;
  _create[MENDIANE] = &Stone::createMendiane;
  _create[PHIRAS] = &Stone::createPhiras;
  _create[THYSTAME] = &Stone::createThystame;

  if (_create.find(type) == _create.end())
    throw (ExceptionDisplay("Invalid type for square"));
  (this->*_create[type])();
}

Stone::~Stone()
{
  _sphere->remove();
}

Stone::eType	Stone::getType() const
{
  return _type;
}

void	Stone::createNourriture()
{
  _sphere = Window::getInstance().getSceneManager()
    ->addSphereSceneNode(0.1f, 16, NULL, -1, irr::core::vector3df(_x, 0.6f, _y));
  _sphere->setMaterialFlag(irr::video::EMF_LIGHTING, false);
  _sphere->setMaterialTexture(0, Window::getInstance().getApple());
}

void	Stone::createLinemate()
{
  _x += (1.0f / 4.0f);
  _y += (1.0f / 6.0f);
  _sphere = Window::getInstance().getSceneManager()
    ->addSphereSceneNode(0.1f, 16, NULL, -1, irr::core::vector3df(_x, 0.6f, _y));
  _sphere->setMaterialFlag(irr::video::EMF_LIGHTING, true);
  _sphere->getMaterial(0).EmissiveColor = irr::video::SColor(255,0,0,0);
}

void	Stone::createDeraumere()
{
  _x += (1.0f / 4.0f);
  _y -= (1.0f / 6.0f);
  _sphere = Window::getInstance().getSceneManager()
    ->addSphereSceneNode(0.1f, 16, NULL, -1, irr::core::vector3df(_x, 0.6f, _y));
  _sphere->setMaterialFlag(irr::video::EMF_LIGHTING, true);
  _sphere->getMaterial(0).EmissiveColor = irr::video::SColor(255,0,120,40);
}

void	Stone::createSibur()
{
  _y += (1.0f / 3.0f);
  _sphere = Window::getInstance().getSceneManager()
    ->addSphereSceneNode(0.1f, 16, NULL, -1, irr::core::vector3df(_x, 0.6f, _y));
  _sphere->setMaterialFlag(irr::video::EMF_LIGHTING, true);
  _sphere->getMaterial(0).EmissiveColor = irr::video::SColor(255,170,0,20);
}

void	Stone::createMendiane()
{
  _x -= (1.0f / 4.0f);
  _y += (1.0f / 6.0f);
  _sphere = Window::getInstance().getSceneManager()
    ->addSphereSceneNode(0.1f, 16, NULL, -1, irr::core::vector3df(_x, 0.6f, _y));
  _sphere->setMaterialFlag(irr::video::EMF_LIGHTING, true);
  _sphere->getMaterial(0).EmissiveColor = irr::video::SColor(255,200,200,200);
}

void	Stone::createPhiras()
{
  _x -= (1.0f / 4.0f);
  _y -= (1.0f / 6.0f);
  _sphere = Window::getInstance().getSceneManager()
    ->addSphereSceneNode(0.1f, 16, NULL, -1, irr::core::vector3df(_x, 0.6f, _y));
  _sphere->setMaterialFlag(irr::video::EMF_LIGHTING, true);
  _sphere->getMaterial(0).EmissiveColor = irr::video::SColor(255,0,0,100);
}

void	Stone::createThystame()
{
  _y -= (1.0f / 3.0f);
  _sphere = Window::getInstance().getSceneManager()
    ->addSphereSceneNode(0.1f, 16, NULL, -1, irr::core::vector3df(_x, 0.6f, _y));
  _sphere->setMaterialFlag(irr::video::EMF_LIGHTING, true);
  _sphere->getMaterial(0).EmissiveColor = irr::video::SColor(255,150,50,210);
}
