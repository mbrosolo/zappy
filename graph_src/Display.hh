//
// Display.hh for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu Jun  7 10:05:09 2012 anna texier
// Last update Sun Jul 15 15:21:38 2012 anna texier
//

#ifndef	__DISPLAY_H_
#define	__DISPLAY_H_

/**
 * @author Anna Texier
 * @file Display.hh
 */

#include	<vector>
#include	<map>
#include	<queue>
#include	<err.h>
#include	"Board.hh"
#include	"Egg.hh"
#include	"Player.hh"
#include	"Token.hh"
#include	"InterpretConnection.hpp"
#include	"AInterpret.hpp"
#include	"AClient.hh"
#include	"LoadSound.hh"

/*!
 * @class Display
 * @brief Manages all
 */
class Display : public AClient,
		public AInterpret<Display, Display>
{
public:
  /*!
   * @brief Defines the type used for the players list
   */
  typedef	std::map<size_t, Player *>	players_t;
  /*!
   * @brief Defines the type used for the eggs list
   */
  typedef	std::map<size_t, Egg *>	eggs_t;
  /*!
   * @brief Defines the type used for the players mesh pointers
   */
  typedef	std::map<irr::scene::ISceneNode *, Player *>	pptrs_t;
  /*!
   * @brief Defines the type used for the eggs mesh pointers
   */
  typedef	std::map<irr::scene::ISceneNode *, Egg *>	eptrs_t;
  /*!
   * @brief Defines the type of the teamnames
   */
  typedef	std::map<std::string, std::pair<size_t, irr::video::SColor> >	teams_t;
  /*! 
   * @brief Defines the type of the tokens queue
   */
  typedef	std::queue<Token>	tokens_t;
  /*!
   * @brief Defines the type of the pt declaration type function
   */
  typedef	void	(Display::*ptqueue)(std::queue<Token> *);
  /*!
   * @brief Defines the type of the pt graph
   */
  typedef	std::map<std::string const , ptqueue>	refcmd;

private:
  Board				_board; /*!< The board of the game */
  players_t			_players; /*!< Players list */
  eggs_t			_eggs; /*!< Eggs list */
  teams_t			_teams; /*!< Teamnames + number of players foreach */
  pptrs_t			_pNodes; /*!< Players mesh nodes */
  eptrs_t			_eNodes; /*!< Eggs mesh nodes */
  irr::gui::IGUIStaticText *	_teamBox; /*!< Text printing the teams */
  bool				_updateTeams; /*!< Update the teamBox */
  Player			*_cameraP; /*!< Camera on the given player */
  irr::scene::ISceneNode *	_updateNode; /*!< Update the players inventory */
  refcmd			_refcmd; /*!<  */
  size_t			_time; /*!<  */
  sound::LoadSound		_soundManager;
  /*!
   * @brief Pops the next element from the token queue
   * @param text Text the string should be added to
   * @param str The string to copy
   */
  void		copyStr(std::wstring &, const std::string &);

  /*!
   * @brief Pops the next element from the token queue
   * @param q Token queue
   * @param check If boolean at true, throws an exception if there is no next element
   */
  void		pop(tokens_t &, bool);
  /*!
   * @brief Gets a size_t from a string in the token queue
   * @param q Token queue
   * @param toPop Boolean to pop an element first
   * @param check Parameter for poping
   * @return The getted value
   */
  size_t	getSizeT(tokens_t &, bool, bool);
  /*!
   * @brief Gets a string from the token queue
   * @param q Token queue
   * @param toPop Boolean to pop an element first
   * @param check Parameter for poping
   * @return The getted value
   */
  std::string	getStr(tokens_t &, bool, bool);

public:
  /*!
   * @brief Constructor with song
   */
  Display(std::string const &);
  /*!
   * @brief Destructor
   */
  ~Display();
  /*!
   * @brief Copy ctor
   */
  Display(const Display &);
  /*!
   * @brief operator =
   */
  Display &operator=(const Display &);

  /*!
   * @brief Calls the run function of the Irrlicht Device
   * @return Returns the same value as the function called
   */
  bool	run_display();

  /*!
   * @brief Draws all the elements on the screen
   */
  void	draw();

  /*!
   * @brief Sets the pointer corresponding to the player the camera needs to follow
   * @param player Pointer to the corresponding player
   */
  void	setCameraP(Player *);
  /*!
   * @brief Sets the pointer to NULL
   */
  void	unsetCameraP();

  /*!
   * @brief Finds out the player corresponding to the given Scene Node
   * @param node Scene Node that needs to be found
   * @return Returns NULL if the node is not from a player, else returns a pointer to the player
   */
  Player	*getPlayerNode(irr::scene::ISceneNode *);
  /*!
   * @brief Finds out the square corresponding to the given Scene Node
   * @param node Scene Node that needs to be found
   * @return Returns NULL if the node is not from a square, else returns a pointer to the square
   */
  Square	*getSquareNode(irr::scene::ISceneNode *);

  /*!
   * @brief Returns the node that needs to be updated
   * @return Returns the pointer
   */
  irr::scene::ISceneNode	*getUpdateNode() const;

  /*!
   * @brief Sets the updateNode to NULL
   */
  void				unsetNode();

private:
  /*!
   * @brief Sets the size of the board
   * @param q Token queue
   */
  void	msz(tokens_t *);
  /*!
   * @brief Lists the ressources on the square
   * @param q Token queue
   */
  void	bct(tokens_t *);
  /*!
   * @brief Sets the teamnames
   * @param q Token queue
   */
  void	tna(tokens_t *);
  /*!
   * @brief New players connects
   * @param q Token queue
   */
  void	pnw(tokens_t *);
  /*!
   * @brief Sets the position of the player
   * @param q Token queue
   */
  void	ppo(tokens_t *);
  /*!
   * @brief Sets the level of the player
   * @param q Token queue
   */
  void	plv(tokens_t *);
  /*!
   * @brief Lists the ressources in the players inventory
   * @param q Token queue
   */
  void	pin(tokens_t *);
  /*!
   * @brief A player pushes out another
   * @param q Token queue
   */
  void	pex(tokens_t *);
  /*!
   * @brief A player sends a broadcast
   * @param q Token queue
   */
  void	pbc(tokens_t *);
  /*!
   * @brief Launches the incantation for the given players
   * @param q Token queue
   */
  void	pic(tokens_t *);
  /*!
   * @brief Ends the incantation with given level
   * @param q Token queue
   */
  void	pie(tokens_t *);
  /*!
   * @brief Player starts to lay an egg
   * @param q Token queue
   */
  void	pfk(tokens_t *);
  /*!
   * @brief Player throws a ressource
   * @param q Token queue
   */
  void	pdr(tokens_t *);
  /*!
   * @brief Player takes a ressource
   * @param q Token queue
   */
  void	pgt(tokens_t *);
  /*!
   * @brief Player died by eating too few
   * @param q Token queue
   */
  void	pdi(tokens_t *);
  /*!
   * @brief Creates an egg on the square
   * @param q Token queue
   */
  void	enw(tokens_t *);
  /*!
   * @brief Egg opens
   * @param q Token queue
   */
  void	eht(tokens_t *);
  /*!
   * @brief Player connected for the egg
   * @param q Token queue
   */
  void	ebo(tokens_t *);
  /*!
   * @brief The open egg died by eating too few
   * @param q Token queue
   */
  void	edi(tokens_t *);
  /*!
   * @brief Sets the time unity
   * @param q Token queue
   */
  void	sgt(tokens_t *);
  /*!
   * @brief End of the game, given team wins the game
   * @param q Token queue
   */
  void	seg(tokens_t *);
  /*!
   * @brief Server message
   * @param q Token queue
   */
  void	smg(tokens_t *);
  /*!
   * @brief No such command
   * @param q Token queue
   */
  void	suc(tokens_t *);
  /*!
   * @brief Wrong parameter for the command
   * @param q Token queue
   */
  void	sbp(tokens_t *);

public:
  /*!
   * @brief
   */
  void	launch(int port, std::string const & hostname);
  /*!
   * @brief
   */
  void	start();
  /*!
   * @brief
   */
  bool	checkCommand(std::queue<Token> *);
};

#endif
