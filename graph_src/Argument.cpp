//
// Argument.cpp for zappy in /home/gressi_b/Epitech/B4/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Tue Jun 19 18:09:32 2012 gressi_b
// Last update Sun Jul 15 19:02:55 2012 anna texier
//

#include <sstream>
#include <cstdlib>

#include "Argument.hh"

// Argument --------------------------------------------------------------------

Argument::Argument()
  : BaseError<std::string>(), _port(0), _hostName("localhost"), _song("./sounds/Alone_in_the_Dark/Alone in the Dark - 02 - Exploring Derceto.mp3")
{
}

// Member functions ------------------------------------------------------------

bool
Argument::_isOption(std::string const& str) const
{
  return (str.size() > 1 && str[0] == '-');
}

bool
Argument::_checkArgs(std::string const& progname)
{
  if (this->_port == 0)
    {
      this->_error = progname + ": invalid port number";
      return false;
    }
  return true;
}

bool
Argument::parse(int ac, char **av)
{
  std::stringstream	ss;
  int			c;

  while ((c = getopt(ac, av, "p:h:m:")) != -1)
    {
      if (optarg && Argument::_isOption(optarg))
	{
	  ss << av[0] << ": option requires an argument -- '" << c << '\'';
	  this->_error = ss.str();
	  return (false);
	}
      switch (c) {
      case 'p': this->_port = atoi(optarg); break;
      case 'h': this->_hostName = optarg; break;
      case 'm': this->_song = optarg; break;
      default: break;
      }
    }
  return (this->_checkArgs(av[0]));
}

#if 0
static int	check_args(char const *progname, t_args *args)
{
  if (args->port == 0)
    {
      fprintf(stderr, S_BADPORT, progname);
      return (1);
    }
  return (0);
}

int		parse_args(int ac, char **av, t_args *args)
{
  int		c;

  args->port = DEFAULT_PORT;
  args->hostname = DEFAULT_HOSTNAME;
  while ((c = getopt(ac, av, "p:h:")) != -1)
    {
      if (IS_OPT(optarg))
	{
	  fprintf(stderr, S_OPARGREQ, av[0], c);
	  return (1);
	}
      if (c == 'p')
	args->port = atoi(optarg);
      else if (c == 'h')
	args->hostname = optarg;
      else
	return (1);
    }
  return (check_args(av[0], args));
}
#endif

// Getters ---------------------------------------------------------------------

std::string const&
Argument::getHostName() const
{
  return this->_hostName;
}

int
Argument::getPort() const
{
  return this->_port;
}

std::string const &	Argument::getSong() const
{
  return (this->_song);
}
