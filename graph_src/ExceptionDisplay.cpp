//
// ExceptionDisplay.cpp for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Wed Jul  4 09:40:43 2012 anna texier
// Last update Fri Jul 13 14:23:28 2012 anna texier
//

#include	"ExceptionDisplay.hh"

ExceptionDisplay::ExceptionDisplay(std::string const &what,
				   std::string const &where) throw()
  : ExceptionRuntime(what, where)
{

}

ExceptionDisplay::~ExceptionDisplay() throw()
{

}
