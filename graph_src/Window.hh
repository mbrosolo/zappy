//
// Window.hh for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Wed Jun  6 15:27:51 2012 anna texier
// Last update Sat Jul 14 10:30:22 2012 tosoni_t
//

#ifndef	__WINDOW_H_
#define	__WINDOW_H_

/**
 * @author Anna Texier
 * @file Window.hh
 */

#include	"irrlicht.h"
#include	"EventManager.hh"

/*!
 * @class Display
 * @brief Predefinition of the Display class
 */
class Display;

/*!
 * @class Window
 * @brief Contains all the basic pointers Irrlicht needs
 */
class Window
{
private:
/*!
 * @brief Define WINX
 */
#define	WINX	(1024)
/*!
 * @brief Define WINY
 */
#define	WINY	(780)

private:
  irr::IrrlichtDevice *			_device; /*!< Irrlicht Device */
  irr::video::IVideoDriver *		_driver; /*!< Irrlicht Video Driver */
  irr::scene::ISceneManager *		_sceneManager; /*!< Irrlicht Scene Manager */
  irr::scene::ICameraSceneNode *	_camera; /*!< Irrlicht Camera */
  irr::gui::IGUIEnvironment *		_GUI; /*!< Irrlicht GUI Manager */
  EventManager				_event; /*!< Event Manager */
  irr::gui::IGUIFont *			_font; /*!< Font for text */
  irr::scene::IAnimatedMesh *		_dragon; /*! Dragon mesh for player */
  irr::scene::IMesh *			_egg; /*! Mesh for egg */
  irr::video::ITexture *		_floor; /*! Texture for the floor */
  irr::video::ITexture *		_fire; /*! Texture for the fire */
  irr::video::ITexture *		_apple; /*! Texture for the apples */

private:
  /*!
   * @brief Constructor, creates all this pointers
   */
  Window();
  /*!
   * @brief Destructor
   */
  ~Window();
  /*!
   * @brief Ctor Cpy
   */
  Window(const Window &);
  /*!
   * @brief operator =
   */
  Window &operator=(const Window &);

public:
  /*!
   * @brief Gets the singleton of this class
   * @return Returns a reference on the singleton
   */
  static Window			&getInstance();

  /*!
   * @brief Checks if the programm has to stop or not
   * @return Returns the state
   */
  bool				stop() const;

  /*!
   * @brief Sets _node to NULL
   * @param node The node to check and unset
   */
  void				unsetNode(irr::scene::ISceneNode *);

  /*!
   * @brief Updates the Scene in funciton of the Event
   * @param d Display pointer
   */
  void				update(Display &);

  /*!
   * @brief Gets the Device
   * @return Returns the corresponding pointer
   */
  irr::IrrlichtDevice		*getDevice() const;
  /*!
   * @brief Gets the Driver
   * @return Returns the corresponding pointer
   */
  irr::video::IVideoDriver	*getDriver() const;
  /*!
   * @brief Gets the Scene Manager
   * @return Returns the corresponding pointer
   */
  irr::scene::ISceneManager	*getSceneManager() const;
  /*!
   * @brief Gets the Camera
   * @return Returns the corresponding pointer
   */
  irr::scene::ICameraSceneNode	*getCamera() const;
  /*!
   * @brief Gets the GUI Manager
   * @return Returns the corresponding pointer
   */
  irr::gui::IGUIEnvironment	*getGUI() const;
  /*!
   * @brief Gets the font for the textes
   * @return Returns the corresponding pointer
   */
  irr::gui::IGUIFont		*getFont() const;
  /*!
   * @brief Gets mesh for the dragon
   * @return Returns the corresponding pointer
   */
  irr::scene::IAnimatedMesh	*getDragon() const;
  /*!
   * @brief Gets mesh for the egg
   * @return Returns the corresponding pointer
   */
  irr::scene::IMesh		*getEgg() const;
  /*!
   * @brief Gets texture for the floor
   * @return Returns the corresponding pointer
   */
  irr::video::ITexture		*getFloor() const;
  /*!
   * @brief Gets texture for the incantation fire
   * @return Returns the corresponding pointer
   */
  irr::video::ITexture		*getFire() const;
  /*!
   * @brief Gets texture for the apples
   * @return Returns the corresponding pointer
   */
  irr::video::ITexture		*getApple() const;
};

#endif
