//
// Interpret.hh for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Mon Jun 18 13:33:44 2012 gaetan senn
// Last update Sat Jul 14 10:15:12 2012 tosoni_t
//

#ifndef __INTERPRET_CONNECTION__
#define __INTERPRET_CONNECTION__

/**
 * @author Gaetan Senn
 * @file InterpretConnection.hpp
 */

#include "AInterpret.hpp"
#include "Lexer.hh"
#include <map>
#include <queue>
#include <sstream>

/*!
 * @class InterpretConnection
 * @brief
 */
template <typename T>
class		InterpretConnection
  : public AInterpret<T, InterpretConnection<T> >
{
public:
  /*!
   * @brief Ctor
   */
  InterpretConnection(T & target)
    : AInterpret<T, InterpretConnection<T> >(&target, this)
  {
    addFunction(&InterpretConnection<T>::WelcomeCheck, Select::READ);
    addFunction(&InterpretConnection<T>::SendGraphic, Select::WRITE);
  }
  /*!
   * @brief Dtor
   */
  ~InterpretConnection()
  {
  }
  /*!
   * @brief
   */
  bool			checkCommand(std::queue<Token>	*)
  {
    return (true);
  }
  /*!
   * @brief
   */
  bool			WelcomeCheck(void *r)
  {
    std::queue<Token>	*q;

    q = static_cast<std::queue<Token> *>(r);
    if (q->size() == 1 && q->front().getType() == Token::STRING &&
	q->front().getData() == "ko")
      throw ExceptionInterpret("Server Full", "void	AInterpret::run");
    if (q->size() == 1 && q->front().getType() == Token::STRING &&
	q->front().getData() == "BIENVENUE")
      return (tools::freeDelete<std::queue<Token> *>(q, true));
    else
      return (tools::freeDelete<std::queue<Token> *>(q, false));
  }
  /*!
   * @brief
   */
  bool			SendGraphic(void *)
  {
    this->_target->addCommand("GRAPHIC\n");
    return (true);
  }
  /*!
   * @brief
   */
  void			start()
  {
    this->run(false);
  }
};

#endif
