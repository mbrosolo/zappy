//
// Egg.cpp for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Thu Jun  7 11:44:35 2012 anna texier
// Last update Fri Jul 13 14:57:24 2012 anna texier
//

#include	"Egg.hh"

Egg::Egg(size_t x, size_t y)
  : _x(x), _y(y)
{
  _egg = Window::getInstance().getSceneManager()
    ->addMeshSceneNode(Window::getInstance().getEgg(), NULL, 1,
		       irr::core::vector3df(_x + 0.18f, 0.6f, _y + 0.0f), // position
		       irr::core::vector3df(0, 0, 0), // rotation
		       irr::core::vector3df(0.2f, 0.2f, 0.2f)); // scale
  // _dragon->setMD2Animation(irr::scene::EMAT_STAND);
  _egg->setMaterialFlag(irr::video::EMF_LIGHTING, true);
  _egg->getMaterial(0).EmissiveColor = irr::video::SColor(255,240,220,150);
}

Egg::~Egg()
{
  if (_egg)
    _egg->remove();
}
