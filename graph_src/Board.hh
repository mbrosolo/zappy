//
// Board.hh for Zappy in /home/sarglen/zappy/graph_src
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Wed Jun  6 14:22:45 2012 anna texier
// Last update Sun Jul 15 15:19:02 2012 anna texier
//

#ifndef	__BOARD_H_
#define	__BOARD_H_

/**
 * @author Anna Texier
 * @file Board.hh
 */

#include	<vector>
#include	"Window.hh"
#include	"Square.hh"

/*!
 * @class Board
 * @brief Board where the players move and play
 */
class Board
{
public:
  /*!
   * @brief Defines the type used for stocking the squares
   */
  typedef	std::vector<Square *>	squares_t;

  /*!
   * @brief Defines the type used for the squares mesh pointers
   */
  typedef	std::map<irr::scene::ISceneNode *, Square *>	sptrs_t;

private:
  size_t	_x; /*!< Width of the board */
  size_t	_y; /*!< Length of the board */
  squares_t	_squares; /*!< Squares forming the board */
  sptrs_t	_sNodes; /*!< Meshes from the squares */

public:
  /*!
   * @brief Constructor for the board
   * @param x The width of the board, default value : 0
   * @param y The length of the board, default value : 0
   */
  Board(size_t = 0, size_t = 0);
  /*!
   * @brief Destructor of the Board, deletes all squares
   */
  ~Board();
  /*!
   * @brief Copy constructor
   */
  Board(const Board &);
  /*!
   * @brief Operator =
   */
  Board &operator=(const Board &);

  /*!
   * @brief Sets the width of the board
   * @param x Width
   */
  void		setX(const size_t);
  /*!
   * @brief Sets the length of the board
   * @param x Length
   */
  void		setY(const size_t);

  /*!
   * @brief Creates and draws every square of the board
   */
  void		draw();
  /*!
   * @brief Gets the square that is in this position on the board
   * @param x X position of the square
   * @param y Y position of the square
   */
  Square	 *getSquare(const size_t, const size_t) const;

  /*!
   * @brief Get square nodes
   */
  Square	*getSquareNode(irr::scene::ISceneNode *);
};

#endif
