//
// ExceptionDisplay.hh for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Wed Jul  4 09:37:15 2012 anna texier
// Last update Fri Jul 13 14:23:23 2012 anna texier
//

#ifndef __EXCEPTION_DISPLAY_HH__
#define __EXCEPTION_DISPLAY_HH__

/**
 * @author Anna Texier
 * @file ExceptionDisplay.hh
 */

#include	<string>
#include	"ExceptionRuntime.hh"

/*!
 * @class ExceptionDisplay
 * @brief Exception for the display
 */
class ExceptionDisplay : public ExceptionRuntime
{
public:
  /*!
   * @brief Constructor
   * @param what Message to know what happend
   * @param where Message to know where it happend
   */
  ExceptionDisplay(std::string const & = "", std::string const & = "") throw();
  /*!
   * @brief Destructor
   */
  virtual ~ExceptionDisplay() throw();
};

#endif
