/*
** enums.h for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src/includes
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Jun 22 15:48:12 2012 matthias brosolo
** Last update Fri Jun 22 15:48:12 2012 matthias brosolo
*/

#ifndef	__ENUMS_H__
# define	__ENUMS_H__

/*
** ENUMS
*/
enum	e_fdselect
  {
    F_READ = 0,
    F_WRITE,
    F_EXCEPT,
    F_NB_FDSET
  };

enum	e_client_cmd
  {
    ADVANCE = 0,
    TURN_RIGHT,
    TURN_LEFT,
    SEE,
    INVENTORY,
    TAKE,
    PUT_DOWN,
    EJECT,
    BROADCAST,
    INCANTATION,
    FORK,
    CONNECT_NBR,
    UNKNOWN_COMMAND,
    INCANTATION_END,
    DEAD,
    HATCHING,
    CLIENT_TIMEOUT,
    NB_CLIENT_CMD
  };

enum	e_client_state
  {
    ST_CONNECTING = 0,
    ST_CONNECTED,
    ST_GRAPHIC,
    ST_TO_REMOVE,
    NB_CLIENT_STATE
  };

enum	e_player_status
  {
    PS_RESERVED,
    PS_WAITING,
    PS_LIVING,
    PS_DEAD,
    NB_PLAYER_STATUS
  };

enum	e_directions
  {
    UP = 0,
    RIGHT,
    DOWN,
    LEFT,
    NB_DIRECTIONS
  };

enum	e_end_of_buffer
  {
    EOB_LF = 0,
    EOB_CRLF,
    NB_EOB
  };

enum	e_graphic_cmd
  {
    GC_MAP_SIZE,
    GC_SQUARE_CONTENT,
    GC_MAP_CONTENT,
    GC_TEAM_NAMES,
    GC_PLAYER_POSITION,
    GC_PLAYER_LEVEL,
    GC_PLAYER_INVENTORY,
    GC_SERVER_GET_TIME,
    GC_SERVER_SET_TIME,
    NB_GRAPHIC_CMD
  };

enum	e_owner_type
  {
    OT_CLIENT,
    OT_PLAYER,
    NB_OWNER_TYPE
  };

/**
 * @brief The list of options (with a tiret) we accept in the command line
 * with a specific order (similar to the order of the enum in e_option_param,
 * it's very important !)
 */
#define	LIST_OPTION	"pxyctn"

/**
 * @brief enum of every type of options possible in the command line
 */
enum	e_option_param
  {
    P_PARAM, /**< The option '-p' */
    X_PARAM, /**< The option '-x' */
    Y_PARAM, /**< The option '-y' */
    C_PARAM, /**< The option '-c' */
    T_PARAM, /**< The option '-t' */
    NB_PARAM, /**< The number of options */
    N_PARAM = NB_PARAM, /**< The option '-n', particular for it treatment */
    INVALID_PARAM /**< None of theses options */
  };

#endif
