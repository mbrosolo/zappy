/*
** defines.h for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src/includes
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Fri Jun 22 15:46:59 2012 matthias brosolo
** Last update Sun Jul 15 22:52:00 2012 benjamin bourdin
*/

#ifndef	__DEFINES_H__
# define	__DEFINES_H__

/*
** DEFINES
*/
# define	ZP_QUEUE_LENGTH		128
# define	OP_LIST			"pyxnct"
# define	S_OPINVALID		"%s: invalid option -- '%c'\n"
# define	S_OPARGREQ		"%s: option requires an argument -- '%c'\n"
# define	DEFAULT_PORT		8080
# define	DEFAULT_WIDTH		10
# define	DEFAULT_HEIGHT		10
# define	DEFAULT_CLIENT		1
# define	DEFAULT_DELAY		100
# define	DEFAULT_HP		10
# define	DEFAULT_LEVEL		1
# define	LEVEL_WIN		8
# define	NBR_PLAYER_TO_WIN	6

# define	MAX(x, y)		((x) > (y) ? (x) : (y))
# define	REAL_SIZE(x)		(sizeof(x) - 1)
# define	SIZE_WORD		50
# define	CLIENT_CMD_LEN		20
# define	END_OF_TRANSMISSION	(4)
# define	BUFFER_SIZE		1024
# define	WELCOME_MSG		"BIENVENUE\n"
# define	DEAD_MSG		"mort"
# define	INCANT_BEING		"elevation en cours"

# define	MIN_WIDTH		1
# define	MIN_HEIGHT		1
# define	MAX_WIDTH		1000
# define	MAX_HEIGHT		1000
# define	DELAY_MIN		1
# define	DELAY_MAX		1000000
# define	PLAYER_NBR_MIN		1
# define	TEAM_NBR_MIN		2
# define	PORT_MIN		1
# define	MIN_CLIENT		1
# define	ALL_HOST		"0.0.0.0"
# define	SIZE_NAME_RESOURCE	15

# define	INVENTORY_STR		"{nourriture %d,linemate %d,deraumere\
 %d,sibur %d,mendiane %d,phiras %d,thystame %d}"
# define	USAGE_STR		"[USAGE]: %s [-p port] [-c max_clients\
] [-t delay] [-x width] [-y height] -n team_name1 team_name2...\n"
/*
** Error messages
*/
# define	E_BAD_DELAY		"time must be between 1 and 1000000"
# define	E_MAP_TOO_SMALL		"the map is too small"
# define	E_MAP_TOO_BIG		"the map is too big"
# define	E_NOT_ENOUGH_TEAM	"there must be at least two team"
# define	E_BAD_PLAYER_NBR	"there must be at least one player by team"
# define	E_PORT_OUT_OF_RANGE	"port is out of range"
# define	E_NOT_ENOUGH_CLIENT	"not enough client per team"

#endif
