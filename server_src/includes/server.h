/*
** server.h for zappy in /home/gressi_b/zappy/server_src
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Mon May 14 15:15:55 2012 gressi_b
** Last update Wed Jun 13 11:05:26 2012 benjamin bourdin
*/

#ifndef	__SERVER_H__
# define	__SERVER_H__

# include	<sys/select.h>
# include	<sys/time.h>
# include	<sys/types.h>
# include	<unistd.h>
# include	"../../common.h"
# include	"map.h"

# include	"defines.h"
# include	"enums.h"
# include	"typedefs.h"
# include	"structures.h"
# include	"prototypes.h"
# include	"global.h"

#endif
