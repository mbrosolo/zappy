/*
** structures.h for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src/includes
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Jun 22 15:48:58 2012 matthias brosolo
** Last update Fri Jun 22 15:48:58 2012 matthias brosolo
*/

#ifndef	__STRUCTURES_H__
# define	__STRUCTURES_H__

/*
** STRUCTURES
*/
typedef	struct	s_2d
{
  int		x;
  int		y;
}		t_2d;

typedef	struct	s_move
{
  int		direction;
  t_2d		position;
}		t_move;

typedef	struct	s_com_buffer
{
  int		count;
  char		current[BUFFER_SIZE + 1];
  t_buffer	buffer;
  short		pos;
  ssize_t	size;
}		t_com_buffer;

typedef	struct	s_com
{
  t_connect	sock;
  t_com_buffer	read;
  t_com_buffer	write;
}		t_com;

typedef struct	s_team
{
  int		connect_nbr;
  char		*name;
  t_list	players_list;
}		t_team;

typedef struct	s_player
{
  int		x;
  int		y;
  int		hp;
  int		status;
  int		resource[R_NB_RESOURCE];
  int		direction;
  char		level;
  size_t	id;
  size_t	egg_id;
  t_task	*live;
  t_team	*team;
  t_client	*client;
}		t_player;

struct		s_client
{
  int		eob;		/**< End of buffer flag (CRLF/LF) */
  int		state;
  t_com		com;
  t_task	*task;
  t_player	*player;
};

typedef	struct	s_cmd
{
  char		name[CLIENT_CMD_LEN + 1];
  int		(*fct)(t_server *s, t_client *c);
}		t_cmd;

struct	s_task
{
  int		id;
  int		owner_type;
  char		buffer[BUFFER_SIZE + 1];
  void		*owner;
  t_timeval	delay;
};

struct	s_server
{
  int		port;
  int		delay;							/**< the delay */
  int		client_per_team;					/**< the number of client per team */
  char		buffer[BUFFER_SIZE + 1];				/**< Temporary buffer */
  char		resources_names[R_NB_RESOURCE][SIZE_NAME_RESOURCE + 1];	/**< Array of strings containig the
									     resources' names */
  t_cmd		client_cmd[NB_CLIENT_CMD];				/**< Array of t_cmd containing the commands
									     for the game */
  t_cmd		graphic_cmd[NB_GRAPHIC_CMD];				/**< Array of t_cmd containing the graphic
									   commands for the game */
  t_map		map;							/**< the map */
  size_t	egg_id;
  size_t	player_id;
  fd_set	fds[F_NB_FDSET];
  t_list	tmp_list;
  t_list	team_list;
  t_list	task_list;
  t_list	client_list;
  t_list	players_list;
  t_team	*winner;
  t_connect	sock;
  t_timeval	time_ref;						/**< Timeout referent */
  t_timeval	task_delay[NB_CLIENT_CMD];
  t_timeval	time_remaining;						/**< Timeout remaining */
};

#endif
