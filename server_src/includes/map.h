/*
** map.h for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Tue May 15 17:46:31 2012 matthias brosolo
** Last update Sun Jul 15 16:28:39 2012 anna texier
*/

#ifndef	__MAP_H__
# define	__MAP_H__

# include	<stdio.h>
# include	"../../common.h"

/*
** STRUCTURES
*/
typedef	struct	s_square
{
  int		x;
  int		y;
  t_list	players_list;
  unsigned int	items[R_NB_RESOURCE];
}		t_square;

typedef	struct	s_map
{
  int		width;
  int		height;
  char		*square_flag;
  size_t	size;		/* number of squares */
  t_square	*square_array;
  int		(*is_set)(struct s_map *m, int x, int y);
  void		(*set)(struct s_map *m, int x, int y);
  void		(*unset_all)(struct s_map *m);
  t_square	*(*get_square)(struct s_map *m, int x, int y);
  unsigned int	refs[R_NB_RESOURCE];
  unsigned int	items[R_NB_RESOURCE];
}		t_map;

/*
** PROTOTYPES
*/
/*
** gen_resources.c
*/
void		generate_resources(t_map *m);
void		add_resource(t_map *m, int n, int type);

/*
** map_create.c
*/
int		create_map(t_map *m);

/*
** map_delete.c
*/
void		delete_map(t_map *map);

/*
** map_methods.c
*/
int		get_real_coord(int max, int c);
int		map_is_set(t_map *m, int x, int y);
void		map_set(t_map *m, int x, int y);
void		map_unset_all(t_map *map);
t_square	*map_get_square(t_map *m, int x, int y);

#endif	/* __MAP_H__ */
