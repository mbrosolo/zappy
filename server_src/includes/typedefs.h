/*
** typedefs.h for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src/includes
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Jun 22 15:50:42 2012 matthias brosolo
** Last update Fri Jun 22 15:50:42 2012 matthias brosolo
*/

#ifndef	__TYPEDEFS_H__
# define	__TYPEDEFS_H__

/*
** TYPEDEFS
*/
typedef	struct	s_client	t_client;
typedef struct	s_server	t_server;
typedef	struct	timeval		t_timeval;
typedef struct	s_task		t_task;
/**
 * @brief Typedef of a pointer on function for assignation to fill t_server structure
 */
typedef void (*t_ptr_arg)(char const * const, t_server *);

#endif
