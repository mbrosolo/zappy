/*
** prototypes.h for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src/includes
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Fri Jun 22 15:49:46 2012 matthias brosolo
** Last update Sun Jul 15 19:30:19 2012 anna texier
*/

#ifndef	__PROTOTYPES_H__
# define	__PROTOTYPES_H__

/*
** PROTOTYPES
*/
/*
** arg_ptr_opt.c
*/
int	get_option(int i, char **av, t_server *s);

/*
** arg_ptr_opt.c
*/
void	set_port(char const * const param, t_server *s);
void	set_width(char const * const param, t_server *s);
void	set_height(char const * const param, t_server *s);
void	set_client_team(char const * const param, t_server *s);
void	set_delay(char const * const param, t_server *s);

/*
** args.c
*/
int	parse_args(int ac, char **av, t_server *s);
void	init_args(t_server *s);

/*
** buffer_utils.c
*/
int	add_eob(char *buffer, int type);
void	sub_eob(char *buffer,  int type);
int	end_of_buffer(char current, char previous, int type);
int	extract_from_buffer(t_com_buffer *b, int type);
int	push_in_buffer(t_com_buffer *c, char *buffer, size_t size, int type);

/*
** clients.c
*/
int	add_client(t_server *s);
void	free_client(void *data);
int	client_timeout(t_server *s, t_client *c);

/*
** client_delete.c
*/
void	attempting_client(t_server *srv, t_client *client);
void	delete_client(t_server *srv, t_client *client, t_list_iterator *it);
void	unlink_player(t_server *srv, t_client *client);

/*
** client_process.c
*/
int	first_response(t_server *s, t_client *c);
int	process_request(t_server *s, t_client *c);
int	process_clients(t_server *srv);

/*
** client_send.c
*/
int	send_to_client(t_client *c, char *buffer, size_t size, fd_set *fds);
int	send_to_all_graphic_client(t_server *s, char *buffer, size_t size,
				   fd_set *fds);

/*
** com.c
*/
int	init_com_buffer(t_com_buffer *buffer);
int	init_com(t_com *com, t_connect *sock);

/*
** end_game.c
*/
void	end_game(t_server *s);
void	check_end_game(t_server *s);

/*
** free_server.c
*/
void	free_server(t_server *s);

/*
** graphic_client.c
*/
int	add_graphic_client(t_server *s, t_client *c);

/*
** graphic_egg.c
*/
int	graphic_egg_hatching(t_server *s, t_player *e);
int	graphic_egg_become_player(t_server *s, t_player *e);
int	graphic_egg_died(t_server *s, t_player *e);

/*
** graphic_map.c
*/
int	graphic_map_size(t_server *s, t_client *c);
int	graphic_map_content(t_server *s, t_client *c);
int	graphic_map_content_to_all_client(t_server *s);
int	send_square_content(t_server *s, t_client *c, t_square *sq);
int	send_square_content_to_all_client(t_server *s, t_square *sq);
int	graphic_square_content_request(t_server *s, t_client *c);

/*
** graphic_message.c
*/
int	graphic_server_end_game(t_server *s, char *team);
int	graphic_server_message(t_server *s, char *msg);
int	graphic_server_unknown_command(t_server *s, t_client *c);
int	graphic_server_bad_parameter(t_server *s, t_client *c);

/*
** graphic_player_actions.c
*/
int	graphic_player_drop(t_server *s, t_player *p, int resource);
int	graphic_player_get(t_server *s, t_player *p, int resource);
int	graphic_player_dead(t_server *s, t_player *p);
int	graphic_player_expulse(t_server *s, t_player *p);

/*
** graphic_player_info.c
*/
int	get_graphic_direction(int direction);
size_t	extract_number_from_str(char *buffer, int eob);
int	graphic_player_position(t_server *s, t_client *c, t_player *p);
int	graphic_player_position_to_all_client(t_server *s, t_player *p);
int	graphic_player_level(t_server *s, t_client *c, t_player *p);
int	graphic_player_level_to_all_client(t_server *s, t_player *p);
int	graphic_player_inventory(t_server *s, t_client *c, t_player *p);
int	graphic_player_inventory_to_all_client(t_server *s, t_player *p);

/*
** graphic_player_info_request.c
*/
int	send_all_new_player(t_server *s, t_client *c);
int	send_all_new_egg(t_server *s, t_client *c);
int	graphic_player_position_request(t_server *s, t_client *c);
int	graphic_player_level_request(t_server *s, t_client *c);
int	graphic_player_inventory_request(t_server *s, t_client *c);

/*
** graphic_player_skills.c
*/
int	graphic_player_broadcast(t_server *s, t_player *p, char *msg);
int	graphic_player_incantation(t_server *s, t_player *p);
int	graphic_player_incantation_end(t_server *s, t_player *p, int result);
int	graphic_player_fork(t_server *s, t_player *p);

/*
** graphic_player_std.c
*/
int	graphic_player_new(t_server *s, t_client *c, t_player *e);
int	graphic_player_new_to_all_client(t_server *s, t_player *p);
int	graphic_egg_new(t_server *s, t_client *c, t_player *e);
int	graphic_egg_new_to_all_client(t_server *s, t_player *e);

/*
** graphic_teams.c
*/
int	graphic_team_names(t_server *s, t_client *c);

/*
** graphic_time.c
*/
int	graphic_server_get_time(t_server *s, t_client *c);
int	graphic_server_set_time(t_server *s, t_client *c);

/*
** init_server.c
*/
int	init_server(t_server *s, int ac, char **av);
void	init_client_command(t_cmd *cmd);
void	init_resources_names(char (*names)[SIZE_NAME_RESOURCE + 1]);
void	init_task_delay(t_timeval *task_delay, int delay);

/*
** player_action.c
*/
int	player_see(t_server *s, t_client *c);
int	player_take(t_server *s, t_client *c);
int	player_put_down(t_server *s, t_client *c);
int	player_eject(t_server *s, t_client *c);
void	coord_by_directions(t_2d *coord, int x, int y, int direction);

/*
** player_broadcast.c
*/
int	first_check_around(t_server *s, t_move *mv, t_square *sq);
int	check_around_square(t_server *s, t_square *sq);
int	broadcast_map_around(t_server *s, t_move *mv, int nb, size_t *nb_total);
int	first_player_broadcast(t_server *s, t_move *mv, t_client *c);
void	init_player_broadcast(size_t id, int *nb, size_t *nb_total, int *pass);
/*
** player_dead.c
*/
int	player_dead(t_server *s, t_player *player);

/*
** player_direction.c
*/
int	first_get_direction(t_server *s, t_square *sq, t_move *m, t_player *p);
int	get_direction_around(t_server *s, t_square *sq, t_move *mv, int *direction);
int	get_direction(t_server *s, t_square *sq, t_player *p);

/*
** player_error.c
*/
int	player_unknown_command(t_server *s, t_client *c);

/*
** player_hatching.c
*/
int	player_hatching(t_server *s, t_player *p);

/*
** player_incantation.c
*/
void	set_square_players_level(t_list *players, char level);
int	check_players_level(t_list *players, char level);
int	check_incantation_square(t_square *sq, char *require, char level);
int	player_incantation_end(t_server *s, t_client *c);

/*
** player_message.c
*/
int	get_message(char *read, char *buffer, int eob);
int	send_to_all_client(t_server *s, t_square *players_sq,
			   t_square *ref_sq);
int	send_to_square_client(t_square *sq, char *buffer, size_t size,
			      fd_set *fds);

/*
** player_move.c
*/
int	player_advance(t_server *s, t_client *c);
int	player_turn_right(t_server *s, t_client *c);
int	player_turn_left(t_server *s, t_client *c);

/*
** player_skills.c
*/
int	player_inventory(t_server *s, t_client *c);
int	player_broadcast(t_server *s, t_client *c);
int	player_incantation(t_server *s, t_client *c);
int	player_fork(t_server *s, t_client *c);
int	player_connect_nbr(t_server *s, t_client *c);

/*
** players.c
*/
t_player	*create_new_player(t_server *s, int x, int y);
int		get_new_player(t_server *s, t_client *client);
void		delete_player(t_server *s, t_player *player);

/*
** predicate.c
*/
int	cmp_data(void *data, void *to_cmp);
int	cmp_timeval(t_task *data, t_timeval *to_cmp);
int	cmp_task_client(t_task *task, void *to_cmp);
int	cmp_team_name(t_team *team, char *name);
int	cmp_player_available(t_player *player, char *team_name);
int	cmp_player_by_id(t_player *player, size_t id);
/*
** read_from_clients.c
*/
int	get_eob(char *b, int size);
int	first_request(t_server *s, t_client *c);
int	check_command(t_cmd *cmd, char *current);
int	read_from_clients(t_server *s);

/*
** reinit.c
*/
void	reinit_map(t_map *map);
void	reinit_clients_list(t_list *list);
void	reinit_players_list(t_list *list);
void	reinit_players_lists_in_teams_list(t_list *list);

/*
** resources.c
*/
void	update_food(t_server *srv, t_task *task);
int	get_resource_id(char (*names)[SIZE_NAME_RESOURCE + 1], char *buffer);

/*
** server.c
*/
t_server	*get_server_ptr(t_server *s);
void	start_server(t_server *s);

/*
** signal.c
*/
int	set_signal_handler();

/*
** square.c
*/
int	write_content_square(t_server *s, t_client *c, int x, int y);

/*
** tasks.c
*/
void	set_timeval(t_timeval *to_set, long sec, long usec);
void	cpy_timeval(t_timeval *to_set, t_timeval *to_cpy);
int	add_task(t_server *s, t_client *client, int cmd);
void	delete_tasks_from_owner(t_server *srv, void *owner);
int	create_dead_task(t_server *s, t_player *player);
int	create_hatching_task(t_server *srv, t_player *egg);
int	create_timeout_task(t_server *srv, t_client *client);

/*
** team.c
*/
void	free_team(t_team *t);
int	create_team(t_server *s, char *team_name);
int	add_player_to_team(t_server *s, t_client *c, char *team_name);
void	sub_player_to_team(t_player *player);
int	add_player_to_team_by_cpy(t_server *s, t_team *team, t_player *player);
void	set_client_per_team(t_list *teams, int cpt);

/*
** time.c
*/
int	get_timeout(t_server *s);
void	update_task(t_server *s, t_list_iterator *it, t_timeval *diff_time);
int	update_tasks(t_server *s);
void	mult_timeval(t_timeval *tv, int factor);
void	sub_timeval(t_timeval *tv, long sec, long usec);
void	add_timeval(t_timeval *tv, t_timeval *to_add);

/*
** write_to_clients.c
*/
int	write_to_clients(t_server *s);

#endif
