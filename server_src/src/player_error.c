/*
** player_error.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Jun  1 09:51:28 2012 matthias brosolo
** Last update Fri Jun  1 09:51:28 2012 matthias brosolo
*/

#include	"server.h"

int	player_unknown_command(t_server *s, t_client *c)
{
  send_to_client(c, "ko", REAL_SIZE("ko"), &s->fds[F_WRITE]);
  return (0);
}
