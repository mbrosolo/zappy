/*
** graphic_player_info.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sat Jul 14 15:34:59 2012 matthias brosolo
** Last update Sat Jul 14 15:34:59 2012 matthias brosolo
*/

#include	"server.h"

int		graphic_player_position_request(t_server *s, t_client *c)
{
  size_t	id;
  t_player	*p;

  if (!(id = extract_number_from_str(c->com.read.current, c->eob)))
    graphic_server_bad_parameter(s, c);
  else
    {
      if (!(p = s->players_list.find(&s->players_list, (int (*)(void *, void*))
				     &cmp_player_by_id, (void *) id)))
	return (1);
      graphic_player_position(s, c, p);
    }
  return (0);
}

int		graphic_player_level_request(t_server *s, t_client *c)
{
  size_t	id;
  t_player	*p;

  if (!(id = extract_number_from_str(c->com.read.current, c->eob)))
    graphic_server_bad_parameter(s, c);
  else
    {
      if (!(p = s->players_list.find(&s->players_list, (int (*)(void *, void*))
				     &cmp_player_by_id, (void *) id)))
	return (1);
      graphic_player_level(s, c, p);
    }
  return (0);
}

int		graphic_player_inventory_request(t_server *s, t_client *c)
{
  size_t	id;
  t_player	*p;

  if (!(id = extract_number_from_str(c->com.read.current, c->eob)))
    graphic_server_bad_parameter(s, c);
  else
    {
      if (!(p = s->players_list.find(&s->players_list, (int (*)(void *, void*))
				     &cmp_player_by_id, (void *) id)))
	return (1);
      graphic_player_level(s, c, p);
    }
  return (0);
}
