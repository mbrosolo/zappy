/*
** graphic_egg.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 10:19:31 2012 anna texier
** Last update Sun Jul 15 19:19:11 2012 anna texier
*/

#include	"server.h"

int		graphic_egg_hatching(t_server *s, t_player *e)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "eht %lu", e->id);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_egg_become_player(t_server *s,
					  t_player *e)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "ebo %lu", e->id);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  graphic_player_new_to_all_client(s, e);
  return (0);
}

int		graphic_egg_died(t_server *s, t_player *e)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "edi %lu", e->id);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}
