/*
** free_server.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Jul 15 18:30:10 2012 matthias brosolo
** Last update Sun Jul 15 19:32:14 2012 anna texier
*/

#include	"server.h"

void		free_server(t_server *s)
{
  delete_map(&s->map);
  s->tmp_list.delete(&s->tmp_list, NULL);
  s->team_list.delete(&s->team_list, (void (*)(void *)) &free_team);
  s->task_list.delete(&s->task_list, &free);
  s->client_list.delete(&s->client_list, &free_client);
  s->players_list.delete(&s->players_list, &free);
  s->sock.destroy(&s->sock);
  free(s);
}
