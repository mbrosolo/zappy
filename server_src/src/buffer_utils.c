/*
** buffer_utils.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Mon Jun  4 13:00:49 2012 matthias brosolo
** Last update Mon Jun  4 13:00:49 2012 matthias brosolo
*/

#include	"server.h"

/**
 * @file   buffer_utils.c
 * @Author matthias brosolo
 * @date   June, 2012
 * @brief  Buffer utils like end of buffer
 */

/**
 * @brief Return if it is the end of buffer or not
 * @param buffer The string to add the eob
 * @param type The type of terminaison (combination) character(s)
 * @return The number of characters added
 */
int		add_eob(char *buffer, int type)
{
  if (type == EOB_LF)
    {
      strcat(buffer, "\n");
      return (1);
    }
  else if (type == EOB_CRLF)
    {
      strcat(buffer, "\r\n");
      return (2);
    }
  return (0);
}

/**
 * @brief Substract the end of buffer
 * @param buffer The string to substract eob
 * @param type The type of terminaison (combination) character(s)
 */
void		sub_eob(char *buffer,  int type)
{
  int		i;
  char		prev;

  i = -1;
  prev = 0;
  while (++i < BUFFER_SIZE)
    {
      if (end_of_buffer(buffer[i], prev, type))
	{
	  if (type == EOB_LF)
	    buffer[i] = '\0';
	  else if (type == EOB_CRLF && i)
	    {
	      buffer[i] = '\0';
	      buffer[i - 1] = '\0';
	    }
	  return ;
	}
      prev = buffer[i];
    }
}

/**
 * @brief Return if it is the end of buffer or not
 * @param current The current character get from the buffer
 * @param previous The previous character get from the buffer
 * @param type The type of terminaison (combination) character(s)
 * @return Flag for end of buffer true/false
 */
int		end_of_buffer(char current, char previous, int type)
{
  if (type == EOB_LF && current == '\n')
    return (1);
  if (type == EOB_CRLF && current == '\n' && previous == '\r')
    return (1);
  return (0);
}

/**
 * @brief Extract string from buffer
 * @param s Connection data containing ringbuffer
 * @param w The word string to extract
 * @return Size of the string
 */
int		extract_from_buffer(t_com_buffer *c, int type)
{
  char		prev;
  ssize_t	i;

  i = -1;
  prev = 0;
  memset(c->current, 0, BUFFER_SIZE + 1);
  while (++i < BUFFER_SIZE)
    {
      if (get_circ(&c->buffer, &c->current[i]) < 0)
	{
	  fprintf(stderr, "Can't add more char in buffer\n");
	  return (-1);
	}
      if (end_of_buffer(c->current[i], prev, type))
	{
	  --c->count;
	  return (i + 1);
	}
      prev = c->current[i];
    }
  return (i + 1);
}

/**
 * @brief Push string in buffer
 * @param c The client's communication data
 * @param s String to put in the ringbuffer
 * @param size The size of the string
 * @return Size or -1 in case of error
 */
int		push_in_buffer(t_com_buffer *c, char *buffer, size_t size, int type)
{
  char		prev;
  size_t	i;

  i = 0;
  prev = 0;
  while (i < size)
    {
      if (put_circ(&c->buffer, buffer[i]) < 0)
	{
	  fprintf(stderr, "Can't add more char in buffer\n");
	  return (i);
	}
      if (end_of_buffer(buffer[i], prev, type))
	++c->count;
      prev = buffer[i];
      ++i;
    }
  return (i);
}
