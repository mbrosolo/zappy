/*
** map_delete.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Mon Jun 25 12:27:50 2012 matthias brosolo
** Last update Mon Jun 25 12:27:50 2012 matthias brosolo
*/

#include	"server.h"

void		delete_map(t_map *map)
{
  int		x;
  int		y;
  t_square	*sq;

  y = -1;
  while (++y < map->height)
    {
      x = -1;
      while (++x < map->width)
	{
	  sq = map->get_square(map, x, y);
	  sq->players_list.delete(&sq->players_list, NULL);
	}
    }
  free(map->square_array);
}
