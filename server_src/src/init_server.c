/*
** init_server.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Fri Jun  1 09:06:02 2012 matthias brosolo
** Last update Sun Jul 15 22:59:46 2012 anna texier
*/

#include	"server.h"

int	init_server(t_server *s, int ac, char **av)
{
  init_args(s);
  if (create_list(&s->team_list) || parse_args(ac, av, s)
      || set_signal_handler()
      || connect_init(&s->sock, ALL_HOST, s->port)
      || s->sock.set_server(&s->sock, ZP_QUEUE_LENGTH)
      || create_list(&s->client_list)
      || create_list(&s->tmp_list)
      || create_list(&s->task_list)
      || create_list(&s->players_list))
    return (1);
  s->egg_id = 1;
  s->winner = NULL;
  s->player_id = 1;
  init_client_command(s->client_cmd);
  init_resources_names(s->resources_names);
  init_task_delay(s->task_delay, s->delay);
  FD_ZERO(&s->fds[F_READ]);
  FD_ZERO(&s->fds[F_WRITE]);
  return (0);
}

void	init_graphic_command(t_cmd *cmd)
{
  memset(cmd, 0, sizeof(*cmd) * NB_GRAPHIC_CMD);
  strcpy(cmd[GC_MAP_SIZE].name, "msz");
  cmd[GC_MAP_SIZE].fct = &graphic_map_size;
  strcpy(cmd[GC_SQUARE_CONTENT].name, "bct");
  cmd[GC_SQUARE_CONTENT].fct = &graphic_square_content_request;
  strcpy(cmd[GC_MAP_CONTENT].name, "mct");
  cmd[GC_MAP_CONTENT].fct = &graphic_map_content;
  strcpy(cmd[GC_TEAM_NAMES].name, "tna");
  cmd[GC_TEAM_NAMES].fct = &graphic_team_names;
  strcpy(cmd[GC_PLAYER_POSITION].name, "ppo");
  cmd[GC_PLAYER_POSITION].fct = &graphic_player_position_request;
  strcpy(cmd[GC_PLAYER_LEVEL].name, "plv");
  cmd[GC_PLAYER_LEVEL].fct = &graphic_player_level_request;
  strcpy(cmd[GC_PLAYER_INVENTORY].name, "pin");
  cmd[GC_PLAYER_INVENTORY].fct = &graphic_player_inventory_request;
  strcpy(cmd[GC_SERVER_GET_TIME].name, "sgt");
  cmd[GC_SERVER_GET_TIME].fct = &graphic_server_get_time;
  strcpy(cmd[GC_SERVER_SET_TIME].name, "sst");
  cmd[GC_SERVER_SET_TIME].fct = &graphic_server_set_time;
}

void	init_resources_names(char (*names)[SIZE_NAME_RESOURCE + 1])
{
  memset(names, 0, sizeof(names));
  strncpy(names[R_FOOD], "nourriture", SIZE_NAME_RESOURCE);
  strncpy(names[R_LINEMATE], "linemate", SIZE_NAME_RESOURCE);
  strncpy(names[R_DERAUMERE], "deraumere", SIZE_NAME_RESOURCE);
  strncpy(names[R_SIBUR], "sibur", SIZE_NAME_RESOURCE);
  strncpy(names[R_MENDIANE], "mendiane", SIZE_NAME_RESOURCE);
  strncpy(names[R_PHIRAS], "phiras", SIZE_NAME_RESOURCE);
  strncpy(names[R_THYSTAME], "thystame", SIZE_NAME_RESOURCE);
}
