/*
** player_action.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Jun  1 09:30:34 2012 matthias brosolo
** Last update Sun Jul 15 23:30:49 2012 anna texier
*/

#include	"server.h"

void	coord_by_directions(t_2d *coord, int x, int y, int direction)
{
  int	tmp;

  if (direction == UP)
    y *= -1;
  if (direction == RIGHT)
    {
      tmp = y;
      y = x;
      x = tmp;
    }
  if (direction == DOWN)
    x *= -1;
  if (direction == LEFT)
    {
      tmp = y;
      y = -x;
      x = -tmp;
    }
  coord->x += x;
  coord->y += y;
}

int	player_see(t_server *s, t_client *c)
{
  int	i;
  int	lvl;
  t_2d	coord;

  printf("Client %d requests vision in %d - %d\n", c->com.sock.fd,
	 c->player->x, c->player->y);
  push_in_buffer(&c->com.write, "{", REAL_SIZE("{"), c->eob);
  lvl = -1;
  while (++lvl <= c->player->level)
    {
      i = -1;
      coord.x = c->player->x;
      coord.y = c->player->y;
      coord_by_directions(&coord, -1 * lvl, 1 * lvl, c->player->direction);
      while (++i < (1 + lvl * 2))
	{
	  write_content_square(s, c, coord.x, coord.y);
	  coord_by_directions(&coord, 1, 0, c->player->direction);
	  if (lvl < c->player->level || i + 1 < (1 + lvl * 2))
	    push_in_buffer(&c->com.write, ",", REAL_SIZE(","), c->eob);
	}
    }
  send_to_client(c, "}", REAL_SIZE("}"), &s->fds[F_WRITE]);
  return (0);
}

int		player_take(t_server *s, t_client *c)
{
  int		resource;
  t_square	*sq;

  printf("Client %d requests take\n", c->com.sock.fd);
  sub_eob(c->com.read.current, c->eob);
  resource = get_resource_id(s->resources_names, c->com.read.current);
  sq = s->map.get_square(&s->map, c->player->x, c->player->y);
  if (resource != -1 && sq->items[resource])
    {
      --sq->items[resource];
      --s->map.items[resource];
      ++c->player->resource[resource];
      if (resource == R_FOOD)
	{
	  add_resource(&s->map, 1, R_FOOD);
	  add_timeval(&c->player->live->delay, &s->task_delay[DEAD]);
	}
      graphic_player_get(s, c->player, resource);
      send_to_client(c, "ok", REAL_SIZE("ok"), &s->fds[F_WRITE]);
    }
  else
    send_to_client(c, "ko", REAL_SIZE("ko"), &s->fds[F_WRITE]);
  return (0);
}

