/*
** client_send.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src/src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Mon Jun 25 09:27:08 2012 matthias brosolo
** Last update Mon Jun 25 09:27:08 2012 matthias brosolo
*/

#include	"server.h"

/**
 * @brief Add a string to pushed on a buffer
 * @param s The connection object
 * @param w The fd_set write
 * @param b The string to send
 */
int	send_to_client(t_client *c,
		       char *buffer,
		       size_t size,
		       fd_set *fds)
{
  char	tmp[3];

  memset(tmp, 0, 3);
  push_in_buffer(&c->com.write, buffer, size, c->eob);
  size = add_eob(tmp, c->eob);
  push_in_buffer(&c->com.write, tmp, size, c->eob);
  FD_SET(c->com.sock.fd, fds);
  return (0);
}

/**
 * @brief Add a string to pushed on a buffer
 * @param s The connection object
 * @param w The fd_set write
 * @param b The string to send
 */
int			send_to_all_graphic_client(t_server *s,
						   char *buffer,
						   size_t size,
						   fd_set *fds)
{
  t_client		*c;
  t_list_iterator	it;

  s->client_list.begin(&s->client_list, &it);
  while (it.current(&it) != s->client_list.end(&s->client_list))
    {
      c = it.data;
      if (c->state == ST_GRAPHIC)
	send_to_client(c, buffer, size, fds);
      it.next(&it);
    }
  return (0);
}
