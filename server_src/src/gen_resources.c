/*
** generate_resources.c for Zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sat Jul 14 13:23:12 2012 anna texier
** Last update Sun Jul 15 22:21:08 2012 anna texier
*/

#include	"map.h"

void		generate_resources(t_map *m)
{
  memset(m->items, '\0', R_NB_RESOURCE);
  m->refs[R_FOOD] = 0;
  m->refs[R_LINEMATE] = 18 + 18 * m->width * m->height / 120;
  m->refs[R_DERAUMERE] = 16 + 16 * m->width * m->height / 120;
  m->refs[R_SIBUR] = 20 + 20 * m->width * m->height / 120;
  m->refs[R_MENDIANE] = 10 + 10 * m->width * m->height / 120;
  m->refs[R_PHIRAS] = 12 + 12 * m->width * m->height / 120;
  m->refs[R_THYSTAME] = 2 + 2 * m->width * m->height / 120;
  add_resource(m, m->refs[R_LINEMATE], R_LINEMATE);
  add_resource(m, m->refs[R_DERAUMERE], R_DERAUMERE);
  add_resource(m, m->refs[R_SIBUR], R_SIBUR);
  add_resource(m, m->refs[R_MENDIANE], R_MENDIANE);
  add_resource(m, m->refs[R_PHIRAS], R_PHIRAS);
  add_resource(m, m->refs[R_THYSTAME], R_THYSTAME);
}

void		add_resource(t_map *m, int n, int type)
{
  t_square	*s;
  int		x;
  int		y;
  int		i;

  i = -1;
  while (++i < n)
    {
      x = rand() % m->width;
      y = rand() % m->height;
      if ((s = m->get_square(m, x, y)))
	{
	  ++s->items[type];
	  ++m->items[type];
	}
    }
}

void		update_resources(t_map *m)
{
  int		i;

  i = R_FOOD - 1;
  while (++i < R_NB_RESOURCE)
    {
      if (m->items[i] <= (m->refs[i] / 4))
	add_resource(m, m->refs[i] - m->items[i], i);
    }
}
