/*
** server.c for zappy in /home/gressi_b/zappy/server_src
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Fri May 25 16:31:08 2012 gressi_b
** Last update Sun Jul 15 23:03:26 2012 benjamin bourdin
*/

#include	<errno.h>
#include	"server.h"

/**
 * @file   server.c
 * @Author benjamin gressier
 * @date   May, 2012
 * @brief  Core server
 */

t_server		*get_server_ptr(t_server *s)
{
  static void		*server = NULL;

  if (!server)
    server = s;
  return (server);
}

/**
 * @brief Set the socket fd on the read fds for select
 * @param s Serve data
 * @param fdmax the maximum fd argument for select
 */
void			set_read_fds(t_server *srv, int *fdmax)
{
  t_client		*client;
  t_list_iterator	it;

  *fdmax = srv->sock.fd;
  FD_ZERO(&srv->fds[F_READ]);
  FD_SET(srv->sock.fd, &srv->fds[F_READ]);
  srv->client_list.begin(&srv->client_list, &it);
  while (it.current(&it) != srv->client_list.end(&srv->client_list))
    {
      client = (t_client *) it.data;
      if (client->state != ST_TO_REMOVE)
	FD_SET(client->com.sock.fd, &srv->fds[F_READ]);
      *fdmax = MAX(*fdmax, client->com.sock.fd);
      it.next(&it);
    }
}

/**
 * @brief Process the server
 * @param s Server data
 */
void			start_server(t_server *s)
{
  int			fd_max;

  while (1)
    {
      if (s->winner)
	end_game(s);
      set_read_fds(s, &fd_max);
      get_timeout(s);
      if (select(fd_max + 1, &s->fds[F_READ], &s->fds[F_WRITE], NULL,
		 s->time_ref.tv_sec == -1 ? NULL : &s->time_remaining)
	  == -1 && errno == ENOMEM)
	perror("select(2)");
      else
	{
	  write_to_clients(s);
	  update_tasks(s);
	  if (FD_ISSET(s->sock.fd, &s->fds[F_READ]))
	    add_client(s);
	  read_from_clients(s);
	  process_clients(s);
	}
    }
}
