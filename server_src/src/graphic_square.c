/*
** graphic_square.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 22:51:54 2012 anna texier
** Last update Sun Jul 15 22:53:53 2012 anna texier
*/

#include	"server.h"

int		extract_coord(char *buffer, t_2d *coord, int eob)
{
  char		*tmp;

  sub_eob(buffer, eob);
  if (!strtok(buffer, " ") || !(tmp = strtok(NULL, " ")))
    return (1);
  coord->x = atoi(tmp);
  if (!(tmp = strtok(NULL, " ")))
    return (1);
  coord->y = atoi(tmp);
  return (0);
}

int		send_square_content(t_server *s, t_client *c, t_square *sq)
{
  char		buffer[256];
  ssize_t	size;

  size = snprintf(buffer, 252, "bct %d %d %d %d %d %d %d %d %d", sq->x, sq->y,
		  sq->items[R_FOOD],
		  sq->items[R_LINEMATE], sq->items[R_DERAUMERE],
		  sq->items[R_SIBUR], sq->items[R_MENDIANE],
		  sq->items[R_PHIRAS], sq->items[R_THYSTAME]);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		send_square_content_to_all_client(t_server *s, t_square *sq)
{
  char		buffer[256];
  ssize_t	size;

  size = snprintf(buffer, 252, "bct %d %d %d %d %d %d %d %d %d", sq->x, sq->y,
		  sq->items[R_FOOD],
		  sq->items[R_LINEMATE], sq->items[R_DERAUMERE],
		  sq->items[R_SIBUR], sq->items[R_MENDIANE],
		  sq->items[R_PHIRAS], sq->items[R_THYSTAME]);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_square_content_request(t_server *s, t_client *c)
{
  t_2d		coord;

  if (extract_coord(c->com.read.current, &coord, c->eob))
    graphic_server_bad_parameter(s, c);
  else
    send_square_content(s, c, s->map.get_square(&s->map, coord.x, coord.y));
  return (0);
}
