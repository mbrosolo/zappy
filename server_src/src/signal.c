/*
** signal.c for zappy in /home/gressi_b/zappy/server_src
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Tue May 15 17:02:42 2012 gressi_b
** Last update Tue May 15 17:02:42 2012 gressi_b
*/

#include <stdio.h>
#include <signal.h>
#include "server.h"

static sighandler_t	wsignal(int signum, sighandler_t handler)
{
  sighandler_t		old;

  old = signal(signum, handler);
  if (old == SIG_ERR)
    perror("signal(2)");
  return (old);
}

static void	catch_h(int signum)
{
  (void) signum;
  fprintf(stderr, "Connection with peer ended\n");
  wsignal(signum, &catch_h);
}

/*
** Handler for signal which have to be catched then raised
** in order to stop the server properly
*/
static void	catch_and_raise_h(int signum)
{
  free_server(get_server_ptr(NULL));
  wsignal(signum, SIG_DFL);
  raise(signum);
}

/*
** The same than catch_and_raise_h, except it is not used for exiting server
*/
int	set_signal_handler()
{
  int	b;

  b = 0;
  b += CHECK_SIG(wsignal(SIGPIPE, &catch_h));
  b += CHECK_SIG(wsignal(SIGINT, &catch_and_raise_h));
  b += CHECK_SIG(wsignal(SIGQUIT, &catch_and_raise_h));
  return (b);
}
