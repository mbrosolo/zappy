/*
** timout.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 23:26:54 2012 anna texier
** Last update Sun Jul 15 23:27:25 2012 anna texier
*/

#include	"server.h"

/**
 * @brief Set the timevalue
 * @param 1: timeval structure, 2: seconds, 3: useconds
 */
void		set_timeval(t_timeval *to_set, long sec, long usec)
{
  to_set->tv_sec = sec;
  to_set->tv_usec = usec;
}

/**
 * @brief Copy timevalue
 * @param 1: timevalue structure, 2: copy timevalue
 */
void		cpy_timeval(t_timeval *to_set, t_timeval *to_cpy)
{
  to_set->tv_sec = to_cpy->tv_sec;
  to_set->tv_usec = to_cpy->tv_usec;
}

int		create_timeout_task(t_server *srv, t_client *client)
{
  t_task	*task;

  if (!(task = wmalloc(sizeof(*task))))
    return (1);
  task->id = CLIENT_TIMEOUT;
  cpy_timeval(&task->delay, &srv->task_delay[CLIENT_TIMEOUT]);
  if (!srv->task_list.insert_pred(&srv->task_list, task,
  				(int (*)(void *, void *)) &cmp_timeval,
  				&task->delay))
    return (1);
  task->owner = client;
  task->owner_type = OT_CLIENT;
  client->task = task;
  return (0);
}
