/*
** client_process.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed Jul  4 11:40:04 2012 matthias brosolo
** Last update Wed Jul  4 11:40:04 2012 matthias brosolo
*/

#include	"server.h"

/**
 * @brief Process request
 * @param s The server's data object
 * @param c The client's data object
 * @return Flag true or false
 */
int			first_response(t_server *s, t_client *c)
{
  if (extract_from_buffer(&c->com.read, c->eob) == -1)
    return (1);
  sub_eob(c->com.read.current, c->eob);
  if (!strcmp(c->com.read.current, "GRAPHIC"))
    add_graphic_client(s, c);
  else
    {
      if (add_player_to_team(s, c, c->com.read.current))
	return (1);
      if (create_dead_task(s, c->player))
	return (1);
      c->com.write.size = snprintf(c->com.write.current, 12, "%d",
				   c->player->team->connect_nbr);
      send_to_client(c, c->com.write.current, c->com.write.size,
		     &s->fds[F_WRITE]);
      c->com.write.size = snprintf(c->com.write.current, 24, "%d %d",
				   s->map.width, s->map.height);
      send_to_client(c, c->com.write.current, c->com.write.size,
		     &s->fds[F_WRITE]);
      c->state = ST_CONNECTED;
    }
  s->task_list.remove(&s->task_list, c->task, &free);
  c->task = NULL;
  return (0);
}

/**
 * @brief Process request
 * @param s The server's data object
 * @param c The client's data object
 * @return Flag true or false
 */
int			process_request(t_server *s, t_client *c)
{
  int			cmd;

  if (extract_from_buffer(&c->com.read, c->eob) == -1)
    return (1);
  cmd = check_command(s->client_cmd, c->com.read.current);
  if (cmd == FORK)
    graphic_player_fork(s, c->player);
  if (cmd == UNKNOWN_COMMAND || cmd == CONNECT_NBR || cmd == INCANTATION)
    s->client_cmd[cmd].fct(s, c);
  else
    add_task(s, c, cmd);
  return (0);
}

/**
 * @brief Process clients' requests
 * @param s The server's data object
 * @return Flag true or false
 */
int			process_clients(t_server *srv)
{
  int			ret;
  t_client		*client;
  t_list_iterator	it;

  srv->client_list.begin(&srv->client_list, &it);
  while (it.current(&it) != srv->client_list.end(&srv->client_list))
    {
      client = (t_client *) it.data;
      if (client->state == ST_TO_REMOVE && !client->com.write.count)
	delete_client(srv, client, &it);
      else if (client->state != ST_TO_REMOVE && client->com.read.count
	       && (!client->task
		   || (client->task && client->task->id == CLIENT_TIMEOUT))
	       && (ret = (client->state == ST_CONNECTED
			  ? process_request(srv, client)
			  : first_response(srv, client))))
	attempting_client(srv, client);
      it.next(&it);
    }
  return (0);
}
