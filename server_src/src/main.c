/*
** main.c for zappy in /home/gressi_b/Epitech/B4/zappy/server_src
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Wed May  9 16:26:58 2012 gressi_b
** Last update Sun Jul 15 19:27:14 2012 anna texier
*/

#include	<unistd.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<time.h>
#include	"server.h"

/**
 * @file   main.c
 * @Author benjamin gressier
 * @date   May, 2012
 * @brief  The magical main function
 */

/**
 * @brief delete the server properly
 * @param 1: server structure
 */
static void	del_server(t_server *s)
{
  s->sock.destroy(&s->sock);
  s->team_list.delete(&s->team_list, (void (*)(void *))&free_team);
}

/**
 * @brief prints the usage
 * @param 1: name
 */
static int	usage(char const *name)
{
  fprintf(stderr, USAGE_STR, name);
  return (1);
}

/**
 * @brief The start of everything in the magical zappy world
 * @param 1: number of arguments, 2: array of arguments
 */
int		main(int ac, char **av)
{
  t_server	*s;

  if (ac == 1)
    return (usage(av[0]));
  if (!(s = wmalloc(sizeof(*s))) || init_server(s, ac, av))
    return (EXIT_FAILURE);
  srand(time(0) * getpid());
  if (create_map(&s->map))
    return (1);
  get_server_ptr(s);
  start_server(s);
  del_server(s);
  return (EXIT_SUCCESS);
}
