/*
** player_hatching.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sat Jul 14 00:41:05 2012 matthias brosolo
** Last update Sat Jul 14 00:41:05 2012 matthias brosolo
*/

#include	"server.h"

int		player_hatching(t_server *s, t_player *p)
{
  (void) s;
  ++p->team->connect_nbr;
  p->status = PS_WAITING;
  p->live = NULL;
  graphic_egg_hatching(s, p);
  return (0);
}
