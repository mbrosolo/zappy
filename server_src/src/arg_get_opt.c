/*
** arg_get_opt.c for  in /home/bourdi_b/zappy/server_src
**
** Made by benjamin bourdin
** Login   <bourdi_b@epitech.net>
**
** Started on  Sun Jul 15 19:59:25 2012 benjamin bourdin
** Last update Sun Jul 15 20:07:07 2012 benjamin bourdin
*/

#include "server.h"

/**
 * @brief Get the type of an option
 * @param c The character corresponding to the option
 * @param list_option The string which contains all option possibles.
 * @return The option type (enum) if the character c match in list_option,
 * otherwise INVALID_PARAM
 */
enum e_option_param	get_type_option(const char c,
					char const * const list_option)
{
  int			i;

  i = 0;
  while (list_option[i])
    {
      if (list_option[i] == c)
	return ((enum e_option_param)i);
      i++;
    }
  return (INVALID_PARAM);
}

int		get_option(int i, char **av, t_server *s)
{
  int		option_type;
  char		c;
  t_ptr_arg	opt[NB_PARAM];

  opt[P_PARAM] = &set_port;
  opt[X_PARAM] = &set_width;
  opt[Y_PARAM] = &set_height;
  opt[C_PARAM] = &set_client_team;
  opt[T_PARAM] = &set_delay;  c = av[i][1];
  if (av[i + 1] == NULL || IS_OPT(av[i + 1]))
    return (fprintf(stderr, S_OPARGREQ, av[0], c));
  option_type = get_type_option(c, LIST_OPTION);
  if (option_type != INVALID_PARAM && option_type < NB_PARAM)
    opt[option_type](av[i + 1], s);
  else if (option_type == N_PARAM)
    {
      ++i;
      while (av[i] && !IS_OPT(av[i]))
  	if (create_team(s, av[i++]))
	  return (ret_error("Can't add two team with the same name"));
    }
  else
    return (1);
  return (0);
}
