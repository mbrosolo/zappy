/*
** player_move.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Fri Jun  1 09:28:50 2012 matthias brosolo
** Last update Tue Jun 12 16:05:24 2012 benjamin bourdin
*/

#include	"server.h"

int		player_advance(t_server *s, t_client *c)
{
  t_square	*sq;

  printf("Client %d requests advance\n", c->com.sock.fd);
  sq = s->map.get_square(&s->map, c->player->x, c->player->y);
  sq->players_list.remove(&sq->players_list, c->player, NULL);
  if (c->player->direction == UP)
    c->player->y = (c->player->y > 0 ? c->player->y : s->map.height) - 1;
  if (c->player->direction == RIGHT)
    c->player->x = (c->player->x < s->map.width - 1 ? c->player->x + 1 : 0);
  if (c->player->direction == DOWN)
    c->player->y = (c->player->y < s->map.height - 1 ? c->player->y + 1 : 0);
  if (c->player->direction == LEFT)
    c->player->x = (c->player->x > 0 ? c->player->x - 1 : s->map.width - 1);
  sq = s->map.get_square(&s->map, c->player->x, c->player->y);
  if (!sq->players_list.push_back(&sq->players_list, c->player))
    return (1);
  graphic_player_position_to_all_client(s, c->player);
  send_to_client(c, "ok", REAL_SIZE("ok"), &s->fds[F_WRITE]);
  return (0);
}

int	player_turn_right(t_server *s, t_client *c)
{
  printf("Client %d requests right\n", c->com.sock.fd);
  c->player->direction = (c->player->direction < LEFT
			  ? c->player->direction + 1 : 0);
  graphic_player_position_to_all_client(s, c->player);
  send_to_client(c, "ok", REAL_SIZE("ok"), &s->fds[F_WRITE]);
  return (0);
}

int	player_turn_left(t_server *s, t_client *c)
{
  printf("Client %d requests left\n", c->com.sock.fd);
  c->player->direction = (c->player->direction > UP
			  ? c->player->direction - 1 : LEFT);
  graphic_player_position_to_all_client(s, c->player);
  send_to_client(c, "ok", REAL_SIZE("ok"), &s->fds[F_WRITE]);
  return (0);
}
