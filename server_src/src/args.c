/*
** args.c for zappy in /home/gressi_b/Epitech/B4/zappy/server_src
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Mon May 21 11:57:55 2012 gressi_b
** Last update Sun Jul 15 20:22:58 2012 benjamin bourdin
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "server.h"

static int	check_args(char const *progname, t_server *s)
{
  (void) progname;
  if (s->map.width < MIN_WIDTH || s->map.height < MIN_HEIGHT)
    return (ret_error(E_MAP_TOO_SMALL));
  if (s->map.width > MAX_WIDTH || s->map.height > MAX_HEIGHT)
    return (ret_error(E_MAP_TOO_BIG));
  if (s->delay < DELAY_MIN || s->delay > DELAY_MAX)
    return (ret_error(E_BAD_DELAY));
  if (s->client_per_team < PLAYER_NBR_MIN)
    return (ret_error(E_BAD_PLAYER_NBR));
  if (s->port < PORT_MIN)
    return (ret_error(E_PORT_OUT_OF_RANGE));
  if (s->team_list.size(&s->team_list) < TEAM_NBR_MIN)
    return (ret_error(E_NOT_ENOUGH_TEAM));
  if (s->client_per_team < MIN_CLIENT)
    return (ret_error(E_NOT_ENOUGH_TEAM));
  return (0);
}

static int	is_in(char const *s, char c)
{
  while (*s)
    {
      if (c == *s)
	return (1);
      ++s;
    }
  return (0);
}

int		parse_args(int ac, char **av, t_server *s)
{
  int		i;

  i = 1;
  while (i < ac)
    {
      if (IS_OPT(av[i]))
	{
	  if (!is_in(OP_LIST, av[i][1]))
	    {
	      fprintf(stderr, S_OPINVALID, av[0], av[i][1]);
	      return (1);
	    }
	  if (get_option(i, av, s))
	    return (1);
	}
      ++i;
    }
  set_client_per_team(&s->team_list, s->client_per_team);
  printf("port:\t\t\t(%d)\n", s->port);
  printf("width:\t\t\t(%d)\n", s->map.width);
  printf("height:\t\t\t(%d)\n", s->map.height);
  printf("client_per_team:\t(%d)\n", s->client_per_team);
  printf("delay:\t\t\t(%d)\n", s->delay);
  return (check_args(av[0], s));
}

void	init_args(t_server *s)
{
  s->port = DEFAULT_PORT;
  s->map.width = DEFAULT_WIDTH;
  s->map.height = DEFAULT_HEIGHT;
  s->client_per_team = DEFAULT_CLIENT;
  s->delay = DEFAULT_DELAY;
}
