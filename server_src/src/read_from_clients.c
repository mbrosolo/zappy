/*
** read_from_clients.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Thu May 31 21:56:43 2012 matthias brosolo
** Last update Tue Jun 12 17:43:30 2012 benjamin bourdin
*/

#include	"server.h"

/**
 * @file   read_from_clients.c
 * @Author matthias brosolo
 * @date   May, 2012
 * @brief  Read and process clients' requests
 */

/**
 * @brief Get the type of eob
 * @param b The buffer containing characters
 * @param size The size read
 */
int			get_eob(char *b, int size)
{
  if (b[size - 1] == '\n')
    {
      if (size > 1 && b[size - 2] == '\r')
	return (EOB_CRLF);
      return (EOB_LF);
    }
  return (-1);
}

/**
 * @brief First read from client
 * @param s Connection data containing ringbuffer
 * @param w The word string to extract
 * @return Error case
 */
int			first_request(t_server *s, t_client *c)
{
  int			ret;
  t_connect		*sock;
  t_com_buffer		*read;

  (void) s;
  sock = &c->com.sock;
  read = &c->com.read;
  memset(read->current, 0, BUFFER_SIZE + 1);
  if ((read->size = sock->recv(sock, read->current, BUFFER_SIZE)) <= 0
      || (read->size == 1 && read->current[1] == END_OF_TRANSMISSION))
    return (1);
  write(1, "Received \"", REAL_SIZE("Received \""));
  write(1, read->current, read->size - (c->eob + 1));
  printf("\" from client %lu size %ld eob %d\n", c->player
	 ? c->player->id : 0, read->size, c->eob);
  if ((ret = get_eob(read->current, read->size)) == -1)
      return (ret_error("Bad end of buffer."));
  c->eob = ret;
  push_in_buffer(read, read->current, read->size, c->eob);
  return (0);
}

/**
 * @brief Check if a command exist and return the id associated
 * @param s Server data
 * @param w The command to check
 */
int			check_command(t_cmd *cmd, char *current)
{
  int			i;

  i = -1;
  while (++i < UNKNOWN_COMMAND)
    {
      if (!strncmp(cmd[i].name, current, strlen(cmd[i].name)))
	return (i);
    }
  return (i);
}

/**
 * @brief Read on a socket client and the request
 * @param s Server data
 * @param c The client to read
 */
int			read_client(t_server *s, t_client *c)
{
  memset(c->com.read.current, 0, sizeof(c->com.read.current));
  if (c->state == ST_CONNECTING)
    return (first_request(s, c));
  if ((c->com.read.size = c->com.sock.recv(&c->com.sock, c->com.read.current,
					   BUFFER_SIZE)) <= 0)
    return (1);
  write(1, "Received \"", REAL_SIZE("Received \""));
  write(1, c->com.read.current, c->com.read.size - (c->eob + 1));
  printf("\" from client %lu\n", c->player ? c->player->id : 0);
  if (c->com.read.count < 10)
    push_in_buffer(&c->com.read, c->com.read.current, c->com.read.size,
		   c->eob);
  return (0);
}

/**
 * @brief Check if a socket is ready on reading
 * @param s Server data
 */
int			read_from_clients(t_server *s)
{
  t_client		*c;
  t_list_iterator	it;

  s->client_list.begin(&s->client_list, &it);
  while (it.current(&it) != s->client_list.end(&s->client_list))
    {
      c = it.data;
      if (FD_ISSET(c->com.sock.fd, &s->fds[F_READ])
	  && read_client(s, c))
	attempting_client(s, c);
      it.next(&it);
    }
  return (0);
}
