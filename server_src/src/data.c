/*
** data.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 23:28:36 2012 anna texier
** Last update Sun Jul 15 23:28:50 2012 anna texier
*/

#include	"server.h"

int		cmp_data(void *data, void *to_cmp)
{
  if (data == to_cmp)
    return (1);
  return (0);
}

int		cmp_timeval(t_task *task, t_timeval *to_cmp)
{
  if (task->delay.tv_sec > to_cmp->tv_sec
      || (task->delay.tv_sec == to_cmp->tv_sec
	  && task->delay.tv_usec > to_cmp->tv_usec))
    return (1);
  return (0);
}

int		cmp_task_client(t_task *task, void *to_cmp)
{
  if (task->owner == to_cmp)
    return (1);
  return (0);
}
