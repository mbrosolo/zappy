/*
** player_message.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Tue Jul  3 15:58:19 2012 matthias brosolo
** Last update Sun Jul 15 22:41:07 2012 anna texier
*/

#include	"server.h"

int			get_message(char *read, char *buffer, int eob)
{
  char		*tmp;

  sub_eob(read, eob);
  memset(buffer, 0, BUFFER_SIZE + 1);
  if (!strtok(read, " ") || !(tmp = strtok(NULL, "")))
    return (1);
  strncpy(buffer, tmp, BUFFER_SIZE - 4);
  return (0);
}

int			send_message_to_client(t_server *s, t_square *ref_sq,
					       t_list_iterator *it)
{
  char			tmp[BUFFER_SIZE + 1];
  ssize_t		size;
  t_player		*p;

  p = it->data;
  if (!s->tmp_list.find(&s->tmp_list, &cmp_data, p))
    {
      if (p->status == PS_LIVING && p->client)
	{
	  memset(tmp, 0, BUFFER_SIZE + 1);
	  size = snprintf(tmp, BUFFER_SIZE - 2, "message %d,%s",
			  get_direction(s, ref_sq, p), s->buffer);
	  send_to_client(p->client, tmp, size, &s->fds[F_WRITE]);
	}
      if (!s->tmp_list.push_back(&s->tmp_list, p))
	return (1);
    }
  return (0);
}

int			send_to_all_client(t_server *s, t_square *players_sq,
					   t_square *ref_sq)
{
  t_list_iterator	it;

  if (players_sq->players_list.size(&players_sq->players_list))
    {
      players_sq->players_list.begin(&players_sq->players_list, &it);
      while (it.current(&it)
	     != players_sq->players_list.end(&players_sq->players_list))
	{
	  if (send_message_to_client(s, ref_sq, &it))
	    return (1);
	  it.next(&it);
	}
    }
  return (0);
}

int			send_to_square_client(t_square *sq, char *buffer,
					      size_t size, fd_set *fds)
{
  t_player		*p;
  t_list_iterator	it;

  sq->players_list.begin(&sq->players_list, &it);
  while (it.current(&it) != sq->players_list.end(&sq->players_list))
    {
      p = it.data;
      if (p->status == PS_LIVING && p->client)
	send_to_client(p->client, buffer, size, fds);
      it.next(&it);
    }
  return (0);
}
