/*
** square.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Mon Jun 25 17:31:46 2012 matthias brosolo
** Last update Mon Jun 25 17:31:46 2012 matthias brosolo
*/

#include	"server.h"

int		write_content_square(t_server *s, t_client *c, int x, int y)
{
  int		i;
  t_square	*sq;
  unsigned int	j;

  if (!(sq = s->map.get_square(&s->map, x, y)))
    return (1);
  j = 0;
  while (j++ < sq->players_list.size(&sq->players_list))
    push_in_buffer(&c->com.write, " joueur", REAL_SIZE(" joueur"), c->eob);
  i = -1;
  while (++i < R_NB_RESOURCE)
    {
      j = 0;
      while (j++ < sq->items[i])
	{
	  push_in_buffer(&c->com.write, " ", REAL_SIZE(" "), c->eob);
	  push_in_buffer(&c->com.write, s->resources_names[i],
			 strlen(s->resources_names[i]), c->eob);
	}
    }
  return (0);
}
