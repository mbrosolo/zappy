/*
** player_team.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 23:23:22 2012 anna texier
** Last update Sun Jul 15 23:23:45 2012 anna texier
*/

#include	"server.h"

int		set_new_player(t_server *s, t_client *c, t_player *tmp,
			       t_team *t)
{
  if (tmp)
    {
      delete_player(s, c->player);
      c->player = tmp;
      tmp->client = c;
      graphic_egg_become_player(s, tmp);
    }
  else
    {
      if (!t->players_list.push_back(&t->players_list, c->player))
	return (1);
      --t->connect_nbr;
      c->player->team = t;
      graphic_player_new_to_all_client(s, c->player);
    }
  return (0);
}

/**
 * @brief Push client in team's lit
 * @param teams The teams' list
 * @param c he client to add
 * @param team_name The team's name
 */
int		add_player_to_team(t_server *s, t_client *c, char *team_name)
{
  t_team	*t;
  t_player	*tmp;

  tmp = NULL;
  if (!(t = s->team_list.find(&s->team_list, (t_pred) &cmp_team_name,
			      (void *) team_name)))
    {
      fprintf(stderr, "Team %s does not exist.\n", team_name);
      s->task_list.remove(&s->task_list, c->task, &free);
      c->task = NULL;
      return (1);
    }
  tmp = s->players_list.find(&s->players_list, (int (*)(void *, void *))
			     &cmp_player_available, team_name);
  if (!t->connect_nbr && !tmp)
    return (ret_error("No more slot available.\n"));
  return (set_new_player(s, c, tmp, t));
}

/**
 * @brief Push client in team's lit
 * @param teams The teams' list
 * @param c he client to add
 * @param team_name The team's name
 */
int		add_player_to_team_by_cpy(t_server *s, t_team *team,
					  t_player *player)
{
  (void) s;
  if (!team->players_list.push_back(&team->players_list, player))
    return (1);
  player->team = team;
  return (0);
}

/**
 * @brief Push client in team's list
 * @param teams The teams' list
 * @param c he client to add
 * @param team_name The team's name
 */
void		sub_player_to_team(t_player *player)
{
  player->team->players_list.remove(&player->team->players_list, player, NULL);
  ++player->team->connect_nbr;
  player->team = NULL;
}
