/*
** graphic_map.c for zappy in /home/brosol_m/zappy/server_src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Sat Jul 14 14:34:17 2012 matthias brosolo
** Last update Sun Jul 15 22:53:49 2012 anna texier
*/

#include	"server.h"

int		graphic_map_size(t_server *s, t_client *c)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "msz %d %d", s->map.width, s->map.height);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_map_content(t_server *s, t_client *c)
{
  int		x;
  int		y;

  x = 0;
  while (x < s->map.width)
    {
      y = 0;
      while (y < s->map.height)
	{
	  send_square_content(s, c, s->map.get_square(&s->map, x, y));
	  ++y;
	}
      ++x;
    }
  return (0);
}

int		graphic_map_content_to_all_client(t_server *s)
{
  int		x;
  int		y;

  x = 0;
  while (x < s->map.width)
    {
      y = 0;
      while (y < s->map.height)
	{
	  send_square_content_to_all_client(s, s->map.get_square(&s->map, x, y));
	  ++y;
	}
      ++x;
    }
  return (0);
}
