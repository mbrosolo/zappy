/*
** graphic_player_info.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sat Jul 14 15:34:59 2012 matthias brosolo
** Last update Sun Jul 15 22:48:59 2012 anna texier
*/

#include	"server.h"

size_t		extract_number_from_str(char *buffer, int eob)
{
  char		*tmp;

  sub_eob(buffer, eob);
  if (!strtok(buffer, " ") || !(tmp = strtok(NULL, " ")))
    return (0);
  return (strtoul(tmp, NULL, 10));
}

int		graphic_player_level(t_server *s, t_client *c, t_player *p)
{
  char		buffer[64];
  ssize_t	size;

  size = snprintf(buffer, 60, "plv %lu %d", p->id, p->level);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_player_level_to_all_client(t_server *s, t_player *p)
{
  char		buffer[64];
  ssize_t	size;

  size = snprintf(buffer, 60, "plv %lu %d", p->id, p->level);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_player_inventory(t_server *s, t_client *c, t_player *p)
{
  char		buffer[256];
  ssize_t	size;

  update_food(s, p->live);
  size = snprintf(buffer, 252, "pin %lu %d %d %d %d %d %d %d %d %d",
		  p->id, p->x, p->y,
		  p->resource[R_FOOD],
		  p->resource[R_LINEMATE], p->resource[R_DERAUMERE],
		  p->resource[R_SIBUR],
		  p->resource[R_MENDIANE], p->resource[R_PHIRAS],
		  p->resource[R_THYSTAME]);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_player_inventory_to_all_client(t_server *s, t_player *p)
{
  char		buffer[256];
  ssize_t	size;

  update_food(s, p->live);
  size = snprintf(buffer, 252, "pin %lu %d %d %d %d %d %d %d %d %d",
		  p->id, p->x, p->y,
		  p->resource[R_FOOD],
		  p->resource[R_LINEMATE], p->resource[R_DERAUMERE],
		  p->resource[R_SIBUR],
		  p->resource[R_MENDIANE], p->resource[R_PHIRAS],
		  p->resource[R_THYSTAME]);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}
