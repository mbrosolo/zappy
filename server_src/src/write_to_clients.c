/*
** write_to_clients.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Jun  1 09:55:49 2012 matthias brosolo
** Last update Fri Jun  1 09:55:49 2012 matthias brosolo
*/

#include	"server.h"

/**
 * @file   write_to_clients.c
 * @Author matthias brosolo
 * @date   June, 2012
 * @brief  Send response to clients
 */

/**
 * @brief Write reponse to client
 * @param s Server data
 * @param c Client data
 * @return Size of the extracted command
 */
int			write_client(t_server *s, t_client *c)
{
  if ((c->com.write.size = extract_from_buffer(&c->com.write, c->eob)) <= 0)
    {
      printf("Can't extract from buffer write\n");
      return (1);
    }
  write(1, "Sending \"", REAL_SIZE("Sending \""));
  write(1, c->com.write.current, c->com.write.size - (c->eob + 1));
  printf("\" to client %lu\n", c->player ? c->player->id : 0);
  if ((c->com.write.pos = c->com.sock.send(&c->com.sock, c->com.write.current,
					   c->com.write.size)) < 0)
    {
      perror("send(2)");
      return (1);
    }
  if (!(c->com.write.count))
    FD_CLR(c->com.sock.fd, &s->fds[F_WRITE]);
  return (0);
}

int			write_to_clients(t_server *s)
{
  t_client		*c;
  t_list_iterator	it;

  s->client_list.begin(&s->client_list, &it);
  while (it.current(&it) != s->client_list.end(&s->client_list))
    {
      c = it.data;
      if (FD_ISSET(c->com.sock.fd, &s->fds[F_WRITE]) && write_client(s, c))
	delete_client(s, c, &it);
      it.next(&it);
    }
  return (0);
}
