/*
** graphic_time.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sat Jul 14 14:42:22 2012 matthias brosolo
** Last update Sat Jul 14 14:42:22 2012 matthias brosolo
*/

#include	"server.h"

int		graphic_server_get_time(t_server *s, t_client *c)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "sgt %d", s->delay);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_server_get_time_to_all_client(t_server *s)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "sgt %d", s->delay);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_server_set_time(t_server *s, t_client *c)
{
  size_t	delay;

  if (!(delay = extract_number_from_str(c->com.read.current, c->eob))
      || delay > 1000000)
    graphic_server_bad_parameter(s, c);
  else
    {
      s->delay = delay;
      init_task_delay(s->task_delay, s->delay);
      graphic_server_get_time_to_all_client(s);
    }
  return (0);
}
