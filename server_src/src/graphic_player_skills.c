/*
** graphic_player_actions.c for zappy in /home/sarglen/zappy/server_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sat Jul 14 16:43:38 2012 anna texier
** Last update Sun Jul 15 19:20:53 2012 anna texier
*/

#include	"server.h"

int		graphic_player_broadcast(t_server *s, t_player *p, char *msg)
{
  char		buffer[BUFFER_SIZE + 1];
  ssize_t	size;

  size = snprintf(buffer, BUFFER_SIZE - 3, "pbc %lu %s", p->id, msg);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_player_incantation(t_server *s, t_player *p)
{
  char			buffer[BUFFER_SIZE + 1];
  ssize_t		size;
  t_player		*tmp;
  t_square		*sq;
  t_list_iterator	it;

  size = snprintf(buffer, BUFFER_SIZE - 3, "pic %d %d %d %lu", p->x, p->y,
		  p->level, p->id);
  sq = s->map.get_square(&s->map, p->x, p->y);
  sq->players_list.begin(&sq->players_list, &it);
  while (it.current(&it) != sq->players_list.end(&sq->players_list))
    {
      tmp = it.data;
      if (tmp != p && ((tmp->status == PS_LIVING)
		       || (tmp->status == PS_WAITING && !tmp->live)))
	size = snprintf(buffer, BUFFER_SIZE - 3, "%s %lu", buffer, p->id);
      it.next(&it);
    }
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int			graphic_player_incantation_end(t_server *s,
						       t_player *p, int result)
{
  char			buffer[64];
  ssize_t		size;
  t_player		*tmp;
  t_square		*sq;
  t_list_iterator	it;

  size = snprintf(buffer, 60, "pie %d %d %d", p->x, p->y, result);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  sq = s->map.get_square(&s->map, p->x, p->y);
  sq->players_list.begin(&sq->players_list, &it);
  while (it.current(&it) != sq->players_list.end(&sq->players_list))
    {
      tmp = it.data;
      if (((tmp->status == PS_LIVING)
	   || (tmp->status == PS_WAITING && !tmp->live)))
	graphic_player_level_to_all_client(s, tmp);
      it.next(&it);
    }
  graphic_map_content_to_all_client(s);
  return (0);
}

int		graphic_player_fork(t_server *s, t_player *p)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "pfk %lu", p->id);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}
