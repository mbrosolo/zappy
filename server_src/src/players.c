/*
** players.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Tue Jun  5 11:44:35 2012 matthias brosolo
** Last update Sun Jul 15 22:42:50 2012 anna texier
*/

#include	"server.h"

/**
 * @file   players.c
 * @Author matthias brosolo
 * @date   June, 2012
 * @brief  Players' functions
 */

/**
 * @brief Get the type of eob
 * @param b The buffer containing characters
 * @param size The size read
 */
t_player	*create_new_player(t_server *s, int x, int y)
{
  t_player	*p;
  t_square	*sq;

  if (!(p = wmalloc(sizeof(*p))))
    return (NULL);
  p->x = x;
  p->y = y;
  sq = s->map.get_square(&s->map, p->x, p->y);
  if (!sq->players_list.push_back(&sq->players_list, p)
      || !s->players_list.push_back(&s->players_list, p))
    return (NULL);
  add_resource(&s->map, 10, R_FOOD);
  p->egg_id = 0;
  p->id = s->player_id++;
  p->hp = DEFAULT_HP;
  p->status = PS_RESERVED;
  p->live = NULL;
  p->team = NULL;
  p->client = NULL;
  p->direction = rand() % NB_DIRECTIONS;
  memset(p->resource, 0, sizeof(p->resource));
  p->resource[R_FOOD] = DEFAULT_HP;
  p->level = DEFAULT_LEVEL;
  return (p);
}

/**
 * @brief Delete a player
 * @param s The server's data
 * @param player The player to delete
 */
void		delete_player(t_server *s, t_player *player)
{
  t_square	*sq;

  if (!player || !(sq = s->map.get_square(&s->map, player->x, player->y)))
    return;
  printf("Deleting player %lu\n", player->id);
  if (player->team)
    sub_player_to_team(player);
  sq->players_list.remove(&sq->players_list, player, NULL);
  s->players_list.remove(&s->players_list, player, &free);
}
