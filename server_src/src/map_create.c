/*
** map.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Tue May 29 15:20:09 2012 matthias brosolo
** Last update Sat Jul 14 15:32:06 2012 anna texier
*/

#include	"map.h"

static int	init_square(t_square *sq, int const x, int const y)
{
  sq->x = x;
  sq->y = y;
  if (create_list(&sq->players_list))
    return (1);
  return (0);
}

static int	init_square_array(t_map *m)
{
  int	x;
  int	y;

  y = -1;
  while (++y < m->height)
    {
      x = -1;
      while (++x < m->width)
	{
	  if (init_square(&m->square_array[x + (y * m->width)], x, y))
	    return (1);
	}
    }
  return (0);
}

int		create_map(t_map *m)
{
  m->size = sizeof(*(m->square_array)) * (m->width * m->height);
  if (!(m->square_array = wmalloc(m->size)))
    return (1);
  memset(m->square_array, 0, m->size);
  m->size = m->width * m->height;
  if (!(m->square_flag = wmalloc(sizeof(*m->square_flag) * m->size)))
    return (1);
  memset(m->square_flag, 0, m->size);
  if (init_square_array(m))
    return (1);
  m->is_set = &map_is_set;
  m->set = &map_set;
  m->unset_all = &map_unset_all;
  m->get_square = &map_get_square;
  generate_resources(m);
  return (0);
}

void		delete_map(t_map *map)
{
  t_square	*sq;
  unsigned int	i;

  i = 0;
  while (i < map->size)
    {
      sq = &map->square_array[i];
      sq->players_list.delete(&sq->players_list, NULL);
      ++i;
    }
  free(map->square_array);
  free(map->square_flag);
}
