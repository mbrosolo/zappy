/*
** init_client.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 22:58:48 2012 anna texier
** Last update Sun Jul 15 22:59:48 2012 anna texier
*/

#include	<math.h>
#include	"server.h"

static void	init_client_command_skills(t_cmd *cmd)
{
  strcpy(cmd[INCANTATION].name, "incantation");
  cmd[INCANTATION].fct = &player_incantation;
  strcpy(cmd[FORK].name, "fork");
  cmd[FORK].fct = &player_fork;
  strcpy(cmd[CONNECT_NBR].name, "connect_nbr");
  cmd[CONNECT_NBR].fct = &player_connect_nbr;
  cmd[UNKNOWN_COMMAND].fct = &player_unknown_command;
  cmd[INCANTATION_END].fct = &player_incantation_end;
  cmd[DEAD].fct = (int (*)(t_server *, t_client *)) &player_dead;
  cmd[HATCHING].fct = (int (*)(t_server *, t_client *)) &player_hatching;
  cmd[CLIENT_TIMEOUT].fct = &client_timeout;
}

void	init_client_command(t_cmd *cmd)
{
  memset(cmd, 0, sizeof(*cmd) * NB_CLIENT_CMD);
  strcpy(cmd[ADVANCE].name, "avance");
  cmd[ADVANCE].fct = &player_advance;
  strcpy(cmd[TURN_RIGHT].name, "droite");
  cmd[TURN_RIGHT].fct = &player_turn_right;
  strcpy(cmd[TURN_LEFT].name, "gauche");
  cmd[TURN_LEFT].fct = &player_turn_left;
  strcpy(cmd[SEE].name, "voir");
  cmd[SEE].fct = &player_see;
  strcpy(cmd[INVENTORY].name, "inventaire");
  cmd[INVENTORY].fct = &player_inventory;
  strcpy(cmd[TAKE].name, "prend");
  cmd[TAKE].fct = &player_take;
  strcpy(cmd[PUT_DOWN].name, "pose");
  cmd[PUT_DOWN].fct = &player_put_down;
  strcpy(cmd[EJECT].name, "expulse");
  cmd[EJECT].fct = &player_eject;
  strcpy(cmd[BROADCAST].name, "broadcast");
  cmd[BROADCAST].fct = &player_broadcast;
  init_client_command_skills(cmd);
}

void	init_task_delay(t_timeval *task_delay, int delay)
{
  int	i;
  long	usec;
  float	tmp;

  i = -1;
  memset(task_delay, 0, sizeof(*task_delay) * NB_CLIENT_CMD);
  usec = (long) (modff(7.f / (float) delay, &tmp) * 1000000.f);
  while (++i < FORK)
    set_timeval(&task_delay[i], tmp, usec);
  usec = (long) (modff(300.f / (float) delay, &tmp) * 1000000.f);
  set_timeval(&task_delay[INCANTATION_END], tmp, usec);
  usec = (long) (modff(42.f / (float) delay, &tmp) * 1000000.f);
  set_timeval(&task_delay[FORK], tmp, usec);
  usec = (long) (modff(126.f / (float) delay, &tmp) * 1000000.f);
  set_timeval(&task_delay[DEAD], tmp, usec);
  usec = (long) (modff(600.f / (float) delay, &tmp) * 1000000.f);
  set_timeval(&task_delay[HATCHING], tmp, usec);
  task_delay[CLIENT_TIMEOUT].tv_sec = 10;
}
