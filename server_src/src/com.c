/*
** communication.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Jul  5 09:37:36 2012 matthias brosolo
** Last update Thu Jul  5 09:37:36 2012 matthias brosolo
*/

#include	"server.h"

/**
 * @brief Init a t_com_buffer object
 * @param com t_com_buffer data
 * @return Flag true or false
 */
int	init_com_buffer(t_com_buffer *com)
{
  com->count = 0;
  com->pos = 0;
  com->size = 0;
  if (create_ringbuffer(&com->buffer, BUFFER_SIZE))
    return (1);
  return (0);
}

/**
 * @brief Init a t_com object
 * @param com t_com data
 * @param sock t_connect data
 * @return Flag true or false
 */
int	init_com(t_com *com, t_connect *sock)
{
  memcpy(&com->sock, sock, sizeof(*sock));
  if (init_com_buffer(&com->read) || init_com_buffer(&com->write))
    return (1);
  return (0);
}
