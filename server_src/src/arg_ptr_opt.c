/*
** arg_ptr_opt.c for  in /home/bourdi_b/zappy/server_src
**
** Made by benjamin bourdin
** Login   <bourdi_b@epitech.net>
**
** Started on  Tue Jun 12 16:33:44 2012 benjamin bourdin
** Last update Tue Jun 12 17:25:05 2012 benjamin bourdin
*/

#include	"server.h"

void	set_port(char const * const param, t_server *s)
{
  s->port = atoi(param);
}

void	set_width(char const * const param, t_server *s)
{
  s->map.width = atoi(param);
}

void	set_height(char const * const param, t_server *s)
{
  s->map.height = atoi(param);
}

void	set_client_team(char const * const param, t_server *s)
{
  s->client_per_team = atoi(param);
}

void	set_delay(char const * const param, t_server *s)
{
  s->delay = atoi(param);
}
