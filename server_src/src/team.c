/*
** team.c for zappy in /home/gressi_b/zappy/server_src
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Mon May 14 16:02:59 2012 gressi_b
** Last update Sun Jul 15 23:23:57 2012 anna texier
*/

#include	<stdlib.h>
#include	<string.h>
#include	"server.h"

/**
 * @file   team.c
 * @Author benjamin gressier
 * @date   May, 2012
 * @brief  Team's function
 */

/**
 * @brief Free a team node
 * @param t The team node
 */
void		free_team(t_team *t)
{
  free(t->name);
  t->players_list.delete(&t->players_list, NULL);
  free(t);
}

/**
 * @brief Create a team node and push it in a t_list
 * @param teams The teams' list
 * @param team_name The team's name
 */
int		create_team(t_server *s, char *team_name)
{
  t_team	*new;

  if (s->team_list.find(&s->team_list, (int (*)(void *, void *))
			&cmp_team_name, team_name)
      || !(new = wmalloc(sizeof(*new))))
    return (1);
  if (!(new->name = strndup(team_name, 255)) ||
      create_list(&new->players_list))
    {
      if (new->name)
	free(new->name);
      free(new);
      return (1);
    }
  new->connect_nbr = s->client_per_team;
  if (!s->team_list.push_back(&s->team_list, new))
    {
      free(new->name);
      new->players_list.delete(&new->players_list, NULL);
      free(new);
      return (1);
    }
  return (0);
}

/**
 * @brief Set the connect_nbr of all teams
 * @param teams The teams' list
 * @param cpt The nbr of client per team
 */
void			set_client_per_team(t_list *teams, int cpt)
{
  t_team		*t;
  t_list_iterator	it;

  teams->begin(teams, &it);
  while (it.current(&it) != teams->end(teams))
    {
      t = it.data;
      t->connect_nbr = cpt;
      it.next(&it);
    }
}
