/*
** tasks.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Jul  5 13:22:21 2012 matthias brosolo
** Last update Sun Jul 15 23:27:23 2012 anna texier
*/

#include	"server.h"

/**
 * @file   tasks.c
 * @Author matthias brosolo
 * @date   Jul, 2012
 * @brief  Tasks's functions
 */

/**
 * @brief delete tasks from owner
 * @param 1: server structure, 2: client or player
 */
void		delete_tasks_from_owner(t_server *srv, void *owner)
{
  srv->task_list.remove_if(&srv->task_list, (int (*)(void *, void *))
			   &cmp_task_client, owner, &free);
}

/**
 * @brief add task to task list of server structure
 * @param  1: server structure, 2: client structure, 3: commande number
 */
int		add_task(t_server *s, t_client *client, int cmd)
{
  t_task	*task;

  if (!(task = wmalloc(sizeof(*task))))
    return (1);
  if (!s->task_list.insert_pred(&s->task_list, task,
				(int (*)(void *, void *)) &cmp_timeval,
				&s->task_delay[cmd]))
    return (1);
  task->id = cmd;
  memset(task->buffer, 0, BUFFER_SIZE + 1);
  if (cmd == TAKE || cmd == PUT_DOWN || cmd == BROADCAST)
    strncat(task->buffer, client->com.read.current, BUFFER_SIZE);
  cpy_timeval(&task->delay, &s->task_delay[cmd]);
  client->task = task;
  task->owner = client;
  task->owner_type = OT_CLIENT;
  return (0);
}

/**
 * @brief create dead task
 * @param 1: server structure, 2: player structure
 */
int		create_dead_task(t_server *srv, t_player *player)
{
  t_task	*task;

  if (!(task = wmalloc(sizeof(*task))))
    return (1);
  task->id = DEAD;
  cpy_timeval(&task->delay, &srv->task_delay[DEAD]);
  mult_timeval(&task->delay, player->resource[R_FOOD]);
  if (!srv->task_list.insert_pred(&srv->task_list, task,
  				(int (*)(void *, void *)) &cmp_timeval,
  				&task->delay))
    return (1);
  task->owner = player;
  task->owner_type = OT_PLAYER;
  player->live = task;
  player->status = PS_LIVING;
  return (0);
}

/**
 * @brief create hatching task
 * @param 1: server structure, 2: player structure
 */
int		create_hatching_task(t_server *srv, t_player *egg)
{
  t_task	*task;

  if (!(task = wmalloc(sizeof(*task))))
    return (1);
  task->id = HATCHING;
  cpy_timeval(&task->delay, &srv->task_delay[HATCHING]);
  if (!srv->task_list.insert_pred(&srv->task_list, task,
  				(int (*)(void *, void *)) &cmp_timeval,
  				&task->delay))
    return (1);
  task->owner = egg;
  task->owner_type = OT_PLAYER;
  egg->egg_id = srv->egg_id++;
  egg->live = task;
  graphic_egg_new_to_all_client(srv, egg);
  egg->id = srv->player_id++;
  return (0);
}
