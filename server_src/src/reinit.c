/*
** reinit.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Jul 15 19:57:34 2012 matthias brosolo
** Last update Sun Jul 15 23:03:01 2012 benjamin bourdin
*/

#include	"server.h"

void		reinit_map(t_map *map)
{
  t_square	*sq;
  unsigned int	i;

  i = 0;
  while (i < map->size)
    {
      sq = &map->square_array[i];
      sq->players_list.clear(&sq->players_list, NULL);
      ++i;
    }
  generate_resources(map);
}

void		reinit_clients_list(t_list *list)
{
  t_client		*c;
  t_list_iterator	it;

  list->begin(list, &it);
  while (it.current(&it) != list->end(list))
    {
      c = it.data;
      if (c->state != ST_GRAPHIC)
	it.remove(&it, free_client);
      it.next(&it);
    }
}

void		reinit_players_list(t_list *list)
{
  t_player		*p;
  t_list_iterator	it;

  list->begin(list, &it);
  while (it.current(&it) != list->end(list))
    {
      p = it.data;
      if (!p->client || p->client->state != ST_GRAPHIC)
	it.remove(&it, &free);
      it.next(&it);
    }
}

void			reinit_players_lists_in_teams_list(t_list *list)
{
  t_team		*t;
  t_list_iterator	it;

  list->begin(list, &it);
  while (it.current(&it) != list->end(list))
    {
      t = it.data;
      t->players_list.clear(&t->players_list, NULL);
      it.next(&it);
    }
}
