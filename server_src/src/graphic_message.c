/*
** graphic_server_datas.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sat Jul 14 16:17:44 2012 anna texier
** Last update Sat Jul 14 16:35:05 2012 anna texier
*/

#include	"server.h"

int		graphic_server_end_game(t_server *s, char *team)
{
  char		buffer[512];
  ssize_t	size;

  size = snprintf(buffer, 508, "seg %s", team);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_server_message(t_server *s, char *msg)
{
  char		buffer[BUFFER_SIZE + 1];
  ssize_t	size;

  size = snprintf(buffer, BUFFER_SIZE - 3, "smg %s", msg);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_server_unknown_command(t_server *s, t_client *c)
{
  send_to_client(c, "suc", REAL_SIZE("suc"), &s->fds[F_WRITE]);
  return (0);
}

int		graphic_server_bad_parameter(t_server *s, t_client *c)
{
  send_to_client(c, "sbp", REAL_SIZE("sbp"), &s->fds[F_WRITE]);
  return (0);
}
