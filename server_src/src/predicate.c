/*
** predicate.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Tue Jul  3 15:21:30 2012 matthias brosolo
** Last update Sun Jul 15 23:28:48 2012 anna texier
*/

#include	"server.h"

/**
 * @brief Compare team name with another string
 * @param team The team object
 * @param name The name to compare
 * @return True or false
 */
int		cmp_team_name(t_team *team, char *name)
{
  if (!strcmp(team->name, name))
    return (1);
  return (0);
}

/**
 * @brief Compare player available
 * @param player The player to compare
 * @param team_name The team's name which must contains the player
 * @return True or false
 */
int		cmp_player_available(t_player *player, char *team_name)
{
  if (player->status == PS_WAITING
      && player->live == NULL
      && !strcmp(player->team->name, team_name))
    return (1);
  return (0);
}

/**
 * @brief Compare player by id
 * @param player The player to compare
 * @param id The id to be compared
 * @return True or false
 */
int		cmp_player_by_id(t_player *player, size_t id)
{
  if (player->id == id && player->status == PS_LIVING)
    return (1);
  return (0);
}
