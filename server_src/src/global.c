/*
** global.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed Jul 11 10:14:33 2012 matthias brosolo
** Last update Wed Jul 11 10:14:33 2012 matthias brosolo
*/

char	g_incantation[7][7] =
  {
    {1, 1, 0, 0, 0, 0, 0},
    {2, 1, 1, 1, 0, 0, 0},
    {2, 2, 0, 1, 0, 2, 0},
    {4, 1, 1, 2, 0, 2, 0},
    {4, 1, 2, 1, 3, 0, 0},
    {6, 1, 2, 3, 0, 1, 0},
    {6, 2, 2, 2, 2, 2, 1},
  };
