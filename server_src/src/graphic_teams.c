/*
** graphic_teams.c for Zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sat Jul 14 16:00:15 2012 anna texier
** Last update Sat Jul 14 16:20:41 2012 anna texier
*/

#include	"server.h"

int			graphic_team_names(t_server *s, t_client *c)
{
  char			buffer[512];
  t_team		*t;
  ssize_t		size;
  t_list_iterator	it;

  s->team_list.begin(&s->team_list, &it);
  while (it.current(&it) != s->team_list.end(&s->team_list))
    {
      t = it.data;
      size = snprintf(buffer, 508, "tna %s", t->name);
      send_to_client(c, buffer, size, &s->fds[F_WRITE]);
      it.next(&it);
    }
  return (0);
}

