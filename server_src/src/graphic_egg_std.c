/*
** graphic_egg_std.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 22:55:18 2012 anna texier
** Last update Sun Jul 15 22:55:47 2012 anna texier
*/

#include	"server.h"

int		send_all_new_egg(t_server *s, t_client *c)
{
  t_player		*p;
  t_list_iterator	it;

  s->players_list.begin(&s->players_list, &it);
  while (it.current(&it) != s->players_list.end(&s->players_list))
    {
      p = it.data;
      if (p->status == PS_WAITING && p->live)
	graphic_egg_new(s, c, p);
      it.next(&it);
    }
  return (0);
}

int		graphic_egg_new(t_server *s, t_client *c, t_player *e)
{
  char		buffer[64];
  ssize_t	size;

  size = snprintf(buffer, 60, "enw %lu %lu %d %d",
		  e->egg_id, e->id, e->x, e->y);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_egg_new_to_all_client(t_server *s, t_player *e)
{
  char		buffer[64];
  ssize_t	size;

  size = snprintf(buffer, 60, "enw %lu %lu %d %d",
		  e->egg_id, e->id, e->x, e->y);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}
