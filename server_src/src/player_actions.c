/*
** player_actions.c for zapyy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 23:30:34 2012 anna texier
** Last update Sun Jul 15 23:30:57 2012 anna texier
*/

#include	"server.h"

int	player_put_down(t_server *s, t_client *c)
{
  int		resource;
  t_square	*sq;

  sub_eob(c->com.read.current, c->eob);
  printf("Client %d requests put down\n", c->com.sock.fd);
  resource = get_resource_id(s->resources_names, c->com.read.current);
  sq = s->map.get_square(&s->map, c->player->x, c->player->y);
  if (resource != -1 && c->player->resource[resource])
    {
      ++s->map.items[resource];
      ++sq->items[resource];
      --c->player->resource[resource];
      if (resource == R_FOOD)
	sub_timeval(&c->player->live->delay, s->task_delay[DEAD].tv_sec,
		    s->task_delay[DEAD].tv_usec);
      graphic_player_drop(s, c->player, resource);
      send_to_client(c, "ok", REAL_SIZE("ok"), &s->fds[F_WRITE]);
    }
  else
    send_to_client(c, "ko", REAL_SIZE("ko"), &s->fds[F_WRITE]);
  return (0);
}

int			eject_client(t_server *s, t_square *sq_ref,
				     t_square *sq, t_player *p)
{
  char			buffer[128];
  ssize_t		size;

  size = 0;
  p->x = sq->x;
  p->y = sq->y;
  if (p->client)
    {
      size = snprintf(buffer, 128, "deplacement: %d",
		      get_direction(s, sq_ref, p));
      send_to_client(p->client, buffer, size, &s->fds[F_WRITE]);
    }
  return (size);
}

int			player_eject_all(t_server *s, t_square *sq_ref,
					 t_client *c, t_square *sq)
{
  int			mark;
  t_player		*p;
  t_list_iterator	it;

  mark = 0;
  sq_ref->players_list.begin(&sq_ref->players_list, &it);
  while (it.current(&it) != sq_ref->players_list.end(&sq_ref->players_list))
    {
      p = (t_player *) it.data;
      if (p != c->player
	  && (p->status == PS_LIVING || (p->status == PS_WAITING && !p->live)))
	{
	  mark = eject_client(s, sq_ref, sq, p);
	  sq->players_list.push_back(&sq->players_list, p);
	  it.remove(&it, NULL);
	  it.prev(&it);
	}
      it.next(&it);
    }
  return (mark);
}

int			player_eject(t_server *s, t_client *c)
{
  t_2d			pos;
  t_square		*sq;
  t_square		*sq_ref;

  printf("Client %lu requests eject in %d %d\n", c->player->id,
	 c->player->x, c->player->y);
  sq_ref = s->map.get_square(&s->map, c->player->x, c->player->y);
  pos.x = c->player->x;
  pos.y = c->player->y;
  coord_by_directions(&pos, 0, 1, c->player->direction);
  sq = s->map.get_square(&s->map, pos.x, pos.y);
  if (player_eject_all(s, sq_ref, c, sq))
    send_to_client(c, "ok", REAL_SIZE("ok"), &s->fds[F_WRITE]);
  else
    send_to_client(c, "ko", REAL_SIZE("ko"), &s->fds[F_WRITE]);
  graphic_player_expulse(s, c->player);
  return (0);
}
