/*
** map.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Tue May 29 15:20:09 2012 matthias brosolo
** Last update Wed Jun 13 10:27:39 2012 benjamin bourdin
*/

#include	"map.h"

int		get_real_coord(int max, int c)
{
  c = c % max;
  if (c < 0)
    c += max;
  return (c);
}

int		map_is_set(t_map *m, int x, int y)
{
  x = get_real_coord(m->width, x);
  y = get_real_coord(m->height, y);
  return (m->square_flag[x + y * m->width]);
}

void		map_set(t_map *m, int x, int y)
{
  x = get_real_coord(m->width, x);
  y = get_real_coord(m->height, y);
  m->square_flag[x + y * m->width] = 1;
}

void		map_unset_all(t_map *map)
{
  memset(map->square_flag, 0, map->size);
}

t_square	*map_get_square(t_map *m, int x, int y)
{
  x = get_real_coord(m->width, x);
  y = get_real_coord(m->height, y);
  return (&(m->square_array[x + y * m->width]));
}

