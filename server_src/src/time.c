/*
** time.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Jul  5 16:32:36 2012 matthias brosolo
** Last update Sun Jul 15 22:44:57 2012 anna texier
*/

#include	"server.h"

void		mult_timeval(t_timeval *tv, int factor)
{
  long		tmp;

  tv->tv_sec *= factor;
  tv->tv_usec *= factor;
  tmp = tv->tv_usec / 1000000;
  tv->tv_sec += tmp;
  tv->tv_usec %= 1000000;
}

void		sub_timeval(t_timeval *tv, long sec, long usec)
{
  tv->tv_sec -= sec;
  tv->tv_usec -= usec;
  if (tv->tv_usec < 0)
    {
      if (tv->tv_sec > 0)
	{
	  --tv->tv_sec;
	  tv->tv_usec += 1000000;
	}
      else
	{
	  tv->tv_sec = 0;
	  tv->tv_usec = 0;
	}
    }
}

void		add_timeval(t_timeval *tv, t_timeval *to_add)
{
  tv->tv_sec += to_add->tv_sec;
  tv->tv_usec += to_add->tv_usec;
}

int			get_timeout(t_server *s)
{
  t_task		*task;

  s->time_ref.tv_sec = -1;
  s->time_ref.tv_usec = 0;
  if (s->task_list.size(&s->task_list))
    {
      if (!(task = s->task_list.front(&s->task_list)))
	return (1);
      s->time_ref.tv_sec = task->delay.tv_sec;
      s->time_ref.tv_usec = task->delay.tv_usec;
    }
  s->time_remaining.tv_sec = s->time_ref.tv_sec;
  s->time_remaining.tv_usec = s->time_ref.tv_usec;
  return (0);
}
