/*
** graphic_client.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sat Jul 14 14:09:32 2012 matthias brosolo
** Last update Sun Jul 15 19:18:17 2012 anna texier
*/

#include	"server.h"

int		add_graphic_client(t_server *s, t_client *c)
{
  c->state = ST_GRAPHIC;
  graphic_map_size(s, c);
  graphic_server_get_time(s, c);
  graphic_map_content(s, c);
  graphic_team_names(s, c);
  send_all_new_player(s, c);
  send_all_new_egg(s, c);
  return (0);
}
