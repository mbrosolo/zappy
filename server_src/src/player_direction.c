/*
** player_direction.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Tue Jul  3 15:41:47 2012 matthias brosolo
** Last update Sun Jul 15 22:39:41 2012 anna texier
*/

#include	"server.h"

int		first_get_direction(t_server *s, t_square *sq,
				    t_move *mv, t_player *p)
{
  t_square	*tmp;

  mv->position.x = p->x;
  mv->position.y = p->y;
  mv->direction = p->direction;
  coord_by_directions(&mv->position, 0, 1, mv->direction);
  tmp = s->map.get_square(&s->map, mv->position.x, mv->position.y);
  if (tmp->x == sq->x && tmp->y == sq->y)
    return (1);
  coord_by_directions(&mv->position, -1, 0, mv->direction);
  tmp = s->map.get_square(&s->map, mv->position.x, mv->position.y);
  if (tmp->x == sq->x && tmp->y == sq->y)
    return (2);
  return (0);
}

int		get_direction_around(t_server *s, t_square *sq,
				     t_move *mv, int *direction)
{
  int		count;
  t_square	*tmp;

  count = -1;
  while (++count < 2)
    {
      ++(*direction);
      coord_by_directions(&mv->position, -1, 0, mv->direction);
      tmp = s->map.get_square(&s->map, mv->position.x, mv->position.y);
      if (tmp->x == sq->x && tmp->y == sq->y)
	return (1);
    }
  return (0);
}

int		get_direction(t_server *s, t_square *sq, t_player *p)
{
  int		nb;
  int		direction;
  t_move	mv;

  direction = 2;
  if (sq->x == p->x && sq->y == p->y)
    return (0);
  if ((nb = first_get_direction(s, sq, &mv, p)))
    return (nb);
  mv.direction = (mv.direction > UP ? mv.direction - 1 : LEFT);
  nb = 2;
  while (nb < 8)
    {
      if (get_direction_around(s, sq, &mv, &direction))
	return (direction);
      nb += 2;
      mv.direction = (mv.direction > UP ? mv.direction - 1 : LEFT);
    }
  return (direction);
}
