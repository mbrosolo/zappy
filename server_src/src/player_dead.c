/*
** player_dead.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Jul  5 18:34:24 2012 matthias brosolo
** Last update Thu Jul  5 18:34:24 2012 matthias brosolo
*/

#include	"server.h"

int	player_dead(t_server *srv, t_player *player)
{
  printf("Player %lu is dead\n", player->id);
  player->status = PS_DEAD;
  graphic_player_dead(srv, player);
  if (player->client)
    {
      send_to_client(player->client, DEAD_MSG, REAL_SIZE(DEAD_MSG),
		     &srv->fds[F_WRITE]);
      player->client->state = ST_TO_REMOVE;
      player->live = NULL;
    }
  else
    delete_player(srv, player);
  return (0);
}
