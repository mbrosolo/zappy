/*
** player_skills.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Jun  1 09:34:42 2012 matthias brosolo
** Last update Sun Jul 15 22:43:36 2012 anna texier
*/

#include	"server.h"

/**
 * @file   player_skills.c
 * @Author matthias brosolo
 * @date   June, 2012
 * @brief  Player's skills
 */

/**
 * @brief Send his inventory to a player
 * @param s The server's data
 * @param c The client's data
 * @return True
 */
int		player_inventory(t_server *s, t_client *c)
{
  int		*resource;
  char		buffer[256];
  ssize_t	size;

  update_food(s, c->player->live);
  resource = c->player->resource;
  printf("Player %lu requests inventory\n", c->player->id);
  size = snprintf(buffer, 253, INVENTORY_STR, resource[R_FOOD],
		  resource[R_LINEMATE], resource[R_DERAUMERE],
		  resource[R_SIBUR],
		  resource[R_MENDIANE], resource[R_PHIRAS],
		  resource[R_THYSTAME]);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

/**
 * @brief Send a player's message to all the other players
 * @param s The server's data
 * @param c The client's data
 * @return Flag true or false
 */
int		player_broadcast(t_server *s, t_client *c)
{
  int		nb;
  int		pass;
  t_move	mv;
  size_t	nb_total;

  init_player_broadcast(c->player->id, &nb, &nb_total, &pass);
  if (get_message(c->com.read.current, s->buffer, c->eob))
    {
      send_to_client(c, "ko", REAL_SIZE("ko"), &s->fds[F_WRITE]);
      return (0);
    }
  if (first_player_broadcast(s, &mv, c))
    return (1);
  while (nb_total < s->map.size)
    {
      if (broadcast_map_around(s, &mv, nb, &nb_total))
	return (1);
      if (!(pass++ % 2))
	++nb;
      mv.direction = (mv.direction > UP ? mv.direction - 1 : LEFT);
    }
  s->tmp_list.clear(&s->tmp_list, NULL);
  s->map.unset_all(&s->map);
  send_to_client(c, "ok", REAL_SIZE("ok"), &s->fds[F_WRITE]);
  return (0);
}

/**
 * @brief Start incantation for a group of player
 * @param s The server's data
 * @param c The client's data
 * @return Flag true or false
 */
int		player_incantation(t_server *s, t_client *c)
{
  t_square	*sq;

  printf("Client %lu requests incantation\n", c->player->id);
  sq = s->map.get_square(&s->map, c->player->x, c->player->y);
  if (!check_incantation_square(sq, g_incantation[c->player->level - 1],
				c->player->level))
    {
      send_to_square_client(sq, INCANT_BEING, REAL_SIZE(INCANT_BEING),
			    &s->fds[F_WRITE]);
      add_task(s, c, INCANTATION_END);
      graphic_player_incantation(s, c->player);
    }
  else
    send_to_client(c, "ko", REAL_SIZE("ko"), &s->fds[F_WRITE]);
  return (0);
}

/**
 * @brief Create a new egg for player's team
 * @param s The server's data
 * @param c The client's data
 * @return Flag true or false
 */
int		player_fork(t_server *s, t_client *c)
{
  t_player	*p;

  printf("Client %d requests fork\n", c->com.sock.fd);
  if (!(p = create_new_player(s, c->player->x, c->player->y)))
    return (1);
  s->player_id--;
  p->id = c->player->id;
  if (add_player_to_team_by_cpy(s, c->player->team, p)
      || create_hatching_task(s, p))
    return (1);
    send_to_client(c, "ok", REAL_SIZE("ok"), &s->fds[F_WRITE]);
  return (0);
}

/**
 * @brief Send the number of connections available for the team
 * @param s The server's data
 * @param c The client's data
 * @return Flag true or false
 */
int		player_connect_nbr(t_server *s, t_client *c)
{
  char		buffer[15];
  ssize_t	size;

  size = snprintf(buffer, 10, "%d", c->player->team->connect_nbr);
  printf("Player %lu requests connect_nbr\n", c->player->id);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}
