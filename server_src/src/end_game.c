/*
** end_game.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Jul 15 19:38:46 2012 matthias brosolo
** Last update Sun Jul 15 22:59:33 2012 benjamin bourdin
*/

#include	"server.h"

void		end_game(t_server *s)
{
  FD_ZERO(&s->fds[F_WRITE]);
  reinit_map(&s->map);
  reinit_clients_list(&s->client_list);
  reinit_players_list(&s->players_list);
  s->task_list.clear(&s->task_list, &free);
  reinit_players_lists_in_teams_list(&s->team_list);
  graphic_server_end_game(s, s->winner->name);
  s->winner = NULL;
}

int			count_nbr_level_win(t_list *list)
{
  int			count;
  t_player		*p;
  t_list_iterator	it;

  count = 0;
  list->begin(list, &it);
  while (it.current(&it) != list->end(list))
    {
      p = it.data;
      if (p->status == PS_LIVING && p->level == LEVEL_WIN)
	++count;
      it.next(&it);
    }
  return (count);
}

void			check_end_game(t_server *s)
{
  t_team		*t;
  t_list_iterator	it;

  s->team_list.begin(&s->team_list, &it);
  while (it.current(&it) != s->team_list.end(&s->team_list))
    {
      t = it.data;
      if (count_nbr_level_win(&t->players_list) == NBR_PLAYER_TO_WIN)
	{
	  printf("There is a winner !\n");
	  s->winner = t;
	  return;
	}
      it.next(&it);
    }
}
