/*
** player_incantation.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Jul  5 13:10:01 2012 matthias brosolo
** Last update Sun Jul 15 22:40:40 2012 anna texier
*/

#include	"server.h"

/**
 * @brief Set the level of the differents players
 * @param players The list which contains the players
 * @param level The level to set
 * @return Flag true or false
 */
void			set_square_players_level(t_list *players, char level)
{
  t_player		*p;
  t_list_iterator	it;

  players->begin(players, &it);
  while (it.current(&it) != players->end(players))
    {
      p = it.data;
      p->level = level;
      it.next(&it);
    }
}

/**
 * @brief Check if the level of the differents players is equal or not
 * @param players The list which contains the players
 * @param level The level to compare
 * @return Flag true or false
 */
int			check_players_level(t_list *players, char level)
{
  t_player		*p;
  t_list_iterator	it;

  players->begin(players, &it);
  while (it.current(&it) != players->end(players))
    {
      p = it.data;
      if (p->status != PS_LIVING || p->level != level)
	return (1);
      it.next(&it);
    }
  return (0);
}

/**
 * @brief Check if incantation is succeeded
 * @param s The server's data
 * @param c The client's data
 * @return Flag true or false
 */
int		player_incantation_end(t_server *s, t_client *c)
{
  char		tmp[32];
  ssize_t	size;
  t_square	*sq;

  printf("End incantation for client %lu\n", c->player->id);
  sq = s->map.get_square(&s->map, c->player->x, c->player->y);
  if (!check_incantation_square(sq, g_incantation[c->player->level - 1],
				c->player->level))
    {
      set_square_players_level(&sq->players_list, c->player->level + 1);
      memset(&sq->items[R_LINEMATE], 0,
	     sizeof(sq->items) - sizeof(sq->items[R_FOOD]));
      size = snprintf(tmp, 32, "niveau actuel : %d", c->player->level);
      send_to_square_client(sq, tmp, size, &s->fds[F_WRITE]);
      graphic_player_incantation_end(s, c->player, 1);
      check_end_game(s);
    }
  else
    {
      send_to_square_client(sq, "ko", REAL_SIZE("ko"), &s->fds[F_WRITE]);
      graphic_player_incantation_end(s, c->player, 0);
    }
  return (0);
}

/**
 * @brief Check the strictly needed resources
 * @param sq The square's data
 * @param require The resources reference
 * @param level The level of incantation
 * @return Flag true or false
 */
int		check_incantation_square(t_square *sq, char *require, char level)
{
  if (sq->players_list.size(&sq->players_list) != (size_t) require[0]
      || check_players_level(&sq->players_list, level)
      || sq->items[R_LINEMATE] != (size_t) require[R_LINEMATE]
      || sq->items[R_DERAUMERE] != (size_t) require[R_DERAUMERE]
      || sq->items[R_SIBUR] != (size_t) require[R_SIBUR]
      || sq->items[R_MENDIANE] != (size_t) require[R_MENDIANE]
      || sq->items[R_PHIRAS] != (size_t) require[R_PHIRAS]
      || sq->items[R_THYSTAME] != (size_t) require[R_THYSTAME])
    return (1);
  return (0);
}
