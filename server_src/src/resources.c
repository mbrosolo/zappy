/*
** resources.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Tue Jun 26 19:34:39 2012 matthias brosolo
** Last update Tue Jun 26 19:34:39 2012 matthias brosolo
*/

#include	"server.h"

void		update_food(t_server *srv, t_task *task)
{
  int		food;
  t_timeval	tmp;
  t_timeval	*ref;

  food = 0;
  ref = &srv->task_delay[DEAD];
  cpy_timeval(&tmp, &task->delay);
  while (tmp.tv_sec > ref->tv_sec
	 || (tmp.tv_sec == ref->tv_sec && tmp.tv_usec > ref->tv_usec))
    {
      sub_timeval(&tmp, ref->tv_sec, ref->tv_usec);
      ++food;
    }
  if (tmp.tv_sec > 0 || tmp.tv_usec > 0)
    ++food;
  ((t_player *) task->owner)->resource[R_FOOD] = food;
}

int	get_resource_id(char (*names)[SIZE_NAME_RESOURCE + 1], char *buffer)
{
  int	i;
  int	j;
  int	res;
  char	resource[SIZE_NAME_RESOURCE + 1];

  i = 0;
  memset(resource, 0, SIZE_NAME_RESOURCE + 1);
  while (buffer[i] && buffer[i] != ' ')
    ++i;
  if (buffer[i])
    ++i;
  j = 0;
  while (buffer[i] && buffer[i] != ' ' && j < SIZE_NAME_RESOURCE)
    resource[j++] = buffer[i++];
  res = -1;
  while (++res < R_NB_RESOURCE)
    if (!strcmp(resource, names[res]))
      return (res);
  return (-1);
}
