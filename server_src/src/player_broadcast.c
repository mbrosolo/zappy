/*
** player_broadcast.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Tue Jul  3 15:04:48 2012 matthias brosolo
** Last update Sun Jul 15 22:39:19 2012 anna texier
*/

#include	"server.h"

int		first_check_around(t_server *s, t_move *mv, t_square *sq)
{
  t_square	*tmp_sq;

  mv->position.x = sq->x;
  mv->position.y = sq->y;
  mv->direction = LEFT;
  coord_by_directions(&mv->position, 0, 1, mv->direction);
  tmp_sq = s->map.get_square(&s->map, mv->position.x, mv->position.y);
  if (tmp_sq->players_list.size(&tmp_sq->players_list)
      && send_to_all_client(s, tmp_sq, sq))
    return (1);
  coord_by_directions(&mv->position, -1, 0, mv->direction);
  tmp_sq = s->map.get_square(&s->map, mv->position.x, mv->position.y);
  if (tmp_sq->players_list.size(&tmp_sq->players_list)
      && send_to_all_client(s, tmp_sq, sq))
    return (1);
  --mv->direction;
  return (0);
}

int		check_around_square(t_server *s, t_square *sq)
{
  int		nb;
  int		count;
  t_move	mv;
  t_square	*tmp_sq;

  nb = 2;
  if (first_check_around(s, &mv, sq))
    return (1);
  while (nb < 8)
    {
      count = -1;
      while (++count < 2)
	{
	  coord_by_directions(&mv.position, -1, 0, mv.direction);
	  tmp_sq = s->map.get_square(&s->map, mv.position.x, mv.position.y);
	  if (tmp_sq->players_list.size(&tmp_sq->players_list)
	      && send_to_all_client(s, tmp_sq, sq))
	    return (1);
	}
      nb += count;
      --mv.direction;
    }
  return (0);
}

int		broadcast_map_around(t_server *s, t_move *mv, int nb,
				     size_t *nb_total)
{
  int		count;
  int		already_done;
  t_square	*sq;

  count = 0;
  already_done = 0;
  while (count < nb)
    {
      coord_by_directions(&mv->position, -1, 0, mv->direction);
      sq = s->map.get_square(&s->map, mv->position.x, mv->position.y);
      if (s->map.is_set(&s->map, sq->x, sq->y))
	++already_done;
      else
	{
	  s->map.set(&s->map, sq->x, sq->y);
	  if (check_around_square(s, sq))
	    return (1);
	}
      ++count;
    }
  *nb_total += count - already_done;
  return (0);
}

void		init_player_broadcast(size_t id, int *nb, size_t *nb_total,
				      int *pass)
{
  *nb = 1;
  *nb_total = 2;
  *pass = 0;
  printf("Client %lu broadcast requests broadcast\n", id);
}

int		first_player_broadcast(t_server *s, t_move *mv, t_client *c)
{
  t_square	*sq;

  if (!s->tmp_list.push_back(&s->tmp_list, c->player))
    return (1);
  graphic_player_broadcast(s, c->player, s->buffer);
  mv->position.x = c->player->x;
  mv->position.y = c->player->y;
  mv->direction = c->player->direction;
  sq = s->map.get_square(&s->map, mv->position.x, mv->position.y);
  if (sq->players_list.size(&sq->players_list) > 1
      && send_to_all_client(s, sq, sq))
    return (1);
  if (check_around_square(s, sq))
    return (1);
  s->map.set(&s->map, sq->x, sq->y);
  coord_by_directions(&mv->position, 0, 1, mv->direction);
  sq = s->map.get_square(&s->map, mv->position.x, mv->position.y);
  if (check_around_square(s, sq))
    return (1);
  s->map.set(&s->map, sq->x, sq->y);
  return (0);
}
