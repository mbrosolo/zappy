/*
** tasks_update.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Jul 15 22:33:49 2012 matthias brosolo
** Last update Sun Jul 15 22:45:03 2012 anna texier
*/

#include	"server.h"

void			launch_task_client(t_server *s, t_task *task, t_client *client)
{
  if (task->id == TAKE || task->id == PUT_DOWN
      || task->id == BROADCAST)
    {
      strncpy(client->com.read.current, task->buffer, BUFFER_SIZE);
      client->com.read.size = strlen(client->com.read.current);
    }
  if (s->client_cmd[task->id].fct(s, client))
    client->state = ST_TO_REMOVE;
  client->task = NULL;
}

void			update_task(t_server *s, t_list_iterator *it,
				    t_timeval *diff_time)
{
  t_task		*task;

  task = it->data;
  sub_timeval(&task->delay, diff_time->tv_sec, diff_time->tv_usec);
  if (task->delay.tv_sec <= 0 && task->delay.tv_usec <= 0)
    {
      if (task->owner_type == OT_CLIENT)
	launch_task_client(s, task, task->owner);
      else
	s->client_cmd[task->id].fct(s, task->owner);
      it->remove(it, &free);
      it->prev(it);
    }
}

int			update_tasks(t_server *s)
{
  t_timeval		diff_time;
  t_list_iterator	it;

  diff_time.tv_sec = s->time_ref.tv_sec;
  diff_time.tv_usec = s->time_ref.tv_usec;
  sub_timeval(&diff_time, s->time_remaining.tv_sec, s->time_remaining.tv_usec);
  s->task_list.begin(&s->task_list, &it);
  while (it.current(&it) != s->task_list.end(&s->task_list))
    {
      update_task(s, &it, &diff_time);
      it.next(&it);
    }
  return (0);
}
