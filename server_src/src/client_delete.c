/*
** client_delete.c for zappy in /home/brosol_m/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Jul 15 22:09:59 2012 matthias brosolo
** Last update Sun Jul 15 22:09:59 2012 matthias brosolo
*/

#include	"server.h"

/**
 * @brief Prepare a client to be disconnected
 * @param srv The server's data
 * @param client The client's data
 */
void		attempting_client(t_server *srv, t_client *client)
{
  printf("Attempting to delete client %lu...\n", client->player->id);
  FD_CLR(client->com.sock.fd, &srv->fds[F_READ]);
  if (client->state != ST_TO_REMOVE)
    {
      player_unknown_command(srv, client);
      client->state = ST_TO_REMOVE;
    }
}

void		unlink_player(t_server *srv, t_client *client)
{
  if (client->player)
    {
      client->player->client = NULL;
      if (!client->player->live)
	delete_player(srv, client->player);
      client->player = NULL;
    }
}

/**
 * @brief Substract a client to the clients_list
 * @param srv The server's data
 * @param client The client's data
 * @param it The iterator of clients_list
 */
void		delete_client(t_server *srv, t_client *client,
			      t_list_iterator *it)
{
  printf("Deleting client %lu...\n", client->player->id);
  delete_tasks_from_owner(srv, client);
  unlink_player(srv, client);
  FD_CLR(client->com.sock.fd, &srv->fds[F_WRITE]);
  FD_CLR(client->com.sock.fd, &srv->fds[F_READ]);
  it->remove(it, &free_client);
  it->prev(it);
}
