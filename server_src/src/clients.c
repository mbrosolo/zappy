/*
** client.c for zappy in /home/brosol_m/docs/systeme_unix/zappy/server_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Jun  1 07:45:15 2012 matthias brosolo
** Last update Tue Jun 12 17:35:30 2012 benjamin bourdin
*/

#include	"server.h"

/**
 * @file   clients.c
 * @Author matthias brosolo
 * @date   June, 2012
 * @brief  Clients utils
 */

/**
 * @brief Add a client to client's list
 * @param s Server data
 * @return Flag true or false
 */
int		add_client(t_server *s)
{
  t_client	*client;
  t_connect	sock;

  if (connect_init(&sock, ALL_HOST, s->port)
      || s->sock.accept(&s->sock, &sock) < 0
      || !(client = wmalloc(sizeof(*client))))
    return (1);
  if (init_com(&client->com, &sock))
    return (1);
  if (!(client->player = create_new_player(s, rand() % s->map.width,
					   rand() % s->map.height)))
    return (1);
  client->player->client = client;
  client->eob = EOB_LF;
  client->state = ST_CONNECTING;
  client->task = NULL;
  push_in_buffer(&client->com.write, WELCOME_MSG, REAL_SIZE(WELCOME_MSG),
		 client->eob);
  FD_SET(client->com.sock.fd, &s->fds[F_WRITE]);
  if (!s->client_list.push_back(&s->client_list, client)
      || create_timeout_task(s, client))
    return (1);
  return (0);
}

/**
 * @brief Free a client
 * @param data The client object
 */
void		free_client(void *data)
{
  t_client	*client;

  client = data;
  if (client->com.sock.destroy(&client->com.sock))
    perror("close(2)");
  delete_ringbuffer(&client->com.read.buffer);
  delete_ringbuffer(&client->com.write.buffer);
  free(client);
}

int		client_timeout(t_server *s, t_client *c)
{
  (void) s;
  (void) c;
  return (1);
}
