/*
** graphic_player_actions2.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 10:01:20 2012 anna texier
** Last update Sun Jul 15 10:15:18 2012 anna texier
*/

#include	"server.h"

int		graphic_player_drop(t_server *s, t_player *p, int resource)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "pdr %lu %d", p->id, resource);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  graphic_player_inventory_to_all_client(s, p);
  send_square_content_to_all_client(s, s->map.get_square(&s->map, p->x, p->y));
  return (0);
}

int		graphic_player_get(t_server *s, t_player *p, int resource)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "pgt %lu %d", p->id, resource);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  graphic_player_inventory_to_all_client(s, p);
  send_square_content_to_all_client(s, s->map.get_square(&s->map, p->x, p->y));
  return (0);
}

int		graphic_player_dead(t_server *s, t_player *p)
{
  char		buffer[32];
  ssize_t	size;

  size = snprintf(buffer, 28, "pdi %lu", p->id);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int			graphic_player_expulse(t_server *s, t_player *p)
{
  char			buffer[32];
  ssize_t		size;
  t_player		*tmp;
  t_square		*sq;
  t_list_iterator	it;

  size = snprintf(buffer, 28, "pex %lu", p->id);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  sq = s->map.get_square(&s->map, p->x, p->y);
  sq->players_list.begin(&sq->players_list, &it);
  while (it.current(&it) != sq->players_list.end(&sq->players_list))
    {
      tmp = it.data;
      if (tmp != p && ((tmp->status == PS_LIVING)
		       || (tmp->status == PS_WAITING && !tmp->live)))
	graphic_player_position_to_all_client(s, tmp);
      it.next(&it);
    }
  return (0);
}
