/*
** graphic_player_std.c for zappy in /home/brosol_m/zappy/server_src
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Sat Jul 14 15:00:15 2012 matthias brosolo
** Last update Sun Jul 15 22:55:45 2012 anna texier
*/

#include	"server.h"

int		send_all_new_player(t_server *s, t_client *c)
{
  t_player		*p;
  t_list_iterator	it;

  s->players_list.begin(&s->players_list, &it);
  while (it.current(&it) != s->players_list.end(&s->players_list))
    {
      p = it.data;
      if (p->status == PS_LIVING)
	graphic_player_new(s, c, p);
      it.next(&it);
    }
  return (0);
}

int		graphic_player_new(t_server *s, t_client *c, t_player *p)
{
  char		buffer[512];
  ssize_t	size;

  size = snprintf(buffer, 508, "pnw %lu %d %d %d %d %s",
		  p->id, p->x, p->y, get_graphic_direction(p->direction), p->level,
		  p->team->name);
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_player_new_to_all_client(t_server *s, t_player *p)
{
  char		buffer[512];
  ssize_t	size;

  size = snprintf(buffer, 508, "pnw %lu %d %d %d %d %s",
		  p->id, p->x, p->y, get_graphic_direction(p->direction),
		  p->level, p->team->name);
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}
