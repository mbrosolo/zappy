/*
** graphic_player_position.c for zappy in /home/sarglen/zappy/server_src/src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Jul 15 22:48:11 2012 anna texier
** Last update Sun Jul 15 22:49:02 2012 anna texier
*/

#include	"server.h"

int		get_graphic_direction(int direction)
{
  if (direction == UP)
    return (1);
  if (direction == LEFT)
    return (4);
  if (direction == DOWN)
    return (3);
  if (direction == RIGHT)
    return (2);
  return (0);
}

int		graphic_player_position(t_server *s, t_client * c, t_player *p)
{
  char		buffer[64];
  ssize_t	size;

  size = snprintf(buffer, 60, "ppo %lu %d %d %d", p->id, p->x, p->y,
		  get_graphic_direction(p->direction));
  send_to_client(c, buffer, size, &s->fds[F_WRITE]);
  return (0);
}

int		graphic_player_position_to_all_client(t_server *s, t_player *p)
{
  char		buffer[64];
  ssize_t	size;

  size = snprintf(buffer, 60, "ppo %lu %d %d %d", p->id, p->x, p->y,
		  get_graphic_direction(p->direction));
  send_to_all_graphic_client(s, buffer, size, &s->fds[F_WRITE]);
  return (0);
}
