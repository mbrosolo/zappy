//
// Lexer.cpp for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat Jun 16 15:00:32 2012 gaetan senn
// Last update Sun Jul 15 15:28:13 2012 tosoni_t
//

//#include	"LexerException.hh"
#include	"Lexer.hh"
#include	<algorithm>

Lexer::Lexer(std::string const &file):
  _identify(*this), _automatic(*this), _file(file), _position(0),
  _stack(new std::queue<Token>)
{
  this->_automatic.addState(0, &Lexer::_stateNextToken)
    .addState(1, &Lexer::_stateNextTokenString)
    .addState(2, &Lexer::_statenextTokenUnsignedInt)
    .addState(3, &Lexer::_stateNextTokenInt)
    .addState(4, &Lexer::_stateNextTokenBlank)
    .addState(5, &Lexer::_stateNextTokenOpenBraket)
    .addState(6, &Lexer::_stateNextTokenCloseBraket)
    .addState(7, &Lexer::_stateNextTokenComma)
    .addStateLink(0, 1, 1)
    .addStateLink(0, 2, 2)
    .addStateLink(0, 3, 3)
    .addStateLink(0, 4, 4)
    .addStateLink(0, 5, 5)
    .addStateLink(0, 6, 6)
    .addStateLink(0, 7, 7)
    .addStateLink(1, 0, 0)
    .addStateLink(2, 0, 0)
    .addStateLink(3, 0, 0)
    .addStateLink(4, 0, 0)
    .addStateLink(5, 0, 0)
    .addStateLink(6, 0, 0)
    .addStateLink(7, 0, 0);
}

std::string const	Lexer::getCurrentString() const
{
  return (this->_file.substr(this->_position));
}

void			Lexer::turnToToken()
{
  this->_automatic.run();
}

int			Lexer::_stateNextToken()
{
  if (this->_position >= this->_file.size())
    return (-1);
  return (this->_identify.identify());
}

void			Lexer::_addTokenUsing(std::string const &accept,
					      Token::eType type)
{
  int			end = this->_file.find_first_not_of(accept,
							    this->_position);
  std::string		token = this->_file.substr(this->_position,
						   end - this->_position);

  this->_stack->push(Token(token, type));
  this->_position += end - this->_position;
}

int			Lexer::_stateNextTokenString()
{
  this->_addTokenUsing("~`!@#$%^&*?()_+=/<>?\\:'\"[]|.AZERTYUIOPQSDFGHJKLMWXCVBNqwertyuioplkjhgfdsazxcvbnm0123456789", Token::STRING);
  return (0);
}

int			Lexer::_statenextTokenUnsignedInt()
{
  this->_addTokenUsing("0123456789", Token::UNSIGNED_INT);
  return (0);
}

int			Lexer::_stateNextTokenInt()
{
  this->_addTokenUsing("-0123456789", Token::INT);
  return (0);
}

int			Lexer::_stateNextTokenBlank()
{
  this->_position += this->_file.find_first_not_of(" ", this->_position) - this->_position;
  this->_stack->push(Token(" ", Token::BLANK));
  return (0);
}

int			Lexer::_stateNextTokenOpenBraket()
{
  this->_stack->push(Token("{", Token::OPEN_BRAKET));
  this->_position++;
  return (0);
}

int			Lexer::_stateNextTokenCloseBraket()
{
  this->_stack->push(Token("}", Token::CLOSE_BRAKET));
  this->_position++;
  return (0);
}

int			Lexer::_stateNextTokenComma()
{
  this->_stack->push(Token(",", Token::COMMA));
  this->_position++;
  return (0);
}

std::queue<Token> *Lexer::getStack()
{
  return (this->_stack);
}
