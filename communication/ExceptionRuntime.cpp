//
// ExceptionRuntime.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:48:58 2012 benjamin bourdin
// Last update Fri Jul 13 14:20:30 2012 anna texier
//

#include "ExceptionRuntime.hh"

// Exception -------------------------------------------------------------------

ExceptionRuntime::ExceptionRuntime(std::string const& what, std::string const& where)
  : std::runtime_error(what), _what(what), _where(where)
{
}

// Member functions ------------------------------------------------------------

char const*	ExceptionRuntime::what() const throw()
{
  return (this->_what.c_str());
}

std::string const&	ExceptionRuntime::where() const
{
  return (this->_where);
}
