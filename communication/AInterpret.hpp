//
// AInterpret.hpp for zappy in /home/tosoni_t/BitBucket/zappy/communication
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Sat Jul 14 16:26:31 2012 tosoni_t
// Last update Sun Jul 15 19:14:43 2012 benjamin bourdin
//

#ifndef __AINTERPRET__
#define __AINTERPRET__

/**
 * @author Gaetan Senn
 * @file IInterpret.hh
 */

#include <queue>
#include "Select.hh"
#include "Token.hh"
#include "ExceptionInterpret.hh"
#include "ExceptionLexer.hh"
#include "AClient.hh"
#include "ExceptionQuit.hh"

/*!
 * @brief Time Wait Task Command define
 */
#define TIMEWAITASKCOMMAND 20000

/*!
 * @brief tools namespace
 */
namespace tools
{
  /*!
   * @brief free Delete
   */
  template <typename TYPE>
  static bool			freeDelete(TYPE data, bool ret)
  {
    delete data;
    return (ret);
  }
}

/*!
 * @class AInterpret
 * @brief
 */
template <typename T, class TARGET>
class		AInterpret
{
protected:
  typedef bool		(TARGET::*action)(void *); /*!< function pointer on target action  */
  /*!
   * @brief data structure
   */
  typedef struct
  {
    action		function; /*!< an action */
    void		*args; /*!< arguments  */
    Select::eType	type; /*!<  type of Select */
  }			finfo;
  T			*_target; /*!< New target  */
  TARGET		*_original; /*!<  Original Target */
  std::queue<finfo *>	_mapaction; /*!<  Queue of map actions */
  std::list<finfo *>	_prioritycommand; /*!<  List of priority commands */
public:
  /*!
   * @brief AInterpret constructor
   */
  explicit	AInterpret(T *target, TARGET *original)
    : _target(NULL), _original(NULL), _mapaction(), _prioritycommand()
  {
    _target = target;
    _original = original;
  }
  /*!
   * @brief AInterpret destructor
   */
  virtual ~AInterpret(){};
  /*!
   * @brief AInterpret cpy Ctor
   */
  AInterpret(AInterpret const &);
  /*!
   * @brief AInterpret operator =
   */
  AInterpret const & operator=(AInterpret const &o);
  /*!
   * @brief adds command to priority list
   */
  virtual void			addPriorityCommand(action a, Select::eType t)
  {
    finfo		*f = new finfo;

    f->function  = a;
    f->type = t;
    f->args = NULL;
    _prioritycommand.push_back(f);
  }
  /*!
   * @brie Add an action to send to the actions map
   */
  virtual void			addSendFunction(action a, void *args)
  {
    finfo		*f = new finfo;

    f->function = a;
    f->args = args;
    f->type = Select::WRITE;
    _mapaction.push(f);
  }
  /*!
   * @brief Add an action to receive to the actions map
   */
  virtual void			addReceivedFunction(action a)
  {
    finfo		*f = new finfo;

    f->function = a;
    f->args= NULL;
    f->type = Select::READ;
    _mapaction.push(f);
  }
  /*!
   * @brief Add an action to the maps action
   */
  virtual void			addFunction(action a, Select::eType t)
  {
    finfo		*f = new finfo;

    f->function = a;
    f->type = t;
    _mapaction.push(f);
  }
  /*!
   * @brief Get most important command
   */
  bool				getPriorityCommand(std::queue<Token>	*pt)
  {
    finfo		       *data;

    if (pt)
      {
	typename std::list<finfo *>::iterator		it;
	for (it = _prioritycommand.begin(); it != _prioritycommand.end();++it)
	  {
	    data = *it;
	    if ((_original->*(data->function))(pt))
	      return (true);
	  }
      }
    return (false);
  }
  /*!
   * @brief try to read of send Tasked Command
   */
  virtual void			run(bool tryread)
  {
    std::queue<Token>				*pt;
    finfo					*data;
    size_t					timeout;
    bool					todel;

    while (true)
      {
	todel = true;
	pt = NULL;
	data = NULL;
	timeout = 0;
	if (!_mapaction.empty())
	  {
	    data = _mapaction.front();
	    if (data->type == Select::READ)
	      {
		while (!pt && timeout < TIMEWAITASKCOMMAND)
		  {
		    if (!_target->getCommand(true))
		      throw ExceptionInterpret("READ | Select error from readed tasked",
					       "void	AInterpret::run");
		    try{
		      pt = _target->getToken();
		    }
		    catch (ExceptionLexer &e) {
		      throw ExceptionInterpret(e.what(), e.where());
		    }
		    timeout++;
		  }
		if (pt && pt->empty())
		  {
		    delete pt;
		    throw ExceptionInterpret("READ | Command Empty",
					     "void	AInterpret::run");
		  }
		if (pt)
		  {
		    if (!(_original->*data->function)(pt))
		      {
			todel = false;
			if (!getPriorityCommand(pt))
			  throw ExceptionInterpret("READ | Command Error from pt",
						   "void	AInterpret::run");
		      }
		    else
		      _mapaction.pop();
		  }
	      }
	    if (data->type == Select::WRITE)
	      {
		(_original->*(data->function))(data->args);
		_target->sendCommand();
		_mapaction.pop();
	      }
	    if (todel)
	      delete data;
	  }
	else
	  {
	    if (!tryread)
	      return ;
	    if (!(this->_target->getCommand(false)))
	      throw ExceptionQuit("Select error" + std::string(strerror(errno)), "bool select()");
	    std::queue<Token>		*pt;
	    while ((pt = this->_target->getToken()))
	      {
		if(!_original->checkCommand(pt))
		  {
		    if (!getPriorityCommand(pt))
		      throw ExceptionInterpret("READ | Command Error from tasked command",
					       "void	AInterpret::run");
		  }
	      }
	    if (tryread)
	      return ;
	  }
      }
  }
  virtual void			start() = 0;
};

#endif
