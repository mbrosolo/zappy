//
// FactoryZappyCommand.hh for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 20 13:48:33 2012 gaetan senn
// Last update Sat Jul 14 16:49:22 2012 tosoni_t
//

#ifndef __FACTORY_COMMAND__
#define __FACTORY_COMMAND__

/**
 * @author Gaetan Senn
 * @file FactoryHeader.hh
 */

/*!
 * @brief namespace for zappy
 */
namespace CommandZappy
{
  /*!
   * @brief enum of zappy commands
   */
  typedef enum
    {
      avance,
      droite,
      gauche,
      voir,
      inventaire,
      prend_objet,
      pose_objet,
      expluse,
      broadcast_texte,
      incantation,
      fork,
      connect_nbr,
      mort,
      message,
      level
    }		eType;
  /*!
   * @brief enum to knowwhat you did with the commands! 
   */
  typedef enum
    {
      SEND,
      RECEIVED
    }		eWay;
}

#endif
