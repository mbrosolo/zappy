//
// Socket.hh for Client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun  6 16:47:20 2012 gaetan senn
// Last update Sun Jul 15 15:43:04 2012 tosoni_t
//

#ifndef		__SOCKET__
#define		__SOCKET__

/**
 * @author Gaetan Senn
 * @file Socket.hh
 */

#include	<map>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netdb.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>
#include	<iostream>
#include	<errno.h>
#include	<stdio.h>
#include	<string.h>
#include	"ExceptionSocket.hh"

/* CURRENTLY NOT IMPLEMENTED SOCKET ABOUT SERVER CONNECTION JUST THE CLIENT CONNECTION SOCKET */

/*!
 * @class Socket
 * @brief
 */
class		Socket
{
public:
  /*!
   * @brief enum of Socket Type
   */
  enum		eSocket
    {
      TCP
    };
  /*!
   * @brief enum of Type
   */
  enum		eType
    {
      CLIENT,
      SERVER
    };
private:
  int		_socket; /*!< socket  */
  eType		_type; /*!<  type of Connection */
  std::map<eSocket, std::string>	_relprotocol; /*!<  map of socket and host */
  std::string	_ip; /*!<  ip converted */
public:
  /*!
   * @brief constructor
   */
  Socket(Socket::eType type = CLIENT);
  /*!
   * @brief destructor
   */
  ~Socket();
  /*!
   * @brief creation of socket
   */
  int			createSocket(int domain = AF_INET,
				     int type = SOCK_STREAM,
				     eSocket protocol = TCP);
  /*!
   * @brief getSocket
   */
  int			getSocket() const;
  /*!
   * @brief connection with socket
   */
  void			connection(unsigned short family = AF_INET,
				   unsigned short port = 5555,
				   std::string const &host = "localhost");
  /*!
   * @brief setCapacity about server
   */
  void			setCapacity(unsigned int nb);
  /*!
   * @brief closeSocket
   */
  void			closeSocket() const;
  /*!
   * @brief tool for convertion
   */
  std::string const &	ipFromHost(std::string const &host);
};


#endif
