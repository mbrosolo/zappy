//
// ExceptionLoading.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:59:48 2012 benjamin bourdin
// Last update Sat Jul 14 16:40:06 2012 tosoni_t
//

#ifndef __EXCEPTION_INTERPRET_HH__
#define __EXCEPTION_INTERPRET_HH__

/**
 * @author Gaetan Senn
 * @file ExceptionLoading.hh
 */

#include	<string>
#include	"ExceptionRuntime.hh"

/*!
 * @class ExceptionInterpret
 * @brief Launches Exceptions for Interpretations
 */
class ExceptionInterpret : public ExceptionRuntime
{
public:
  /*!
   * @brief Exception Interpretation Ctor
   */
  ExceptionInterpret(std::string const &what = "", std::string const &where = "") throw();
  /*!
   * @brief Exception Interpretation Dtor
   */
  virtual ~ExceptionInterpret() throw();
};

#endif
