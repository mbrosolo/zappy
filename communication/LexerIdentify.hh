//
// LexerIdentify.hh for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat Jun 16 13:52:16 2012 gaetan senn
// Last update Sun Jul 15 15:32:05 2012 tosoni_t
//

#ifndef __LEXER_IDENTIFY__
#define __LEXER_IDENTIFY__

/**
 * @author Gaetan Senn
 * @file LexerIdentify.hh
 */

#include <sstream>
#include <iostream>
#include "Automatic.hpp"
#include "ExceptionLexer.hh"

/*!
 * @class Lexer
 * @brief Lexer class
 */
class		Lexer;

/*!
 * @class LexerIdentify
 * @brief Identifies for Lexer class
 */
class		LexerIdentify
{
  int		_ret; /*!<  saved return */
  Lexer		&_target; /*!< Lexer target  */
  std::string	_data; /*!< data string  */
  Automatic<LexerIdentify>	_automatic; /*!< LexerIdentify  */
public:
  /*!
   * @brief Lexer Identify Ctor
   */
  LexerIdentify(Lexer &);
  /*!
   * @brief Lexer Identify Dtor
   */
  ~LexerIdentify();
  /*!
   * @brief
   */
  int		identify();
private:
  /*!
   * @brief check if string is ok
   */
  int		_isString();
  /*!
   * @brief unsigned int check
   */
  int		_isUnsignedInt();
  /*!
   * @brief int check
   */
  int		_isInt();
  /*!
   * @brief blank check
   */
  int		_isBlank();
  /*!
   * @brief open bracket check
   */
  int		_isOpenBraket();
  /*!
   * @brief close bracket check
   */
  int		_isCloseBraket();
  /*!
   * @brief coma check
   */
  int		_isComma();
};

#endif
