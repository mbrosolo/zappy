//
// Automatic.hpp for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat Jun 16 13:20:09 2012 gaetan senn
// Last update Sun Jul 15 19:15:27 2012 benjamin bourdin
//

#ifndef __AUTOMATIC__
#define __AUTOMATIC__

/**
 * @author Gaetan Senn
 * @file Automatic.hpp
 */

#include <map>
#include <iostream>

/*!
 * @class Automatic
 * @brief
 */
template <typename T>
class		Automatic
{
  int		_currentState; /*!<  To determine current state */
  T		&_target; /*!< A target   */
  std::map<int, std::map<int, int> >	_statesLink; /*!<  A map which gives link statuses */
  std::map<int, int (T::*)()>	_states; /*!< A map of statuses */
public:
  /*!
   * @brief Automatic Ctor
   */
  Automatic(T &target):
    _currentState(0), _target(target), _statesLink(), _states() {}
  /*!
   * @brief Automatic Dtor
   */
  ~Automatic() {}
  /*!
   * @brief Add Status Link to the map
   */
  Automatic<T>		&addStateLink(int stateFrom,
				      int returnValue, int stateTo)
  {
    if (_statesLink.find(stateFrom) != _statesLink.end() && _statesLink[stateFrom].find(returnValue) != _statesLink[stateFrom].end())
      {
	std::cerr << "State Link already exist" << std::endl;
	throw ;
      }
    this->_statesLink[stateFrom][returnValue] = stateTo;
    return (*this);
  }
  /*!
   * @brief A status to map
   */
  Automatic<T>		&addState(int state, int (T::*action)())
  {
    if (_states.find(state) != _states.end())
      {
	std::cerr << "State function already exist" << std::endl;
	throw ;
      }
    this->_states[state] = action;
    return (*this);
  }
  /*!
   * @brief Resets current state
   */
  void			resetState()
  {
    this->_currentState = 0;
  }
  /*!
   * @brief Stops the automate
   */
  bool			stopAutomate() const
  {
    if (_currentState == -1)
      return (true);
    return (false);
  }
  /*!
   * @brief run automat with ordered states
   */
  void			run()
  {
    int			result;

    this->resetState();
    while (!stopAutomate())
      {
	if (_states.find(this->_currentState) == _states.end() ||
	    _statesLink.find(this->_currentState) == _statesLink.end())
	  {
	    std::cerr << "Wrong current state" << std::endl;
	    throw ;
	  }
	result = (this->_target.*(this->_states[this->_currentState]))();
	// if (result == -1) stop automat or continue
	if (result != -1)
	  {
	    // NORMALY RETURN 0 OF SECOND MAP SO CHECK IF EXIST
	    if (_statesLink[_currentState].find(result) ==
		_statesLink[_currentState].end())
	      {
		std::cerr << "Wrong next states" << std::endl;
		throw ;
	      }
	    this->_currentState = this->_statesLink[this->_currentState][result];
	  }
	else
	  this->_currentState = -1;
      }
  }
};

#endif
