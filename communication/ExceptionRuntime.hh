//
// ExceptionRuntime.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:47:42 2012 benjamin bourdin
// Last update Sat Jul 14 16:47:16 2012 tosoni_t
//

#ifndef __EXCEPTION_RUNTIME_HH__
#define __EXCEPTION_RUNTIME_HH__

/**
 * @author bourdi_b
 * @file ExceptionRuntime
 */

#include	<stdexcept>
#include	<exception>
#include	<string>

/*!
 * @class ExceptionRuntime
 * @brief Launches Runtime Exceptions
 */
class ExceptionRuntime : public std::runtime_error
{
public:
protected:
  std::string const	_what; /*!<  To store the kind of exception */
  std::string const	_where; /*!<  To store position of the exception */

public:
  /*!
   * @brief Exception Runtime Ctor
   */
  ExceptionRuntime(std::string const&, std::string const& = "");
  /*!
   * @brief Exception Runtime Dtor
   */
  virtual ~ExceptionRuntime() throw() {}

  /*!
   * @brief To know what is the exception
   */
  virtual char const*	what() const throw();
  /*!
   * @brief To know where is the exception
   */
  std::string const&	where() const;
};

#endif
