//
// Lexer.hh for Lexer in /home/tosoni_t/BitBucket/zappy
//
// Made by tosoni_t
// Login   <tosoni_t@epitech.net>
//
// Started on  Sat Jul 14 15:55:11 2012 tosoni_t
// Last update Sun Jul 15 15:28:11 2012 tosoni_t
//

#ifndef		__LEXER_HH__
#define		__LEXER_HH__

/**
 * @author Gaetan Senn
 * @file Lexer
 */

#include	<string>
#include	<queue>
#include	"Automatic.hpp"
#include	"LexerIdentify.hh"
#include	"Token.hh"

/*!
 * @class Lexer
 * @brief Lexer class to check stringsx
 */
class	Lexer
{
public:
  /*!
   * @brief Lexer Ctor
   */
  Lexer(std::string const &);
  /*!
   * @brief Lexer op =
   */
  Lexer const &operator=(Lexer const &);
  /*!
   * @brief Lexer cpy Ctor
   */
  Lexer(Lexer const &);
public:
  /*!
   * @brief Gets Current String
   */
  std::string const		getCurrentString() const;
  /*!
   * @brief turn String to Token
   */
  void				turnToToken();
  /*!
   * @brief Get queue of Tokens
   */
  std::queue<Token>		*getStack();

private:
  /*!
   * @brief Check next token state
   */
  int	_stateNextToken();
  /*!
   * @brief Check next token string state
   */
  int	_stateNextTokenString();
  /*!
   * @brief Check next token unsigned int state
   */
  int	_statenextTokenUnsignedInt();
  /*!
   * @brief Check next token int state
   */
  int	_stateNextTokenInt();
  /*!
   * @brief Check next blank token state
   */
  int	_stateNextTokenBlank();
  /*!
   * @brief Check next opening bracket token state
   */
  int	_stateNextTokenOpenBraket();
  /*!
   * @brief Check next closing bracket token state
   */
  int	_stateNextTokenCloseBraket();
  /*!
   * @brief Check next coma token state
   */
  int	_stateNextTokenComma();
  /*!
   * @brief Adds token
   */
  void	_addTokenUsing(std::string const &, Token::eType);

private:
  LexerIdentify			_identify;  /*!< Lexer Identifier  */
  Automatic<Lexer>		_automatic; /*!<  An automatic of Lexer */
  std::string			_file; /*!<  name of file */
  unsigned int			_position; /*!< position registered  */
  std::queue<Token>		*_stack; /*!< queue of Tokens  */
};

#endif
