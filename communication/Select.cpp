//
// Select.cpp for  in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 20 13:19:49 2012 gaetan senn
//

#include "Select.hh"

namespace Select {

  eType operator|(eType a, eType b)
  {
    return (static_cast<eType>
	    (static_cast<int>(a) | static_cast<int>(b)));
  }

  eType operator^(eType a, eType b)
  {
    return (static_cast<eType>
	    (static_cast<int>(a) ^ static_cast<int>(b)));
  }

  eType operator&(eType a, eType b)
  {
    return (static_cast<eType>
	    (static_cast<int>(a) & static_cast<int>(b)));
  }

}
