//
// ExceptionLoading.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:59:48 2012 benjamin bourdin
// Last update Sat Jul 14 16:47:51 2012 tosoni_t
//

#ifndef __EXCEPTION_SOCKET_HH__
#define __EXCEPTION_SOCKET_HH__

/**
 * @author bourdi_b
 * @file ExceptionLoading.hh
 */

#include	<string>
#include	"ExceptionRuntime.hh"

/*!
 * @class ExceptionSocket
 * @brief Launches socket exceptions
 */
class ExceptionSocket : public ExceptionRuntime
{
public:
  /*!
   * @brief ExceptionSocket Ctor
   */
  ExceptionSocket(std::string const &what = "", std::string const &where = "") throw();
  /*!
   * @brief ExceptionSocket Dtor
   */
  virtual ~ExceptionSocket() throw();
};

#endif
