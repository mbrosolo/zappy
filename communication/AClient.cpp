//
// AClient.cpp for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 20 11:23:51 2012 gaetan senn
// Last update Sun Jul 15 18:29:08 2012 gaetan senn
//

#include "AClient.hh"

AClient::AClient()
  : _socket(), _select(this), _ringbuffer(),
    _formateddata(), _identifiedcommand(), _identifiedmessage(),
    _identifiedlevel(), _towrite(), _blockread(true)
{
}

AClient::~AClient()
{
}

bool			AClient::getCommand(bool blockread)
{
  bool			select = true;

  _blockread = blockread;
  _select.addType(_socket.getSocket(), Select::READ);
  if (!blockread)
    select = _select.select(0, TIMENOASKEDCOMMAND);
  else
    select = _select.select(0, 0);
  _select.removeType(_socket.getSocket(), Select::READ);
  return (select);
}

void			AClient::sendCommand()
{
  _select.addType(_socket.getSocket(), Select::WRITE);
  _select.select(0, 0);
  _select.removeType(_socket.getSocket(), Select::WRITE);
}

void			AClient::checkData()
{
  std::string		data;
  char			tmp;

  while (_ringbuffer.getPosDelim('\n') != -1)
    {
      data.clear();
      while (_ringbuffer.popData(tmp) && tmp != '\n')
	data += tmp;
      std::cout << "Client : Received command => " << data << std::endl;
      _formateddata.push_back(data);
    }
}

void			AClient::addCommand(std::string const &data)
{
  _towrite.push_back(data);
}

void			*AClient::writeServer(void *)
{
  static int		* ret = new int;
  size_t		send;

  if (_towrite.empty())
    {
      *ret = -1;
      return (ret);
    }
  std::cout << "Client : Send command => " << _towrite.front() << std::endl;
  send = 0;
  while (send < _towrite.size())
    {
      *ret = ::send(_socket.getSocket(), _towrite.front().c_str(),
		    _towrite.front().size(), MSG_NOSIGNAL);
      if (*ret == -1)
	return (ret);
      send += *ret;
    }
  _towrite.pop_front();
  *ret = 1;
  return (ret);
}

std::list<std::map <CommandZappy::eType,
		    std::queue<Token> const > >	&AClient::getCommand()
{
  return (_identifiedcommand);
}

std::list<std::map <CommandZappy::eType,
		    std::queue<Token> const > >	&AClient::getCommandMessage()
{
  return (_identifiedmessage);
}

std::list<std::map <CommandZappy::eType,
		    std::queue<Token> const > >	&AClient::getCommandLevel()
{
  return (_identifiedlevel);
}

void			*AClient::readServer(void *)
{
  static int		* ret = new int;
  char			tmp[1025];
  int			i = 0;
  int			flags;

  if (_blockread)
    flags = MSG_NOSIGNAL;
  else
    flags = MSG_DONTWAIT;
  memset(tmp, 0, 1025);
  if (recv(_socket.getSocket(), tmp, 1024, flags) <= 0)
    *ret = -1;
  else
    {
      *ret = 1;
      while (tmp[i])
	{
	  _ringbuffer.pushData(tmp[i]);
	  i++;
	}
    }
  checkData();
  _blockread = true;
  return (ret);
}

std::queue<Token>	*AClient::getToken()
{
  if (!_formateddata.empty())
    {
      Lexer	    l(_formateddata.front());
      l.turnToToken();
      _formateddata.pop_front();
      return (l.getStack());
    }
  return (NULL);
}


void			AClient::addIdentifiedCommand(CommandZappy::eType type,
						      std::queue<Token>
						      const &data)
{
  _identifiedcommand.push_back(std::map<CommandZappy::eType,
					std::queue<Token> const >());
  (_identifiedcommand.back()).insert(_identifiedcommand.back().begin(),
				     std::pair<CommandZappy::eType,
					       std::queue<Token> const>(type,
									data));
}

void			AClient::addIdentifiedCommandMessage(std::queue<Token>
							     const data)
{
  _identifiedmessage.push_back(std::map<CommandZappy::eType,
					std::queue<Token> const >());
  (_identifiedmessage.back()).insert(_identifiedmessage.back().begin(),
				     std::pair<CommandZappy::eType,
					       std::queue<Token> const>(CommandZappy::message,
									data));
}

void			AClient::addIdentifiedCommandLevel(std::queue<Token>
							     const data)
{
  _identifiedlevel.push_back(std::map<CommandZappy::eType,
					std::queue<Token> const >());
  (_identifiedlevel.back()).insert(_identifiedlevel.back().begin(),
				     std::pair<CommandZappy::eType,
					       std::queue<Token> const>(CommandZappy::level,
									data));
}
