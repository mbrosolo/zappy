//
// ExceptionLoading.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:59:48 2012 benjamin bourdin
// Last update Sat Jul 14 16:40:58 2012 tosoni_t
//

#ifndef __EXCEPTION_RINGBUFFER_HH__
#define __EXCEPTION_RINGBUFFER_HH__

/**
 * @author Gaetan Senn
 * @file ExceptionRingBuffer.hh
 */

#include	<string>
#include	"ExceptionRuntime.hh"

/*!
 * @class ExceptionRingBuffer
 * @brief Launches Ring Buffer Exceptions
 */
class ExceptionRingBuffer : public ExceptionRuntime
{
public:
  /*!
   * @brief Exception Ring Buffer Constructor
   */
  ExceptionRingBuffer(std::string const &what = "", std::string const &where = "") throw();
  /*!
   * @brief Exception Ring Buffer Destructor
   */
  virtual ~ExceptionRingBuffer() throw();
};

#endif
