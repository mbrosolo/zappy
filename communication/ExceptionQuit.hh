//
// ExceptionQuit.hh for Zappy in /home/sarglen/zappy/graph_src
//
// Made by anna texier
// Login   <texier_a@epitech.net>
//
// Started on  Fri Jul 13 14:03:10 2012 anna texier
// Last update Sun Jul 15 18:21:30 2012 benjamin bourdin
//

#ifndef __EXCEPTION_QUIT_HH__
#define __EXCEPTION_QUIT_HH__

/**
 * @author Anna Texier
 * @file ExceptionQuit.hh
 */

#include	<string>
#include	"ExceptionRuntime.hh"

/*!
 * @class ExceptionQuit
 * @brief Exception to quit the programm
 */
class ExceptionQuit : public ExceptionRuntime
{
public:
  /*!
   * @brief Constructor
   * @param what Message to know what happend
   * @param where Message to know where it happend
   */
  ExceptionQuit(std::string const & = "", std::string const & = "") throw();
  /*!
   * @brief Destructor
   */
  virtual ~ExceptionQuit() throw();
};

#endif
