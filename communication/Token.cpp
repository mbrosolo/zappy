//
// Token.cpp for client zappy in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat Jun 16 12:46:02 2012 gaetan senn
// Last update Sun Jun 17 13:56:34 2012 gaetan senn
//


#include "Token.hh"

Token::Token(std::string const &data, eType type)
  : _data(data), _type(type)
{
}

Token::~Token()
{
}

Token	&Token::operator=(Token const &from)
{
  this->_data = from.getData();
  this->_type = from.getType();
  return (*this);
}

Token::eType		Token::getType() const
{
  return (_type);
}

std::string const &Token::getData() const
{
  return (_data);
}
