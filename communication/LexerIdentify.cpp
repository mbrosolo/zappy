//
// LexerIdentify.cpp for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat Jun 16 13:52:51 2012 gaetan senn
// Last update Sun Jul 15 15:32:03 2012 tosoni_t
//

#include "LexerIdentify.hh"
#include "Lexer.hh"
#include <algorithm>

LexerIdentify::LexerIdentify(Lexer &target)
  : _ret(0), _target(target), _data(""), _automatic(*this)
{
  _automatic.addState(0, &LexerIdentify::_isString)
    .addState(1, &LexerIdentify::_isUnsignedInt)
    .addState(2, &LexerIdentify::_isInt)
    .addState(3, &LexerIdentify::_isBlank)
    .addState(4, &LexerIdentify::_isOpenBraket)
    .addState(5, &LexerIdentify::_isCloseBraket)
    .addState(6, &LexerIdentify::_isComma)
    .addStateLink(0, 0, 1)
    .addStateLink(1, 0, 2)
    .addStateLink(2, 0, 3)
    .addStateLink(3, 0, 4)
    .addStateLink(4, 0, 5)
    .addStateLink(5, 0, 6)
    .addStateLink(6, 0, 0);
}

LexerIdentify::~LexerIdentify()
{
}

int		LexerIdentify::identify()
{
  _ret = -1;
  _data = _target.getCurrentString();
  _automatic.run();
  if (_ret == -1)
    throw ExceptionLexer("Invalid caracter", "int LexerIdentify::identity");
  return (this->_ret);
}

int		LexerIdentify::_isString()
{
  if (!isblank(_data[0]) && !isdigit(_data[0]) && isprint(_data[0]) &&
      _data[0] != '{' && _data[0] != '}' && _data[0] != ',')
    {
      _ret = 1;
      return (-1);
    }
  return (0);
}

int		LexerIdentify::_isUnsignedInt()
{
  if (_data[0] != '-' && _data[0] >= '0' && _data[0] <= '9')
    {
      _ret = 2;
      return (-1);
    }
  return (0);
}

int		LexerIdentify::_isInt()
{
  if ((_data[0] == '-' && _data[1] >= '0' && _data[1] <= '9')
      || (_data[0] >= '0' && _data[0] <= '9'))
    {
      _ret = 3;
      return (-1);
    }
  return (0);
}

int		LexerIdentify::_isBlank()
{
  if (_data[0] == ' ')
    {
     _ret = 4;
      return (-1);
    }
  return (0);
}

int		LexerIdentify::_isOpenBraket()
{
  if (_data[0] == '{')
    {
      _ret = 5;
      return (-1);
    }
  return (0);
}

int		LexerIdentify::_isCloseBraket()
{
  if (_data[0] == '}')
    {
      _ret = 6;
      return (-1);
    }
  return (0);
}

int		LexerIdentify::_isComma()
{
  if (_data[0] == ',')
    {
      _ret = 7;
      return (-1);
    }
  return (-1);
}
