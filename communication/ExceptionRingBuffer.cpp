//
// ExceptionLoading.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 07:02:02 2012 benjamin bourdin
// Last update Fri Jul 13 14:22:33 2012 anna texier
//

#include	"ExceptionRingBuffer.hh"

ExceptionRingBuffer::ExceptionRingBuffer(std::string const &what, std::string const &where) throw()
: ExceptionRuntime(what, where)
{
}

ExceptionRingBuffer::~ExceptionRingBuffer() throw()
{
}
