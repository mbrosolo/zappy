//
// ExceptionLoading.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:59:48 2012 benjamin bourdin
// Last update Sat Jul 14 16:39:39 2012 tosoni_t
//

#ifndef __EXCEPTION_LEXER_HH__
#define __EXCEPTION_LEXER_HH__

/**
 * @author Gaetan Senn
 * @file ExceptionLoading.hh
 */

#include	<string>
#include	"ExceptionRuntime.hh"

/*!
 * @class ExceptionLexer
 * @brief Launches Lexer Exceptions
 */
class ExceptionLexer : public ExceptionRuntime
{
public:
  /*!
   * @brief ExceptionLexer Ctor
   */
  ExceptionLexer(std::string const &what = "", std::string const &where = "") throw();
  /*!
   * @brief ExceptionLexer Dtor
   */
  virtual ~ExceptionLexer() throw();
};

#endif
