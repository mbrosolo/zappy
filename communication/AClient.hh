//
// AClient.hh for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 20 11:18:37 2012 gaetan senn
// Last update Sun Jul 15 15:22:01 2012 tosoni_t
//

#ifndef __ACLIENT__
#define __ACLIENT__

/**
 * @author Gaetan Senn
 * @file AClient.hh
 */

#include <iostream>
#include <list>
#include <queue>
#include <iostream>

#include "Socket.hh"
#include "Select.hh"
#include "RingBuffer.hpp"
#include "Token.hh"
#include "Lexer.hh"
#include "FactoryHeader.hh"

/*!
 * @brief Time Nb Aske Command
 */
#define TIMENOASKEDCOMMAND	(1000)

/*!
 * @class AClient
 * @brief Abstract Client
 */
class				AClient
{
protected:
  Socket			_socket; /*!<  A client socket */
  Select::Select<AClient>	_select; /*!<  A client select */
  RingBuffer<char>		_ringbuffer; /*!< A ring buffer */
  std::list<std::string >	_formateddata; /*!<  Formated data */
  std::list<std::map <CommandZappy::eType,
		      std::queue<Token> const > >	_identifiedcommand; /*!<  A map of identified commands*/
  std::list<std::map <CommandZappy::eType,
		      std::queue<Token> const > >	_identifiedmessage; /*!<  A map of identified messages*/
    std::list<std::map <CommandZappy::eType,
		      std::queue<Token> const > >	_identifiedlevel; /*!<  A map of identified Level*/
  std::list<std::string >	_towrite; /*!<  A list of strings to write*/
  bool				_blockread; /*!<  A read blocker */
public:
  /*!
   * @brief AClient Ctor
   */
  explicit			AClient();
  /*!
   * @brief AClient Dtor
   */
  virtual			~AClient();
  /*!
   * @brief Command getter
   */
  bool				getCommand(bool);
  /*!
   * @brief Command sender
   */
  void				sendCommand();
  /*!
   * @brief checkData on RingBuffer
   */
  void				checkData();
  /*!
   * @brief adds a command to the list
   */
  void				addCommand(std::string const &data);
  /*!
   * @brief writes to server
   */
  void				*writeServer(void *);
  /*!
   * @brief reads on server
   */
  void				*readServer(void *);
  /*!
   * @brief get Token and returns a queue of Tokens
   */
  std::queue<Token>		*getToken();
  /*!
   * @brief initialisation of client connection
   */
  virtual void			launch(int port,std::string const &hostname) = 0;
  /*!
   * @brief getIdentified Command
   */
  std::list<std::map <CommandZappy::eType,
		      std::queue<Token> const > >	&getCommand();
  /*!
   * @brief get Identified message
   */
  std::list<std::map <CommandZappy::eType,
		      std::queue<Token> const > >	&getCommandMessage();
    /*!
   * @brief get Level message
   */
  std::list<std::map <CommandZappy::eType,
		      std::queue<Token> const > >	&getCommandLevel();
  /*!
   * @brief add Identified Command
   */
  void				addIdentifiedCommand(CommandZappy::eType,
						     std::queue<Token>
						     const &);
  /*!
   * @brief add Identified Message
   */
  void				addIdentifiedCommandMessage(std::queue<Token> const );
    /*!
   * @brief add Identified Level Up
   */
  void				addIdentifiedCommandLevel(std::queue<Token> const );
};

#endif
