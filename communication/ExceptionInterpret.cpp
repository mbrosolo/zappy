//
// ExceptionLoading.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 07:02:02 2012 benjamin bourdin
// Last update Fri Jul 13 14:20:56 2012 anna texier
//

#include	"ExceptionInterpret.hh"

ExceptionInterpret::ExceptionInterpret(std::string const &what, std::string const &where) throw()
: ExceptionRuntime(what, where)
{
}

ExceptionInterpret::~ExceptionInterpret() throw()
{
}
