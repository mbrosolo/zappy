//
// Socket.cpp for client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun  6 16:48:11 2012 gaetan senn
// Last update Sun Jul 15 19:16:37 2012 benjamin bourdin
//

#include "Socket.hh"


Socket::Socket(eType type)
  : _socket(0), _type(type), _relprotocol(), _ip()
{
  _relprotocol[TCP] = "TCP";
}

Socket::~Socket()
{
}

int			Socket::createSocket(int domain, int type,
					     eSocket protocol)
{
  struct protoent	*proto;

  if ((proto = getprotobyname(_relprotocol[protocol].c_str())) == NULL)
    {
      throw ExceptionSocket("resolution of protocol fail",
			    "Socket::createSocket(int domain, \
int type, eSocket protocol)");
    }
  if ((this->_socket = socket(domain, type, proto->p_proto)) == -1)
    {
      throw ExceptionSocket(strerror(errno),
			    "Socket::createSocket(int domain, \
int type, eSocket protocol)");
    }
  return (_socket);
}

void		Socket::connection(unsigned short family,
			unsigned short port, std::string const &host)
{
  int			rconnect;
  struct sockaddr_in	sockinfo;
  in_addr_t		ipaddr;

  if (_socket == -1)
    {
      std::cerr << "Connection error please create Socket before" << std::endl;
      return ;
    }
  /* GET HOST NAME */
  ipFromHost(host);
  sockinfo.sin_family = family;
  sockinfo.sin_port = htons(port);
  if (_ip.empty())
    sockinfo.sin_addr.s_addr = INADDR_ANY;
  else
    {
      if ((ipaddr = inet_addr(_ip.c_str())) == INADDR_NONE)
	{
	  this->closeSocket();
	  throw ExceptionSocket(strerror(errno),
				"Socket::connection(unsigned short family, unsigned short port, std::string const &host)");
	}
      else
	sockinfo.sin_addr.s_addr = ipaddr;
    }
  if ((rconnect = connect(_socket,
			  (struct sockaddr *)&sockinfo,
			  sizeof(sockinfo))) == -1)
    {
      this->closeSocket();
      	  throw ExceptionSocket(strerror(errno),
				"Socket::connection(unsigned short family, unsigned short port, std::string const &host)");
    }
}

int		Socket::getSocket() const
{
  return (_socket);
}

void		Socket::closeSocket() const
{
  if (close(_socket) == -1)
    throw ExceptionSocket(strerror(errno), "Socket::closeSocket()");
}

std::string const	&Socket::ipFromHost(std::string const &host)
{
  struct	hostent		*he;
  struct in_addr		**addr_list;

  if ((he = gethostbyname(host.c_str())) == NULL)
    {
      throw ExceptionSocket(hstrerror(h_errno),
			    "Socet::closeSocket()");
    }
  addr_list = (struct in_addr **)he->h_addr_list;
  _ip = inet_ntoa(*addr_list[0]);
  return (_ip);
}
