//
// Token.hh for client zappy in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Sat Jun 16 11:20:44 2012 gaetan senn
// Last update Sat Jul 14 17:54:14 2012 tosoni_t
//

#ifndef __TOKEN__
#define __TOKEN__

/**
 * @author Gaetan Senn
 * @file Token.hh
 */

#include <iostream>

/*!
 * @class Token
 * @brief Token class
 */
class		Token
{
public:
  /*!
   * @brief enum on type of tokens
   */
  typedef enum
    {
      STRING,
      UNSIGNED_INT,
      INT,
      BLANK,
      OPEN_BRAKET,
      CLOSE_BRAKET,
      COMMA
    }			eType;
private:
  std::string	_data;  /*!<  data string  */
  eType		_type; /*!<  type enum */
public:
  /*!
   * @brief Token Ctor
   */
  Token(std::string const &_data, eType type);
  /*!
   * @brief Token Dtor
   */
  ~Token();
  /*!
   * @brief Returns Type
   */
  eType		getType() const;
  /*!
   * @brief Get data string
   */
  std::string const &getData() const;
  /*!
   * @brief Get operator =
   */
  Token	&operator=(Token const &);
};


#endif
