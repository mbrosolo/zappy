//
// Select.hh for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Tue Jun 12 10:14:18 2012 gaetan senn
// Last update Sun Jul 15 19:16:19 2012 benjamin bourdin
//

#ifndef __SELECT_FD__
#define __SELECT_FD__

/**
 * @author Gaetan Senn
 * @file Select.hh
 */

#include	<map>
#include	<errno.h>
#include	<sys/select.h>
#include	<sys/time.h>
#include	<sys/types.h>
#include	<unistd.h>
#include	<cstdlib>
#include	<cstdio>
#include	<iostream>
#include	<string.h>
#include	"ExceptionQuit.hh"

/*!
 * @brief namespace for the select
 */
namespace Select
{
  /*!
   * @brief kind of select
   */
  typedef enum
    {
      READ = 2,
      WRITE = 4,
      ERROR = 6,
      EMPTY = 8,
      READNOASKED = 16
    }			eType;
  /*!
   * @class Templated Select Class
   * @brief Class to use select() properly
   */
  template <typename T>
  class		Select
  {
    typedef	void		*(T::*ptfd)(void *); /*!< Function pointer on templated select  */
  public:
    /*!
     * @brief
     */
    typedef struct
    {
      ptfd	read; /*!<   */
      ptfd	write; /*!<   */
      ptfd	error; /*!<   */
      eType	type; /*!<   */
    }			sData;
  private:
    std::map<int, sData >		fdlist; /*!<   */
    fd_set			rds; /*!<   */
    fd_set			wds; /*!<   */
    fd_set			eds; /*!<   */
    T				*_class; /*!<   */
  public:
    /*!
     * @brief Select Ctor
     */
    Select(T *Class)
      : fdlist(), rds(), wds(), eds(), _class(Class)
    {
    };
    /*!
     * @brief Select Dtor
     */
    ~Select()
    {
    };
    /*!
     * @brief Select operator =
     */
    Select const & operator=(Select const &o)
    {
      fdlist = o.fdlist;
      rds = o.rds;
      wds = o.wds;
      eds = o.eds;
      _class = o._class;
    }
    /*!
     * @brief Select cpy ctor
     */
    Select(Select const &o)
      : fdlist(o.fdlist), rds(o.rds), wds(o.wds), eds(o.eds), _class(o._class)
    {
    }
    /*!
     * @brief Add type to fd list
     */
    bool		addType(int fd, eType Type)
    {
      if (fdlist.find(fd) == fdlist.end())
	return (false);
      if ((fdlist[fd].type & Type) != Type)
	{
	  fdlist[fd].type = fdlist[fd].type ^ Type;
	  return (true);
	}
      return (false);
    }
    /*!
     * @brief remove from fd list with type
     */
    bool		removeType(int fd, eType Type)
    {
      if (fdlist.find(fd) == fdlist.end())
	return (false);
      if ((fdlist[fd].type & Type) == Type)
	{
	  fdlist[fd].type = fdlist[fd].type ^ Type;
	  return (true);
	}
      return (false);
    }
    /*!
     * @brief add fd to fd list of
     */
    bool		addFd(int fd, ptfd read, ptfd write, ptfd error)
    {
      if (fdlist.find(fd) != fdlist.end())
	return (false);
      fdlist[fd].read = read;
      fdlist[fd].write = write;
      fdlist[fd].error = error;
      fdlist[fd].type = EMPTY;
      return (true);
    }
    /*!
     * @brief Remove a fd from the list
     */
    bool		removeFd(int fd)
    {
      typename std::map<int, sData>::iterator		it;

      if ((it = fdlist.find(fd)) == fdlist.end())
	return (false);
      fdlist.erase(it);
      return (true);
    };
    /*!
     * @brief Uses select function with timeout
     */
    bool		select(size_t sec, size_t usec)
    {
      int		res;
      bool		ret;
      struct timeval	tv;

      tv.tv_sec = sec;
      tv.tv_usec = usec;
      if (fdlist.empty())
	{
	  std::cerr << "No fd set" << std::endl;
	  return (false);
	}
      ret = fdSet();
      if (ret)
	{
	  res = ::select(getMax(), &rds, &wds, &eds,
			 ((sec != 0 || usec != 0) ? (&tv) : (NULL)));
	  if (res == -1)
	    {
	      std::string	toto(strerror(errno));
	      throw ExceptionQuit("Select error" + toto, "bool select()");
	    }
	  if (!assignSelect())
	    {
	      std::cerr << "Select pointer function error" << std::endl;
	      return (false);
	    }
	}
      return (true);
    }
    /*!
     * @brief Sets all fds
     */
    bool		fdSet()
    {
      typename std::map<int,  sData>::iterator		it;
      bool		selectok = false;

      FD_ZERO(&rds);
      FD_ZERO(&wds);
      FD_ZERO(&eds);
      for (it = fdlist.begin(); it != fdlist.end() ;++it)
	{
	  if (((*it).second.type & READ) == READ)
	    {
	      selectok = true;
	      FD_SET(it->first, &rds);
	    }
	  if (((*it).second.type & WRITE) == WRITE)
	    {
	      selectok = true;
	      FD_SET(it->first, &wds);
	    }
	  if (((*it).second.type & ERROR) == ERROR)
	    {
	      selectok = true;
	      FD_SET(it->first, &eds);
	    }
	}
      return (selectok);
    }
    /*!
     * @brief Sets the type
     */
    bool		setType(int fd,  eType type)
    {
      typename std::map<int, sData>::iterator		it;
      if ((it = fdlist.find(fd)) == fdlist.end())
	return false;
      if (it->second.type & type != type)
	{
	  it->second = (it->second | type);
	  return (true);
	}
      return (false);
    }
    /*!
     * @brief Gets fd maximum
     */
    int		getMax()
    {
      int		max = 0;
      typename std::map<int,  sData>::iterator		it;

      for(it = fdlist.begin(); it != fdlist.end();++it)
	{
	  if (it->first > max)
	    max = it->first;
	}
      return (max + 1);
    }
    /*!
     * @brief find file descriptor setted and apply pt function
     */
    bool		assignSelect()
    {
      typename std::map<int, sData>::iterator			it;

      for (it = fdlist.begin(); it != fdlist.end(); ++it)
	{
	  if ((it->second.type & READ) == READ)
	    {
	      if (FD_ISSET(it->first, &rds) && it->second.read)
		if ((*(int *)
		     ((_class->*(it->second.read))
		      ((void *)&(it->first)))) == -1)
		  return (false);
	    }
	  if ((it->second.type & WRITE) == WRITE)
	    {
	      if (FD_ISSET(it->first, &wds) && it->second.write)
		if ((*(int *)
		     ((_class->*(it->second.write))
		      ((void *)&(it->first)))) == -1)
		  return (false);
	    }
	  if ((it->second.type & ERROR) == ERROR)
	    {
	      if (FD_ISSET(it->first, &eds) && it->second.error)
		if ((*(int *)
		     ((_class->*(it->second.error))
		      ((void *)&(it->first)))) == -1)
		  return (false);
	    }
	}
      return (true);
    }
  };
  /*!
   * @brief | operator on type
   */
  eType operator|(eType a, eType b);
  /*!
   * @brief ^ operator on type
   */
  eType operator^(eType a, eType b);
  /*!
   * @brief & operator on type
   */
  eType operator&(eType a, eType b);
}

#endif
