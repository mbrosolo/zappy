//
// RingBuffer.hh for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Fri Jun  8 16:51:57 2012 gaetan senn
// Last update Sun Jul 15 21:05:15 2012 gaetan senn
//

#ifndef __RINGBUFFER__
#define __RINGBUFFER__

/**
 * @author Gaetan Senn
 * @file RingBuffer.hh
 */

#include	<vector>
#include	"ExceptionRingBuffer.hh"

/*!
 * @brief Default buffer size
 */
#define		BUFSIZE	(1024)

/*!
 * @class RingBuffer
 * @brief Ring Buffer Class
 */
template <typename T>
class			RingBuffer
{
  /*!
   * @brief kind of buffer
   */
  enum		eType
    {
      READER,
      WRITER
    };
  std::vector<T *>			_data;  /*!<  Vector of data  */
  size_t				_readvectpos; /*!<  Position read vector */
  size_t				_readbuffpos; /*!< Position read buffer  */
  size_t				_writevectpos; /*!<  Position write vector */
  size_t				_writebuffpos; /*!<  Position write buffer */
public:
  /*!
   * @brief RingBuffer Constructor
   */
  RingBuffer()
    :  _data(), _readvectpos(0), _readbuffpos(0), _writevectpos(0),
      _writebuffpos(0)
  {
  }
  /*!
   * @brief Ring Buffer Dtor
   */
  ~RingBuffer()
  {
    typename std::vector<T *>::iterator			it;
    for (it = _data.begin(); it != _data.end();++it)
      delete *it;
  }
  /*!
   * @brief check next in circular buffer with type
   */
  size_t		circularnext(eType type)
  {
    size_t		check;

    if (type == READER)
      check = _readvectpos + 1;
    else
      check = _writevectpos + 1;
    if (_data.size() == check)
      return (0);
    return (check);
  }
  /*!
   * @brief check next in circular buffer with value
   */
  size_t		circularnext(size_t value)
  {
    size_t		check;

    check = value + 1;
    if (_data.size() == check)
      return (0);
    return (check);
  }
  /*!
   * @brief go to next circular with type
   */
  bool			goNext(eType type)
  {
    size_t		check;

    if (type == READER)
      check = _readbuffpos + 1;
    else
      check = _writebuffpos + 1;
    if (check == BUFSIZE)
      return (true);
    return (false);
  }
  /*!
   * @brief go to next circular with value
   */
  bool			goNext(size_t value)
  {
    size_t		check;

    check = value + 1;
    if (check == BUFSIZE)
      return (true);
    return (false);
  }
  /*!
   * @brief Add data to ring buffer
   */
  bool			pushData(T c)
  {
    if (_data.empty())
      {
	try {
	  _data.push_back(new T[BUFSIZE]);
	}
	catch (std::bad_alloc & e) {
	  throw ExceptionRingBuffer("pushData(T)", "on add new data from ringbuffer");
	}
      }
    _data[_writevectpos][_writebuffpos] = c;
    if (goNext(WRITER))
      {
	if (circularnext(_writevectpos) == _readvectpos)
	  {
	    if (circularnext(_writevectpos) == 0)
	      {
		try {
		  _data.push_back(new T[BUFSIZE]);
		}
		catch (std::bad_alloc & e) {
		  throw ExceptionRingBuffer("pushData(T)", "on add new data from ringbuffer");
		}
	      }
	    else
	      {
		try {
		  _data.insert(_data.begin() + _writevectpos + 1, new T[BUFSIZE]);
		  _readvectpos++;
		}
		catch (std::bad_alloc & e) {
		  throw ExceptionRingBuffer("pushData(T)", "on add new data from ringbuffer");
		}
	      }
	    _writevectpos++;
	    _writebuffpos = 0;
	  }
	else
	  {
	    _writevectpos = circularnext(_writevectpos);
	    _writebuffpos = 0;
	  }
      }
    else
      _writebuffpos++;
    return (true);
  }
  /*!
   * @brief Pops data from circular buffer
   */
  bool			popData(T &c)
  {
    if (_readvectpos == _writevectpos && _writebuffpos == _readbuffpos)
      return (false);
    c = _data[_readvectpos][_readbuffpos];
    if (goNext(READER))
      {
	_readvectpos = circularnext(_readvectpos);
	_readbuffpos = 0;
      }
    else
      _readbuffpos++;
    return (true);
  }
  /*!
   * @brief get delimited position
   */
  int			getPosDelim(T delim)
  {
    size_t		pos = 0;
    size_t		_tmpreadvectpos = _readvectpos;
    size_t		_tmpreadbuffpos = _readbuffpos;

    while (true)
      {
	if (_tmpreadvectpos == _writevectpos &&
	    _writebuffpos == _tmpreadbuffpos)
	  return (-1);
	if (_data[_tmpreadvectpos][_tmpreadbuffpos] == delim)
	  return (pos);
	pos++;
	if (goNext(_tmpreadbuffpos))
	  {
	    _tmpreadvectpos = circularnext(_tmpreadvectpos);
	    _tmpreadbuffpos = 0;
	  }
	else
	  _tmpreadbuffpos++;
      }
  }
};

#endif
