/*
** common.h for zappy in /home/gressi_b/zappy
**
** Made by gressi_b
** Login   <gressi_b@epitech.net>
**
** Started on  Tue May 15 14:06:58 2012 gressi_b
** Last update Tue May 15 14:06:58 2012 gressi_b
*/

#ifndef	__COMMON_ZAPPY_H__
#define	__COMMON_ZAPPY_H__

#include <signal.h>

#include "lib/connect/src/connect.h"
#include "lib/object_list/src/list.h"
#include "lib/ringbuffer/src/ringbuffer.h"
#include "lib/warn/warn.h"

/*
** Typedef
*/
typedef __sighandler_t	sighandler_t;

/*
** DEFINES
*/
#define	C_LF			'\n'
#define	C_CR			'\r'
#define	IS_OPT(a)		(a != NULL && a[0] == '-')
#define	CHECK_SIG(a)		((a) == SIG_ERR)

/*
** ENUMS
*/
enum
  {
    R_FOOD,
    R_LINEMATE,
    R_DERAUMERE,
    R_SIBUR,
    R_MENDIANE,
    R_PHIRAS,
    R_THYSTAME,
    R_NB_RESOURCE
  };

enum
  {
    C_PLAYER = R_NB_RESOURCE
  };

#endif
