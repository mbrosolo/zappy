//
// AIAlgorithm.cpp for zappy in /home/gressi_b/Epitech/B4/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Thu Jun 28 14:21:47 2012 gressi_b
// Last update Sun Jul 15 21:36:11 2012 benjamin bourdin
//

#include <iostream>
#include <err.h>

#include "AIAlgorithm.hh"
#include "AIEngine.hh"
#include "InterpretCommand.hpp"
#include "Select.hh"
#include "ClientIA.hh"
#include "signal.hh"

namespace print_info
{
  void where(char const * const name, const size_t level, const size_t nbPlayers)
  {
    std::cout << "\033[33m[Function] \033[35m" << name << "\033[0m with a level of \033[35m" << level << "\033[0m and nbPlayers=" << nbPlayers << std::endl;
  }
}

namespace signal_config
{
  bool signal_g = false;

  void signal_handler(int)
  {
    signal_g = true;
  }
}

ai::Vertex*
ai::start(Vertex* v, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  if (client.isDeathProgrammated())
    return NULL;

  sig::setHandler(SIGINT, &signal_config::signal_handler);
  sig::setHandler(SIGPIPE, &signal_config::signal_handler);
  if (signal_config::signal_g)
    {
      std::cout << "Signal catched" << std::endl;
      client.sendPlayer(ClientIA::MsgSend::MSG_PLAYER_DEC,
			ClientIA::MsgSend::MSG_STONE_DEC, c);
      return NULL;
    }

  while (!client.getCommand().empty())
    client.getCommand().pop_front();

  // the first time we come, we are polite, therefore we say 'hello'
  if (!client.hasSaidHello())
    {
      std::cout << "Hello everybody !" << std::endl;
      c.addSendFunction(&InterpretCommand<ClientIA>::voirSend, NULL);
      c.addReceivedFunction(&InterpretCommand<ClientIA>::voirReceived);
      c.run(false);
      std::queue<Token> listToken = client.getListToken(CommandZappy::voir, client.getCommand());
      if (!listToken.empty())
	{
	  std::vector< std::list<std::string> >		items;
	  size_t n = 0;
	  listToken.pop();
	  while (!listToken.empty())
	    {
	      std::list<std::string>			ret = client.getListItem(&listToken);
	      if (!ret.empty())
		items.push_back(ret);
	      if (listToken.front().getType() == Token::CLOSE_BRAKET)
		break;
	    }
	  n = items.size();

	  size_t line = 3, max = line + 1;
	  while (max < n)
	    {
	      line += 2;
	      max += line;
	      client.incLevel();
	    }
	  client.sayHello(c);

	  std::cout << "I am level " << client.getLevel() << std::endl;
	}
      if (!client.getCommand().empty())
	client.getCommand().pop_front();
    }

  size_t food = client.getFood(c);
  std::cout << "My life is " << food << std::endl;

  // if food < 3, we say it's over, and we leave
  if (food < 3)
    {
      client.sendPlayer(ClientIA::MsgSend::MSG_PLAYER_DEC,
			ClientIA::MsgSend::MSG_STONE_DEC, c);
      client.deathProgrammated();
    }

  // here we set a little system to make a supply of foods first
  if (client.isFirstReserve() == true && food == ClientIA::RECOMMENDED_FOOD_LEVEL_FIRST)
    client.notFirstReserve();

  if ((client.isFirstReserve() == true && food < ClientIA::RECOMMENDED_FOOD_LEVEL_FIRST)
      || (client.isFirstReserve() == false && food < ClientIA::RECOMMENDED_FOOD_LEVEL))
    return yes;
  return no;
}

ai::Vertex*
ai::isFoodOnSquare(Vertex* v, interpreter_t & c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  c.addSendFunction(&InterpretCommand<ClientIA>::voirSend, NULL);
  c.addReceivedFunction(&InterpretCommand<ClientIA>::voirReceived);
  c.run(false);
  std::queue<Token> listToken = client.getListToken(CommandZappy::voir, client.getCommand());
  if (!listToken.empty())
    {
      std::vector< std::list<std::string> > items = client.extractVoirInventaire(&listToken);
      std::list<std::string> listElem = client.onTheCase(items);
      std::list<std::string>::const_iterator it2;
      for (it2 = listElem.begin(); it2 != listElem.end(); ++it2)
	{
	  if (*it2 == "nourriture")
	    {
	      if (!client.getCommand().empty())
		client.getCommand().pop_front();
	      return yes;
	    }
	}
      if (!client.getCommand().empty())
	client.getCommand().pop_front();
    }
  return no;
}

ai::Vertex*
ai::takeFood(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  c.addSendFunction(&InterpretCommand<ClientIA>::prend_objetSend,
		    new std::string("nourriture"));
  c.addReceivedFunction(&InterpretCommand<ClientIA>::prend_objetReceived);
  c.run(false);
  if (!client.getCommand().empty())
    client.getCommand().pop_front();
  return AIEngine::getInstance().getStart();
}

ai::Vertex*
ai::searchFood(Vertex* v, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  c.addSendFunction(&InterpretCommand<ClientIA>::voirSend, NULL);
  c.addReceivedFunction(&InterpretCommand<ClientIA>::voirReceived);
  c.run(false);

  std::queue<Token> listToken = client.getListToken(CommandZappy::voir, client.getCommand());
  if (!listToken.empty())
    {
      std::vector< std::list<std::string> > items = client.extractVoirInventaire(&listToken);
      if (!items.empty())
	{
	  unsigned short sightSide = 1; // = nb case on a line (multiple of 3)
	  unsigned short line = 1; // = current pos on a line

	  // here we analyze all cases and if food present, we set the direction
	  // (right/left/ahead), knowing that ahead is prioritary
	  client.setDirection(ClientIA::UNSET);

	  for (std::vector< std::list<std::string> >::iterator it = items.begin();
	       it != items.end(); ++it)
	    {
	      for (std::list<std::string>::iterator it2 = (*it).begin();
		   it2 != (*it).end(); ++it2)
		{
		  if (*it2 == "nourriture")
		    {
		      if (line <= (sightSide / 2) && client.getDirection() == ClientIA::UNSET)
			client.setDirection(ClientIA::LEFT);
		      else if (line == (sightSide / 2) + 1) // priority ahead
			client.setDirection(ClientIA::UP);
		      else if (client.getDirection() == ClientIA::UNSET)
			client.setDirection(ClientIA::RIGHT);
		    }
		}
	      if (line == sightSide)
		{
		  sightSide += 2;
		  line = 0; // est mis a 0 car + 1 juste apres
		}
	      ++line;
	    }
	}
      if (!client.getCommand().empty())
	client.getCommand().pop_front();
    }
  if (client.getDirection() == ClientIA::UNSET)
    return no; // we found nothing
  return yes;
}

ai::Vertex*
ai::headForClosestFood(Vertex*, interpreter_t &c, ClientIA &client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  switch (client.getDirection()) {
  case ClientIA::UP :
    client.moveAhead(c);
    break;
  case ClientIA::LEFT :
    client.moveLeft(c);
    client.moveAhead(c);
    client.moveRight(c);
    break;
  case ClientIA::RIGHT :
    client.moveRight(c);
    client.moveAhead(c);
    client.moveLeft(c);
    break;
  default :
    break;
  }
  return AIEngine::getInstance().find(&ai::isFoodOnSquare);
}

ai::Vertex*
ai::randomlyMove(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  switch (random() % 3) {
  case ClientIA::UP :
    client.moveAhead(c);
    break;
  case ClientIA::LEFT :
    client.moveLeft(c);
    client.moveAhead(c);
    break;
  case ClientIA::RIGHT :
    client.moveRight(c);
    client.moveAhead(c);
    break;
  }
  return AIEngine::getInstance().getStart();
}

ai::Vertex*
ai::checkBroadcast(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  if (client.getCommandMessage().empty())
    return no;
  return yes;
}

ai::Vertex*
ai::checkSameTeam(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());

  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();
  std::queue<Token> ret = client.getListToken(CommandZappy::message,
					      client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&ret);

  if (client.getTeam() ==
      ClientIA::MsgSend::extractTeam(ClientIA::MsgSend::extractMsgFromToken(list)))
    {
      return yes;
    }

  if (!client.getCommandMessage().empty())
    client.getCommandMessage().pop_front();
  return no;
}

ai::Vertex*
ai::checkLevel(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  std::queue<Token> ret = client.getListToken(CommandZappy::message,
					      client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&ret);

  size_t level = ClientIA::MsgSend::extractLevel(ClientIA::MsgSend::extractMsgFromToken(list));
  if (client.getLevel() == level)
    return yes;
  else
    {
      std::string msg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(list));
      if (msg.compare(0, ClientIA::MsgSend::MSG_PLAYER.size(), ClientIA::MsgSend::MSG_PLAYER) == 0)
	{
	  if (msg.compare(0, ClientIA::MsgSend::MSG_PLAYER_INC.size(), ClientIA::MsgSend::MSG_PLAYER_INC) == 0)
	    client.setPlayer(level - 1, 1);
	  else
	    client.setPlayer(level - 1, -1);
	}
    }
  if (!client.getCommandMessage().empty())
    client.getCommandMessage().pop_front();
  return no;
}

ai::Vertex*
ai::checkData(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  std::queue<Token> q = client.getListToken(CommandZappy::message, client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&q);
  std::string msg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(list));

  if (msg.compare(0, ClientIA::MsgSend::MSG_SEND_DATA.size(), ClientIA::MsgSend::MSG_SEND_DATA) == 0)
    {
      return yes;
    }
  return no;
}

ai::Vertex*
ai::setStoneAndPlayer(Vertex*, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  std::queue<Token> q = client.getListToken(CommandZappy::message, client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&q);
  std::string msg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(list));

  size_t nb = 0;
  std::map<ClientIA::eTypeStone, size_t> stones = ClientIA::MsgSend::unbuildDataMsg(msg, &nb);
  for (std::map<ClientIA::eTypeStone, size_t>::iterator it = stones.begin();
       it != stones.end(); ++it)
    {
      client.setStone(it->first, it->second);
    }
  if (client.getNbPlayers() < nb)
    client.setPlayer(client.getLevel() - 1, nb);
  if (!client.getCommandMessage().empty())
    client.getCommandMessage().pop_front();
  return AIEngine::getInstance().getStart();
}


ai::Vertex*
ai::sendStoneAndPlayerNumber(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  std::string b = ClientIA::MsgSend::buildDataMsg(client.getStones(), client.getNbPlayers());
  client.sendMessage(b, c);
  return AIEngine::getInstance().getStart();
}

ai::Vertex*
ai::checkStoneInfo(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  std::queue<Token> q = client.getListToken(CommandZappy::message, client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&q);
  std::string msg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(list));

  if (msg.compare(0, ClientIA::MsgSend::MSG_STONE.size(), ClientIA::MsgSend::MSG_STONE) == 0)
    {
      return yes;
    }
  return no;
}

ai::Vertex*
ai::incrementStoneNumber(Vertex*, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  std::queue<Token> q = client.getListToken(CommandZappy::message, client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&q);
  std::string msg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(list));

  if (msg.compare(0, ClientIA::MsgSend::MSG_STONE_INC.size(), ClientIA::MsgSend::MSG_STONE_INC) == 0)
    {
      size_t pos = ClientIA::MsgSend::MSG_STONE_INC.size();
      std::string stone = msg.substr(pos, msg.size() - pos);
      client.incStone(client.getTypeStoneFromName(stone), 1);
    }
  else if (msg.compare(0, ClientIA::MsgSend::MSG_STONE_DEC.size(), ClientIA::MsgSend::MSG_STONE_DEC) == 0)
    {
      size_t pos = ClientIA::MsgSend::MSG_STONE_DEC.size();
      std::string stone = msg.substr(pos, msg.size() - pos);
      client.incStone(client.getTypeStoneFromName(stone), -1);
    }
  if (!client.getCommandMessage().empty())
  client.getCommandMessage().pop_front();
  return AIEngine::getInstance().getStart();
}

ai::Vertex*
ai::checkPlayerInfo(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());

  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();
  std::queue<Token> q = client.getListToken(CommandZappy::message, client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&q);
  std::string msg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(list));

  if (msg.compare(0, ClientIA::MsgSend::MSG_PLAYER.size(), ClientIA::MsgSend::MSG_PLAYER) == 0)
    {
      return yes;
    }
  return no;
}

ai::Vertex*
ai::incrementPlayerNumber(Vertex*v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  std::queue<Token> q = client.getListToken(CommandZappy::message, client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&q);
  std::string msg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(list));

  if (msg.compare(0, ClientIA::MsgSend::MSG_PLAYER_INC.size(), ClientIA::MsgSend::MSG_PLAYER_INC) == 0)
    {
      client.incPlayer(client.getLevel() - 1, 1);
      if (!client.getCommandMessage().empty())
      	client.getCommandMessage().pop_front();
      return yes;
    }
  else if (msg.compare(0, ClientIA::MsgSend::MSG_PLAYER_DEC.size(), ClientIA::MsgSend::MSG_PLAYER_DEC) == 0)
    {
      client.incPlayer(client.getLevel() - 1, -1);
    }
  if (!client.getCommandMessage().empty())
    client.getCommandMessage().pop_front();
  return no;
}

ai::Vertex*
ai::checkGather(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());

  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  std::queue<Token> q = client.getListToken(CommandZappy::message, client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&q);
  std::string msg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(list));

  if (msg.compare(0, ClientIA::MsgSend::MSG_GATHER.size(), ClientIA::MsgSend::MSG_GATHER) == 0)
    return yes;
  if (!client.getCommandMessage().empty())
    client.getCommandMessage().pop_front();
  return no;
}


ai::Vertex*
ai::giveDirection(Vertex* v, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  static char pass = 0;

  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  std::queue<Token> q = client.getListToken(CommandZappy::message, client.getCommandMessage());
  std::list<std::string> list = client.getListItemMessage(&q);
  size_t dir = ClientIA::MsgSend::extractDirectionFromToken(list);
  client.emptyMessageGather();

  // a little touch of random :)
  ++pass;
  switch (pass) {
  case 10 : return yes ; break;
  case 20 : dir = 1; break;
  case 30 : dir = 2; break;
  case 40 : dir = 3; pass = 0; break;
  default: break;
  }

  switch (dir) {
  case 0: // it's the good case
    return yes;
    break;
  case 1:
    client.moveAhead(c);
    break;
  case 2:
    client.moveLeft(c);
    client.moveAhead(c);
    client.moveRight(c);
    client.moveAhead(c);
    break;
  case 3:
    client.moveLeft(c);
    client.moveAhead(c);
    break;
  case 4:
    client.moveLeft(c);
    client.moveAhead(c);
    client.moveLeft(c);
    client.moveAhead(c);
    break;
  case 5:
    client.moveRight(c);
    client.moveRight(c);
    client.moveAhead(c);
    break;
  case 6:
    client.moveRight(c);
    client.moveAhead(c);
    client.moveRight(c);
    client.moveAhead(c);
    break;
  case 7:
    client.moveRight(c);
    client.moveAhead(c);
  case 8:
    client.moveRight(c);
    client.moveAhead(c);
    client.moveLeft(c);
    client.moveAhead(c);
    break;
  }
  client.emptyMessageGather();
  return no;
}


ai::Vertex*
ai::layAllStone(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  for (std::map<ClientIA::eTypeStone, size_t>::iterator it = client.getStones().begin();
       it != client.getStones().end(); ++it)
    {
      for ( ; it->second > 0; --it->second)
	{
	  c.addSendFunction(&InterpretCommand<ClientIA>::pose_objetSend,
			    new std::string(ClientIA::CORRESP[it->first]));
	  c.addReceivedFunction(&InterpretCommand<ClientIA>::pose_objetReceived);
	  c.run(false);

	  std::queue<Token> ret = client.getListToken(CommandZappy::pose_objet,
						      client.getCommand());
	  std::list<std::string> rep = client.getListItem(&ret);
	  if (rep.begin() != rep.end())
	    {
	      if (*(rep.begin()) == "ok")
		client.sendMessage(ClientIA::MsgSend::MSG_STONE_DEC + ClientIA::CORRESP[it->first], c);
	    }
	  if (!client.getCommand().empty())
	    client.getCommand().pop_front();
	}
    }
  return AIEngine::getInstance().find(&ai::checkIfTooManyLaidStone);
}

ai::Vertex*
ai::checkIfTooManyLaidStone(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());

  bool fin = false;
  while (fin == false)
    {
      c.addSendFunction(&InterpretCommand<ClientIA>::voirSend, NULL);
      c.addReceivedFunction(&InterpretCommand<ClientIA>::voirReceived);
      c.run(false);

      std::queue<Token> listToken = client.getListToken(CommandZappy::voir, client.getCommand());
      if (!listToken.empty())
	{
	  std::vector< std::list<std::string> > items = client.extractVoirInventaire(&listToken);
	  std::list<std::string> listElem = client.onTheCase(items);

	  std::map<ClientIA::eTypeStone, size_t> stones;
	  stones[ClientIA::LINEMATE] = stones[ClientIA::DERAUMERE]
	    = stones[ClientIA::SIBUR] = stones[ClientIA::MENDIANE]
	    = stones[ClientIA::PHIRAS] = stones[ClientIA::THYSTAME] = 0;

	  for (std::list<std::string>::iterator it = listElem.begin();
	       it != listElem.end(); ++it)
	    {
	      ClientIA::eTypeStone type = client.getTypeStoneFromName(*it);
	      if (type != ClientIA::NB_STONES)
		{
		  ++stones[type];
		}
	    }

	  bool hasTaken = false;
	  for (std::map<ClientIA::eTypeStone, size_t>::iterator itStone = stones.begin();
	       hasTaken == false && itStone != stones.end(); ++itStone)
	    {
	      while (hasTaken == false && itStone->second > client.ressources[client.getLevel() - 1].
		     stones[itStone->first])
		{
		  c.addSendFunction(&InterpretCommand<ClientIA>::prend_objetSend,
				    new std::string(ClientIA::CORRESP[itStone->first]));
		  c.addReceivedFunction(&InterpretCommand<ClientIA>::prend_objetReceived);
		  c.run(false);
		  if (!client.getCommand().empty())
		    client.getCommand().pop_front();
		  --itStone->second;
		  client.sendMessage(ClientIA::MsgSend::MSG_STONE_INC + ClientIA::CORRESP[itStone->first], c);
		  hasTaken = true;
		}
	    }
	  // pop voir
	  if (!client.getCommand().empty())
	    client.getCommand().pop_front();
	  if (!hasTaken)
	    fin = true;
	}
    }
  return AIEngine::getInstance().find(&ai::incantation);
}




ai::Vertex*
ai::incantation(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());

  c.addFunction(&InterpretCommand<ClientIA>::incantationSend, Select::WRITE);
  c.addFunction(&InterpretCommand<ClientIA>::incantationReceived, Select::READ);

  c.run(false);
  bool ko = false;
  if (!client.getCommand().empty())
    {
      std::queue<Token> ret = client.getListToken(CommandZappy::incantation,
						  client.getCommand());
      std::list<std::string> rep = client.getListItem(&ret);
      if (rep.begin() != rep.end())
	if (*(rep.begin()) == "ko")
	  ko = true;
      if (!client.getCommand().empty())
	client.getCommand().pop_front();
    }

  unsigned int t = 0;
  bool end = false;
  while (end == false)
    {
      c.run(true);
      ++t;
      if (!client.getCommandLevel().empty())
	{
	  std::queue<Token> ret = client.getListToken(CommandZappy::level,
						      client.getCommandLevel());
	  std::list<std::string> list = client.getListItemMessage(&ret);
	  if (list.begin() != list.end())
	    if (*(list.begin()) == "ko")
	      end = true;
	  if (end == false)
	    {
	      client.sendPlayer(ClientIA::MsgSend::MSG_PLAYER_DEC,
				ClientIA::MsgSend::MSG_STONE_DEC, c);
	      client.incPlayer(client.getLevel() - 1, -1);
	      client.incLevel();
	      client.sendPlayer(ClientIA::MsgSend::MSG_PLAYER_INC,
				ClientIA::MsgSend::MSG_STONE_INC, c);
	      client.incPlayer(client.getLevel() - 1, 1);
	      client.getCommandLevel().pop_front();
	    }
	  end = true;
	}
      if (end == false && ko)
	{
	  sleep(1);
	}
      if (ko && t == 3)
	{
	  end = true;
	}
    }
  sleep(2);
  c.run(false);
  while (!client.getCommand().empty())
    client.getCommand().pop_front();
  return AIEngine::getInstance().find(&ai::scatter);
}


ai::Vertex*
ai::scatter(Vertex*, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  return AIEngine::getInstance().getStart();
}

ai::Vertex*
ai::checkIfNeedLevelUp(Vertex*, interpreter_t &, ClientIA&client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  if (client.getLevel() < 8)
    return AIEngine::getInstance().find(&ai::hasEnoughStoneToLevelUp);
  return AIEngine::getInstance().getStart();
}

ai::Vertex*
ai::hasEnoughStoneToLevelUp(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  for (std::map<ClientIA::eTypeStone, size_t>::iterator it = client.getStones().begin();
       it != client.getStones().end(); ++it)
    {
      if (it->second < client.ressources[client.getLevel() - 1].stones[it->first])
	return no;
    }
  return yes;
}

ai::Vertex*
ai::hasEnoughPlayerToLevelUp(Vertex*v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  if (client.getNbPlayers() >= client.ressources[client.getLevel() - 1].
      nbPlayersMandatory)
    return yes;
  return no;
}

ai::Vertex*
ai::checkNbConnect(Vertex* v, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  c.addSendFunction(&InterpretCommand<ClientIA>::connect_nbrSend, NULL);
  c.addReceivedFunction(&InterpretCommand<ClientIA>::connect_nbrReceived);
  c.run(false);
  std::queue<Token> listToken = client.getListToken(CommandZappy::connect_nbr, client.getCommand());
  if (!listToken.empty())
    {
      std::list<std::string> rep = client.getListItem(&listToken);
      if (!rep.empty())
	{
	  std::stringstream ss;
	  size_t connect;
	  ss << *(rep.begin());
	  ss >> connect;
	  if (connect == 0)
	    {
	      if (!client.getCommand().empty())
		client.getCommand().pop_front();
	      return yes;
	    }
	}
    }
  if (!client.getCommand().empty())
    client.getCommand().pop_front();
  return no;
}

ai::Vertex*
ai::doFork(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  c.addSendFunction(&InterpretCommand<ClientIA>::forkSend, NULL);
  c.addReceivedFunction(&InterpretCommand<ClientIA>::forkReceived);
  c.run(false);
  std::queue<Token> listToken = client.getListToken(CommandZappy::connect_nbr, client.getCommand());
  (void) listToken; // for the moment
  if (!client.getCommand().empty())
    client.getCommand().pop_front();
  return AIEngine::getInstance().getStart();
}


ai::Vertex*
ai::isThereEnoughPlayerOnCase(Vertex* v, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());

  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  if (client.getFood(c) < ClientIA::RECOMMENDED_FOOD_LEVEL_SEARCH_PLAYER)
    return AIEngine::getInstance().getStart();

  // the 'luck' to have a temporary client on the case with a different level are few
  // we take the risk ;)
  c.addSendFunction(&InterpretCommand<ClientIA>::voirSend, NULL);
  c.addReceivedFunction(&InterpretCommand<ClientIA>::voirReceived);
  c.run(false);

  std::queue<Token> listToken = client.getListToken(CommandZappy::voir, client.getCommand());
  if (!listToken.empty())
    {
      std::vector< std::list<std::string> > items = client.extractVoirInventaire(&listToken);
      std::list<std::string> listElem = client.onTheCase(items);

      size_t nb = 0;
      for (std::list<std::string>::const_iterator it2 = listElem.begin();
	   it2 != listElem.end(); ++it2)
	{
	  if (*it2 == "joueur")
	    ++nb;
	}
      if (!client.getCommand().empty())
	client.getCommand().pop_front();
      if (client.ressources[client.getLevel() - 1].nbPlayersMandatory == nb)
	return yes;
      else if (client.ressources[client.getLevel() - 1].nbPlayersMandatory < nb)
	return AIEngine::getInstance().find(&ai::randomlyMove);
    }
  return no;
}

ai::Vertex*
ai::checkIfStoneInCase(Vertex* v, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  c.addSendFunction(&InterpretCommand<ClientIA>::voirSend, NULL);
  c.addReceivedFunction(&InterpretCommand<ClientIA>::voirReceived);
  c.run(false);

  std::queue<Token> listToken = client.getListToken(CommandZappy::voir, client.getCommand());
  if (!listToken.empty())
    {
      std::vector< std::list<std::string> > items = client.extractVoirInventaire(&listToken);
      std::list<std::string> listElem = client.onTheCase(items);
      for (std::list<std::string>::const_iterator it2 = listElem.begin();
	   it2 != listElem.end(); ++it2)
	{
	  ClientIA::eTypeStone type = client.getTypeStoneFromName(*it2);
	  if (type != ClientIA::NB_STONES)
	    {
	      std::map<ClientIA::eTypeStone, size_t>::iterator itStone = client.getStones().find(type);
	      if (client.ressources[client.getLevel() - 1].stones[itStone->first] > itStone->second)
		{
		  client.setStoneToTake(*it2);
		  if (!client.getCommand().empty())
		    client.getCommand().pop_front();
		  return yes;
		}
	    }
	}
      if (!client.getCommand().empty())
	client.getCommand().pop_front();
    }
  return no;
}

ai::Vertex*
ai::takeStone(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  c.addSendFunction(&InterpretCommand<ClientIA>::prend_objetSend,
		    new std::string(client.getStoneToTake()));
  c.addReceivedFunction(&InterpretCommand<ClientIA>::prend_objetReceived);
  c.run(false);
  if (!client.getCommand().empty())
    {
      std::queue<Token> ret = client.getListToken(CommandZappy::prend_objet,
						  client.getCommand());
      std::list<std::string> rep = client.getListItem(&ret);
      if (rep.begin() != rep.end())
	{
	  if (*(rep.begin()) == "ok")
	    {
	      client.incStone(client.getTypeStoneFromName(client.getStoneToTake()), 1);
	      client.sendMessage(ClientIA::MsgSend::MSG_STONE_INC + client.getStoneToTake(), c);
	    }
	}
      if (!client.getCommand().empty())
      client.getCommand().pop_front();
    }
  client.setStoneToTake("");
  return AIEngine::getInstance().getStart();
}

ai::Vertex*
ai::checkSight(Vertex* v, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  c.addSendFunction(&InterpretCommand<ClientIA>::voirSend, NULL);
  c.addReceivedFunction(&InterpretCommand<ClientIA>::voirReceived);
  c.run(false);

  std::queue<Token> listToken = client.getListToken(CommandZappy::voir, client.getCommand());
  if (!listToken.empty())
    {
      std::vector< std::list<std::string> > items = client.extractVoirInventaire(&listToken);
      if (!items.empty())
	{
	  unsigned short sightSide = 1;
	  unsigned short line = 1;
	  client.setDirection(ClientIA::UNSET);

	  for (std::vector< std::list<std::string> >::iterator it = items.begin();
	       it != items.end(); ++it)
	    {
	      for (std::list<std::string>::iterator it2 = (*it).begin();
		   it2 != (*it).end(); ++it2)
		{
		  std::map<ClientIA::eTypeStone, size_t>::const_iterator it = client.getStones().begin();
		  for ( ; it != client.getStones().end(); ++it)
		    {
		      if (it->second < client.ressources[client.getLevel() - 1].stones[it->first])
			{
			  if (ClientIA::CORRESP[it->first] == *it2)
			    {
			      if (line < (sightSide / 2) && client.getDirection() == ClientIA::UNSET)
				client.setDirection(ClientIA::LEFT);
			      else if (line == (sightSide / 2) + 1)
				client.setDirection(ClientIA::UP);
			      else if (client.getDirection() == ClientIA::UNSET)
				client.setDirection(ClientIA::RIGHT);
			    }
			}
		    }
		}
	      if (line == sightSide)
		{
		  sightSide += 2;
		  line = 0;
		}
	      ++line;
	    }
	}
      if (!client.getCommand().empty())
	client.getCommand().pop_front();
    }
  if (client.getDirection() == ClientIA::UNSET)
    return no;
  return yes;
}


ai::Vertex*
ai::goAhead(Vertex*, interpreter_t &c, ClientIA&client)
{
  static char pass = 0;
  ++pass;
  if (pass == 40)
    {
      pass = 0;
      client.setDirection(ClientIA::UP);
    }
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  switch (client.getDirection()) {
  case ClientIA::UP :
    client.moveAhead(c);
    break;
  case ClientIA::LEFT :
    client.moveLeft(c);
    client.moveAhead(c);
    client.moveRight(c);
    break;
  case ClientIA::RIGHT :
    client.moveRight(c);
    client.moveAhead(c);
    client.moveLeft(c);
    break;
  default :
    break;
  }
  return AIEngine::getInstance().getStart();
}

ai::Vertex*
ai::broadcast(Vertex*, interpreter_t &c, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  static char pass = 0;

  ++pass;
  if (pass == 10)
    pass = 0;
  else
    client.sendMessage(ClientIA::MsgSend::MSG_GATHER, c);
  return AIEngine::getInstance().find(&ai::isThereEnoughPlayerOnCase);
}

ai::Vertex*
ai::checkStoneAndPlayerNumber(Vertex* v, interpreter_t &, ClientIA& client)
{
  print_info::where(__func__, client.getLevel(), client.getNbPlayers());
  ai::Vertex*	yes = v->findOutE(ai::YES)->getInV();
  ai::Vertex*	no = v->findOutE(ai::NO)->getInV();

  (void) no;
  // not implemented, just in case of
  return yes;
}
