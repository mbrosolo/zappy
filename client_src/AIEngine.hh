//
// AIEngine.hh for zappy in /home/gressi_b/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Thu Jun 28 14:02:13 2012 gressi_b
// Last update Sun Jul 15 16:07:44 2012 tosoni_t
//

#ifndef	__AI_ENGINE_HH__
#define	__AI_ENGINE_HH__

#include "../lib/graph/Graph.hpp"
#include "AIAlgorithm.hh"

/*!
 * @author gressi_b
 * @class AIEngine
 * @brief Singleton.
 * This class is ce core of the AI, it set up the Graph of function
 * by inserting function in Vertex, and linking them with Edges.
 * It begin by calling the function in vertex pointed by _start.
 * Then it call the function in the Vertex returned by this functions,
 * util return is NULL.
 */

class AIEngine
{
private:
  /*!
   * @brief Graph built in AIEngine::AIEngine()
   */
  ai::Graph	_g;

  /*!
   * @brief Pointer on the vertex container the first function to execute
   */
  ai::Vertex*	_start;

private:
  /*!
   * @brief AIEngine copy ctor
   */
  AIEngine(AIEngine const&);
  /*!
   * @brief AIEngine operator =
   */
  AIEngine&	operator=(AIEngine const&);

public:
  /*!
   * @brief AIEngine Ctor
   */
  AIEngine();
  /*!
   * @brief  AIEngine Dtor
   */
  ~AIEngine() {}

  /*!
   * @brief Singleton on stack
   */
  static AIEngine&	getInstance();

  /*!
   * @brief Run the AI loop
   */
  bool			run(interpreter_t&, ClientIA&);

  /*!
   * @brief Return a the first vertex containing the fiven pointer on function
   */
  ai::Vertex*		find(ai::function_t const&);

  /*!
   * @brief Return the first edge with the given label
   */
  ai::Edge*		getEdgeByLabel(ai::eLabel const&);

  /*!
   * @brief get start for vertex
   */
  ai::Vertex*		getStart() const;
};

#endif
