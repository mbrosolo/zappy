//
// ExceptionMort.cpp for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Fri Jul 13 14:03:18 2012 anna texier
// Last update Sun Jul 15 17:53:01 2012 gaetan senn
//

#include	"ExceptionMort.hh"

ExceptionMort::ExceptionMort(std::string const &what,
			     std::string const &where) throw()
  : ExceptionRuntime(what, where)
{

}

ExceptionMort::~ExceptionMort() throw()
{

}
