//
// ExceptionRuntime.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:47:42 2012 benjamin bourdin
// Last update Wed May 23 06:53:08 2012 benjamin bourdin
//

#ifndef __EXCEPTION_RUNTIME_HH__
#define __EXCEPTION_RUNTIME_HH__

#include	<stdexcept>
#include	<exception>
#include	<string>

/**
 * @author bourdi_b
 * @class ExceptionRuntime
 * @brief Exception class used when a problem occured, and can be only
 * detected at runtime
 */

class ExceptionRuntime : public std::runtime_error
{
protected:
  std::string const	_what;
  std::string const	_where;

public:
  ExceptionRuntime(std::string const&, std::string const& = "");
  virtual ~ExceptionRuntime() throw() {}

  /*!
   * @brief: return a string which give information
   * about the nature of the exception
   */
  virtual char const*	what() const throw();

  /*!
   * @brief: return a string which give information
   * about where the exception occured
   */
  std::string const&	where() const;
};

#endif
