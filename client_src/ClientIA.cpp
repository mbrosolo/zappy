//
// Client.cpp for client zappy in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 13 13:50:14 2012 gaetan senn
// Last update Sun Jul 15 19:14:15 2012 benjamin bourdin
//

#include "ClientIA.hh"

const size_t ClientIA::RECOMMENDED_FOOD_LEVEL = 60;
const size_t ClientIA::RECOMMENDED_FOOD_LEVEL_FIRST = 100;
const size_t ClientIA::RECOMMENDED_FOOD_LEVEL_SEARCH_PLAYER = 30;
const std::string ClientIA::CORRESP[ClientIA::NB_STONES] = {
  "linemate",
  "deraumere",
  "sibur",
  "mendiane",
  "phiras",
  "thystame"
};

const char ClientIA::MsgSend::DELIM = '|';

const std::string ClientIA::MsgSend::MSG_GATHER = std::string("gather_here");
const std::string ClientIA::MsgSend::MSG_STONE = std::string("stone_");
const std::string ClientIA::MsgSend::MSG_STONE_INC = ClientIA::MsgSend::MSG_STONE + std::string("inc_"); // + type
const std::string ClientIA::MsgSend::MSG_STONE_DEC = ClientIA::MsgSend::MSG_STONE + std::string("dec_"); // + type

const std::string ClientIA::MsgSend::MSG_PLAYER = std::string("player_");
const std::string ClientIA::MsgSend::MSG_PLAYER_INC = ClientIA::MsgSend::MSG_PLAYER + std::string("inc");
const std::string ClientIA::MsgSend::MSG_PLAYER_DEC = ClientIA::MsgSend::MSG_PLAYER + std::string("dec");

const std::string ClientIA::MsgSend::MSG_SEND_DATA = std::string("data_are_"); // + all stone + nb players same level


ClientIA::ClientIA()
  : _team("Unknow"), _freeplayerspace(0), _x(0), _y(0), _elevation(0),
    _stones(), _level(1), ressources(), _direction(UNSET),
    _stoneToTake(), _hello(false), _firstReserve(true), _players(), _deathProg(false)
{
  (void) memset(_players, 0, sizeof(_players));

  size_t st[NB_STONES] = {0};

  _stones[LINEMATE] = _stones[DERAUMERE] = _stones[SIBUR] = _stones[MENDIANE]
    = _stones[PHIRAS] = _stones[THYSTAME] = 0;

  st[LINEMATE] = 1;
  ressources[0].setStones(st); ressources[0].nbPlayersMandatory = 1;

  st[LINEMATE] = st[DERAUMERE] = st[SIBUR] = 1;
  ressources[1].setStones(st); ressources[1].nbPlayersMandatory = 2;

  st[LINEMATE] = st[PHIRAS] = 2; st[DERAUMERE] = 0; st[SIBUR] = 1;
  ressources[2].setStones(st); ressources[2].nbPlayersMandatory = 2;

  st[LINEMATE] = st[PHIRAS] = st[DERAUMERE] = 1; st[SIBUR] = 2;
  ressources[3].setStones(st); ressources[3].nbPlayersMandatory = 4;

  st[LINEMATE] = st[SIBUR] = 1; st[DERAUMERE] = 2; st[PHIRAS] = 0; st[MENDIANE] = 3;
  ressources[4].setStones(st); ressources[4].nbPlayersMandatory = 4;

  st[LINEMATE] = st[PHIRAS] = 1; st[DERAUMERE] = 2; st[SIBUR] = 3; st[MENDIANE] = 0;
  ressources[5].setStones(st); ressources[5].nbPlayersMandatory = 6;

  st[LINEMATE] = st[PHIRAS] = st[DERAUMERE] = st[SIBUR] = st[MENDIANE] = 2; st[THYSTAME] = 1;
  ressources[6].setStones(st); ressources[6].nbPlayersMandatory = 6;
}

ClientIA::~ClientIA()
{
}

std::string	const	&ClientIA::getTeam() const
{
  return _team;
}

std::list<std::string>				ClientIA::getListItem(std::queue<Token> *q)
{
  std::list<std::string>			extracted;

  if (q->empty())
    extracted.push_back("");
  while (!q->empty())
    {
      Token			t = q->front();
      q->pop();
      if (t.getType() == Token::COMMA)
	{
	  if (extracted.empty())
	    extracted.push_back("");
	  return extracted;
	}
      if (t.getType() == Token::CLOSE_BRAKET)
	return (extracted);
      if (t.getType() != Token::BLANK)
	extracted.push_back(t.getData());
    }
  return (extracted);
}

std::list<std::string>				ClientIA::getListItemMessage(std::queue<Token>
									     *q)
{
  std::list<std::string>			extracted;

  if (q->empty())
    extracted.push_back("");
  while (!q->empty())
    {
      Token			t = q->front();
      q->pop();
      if (t.getType() == Token::COMMA)
	{
	  if (extracted.empty())
	    extracted.push_back("");
	}
      if (t.getType() == Token::CLOSE_BRAKET)
	return (extracted);
      if (t.getType() != Token::BLANK)
	extracted.push_back(t.getData());
    }
  return (extracted);
}

std::vector< std::list<std::string> >		ClientIA::extractVoirInventaire(std::queue<Token> *q)
{
  std::vector< std::list<std::string> >		_items;

  q->pop();
  while (!q->empty())
    {
      std::list<std::string>			ret = getListItem(q);

      if (!ret.empty())
	_items.push_back(ret);
      if (q->front().getType() == Token::CLOSE_BRAKET)
	return (_items);
    }
  return (_items);
}

size_t ClientIA::extractFoodFromInventory(std::vector< std::list<std::string> > const &items) const
{
  bool food = false;

  if (!items.empty())
    {
      std::vector< std::list<std::string> >::const_iterator it;
      for (it = items.begin(); it != items.end(); ++it)
	{
	  std::list<std::string>::const_iterator it2;
	  for (it2 = (*it).begin(); it2 != (*it).end(); ++it2)
	    {
	      if (food == false && *it2 == "nourriture")
		food = true;
	      else if (food == true)
		{
		  std::stringstream ss;
		  size_t ret;
		  ss << *it2;
		  ss >> ret;
		  return ret;
		}
	    }
	}
    }
  return 0;
}


std::list<std::string> ClientIA::onTheCase(std::vector< std::list<std::string> > const &items) const
{
  if (!items.empty())
    {
      std::vector< std::list<std::string> >::const_iterator it = items.begin();
      return *it;
    }
  std::list<std::string> ret;
  return ret;
}


std::queue<Token> ClientIA::getListToken(const CommandZappy::eType type,
					 std::list<std::map <CommandZappy::eType,
							     std::queue<Token> const > > & cmd)
{
  std::queue<Token> ret;

  if (!cmd.empty())
    {
      std::map<CommandZappy::eType, std::queue<Token> const > allCmd = cmd.front();
      if (allCmd.find(type) != allCmd.end())
	ret = allCmd[type];
    }
  return ret;
}

void ClientIA::sendMessage(std::string const &msg, interpreter_t &c)
{
  c.addSendFunction(&InterpretCommand<ClientIA>::broadcast_texteSend,
		    new std::string(ClientIA::MsgSend::buildMsg(this->getTeam(),
								this->getLevel(),
								msg)));
  c.addReceivedFunction(&InterpretCommand<ClientIA>::broadcast_texteReceived);
  c.run(false);
  if (!this->getCommand().empty())
    this->getCommand().pop_front();
}





void			ClientIA::setElevation(unsigned int level)
{
  _elevation = level;
}

void			ClientIA::setCoord(unsigned int x, unsigned int y)
{
  _x = x;
  _y = y;
}

void			ClientIA::setTeam(std::string const & team)
{
  _team = team;
}

void			ClientIA::setFreePLayerSpace(unsigned int value)
{
  _freeplayerspace = value;
}

void			ClientIA::launch(int port, std::string const &hostname)
{
  InterpretConnection<ClientIA>		parsingconnection(*this);
  InterpretCommand<ClientIA>		parsingcommand(*this);

  this->_socket.createSocket(AF_INET, SOCK_STREAM, Socket::TCP);
  this->_socket.connection(AF_INET, port, hostname);
  this->_select.addFd(_socket.getSocket(),
		      &ClientIA::readServer, &ClientIA::writeServer, NULL);
  try
    {
      parsingconnection.start();
    }
  catch (ExceptionInterpret &e)
    {
      std::cerr << "Client : Error From => " << e.where() << std::endl;
      std::cerr << "Client : Error Type => " << e.what() << std::endl;
      return ;
    }
  parsingcommand.start();
}

std::map<ClientIA::eTypeStone, size_t>&	ClientIA::getStones()
{
  return _stones;
}

size_t	ClientIA::getNbPlayers() const
{
  return _players[_level - 1];
}

size_t	ClientIA::getLevel() const
{
  return _level;
}

void	ClientIA::incLevel()
{
  ++_level;
}

ClientIA::eDirection	ClientIA::getDirection() const
{
  return _direction;
}

void ClientIA::setDirection(const eDirection dir)
{
  _direction = dir;
}

void ClientIA::setStoneToTake(std::string const &name)
{
  _stoneToTake = name;
}

std::string const & ClientIA::getStoneToTake(void) const
{
  return _stoneToTake;
}

void			ClientIA::setLevel(unsigned int value)
{
  _level = value;
}

void	ClientIA::incStone(const eTypeStone st, const int inc)
{
  if (inc < 0)
    {
      if (static_cast<int>(_stones[st]) >= -inc)
	_stones[st] += inc;
      else
	_stones[st] = 0;
    }
  else
    _stones[st] += inc;
}

void	ClientIA::setStone(const eTypeStone st, const size_t inc)
{
  _stones[st] = inc;
}

bool ClientIA::isFirstReserve(void) const
{
  return _firstReserve;
}

void ClientIA::notFirstReserve(void)
{
  _firstReserve = false;
}

bool ClientIA::hasSaidHello(void) const
{
  return _hello;
}

void ClientIA::hasToSayHello(void)
{
  _hello = false;
}

void ClientIA::sayHello(interpreter_t &c)
{
  this->sendMessage(ClientIA::MsgSend::MSG_PLAYER_INC, c);
  ++_players[_level - 1];
  _hello = true;
}







///// MsgSend methods

std::map<ClientIA::eTypeStone, size_t> ClientIA::MsgSend::unbuildDataMsg(std::string const &msg,
									 size_t *nbPlayers)
{
  size_t pos = MSG_SEND_DATA.size();
  unsigned int i = 0;
  std::map<eTypeStone, size_t> ret;
  for (i = 0; i <= NB_STONES; ++i)
    {
      size_t end_pos = msg.find_first_of('_', pos);
      std::string name = msg.substr(pos, end_pos - pos);
      pos = end_pos + 1;

      // recup nb
      end_pos = msg.find_first_of('_', pos);
      std::string n = msg.substr(pos, end_pos - pos);
      pos = end_pos + 1;
      size_t nb;
      {
	std::stringstream ss;
	ss << n; ss >> nb;
      }
      if (i < NB_STONES)
	{
	  eTypeStone type = ClientIA::getTypeStoneFromName(name);
	  if (type != NB_STONES) // on sait jamais
	    ret[type] = nb;
	}
      else
	*nbPlayers = nb;
    }
  return ret;
}

void ClientIA::sendPlayer(std::string const & type /* MSG_PLAYER_INC or DEC */,
			  std::string const & typeStone /*MSG_STONE_INC or DEC */,
			  interpreter_t &c)
{
  for (std::map<eTypeStone, size_t>::const_iterator it = this->getStones().begin();
       it != this->getStones().end(); ++it)
    {
      for (size_t nb = it->second; nb > 0; --nb)
	{
	  this->sendMessage(typeStone + CORRESP[it->first], c);
	}
    }
  this->sendMessage(type, c);
}

void ClientIA::moveAhead(interpreter_t &c)
{
  c.addFunction(&InterpretCommand<ClientIA>::avanceSend, Select::WRITE);
  c.addFunction(&InterpretCommand<ClientIA>::avanceReceived, Select::READ);
  c.run(false);
  if (!this->getCommand().empty())
    this->getCommand().pop_front();
}

void ClientIA::moveLeft(interpreter_t &c)
{
  c.addFunction(&InterpretCommand<ClientIA>::gaucheSend, Select::WRITE);
  c.addFunction(&InterpretCommand<ClientIA>::gaucheReceived, Select::READ);
  c.run(false);
  if (!this->getCommand().empty())
    this->getCommand().pop_front();

}

void ClientIA::moveRight(interpreter_t &c)
{
  c.addFunction(&InterpretCommand<ClientIA>::droiteSend, Select::WRITE);
  c.addFunction(&InterpretCommand<ClientIA>::droiteReceived, Select::READ);
  c.run(false);
  if (!this->getCommand().empty())
    this->getCommand().pop_front();
}

void ClientIA::incPlayer(const size_t level, const int inc)
{
  if (inc < 0)
    {
      if (static_cast<int>(_players[level]) >= -inc)
	_players[level] += inc;
      else
	_players[level] = 0;
    }
  else
    _players[level] += inc;
}

void ClientIA::setPlayer(const size_t level, const int inc)
{
  _players[level] = inc;
}

bool ClientIA::isDeathProgrammated() const
{
  return _deathProg;
}

void ClientIA::deathProgrammated()
{
  _deathProg = true;
}
