//
// AIEngine.cpp for zappy in /home/gressi_b/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Thu Jun 28 14:03:39 2012 gressi_b
// Last update Sun Jul 15 18:49:08 2012 gaetan senn
//

#include <iostream>
#include "AIEngine.hh"
#include "AInterpret.hpp"
#include "InterpretCommand.hpp"
#include "ClientIA.hh"

// AIEngine --------------------------------------------------------------------

/*
  start
  needFood
  take
  watch
  headForClosestFood
  randomlyMove
*/

AIEngine::AIEngine()
  : _g(), _start(0)
{
  ai::Vertex*	v[43];

  v[0] = _g.vertex(&ai::start);
  v[1] = _g.vertex(&ai::isFoodOnSquare);
  v[2] = _g.vertex(&ai::takeFood);
  v[3] = _g.vertex(&ai::searchFood);
  v[4] = _g.vertex(&ai::headForClosestFood);
  v[5] = _g.vertex(&ai::randomlyMove);
  v[6] = _g.vertex(&ai::checkBroadcast);
  v[7] = _g.vertex(&ai::checkSameTeam);
  v[8] = _g.vertex(&ai::checkLevel);
  v[9] = _g.vertex(&ai::checkData);

  v[10] = _g.vertex(&ai::setStoneAndPlayer);
  v[11] = _g.vertex(&ai::giveDirection);
  v[12] = _g.vertex(&ai::sendStoneAndPlayerNumber);
  v[13] = _g.vertex(&ai::checkStoneInfo);
  v[14] = _g.vertex(&ai::incrementStoneNumber);
  v[15] = _g.vertex(&ai::checkPlayerInfo);
  v[16] = _g.vertex(&ai::incrementPlayerNumber);
  v[17] = _g.vertex(&ai::checkGather);
  v[18] = _g.vertex(&ai::giveDirection);
  v[19] = _g.vertex(&ai::checkGather);

  v[20] = _g.vertex(&ai::layAllStone);
  v[21] = _g.vertex(&ai::checkIfTooManyLaidStone);
  v[22] = _g.vertex(&ai::incrementPlayerNumber);
  v[23] = _g.vertex(&ai::checkPlayerInfo);
  v[24] = _g.vertex(&ai::incantation);
  v[25] = _g.vertex(&ai::checkStoneAndPlayerNumber);
  v[26] = _g.vertex(&ai::scatter);
  v[27] = _g.vertex(&ai::checkIfNeedLevelUp);
  v[28] = _g.vertex(&ai::hasEnoughStoneToLevelUp);
  v[29] = _g.vertex(&ai::hasEnoughPlayerToLevelUp);

  v[30] = _g.vertex(&ai::isThereEnoughPlayerOnCase);
  v[31] = _g.vertex(&ai::checkIfStoneInCase);
  v[32] = _g.vertex(&ai::takeStone);
  v[33] = _g.vertex(&ai::incrementStoneNumber);
  v[34] = _g.vertex(&ai::checkSight);
  v[35] = _g.vertex(&ai::goAhead);
  v[36] = _g.vertex(&ai::broadcast);
  v[37] = _g.vertex(&ai::checkBroadcast);
  v[38] = _g.vertex(&ai::checkSameTeam);
  v[39] = _g.vertex(&ai::checkLevel);

  v[40] = _g.vertex(&ai::checkStoneInfo);
  v[41] = _g.vertex(&ai::checkNbConnect);
  v[42] = _g.vertex(&ai::doFork);
  this->_start = v[0];

  _g.edge(v[0], v[1], ai::YES);
  _g.edge(v[0], v[6], ai::NO);
  _g.edge(v[1], v[2], ai::YES);
  _g.edge(v[1], v[3], ai::NO);
  _g.edge(v[3], v[4], ai::YES);
  _g.edge(v[3], v[5], ai::NO);
  _g.edge(v[4], v[1], ai::NO_MATTER);
  _g.edge(v[5], v[0], ai::NO_MATTER);
  _g.edge(v[6], v[7], ai::YES);
  _g.edge(v[6], v[27], ai::NO);
  _g.edge(v[7], v[8], ai::YES);
  _g.edge(v[7], v[27], ai::NO);
  _g.edge(v[8], v[9], ai::YES);
  _g.edge(v[8], v[27], ai::NO);
  _g.edge(v[9], v[10], ai::YES);
  _g.edge(v[9], v[13], ai::NO);

  _g.edge(v[10], v[0], ai::NO_MATTER);
  _g.edge(v[11], v[30], ai::YES);
  _g.edge(v[11], v[30], ai::NO);
  _g.edge(v[12], v[0], ai::NO_MATTER);
  _g.edge(v[13], v[14], ai::YES);
  _g.edge(v[13], v[15], ai::NO);
  _g.edge(v[14], v[0], ai::NO_MATTER);
  _g.edge(v[15], v[16], ai::YES);
  _g.edge(v[15], v[17], ai::NO);
  _g.edge(v[16], v[12], ai::YES);
  _g.edge(v[16], v[0], ai::NO);
  _g.edge(v[17], v[18], ai::YES);
  _g.edge(v[17], v[27], ai::NO);
  _g.edge(v[18], v[30], ai::YES);
  _g.edge(v[18], v[0], ai::NO);
  _g.edge(v[19], v[11], ai::YES);
  _g.edge(v[19], v[30], ai::NO);

  _g.edge(v[20], v[21], ai::NO_MATTER);
  _g.edge(v[21], v[24], ai::NO_MATTER);
  _g.edge(v[22], v[25], ai::YES);
  _g.edge(v[22], v[25], ai::NO);
  _g.edge(v[23], v[22], ai::YES);
  _g.edge(v[23], v[19], ai::NO);
  _g.edge(v[24], v[26], ai::NO_MATTER);
  _g.edge(v[25], v[30], ai::YES);
  _g.edge(v[25], v[0], ai::NO);
  _g.edge(v[26], v[0], ai::NO_MATTER);
  _g.edge(v[27], v[28], ai::NO_MATTER);
  _g.edge(v[28], v[29], ai::YES);
  _g.edge(v[28], v[31], ai::NO);
  _g.edge(v[29], v[30], ai::YES);
  _g.edge(v[29], v[41], ai::NO);

  _g.edge(v[30], v[20], ai::YES);
  _g.edge(v[30], v[37], ai::NO);
  _g.edge(v[31], v[32], ai::YES);
  _g.edge(v[31], v[34], ai::NO);
  _g.edge(v[32], v[0], ai::NO_MATTER);
  _g.edge(v[33], v[25], ai::NO_MATTER);
  _g.edge(v[34], v[35], ai::YES);
  _g.edge(v[34], v[5], ai::NO);
  _g.edge(v[35], v[0], ai::NO_MATTER);
  _g.edge(v[36], v[37], ai::NO_MATTER);
  _g.edge(v[37], v[38], ai::YES);
  _g.edge(v[37], v[36], ai::NO);
  _g.edge(v[38], v[39], ai::YES);
  _g.edge(v[38], v[30], ai::NO);
  _g.edge(v[39], v[40], ai::YES);
  _g.edge(v[39], v[30], ai::NO);

  _g.edge(v[40], v[33], ai::YES);
  _g.edge(v[40], v[23], ai::NO);

  _g.edge(v[41], v[42], ai::YES);
  _g.edge(v[41], v[0], ai::NO);
  _g.edge(v[42], v[0], ai::NO_MATTER);
}

// Member functions ------------------------------------------------------------

AIEngine&
AIEngine::getInstance()
{
  static AIEngine	_instance;

  return _instance;
}

bool
AIEngine::run(interpreter_t& c, ClientIA& ia)
{
  ai::Vertex*	v = this->_start;

  try {
    while (v)
      {
	v = v->getData().f(v, c, ia);
	c.run(true);
      }
  }
  catch (ExceptionRuntime const&e) {
    std::cerr << "Error in " << e.where() << ": " << e.what() << std::endl;
    return false;
  }
  catch (...)
    {
      // a changer evntuellement selon le changement des throw
      v = this->_start;
      v = v->getData().f(v, c, ia);
      return false;
    }
  return true;
}

ai::Vertex*
AIEngine::find(ai::function_t const& e)
{
  return this->_g.findV(ai::Data(e));
}

ai::Edge*
AIEngine::getEdgeByLabel(ai::eLabel const& label)
{
  return this->_g.findE(label);
}

// Getters ---------------------------------------------------------------------

ai::Vertex*
AIEngine::getStart() const
{
  return this->_start;
}
