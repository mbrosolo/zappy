//
// signal.hh for zappy in /home/gressi_b/Epitech/B4/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Sat Jul 14 14:38:04 2012 gressi_b
// Last update Sat Jul 14 14:38:04 2012 gressi_b
//

#ifndef	__SIGNAL_HH__
#define	__SIGNAL_HH__

#include <signal.h>

/**
 * @author gressi_b
 * @namespace sig
 * @brief encapsulate signal syscall
 */
namespace sig
{
  typedef sighandler_t	fhandler_t;

  fhandler_t	setHandler(int, fhandler_t);
}

#endif
