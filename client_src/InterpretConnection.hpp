//
// Interpret.hh for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Mon Jun 18 13:33:44 2012 gaetan senn
// Last update Sun Jul 15 15:58:30 2012 gaetan senn
//

#ifndef __INTERPRET_CONNECTION__
#define __INTERPRET_CONNECTION__

#include "AInterpret.hpp"
#include "Lexer.hh"
#include <map>
#include <queue>
#include <sstream>

template <typename T>
class		InterpretConnection
  : public AInterpret<T, InterpretConnection<T> >
{
public:
  InterpretConnection(T & target)
    : AInterpret<T, InterpretConnection<T> >(&target, this)
  {
    addReceivedFunction(&InterpretConnection<T>::WelcomeCheck);
    addSendFunction(&InterpretConnection<T>::SendTeam, NULL);
    addReceivedFunction(&InterpretConnection<T>::getNbClient);
    addReceivedFunction(&InterpretConnection<T>::getCoord);
  }
  ~InterpretConnection()
  {
  }
  bool			checkCommand(std::queue<Token>	*)
  {
    return (true);
  }
  bool			WelcomeCheck(void *r)
  {
    std::queue<Token>	*q;
    q = static_cast<std::queue<Token> *>(r);
    if (q->size() == 1 && q->front().getType() == Token::STRING &&
	q->front().getData() == "ko")
      throw ExceptionInterpret("Server Full", "void	AInterpret::run");
    if (q->size() == 1 && q->front().getType() == Token::STRING &&
	q->front().getData() == "BIENVENUE")
      return (tools::freeDelete<std::queue<Token> *>(q, true));
    else
      return (tools::freeDelete<std::queue<Token> *>(q, false));
  }
  bool			SendTeam(void *)
  {
    this->_target->addCommand(this->_target->getTeam() + "\n");
    return (true);
  }
  bool			getNbClient(void *r)//std::queue<Token> *q)
  {
    std::stringstream	ss;
    unsigned int		res;
    std::queue<Token>		*q;

    q = static_cast<std::queue<Token> *>(r);
    if (q->size() == 1 && q->front().getType() == Token::STRING &&
	q->front().getData() == "ko")
      throw ExceptionInterpret("team not exist or not enough place", "void	AInterpret::run");
    if (q->size() == 1 && q->front().getType() == Token::UNSIGNED_INT)
      {
	ss << q->front().getData();
	ss >> res;
	this->_target->setFreePLayerSpace(res);
	return (tools::freeDelete<std::queue<Token> *>(q, true));
      }
    return (tools::freeDelete<std::queue<Token> *>(q, false));
  }
  bool			getCoord(void *r)
  {
    std::stringstream	ss;
    unsigned int	x;
    unsigned int	y;
    std::queue<Token>		*q;

    q = static_cast<std::queue<Token> *>(r);
    if (q->size() == 1 && q->front().getType() == Token::STRING &&
	q->front().getData() == "ko")
      throw ExceptionInterpret("getCoord error", "void	AInterpret::run");
    if (q->size() == 3)
      {
	Token	tmp = q->front();
	q->pop();
	if (tmp.getType() == Token::UNSIGNED_INT)
	  {
	    ss << tmp.getData();
	    ss >> x;
	  }
	else
	  return (tools::freeDelete<std::queue<Token> *>(q, false));
	tmp = q->front();
	q->pop();
	if (tmp.getType() != Token::BLANK)
	  return (tools::freeDelete<std::queue<Token> *>(q, false));
	tmp = q->front();
	q->pop();
	if (tmp.getType() == Token::UNSIGNED_INT)
	  {
	    ss.clear();
	    ss << tmp.getData();
	    ss >> y;
	  }
	else
	  return (tools::freeDelete<std::queue<Token> *>(q, false));
	this->_target->setCoord(x, y);
	return (tools::freeDelete<std::queue<Token> *>(q, true));
      }
    return (tools::freeDelete<std::queue<Token> *>(q, false));
  }
  void			start()
  {
    this->run(false);
  }
};

#endif
