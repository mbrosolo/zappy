//
// InterpretCommand.hpp for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 20 14:56:31 2012 gaetan senn
// Last update Sun Jul 15 14:50:46 2012 benjamin bourdin
//

#ifndef __INTERPRET_COMMAND__
#define __INTERPRET_COMMAND__

#include "AInterpret.hpp"
#include "Lexer.hh"
#include "FactoryZappyCommand.hh"
#include "AIEngine.hh"

class AIEngine;

template <typename T>
class InterpretCommand : public AInterpret<T, InterpretCommand<T> >
{
  T			&_target;
public:
  InterpretCommand(T & target)
    : AInterpret<T, InterpretCommand<T> >(&target, this), _target(target)
  {
    this->addPriorityCommand(&InterpretCommand<T>::mortReceived, Select::READ);
    this->addPriorityCommand(&InterpretCommand<T>::messageReceived,
			     Select::READ);
    this->addPriorityCommand(&InterpretCommand<T>::levelReceived,
			     Select::READ);
  }
  ~InterpretCommand(){}
  bool			callFactorySend(CommandZappy::eType type,
				    std::queue<Token> *q)
  {
    return ((Factory::getInstance()->*(Factory::getInstance()->
				       createCommandSend(type, &_target)))(q));
  }
  bool			callFactoryReceived(CommandZappy::eType type,
				    std::queue<Token> *q)
  {
    return ((Factory::getInstance()->*(Factory::getInstance()->
				       createCommandReceived(type,
							     &_target)))(q));
  }

  /* CALL FACTORY ABOUT FUNCTION */
  bool			avanceSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::avance, r));
  }

  bool			droiteSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::droite, r));
  }

  bool			gaucheSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::gauche, r));
  }

  bool			voirSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::voir, r));
  }

  bool			inventaireSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::inventaire, r));
  }

  bool			prend_objetSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::prend_objet, r));
  }

  bool			pose_objetSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::pose_objet, r));
  }

  bool			expluseSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::expluse, r));
  }

  bool			broadcast_texteSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::broadcast_texte, r));
  }

  bool			incantationSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::incantation, r));
  }

  bool			forkSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::fork, r));
  }

  bool			connect_nbrSend(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactorySend(CommandZappy::connect_nbr, r));
  }

  /* RECEIVED SET */

  bool			avanceReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::avance, r));
  }

  bool			droiteReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::droite, r));
  }

  bool			gaucheReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::gauche, r));
  }

  bool			voirReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::voir, r));
  }

  bool			inventaireReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::inventaire, r));
  }

  bool			prend_objetReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::prend_objet, r));
  }

  bool			pose_objetReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::pose_objet, r));
  }

  bool			expluseReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::expluse, r));
  }

  bool			broadcast_texteReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::broadcast_texte, r));
  }

  bool			incantationReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::incantation, r));
  }

  bool			forkReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::fork, r));
  }

  bool			connect_nbrReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::connect_nbr, r));
  }

  bool			mortReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::mort, r));
  }

  bool			messageReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::message, r));
  }

  bool			levelReceived(void *q)
  {
    std::queue<Token>	*r;
    r = static_cast<std::queue<Token> *>(q);
    return (callFactoryReceived(CommandZappy::level, r));
  }

  bool			checkCommand(std::queue<Token> *q)
  {
    if (q)
      return (Factory::getInstance()->findCommand(q));
    return (false);
  }
  void			start()
  {
    AIEngine::getInstance().run(*this, this->_target);
  }
};

#endif
