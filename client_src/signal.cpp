//
// signal.cpp for zappy in /home/gressi_b/Epitech/B4/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Sat Jul 14 14:38:24 2012 gressi_b
// Last update Sat Jul 14 14:38:24 2012 gressi_b
//

#include <errno.h>
#include <cstring>

#include "Exception.hh"
#include "signal.hh"

sig::fhandler_t
sig::setHandler(int signum, fhandler_t handler)
{
  fhandler_t	old = signal(signum, handler);

  if (old == SIG_ERR)
    throw Exception(strerror(errno), "sig::setHandler");
  return old;
}
