//
// BaseError.hh for plazza in /home/gressi_b/plazza
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Mon Mar 26 16:49:46 2012 gressi_b
// Last update Sun Jul 15 16:24:51 2012 tosoni_t
//

#ifndef	__BASE_ERROR_HPP__
#define	__BASE_ERROR_HPP__

/**
 * @author Banjamin Gressier
 * @file BaseError.hh
 */

#include <string>

// This class is intented to be ONLY used as a base class
// It should never be instanciated

/*!
 * @class BaseError
 * @brief Templated BaseError class
 */
template<typename T>
class BaseError
{
protected:
  mutable T	_error; /*!< templated error */

protected:
  /*!
   * @brief BaseError constructor
   */
  BaseError(T err = 0)
    : _error(err)
  {
  }

public:
  /*!
   * @brief BaseError destructor
   */
  virtual ~BaseError() {}

  /*!
   * @brief get the kind of error
   */
  T		getError() const
  {
    return (this->_error);
  }
};

/*!
 * @class BaseError
 * @brief Templated BaseError class with string
 */
template<>
class BaseError<std::string>
{
protected:
  mutable std::string		_error; /*!< string error */

public:
  /*!
   * @brief BaseError Constructor
   */
  BaseError(std::string const& err = "")
    : _error(err)
  {
  }

  /*!
   * @brief BaseError Dtor
   */
  virtual ~BaseError() {}

  /*!
   * @brief get error name
   */
  std::string const&	getError() const
  {
    return (this->_error);
  }
};

#endif
