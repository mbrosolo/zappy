//
// ExceptionQuit.hh for Zappy in /home/sarglen/zappy/graph_src
// 
// Made by anna texier
// Login   <texier_a@epitech.net>
// 
// Started on  Fri Jul 13 14:03:10 2012 anna texier
// Last update Sun Jul 15 18:06:30 2012 gaetan senn
//

#ifndef __EXCEPTION_MORT_HH__
#define __EXCEPTION_MORT_HH__

/**
 * @author Anna Texier
 * @file ExceptionMort.hh
 */

#include	<string>
#include	"ExceptionRuntime.hh"

/*!
 * @class ExceptionMort
 * @brief Exception to quit the programm
 */
class ExceptionMort : public ExceptionRuntime
{
public:
  /*!
   * @brief Constructor
   * @param what Message to know what happend
   * @param where Message to know where it happend
   */
  ExceptionMort(std::string const & = "", std::string const & = "") throw();
  /*!
   * @brief Destructor
   */
  virtual ~ExceptionMort() throw();
};

#endif
