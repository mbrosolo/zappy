//
// ExceptionRuntime.cpp for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:48:58 2012 benjamin bourdin
// Last update Wed May 23 06:53:58 2012 benjamin bourdin
//

#include "Exception.hh"

// Exception -------------------------------------------------------------------

Exception::Exception(std::string const& what, std::string const& where)
  : _what(what), _where(where)
{
}

// Member functions ------------------------------------------------------------

char const*	Exception::what() const throw()
{
  return (this->_what.c_str());
}

std::string const&	Exception::where() const
{
  return (this->_where);
}
