//
// ClientIA.hh for client zappy in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 13 11:02:56 2012 gaetan senn
// Last update Sun Jul 15 17:37:42 2012 benjamin bourdin
//

#ifndef __CLIENT_CPP__
#define __CLIENT_CPP__

/**
 * @author Gaetan Tahiti Rider Senn
 * @file ClientIA.hh
 */

#include <iostream>
#include <list>
#include <map>

#include "InterpretConnection.hpp"
#include "InterpretCommand.hpp"

#include "AClient.hh"

/*!
 * @class ClientIA
 * @brief Client IA class
 */
class		ClientIA : public AClient
{
  std::string			_team; /*!< Team name */
  unsigned int			_freeplayerspace; /*!< Nb players still accepted in team */
  unsigned int			_x; /*!< x position */
  unsigned int			_y; /*!< y position */
  unsigned int			_elevation; /*!< lvl up */

public:
  static const size_t RECOMMENDED_FOOD_LEVEL;/*!< Recommended food level */
  static const size_t RECOMMENDED_FOOD_LEVEL_FIRST;/*!< Recommended first food level  */
  static const size_t RECOMMENDED_FOOD_LEVEL_SEARCH_PLAYER;/*!< Recommended food level search player */

  /*!
   * @brief Stones types
   */
  enum eTypeStone
    {
      LINEMATE,
      DERAUMERE,
      SIBUR,
      MENDIANE,
      PHIRAS,
      THYSTAME,
      NB_STONES
    };

public:
  static const std::string CORRESP[NB_STONES]; /*!< Array of stones names */
private:

  std::map<eTypeStone, size_t>	_stones;/*!< map of stones and number of stones */
  size_t			_level;/*!< level */

  /*!
   * @brief Resource structure
   */
  struct Ressource
  {
    size_t	stones[NB_STONES];/*!<  array of stones */
    size_t	nbPlayersMandatory;/*!< Number of players to lvl up */

    /*!
     * @brief Ressource ctor
     */
    Ressource()
      : stones(), nbPlayersMandatory(1)
    { }

    /*!
     * @brief set stone to the array
     */
    inline void setStones(size_t st[NB_STONES])
    {
      for (size_t i = 0; i < NB_STONES; ++i)
	stones[i] = st[i];
    }
  };

public:
#define NB_LEVEL	8 /*!< Maximum level */
  Ressource	ressources[NB_LEVEL - 1]; /*!< Ressources needed */

public:
  /*!
   * @brief enum of directions
   */
  enum eDirection
    {
      UP,
      RIGHT,
      LEFT,
      UNSET
    };
private:
  eDirection	_direction;/*!< current direction */

public:
  /*!
   * @class MsgSend
   * @brief Class to send messages
   */
  class MsgSend
  {
  public:
    static const char DELIM; /*!< Delimiters of sent messages */

    static const std::string MSG_GATHER;/*!< Gather message */

    static const std::string MSG_STONE;/*!< Stone message */
    static const std::string MSG_STONE_INC;/*!< Add stone mesage */
    static const std::string MSG_STONE_DEC;/*!< Remove stone message */

    static const std::string MSG_PLAYER;/*!< Player message */
    static const std::string MSG_PLAYER_INC;/*!< Add player message*/
    static const std::string MSG_PLAYER_DEC;/*!< Remove player message */


    static const std::string MSG_SEND_DATA;/*!< Send data message */

    /*!
     * @brief Create message
     */
    static inline std::string buildMsg(const std::string &team,
				       const size_t &level,
				       const std::string &msg)
    {
      std::string ret;

      ret += team; ret += DELIM;
      {
	std::string str;
	std::stringstream ss;
	ss << level; ss >> str;
	ret += str;
      }
      ret += DELIM; ret += msg; ret += DELIM;
      return ret;
    }

    /*!
     * @brief Extract message from received data
     */
    static inline std::string extractMsgFromToken(std::list<std::string> const &list)
    {
      std::list<std::string>::const_iterator it = list.begin();
      if (it != list.end()) ++it;
      if (it != list.end()) ++it;
      if (it != list.end()) ++it;
      if (it != list.end())
	return *it;
      return std::string("");
    }

    /*!
     * @brief Extract direction from token
     */
    static inline size_t extractDirectionFromToken(std::list<std::string> const &list)
    {
      std::list<std::string>::const_iterator it = list.begin();
      if (it != list.end()) ++it;
      if (it != list.end())
	{
	  size_t dir = 0;
	  std::stringstream ss;
	  ss << *it; ss >> dir;
	  return dir;
	}
      return 0;
    }

    /*!
     * @brief Extract Team from message
     */
    static inline std::string extractTeam(std::string const &msg)
    {
      std::string ret;
      size_t pos = msg.find_first_of(DELIM, 0);
      ret = msg.substr(0, pos);
      return ret;
    }

    /*!
     * @brief Extract level from data
     */
    static inline size_t extractLevel(std::string const &msg)
    {
      size_t level = 0;
      std::string ret;
      size_t pos = msg.find_first_of(DELIM, 0);
      if (msg.size() < pos)
	return 0;
      size_t pos_end = msg.find_first_of(DELIM, pos + 1);
      ret = msg.substr(pos + 1, pos_end - pos - 1);
      {
	std::stringstream ss;
	std::string str;
	ss << ret;
	ss >> level;
      }
      return level;
    }

    /*!
     * @brief Extract Message from data
     */
    static inline std::string extractMsg(std::string const &msg)
    {
      std::string ret;
      size_t pos = msg.find_first_of(DELIM, 0);
      size_t pos_mid = msg.find_first_of(DELIM, pos + 1);
      size_t pos_end = msg.find_first_of(DELIM, pos_mid + 1);
      ret = msg.substr(pos_mid + 1, pos_end - pos_mid - 1);
      return ret;
    }

    /*!
     * @brief Create data for message
     */
    static inline std::string buildDataMsg(std::map<eTypeStone, size_t> const &stones,
					   const size_t players)
    {
      std::string ret(MSG_SEND_DATA);
      for (std::map<eTypeStone, size_t>::const_iterator it = stones.begin();
	   it != stones.end(); ++it)
	{
	  ret += CORRESP[it->first];
	  ret += "_";
	  {
	    std::stringstream ss;
	    std::string nb;
	    ss << it->second;
	    ss >> nb;
	    ret += nb;
	  }
	  ret += "_";
	}
      ret += "players_";
      {
	std::stringstream ss;
	std::string nb;
	ss << players; ss >> nb;
	ret += nb;
      }
      ret += "_";
      return ret;
    }

    /*!
     * @brief Remove data from message
     */
    static std::map<eTypeStone, size_t> unbuildDataMsg(std::string const &msg,
						       size_t *nbPlayers);

  };

public:
  /*!
   * @brief get type of the stone from its name
   */
  static inline eTypeStone getTypeStoneFromName(std::string const &name)
  {
    unsigned int i = 0;
    while (i < NB_STONES && name != CORRESP[i])
      ++i;
    // return NB_STONE if not found
    return static_cast<eTypeStone>(i);
  }

  /*!
   * @brief Empty the gather message
   */
  inline void emptyMessageGather()
  {
    for (std::list<std::map <CommandZappy::eType, std::queue<Token> const > >::iterator it = this->getCommandMessage().begin();
	 it != this->getCommandMessage().end(); )
      {
	// std::queue<Token> tmpq = client.getListToken(CommandZappy::message, it);
	std::queue<Token> tmpq;
	if (it->find(CommandZappy::message) != it->end())
	  tmpq = (*it)[CommandZappy::message];

	std::list<std::string> tmplist = this->getListItemMessage(&tmpq);
	std::string tmpmsg = ClientIA::MsgSend::extractMsg(ClientIA::MsgSend::extractMsgFromToken(tmplist));

	if (tmpmsg.compare(0, ClientIA::MsgSend::MSG_GATHER.size(), ClientIA::MsgSend::MSG_GATHER) == 0)
	  it = this->getCommandMessage().erase(it);
	else
	  ++it;
      }
  }

  /*!
   * @brief get food with interpreter
   */
  inline size_t getFood(interpreter_t &c)
  {
    c.addSendFunction(&InterpretCommand<ClientIA>::inventaireSend, NULL);
    c.addReceivedFunction(&InterpretCommand<ClientIA>::inventaireReceived);
    c.run(false);
    std::queue<Token> listToken = this->getListToken(CommandZappy::inventaire, this->getCommand());
    size_t food = 0;
    if (!listToken.empty())
      {
	std::vector< std::list<std::string> > _items = this->extractVoirInventaire(&listToken);
	food = this->extractFoodFromInventory(_items);
	if (!this->getCommand().empty())
	  this->getCommand().pop_front();
      }
    return food;
  }

private:
  std::string _stoneToTake; /*!< Name of stone to take */
  bool _hello; /*!< Say hello */
  bool _firstReserve; /*! Eat 'till enough food */

  size_t _players[NB_LEVEL - 1]; /*! players with levels*/
  bool _deathProg; /*! program the death */

public:
  /*!
   * @brief Client IA Ctor
   */
  ClientIA();
  /*!
   * @brief Client IA Dtor
   */
  ~ClientIA();
  /*!
   * @brief get name of team
   */
  std::string	const	&getTeam() const;
  /*!
   * @brief Extract "Voir" and "Inventaire" commands
   */
  std::vector< std::list<std::string> >		extractVoirInventaire(std::queue<Token> *q);
  /*!
   * @brief Get list items
   */
  std::list<std::string>			getListItem(std::queue<Token> *q);
  /*!
   * @brief Get list item message
   */
  std::list<std::string>			getListItemMessage(std::queue<Token> *q);
  /*!
   * @brief Extract food from inventory
   */
  size_t	extractFoodFromInventory(std::vector< std::list<std::string> > const &) const;
  /*!
   * @brief get token list
   */
  std::queue<Token> getListToken(const CommandZappy::eType,
				 std::list<std::map <CommandZappy::eType,
						     std::queue<Token> const > > &);

  /*!
   * @brief Check if its on square
   */
  std::list<std::string> onTheCase(std::vector< std::list<std::string> > const &) const;

  /*!
   * @brief send message
   */
  void	sendMessage(std::string const &, interpreter_t &);
  /*!
   * @brief send player message
   */
  void sendPlayer(std::string const &, std::string const &, interpreter_t &);

  /*!
   * @brief set a free player space
   */
  void			setFreePLayerSpace(unsigned int value);
  /*!
   * @brief set team for player
   */
  void			setTeam(std::string const & team);
  /*!
   * @brief set elevation nb to player
   */
  void			setElevation(unsigned int level);
  /*!
   * @brief launch
   */
  void			launch(int port, std::string const & hostname);
  /*!
   * @brief set coordiantes for player
   */
  void			setCoord(unsigned int x, unsigned int y);

  /*!
   * @brief check if it is a first reserve
   */
  bool isFirstReserve(void) const;
  /*!
   * @brief check if it is not a first reserve
   */
  void notFirstReserve(void);

  /*!
   * @brief get stones map
   */
  std::map<eTypeStone, size_t>&			getStones();
  /*!
   * @brief get number of players
   */
  size_t					getNbPlayers() const;
  /*!
   * @brief get level number
   */
  size_t					getLevel() const;
  /*!
   * @brief get direction
   */
  eDirection					getDirection() const;

  /*!
   * @brief set direction for player
   */
  void						setDirection(const eDirection);
  /*!
   * @brief add number of players
   */
  void						incPlayer(const int);

  /*!
   * @brief set number of players
   */
  void						setPlayer(const size_t);

  /*!
   * @brief Change level number
   */
  void						incLevel();
  /*!
   * @brief Set level number
   */
  void						setLevel(unsigned int value);
  /*!
   * @brief Add stone to numbwer of stones
   */
  void						incStone(const eTypeStone, const int);
  /*!
   * @brief Set number of stones
   */
  void						setStone(const eTypeStone, const size_t);
  /*!
   * @brief set number of stones to take
   */
  void						setStoneToTake(std::string const &);
  /*!
   * @brief get the stone to take
   */
  std::string const &				getStoneToTake(void) const;

  /*!
   * @brief check if played said "hi!"
   */
  bool hasSaidHello(void) const;
  /*!
   * @brief say hi to the player of a team, it is very very important
   */
  void sayHello(interpreter_t &);
  /*!
   * @brief does the player has to say hello?
   */
  void hasToSayHello(void);

  /*!
   * @brief move player ahead
   */
  void moveAhead(interpreter_t &);
  /*!
   * @brief move player right
   */
  void moveRight(interpreter_t &);
  /*!
   * @brief move player to the left
   */
  void moveLeft(interpreter_t &);

  /*!
   * @brief set player
   */
  void setPlayer(const size_t level, const int inc);
  /*!
   * @brief inc player lvl
   */
  void incPlayer(const size_t level, const int inc);
  /*!
   * @brief say if the death is programmated
   */
  bool isDeathProgrammated() const;
  /*!
   * @brief program the death
   */
  void deathProgrammated();

};

#endif
