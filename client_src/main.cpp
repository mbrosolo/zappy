//
// main.cpp for zappy in /home/gressi_b/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Wed Jun  6 15:52:45 2012 gressi_b
// Last update Sun Jul 15 17:44:36 2012 gaetan senn
//

#include <err.h>
#include <iostream>
#include "ClientIA.hh"
#include "InterpretCommand.hpp"
#include "Argument.hh"
#include "ExceptionSocket.hh"

#define USAGE "./client -n [nom equipe] -p [port] -h [nom de la machine]"

int	main(int ac, char **av)
{
  Argument		args;
  ClientIA		client;
  InterpretCommand<ClientIA>	test(client);

  {
    time_t t = time(NULL);
    if (t == (time_t) -1)
      {
	warn("time");
	t = 1;
      }
    srandom(t * getpid());
  }
  try
    {
      if (!args.parse(ac, av))
	{
	  std::cerr << args.getError() << std::endl;
	  std::cerr << "Usage : " << USAGE << std::endl;
	  return (-1);
	}
      client.setTeam(args.getTeamName());
      client.launch(args.getPort(), args.getHostName());
    }
  catch (ExceptionSocket &e)
    {
      std::cerr << "Client : Error From => " << e.where() << std::endl;
      std::cerr << "Client : Error Type => " << e.what() << std::endl;
    }
  catch (...)
    {
      std::cerr << "Unexpected error occured." << std::endl;
    }
  return (0);
}
