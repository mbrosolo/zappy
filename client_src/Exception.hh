//
// ExceptionRuntime.hh for  in /home/bourdi_b/bomberman
//
// Made by benjamin bourdin
// Login   <bourdi_b@epitech.net>
//
// Started on  Wed May 23 06:47:42 2012 benjamin bourdin
// Last update Wed May 23 06:53:08 2012 benjamin bourdin
//

#ifndef __EXCEPTION_HH__
#define __EXCEPTION_HH__

#include <stdexcept>
#include <exception>
#include <string>

/**
 * @author bourdi_b
 * @class Exception
 * @brief Exception class used everywhere in the project.
 * It is often extended.
 */

class Exception : public std::exception
{
protected:
  std::string const	_what;
  std::string const	_where;

public:
  Exception(std::string const&, std::string const& = "");
  virtual ~Exception() throw() {}

  /*!
   * @brief: return a string which give information
   * about the nature of the exception
   */
  virtual char const*	what() const throw();

  /*!
   * @brief: return a string which give information
   * about where the exception occured
   */
  std::string const&	where() const;
};

#endif
