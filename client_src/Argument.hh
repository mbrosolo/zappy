//
// Argument.hh for zappy in /home/gressi_b/Epitech/B4/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Tue Jun 19 18:08:08 2012 gressi_b
// Last update Sun Jul 15 17:36:06 2012 gaetan senn
//

#ifndef	__ARGUMENT_HH__
#define	__ARGUMENT_HH__

/**
 * @author Banjamin Gressier
 * @file Argument.hh
 */

#include <vector>
#include "BaseError.hpp"
#include <iostream>

/*!
 * @class Argument
 * @brief
 */
class Argument : public BaseError<std::string>
{
private:
  std::string	_teamName;  /*!<  string of team name */
  int		_port;  /*!< port number */
  std::string	_hostName;  /*!< name of the host */

private:
  /*!
   * @brief check if argument is an option
   */
  static bool	_isOption(std::string const&);
  /*!
   * @brief check arguments
   */
  bool		_checkArgs(std::string const&);

public:
  /*!
   * @brief Ctor
   */
  Argument();
  /*!
   * @brief Dtor
   */
  ~Argument() {}

  /*!
   * @brief parsing function
   */
  bool		parse(int, char**);

  /*!
   * @brief get the name of the team
   */
  std::string const&	getTeamName() const;
  /*!
   * @brief get the host name
   */
  std::string const&	getHostName() const;
  /*!
   * @brief get the choasen port
   */
  int			getPort() const;
};

#endif
