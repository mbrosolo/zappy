//
// FactoryZappyCommand.hh for zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 20 13:48:33 2012 gaetan senn
// Last update Sun Jul 15 17:57:45 2012 gaetan senn
//

#ifndef __FACTORY_ZAPPY_COMMAND__
#define __FACTORY_ZAPPY_COMMAND__

#include <queue>
#include <map>

#include "Token.hh"
#include "FactoryHeader.hh"
#include "ExceptionMort.hh"

#define delimitor "\n"

#define command_avance "avance"delimitor
#define command_droite "droite"delimitor
#define command_gauche "gauche"delimitor
#define command_voir "voir"delimitor
#define command_inventaire "inventaire"delimitor
#define command_prend_objet "prend "
#define command_pose_objet "pose "
#define command_expluse "expluse"delimitor
#define command_broadcast_texte "broadcast "
#define command_incantation "incantation"delimitor
#define command_fork "fork"delimitor
#define command_connect_nbr "connect_nbr"delimitor

class AClient;

class		Factory
{
public:
  typedef       bool		(Factory::*action)(void *); /*!< typedef of Factory Function */
private:
  static	Factory		*_instance; /*!< instance variable of Factory */
  std::map<CommandZappy::eWay,
	   std::map< CommandZappy::eType, action> >	_creatorpt; /*!< map of function */
  AClient				*_target; /*!< target of Factory Working space */
public:
  /**
   * @brief Get Instance of Factory
   *
   *
   * @return Factory pt
   */
  static	Factory		*getInstance();
  /**
   * @brief killInstance of Factory
   *
   */
  static	void		killInstance();

  /**
   * @brief Factory Constructor
   *
   */
  Factory();
  /**
   * @brief Factory Coplien
   *
   */
  Factory(Factory const &);
  /**
   * @brief Factory Coplien
   *
   *
   * @return
   */
  Factory	const & operator=(Factory const &);
  /**
   * @brief Factory Dtor
   *
   */
  ~Factory();

  /* TOOLS */
  /**
   * @brief getted of Object from voir Command
   *
   *
   * @return boolean
   */
  bool			getObjectsvoir(std::queue<Token> *);
  /**
   * @brief getObjectsinventaire function
   *
   *
   * @return boolean
   */
  bool			getObjectsinventaire(void *);
  /**
   * @brief addIdentifiedCommand function
   *
   *
   * @return boolean
   */
  bool			addIdentifiedCommand(CommandZappy::eType, std::queue<Token> const &);
  /**
   * @brief addIdentifiedCommandMessage function
   *
   *
   * @return boolean
   */
  bool			addIdentifiedCommandMessage(std::queue<Token> const &q);
    /**
   * @brief addIdentifiedCommandLevel function
   *
   *
   * @return boolean
   */
  bool			addIdentifiedCommandLevel(std::queue<Token> const &q);
  /* METHOD FACTORY */
  /**
   * @brief createCommandSend function to sending data to server
   *
   *
   * @return boolean
   */
  action		createCommandSend(CommandZappy::eType,
					  AClient *target);
    /**
   * @brief createCommandReceived function to Received data from server
   *
   *
   * @return boolean
   */
  action		createCommandReceived(CommandZappy::eType,
					      AClient *target);
    /**
   * @brief findCommand function for find Command Received from server
   *
   *
   * @return boolean
   */
  bool			findCommand(std::queue<Token> *);
  /* SEND METHOD COMMAND ZAPPY */
    /**
   * @brief avance Send Command
   *
   *
   * @return boolean
   */
  bool			avanceSend(void *);
    /**
   * @brief droite Send Command
   *
   *
   * @return boolean
   */
  bool			droiteSend(void *);
    /**
   * @brief gauche Send Command
   *
   *
   * @return boolean
   */
  bool			gaucheSend(void *);
    /**
   * @brief voir Send Command
   *
   *
   * @return boolean
   */
  bool			voirSend(void *);
    /**
   * @brief inventaire Send Command
   *
   *
   * @return boolean
   */
  bool			inventaireSend(void *);
    /**
   * @brief prend Object Send Command
   *
   *
   * @return boolean
   */
  bool			prend_objetSend(void *);
    /**
   * @brief pose Object Send Command
   *
   *
   * @return boolean
   */
  bool			pose_objetSend(void *);
    /**
   * @brief expluse Send Command
   *
   *
   * @return boolean
   */
  bool			expluseSend(void *);
    /**
   * @brief broadcast texte send Command
   *
   *
   * @return boolean
   */
  bool			broadcast_texteSend(void *);
    /**
   * @brief incantation Send Command
   *
   *
   * @return boolean
   */
  bool			incantationSend(void *);
    /**
   * @brief fork Send Command
   *
   *
   * @return boolean
   */
  bool			forkSend(void *);
    /**
   * @brief Connect nbr Send Command
   *
   *
   * @return boolean
   */
  bool			connect_nbrSend(void *);

  /* RECEIVED METHOD COMMAND ZAPPY */

  bool			avanceReceived(void *);
    /**
   * @brief droite Received Command
   *
   *
   * @return boolean
   */
  bool			droiteReceived(void *);
    /**
   * @brief gauche Received Command
   *
   *
   * @return boolean
   */
  bool			gaucheReceived(void *);
    /**
   * @brief voir Received Command
   *
   *
   * @return boolean
   */
  bool			voirReceived(void *);
    /**
   * @brief inventaire Received Command
   *
   *
   * @return boolean
   */
  bool			inventaireReceived(void *);
    /**
   * @brief prend object Received Command
   *
   *
   * @return boolean
   */
  bool			prend_objetReceived(void *);
    /**
   * @brief pose object Received Command
   *
   *
   * @return boolean
   */
  bool			pose_objetReceived(void *);
    /**
   * @brief expluse Received Command
   *
   *
   * @return boolean
   */
  bool			expluseReceived(void *);
    /**
   * @brief broadcast_texteReceived Command
   *
   *
   * @return boolean
   */
  bool			broadcast_texteReceived(void *);
    /**
   * @brief incantation Received Command
   *
   *
   * @return boolean
   */
  bool			incantationReceived(void *);
    /**
   * @brief Fork Received Command
   *
   *
   * @return boolean
   */
  bool			forkReceived(void *);
    /**
   * @brief connect nbr Received Command
   *
   *
   * @return boolean
   */
  bool			connect_nbrReceived(void *);
    /**
   * @brief mort Received Command
   *
   *
   * @return boolean
   */
  bool			mortReceived(void *);
    /**
   * @brief message Received Command
   *
   *
   * @return boolean
   */
  bool			messageReceived(void *);
      /**
   * @brief level Received Command
   *
   *
   * @return boolean
   */
  bool			levelReceived(void *);
};

#endif
