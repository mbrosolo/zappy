//
// FactoryZappyCommand.cpp for Zappy client in /home/ga/Documents/Projet/Zappy/git/zappy/client_src
//
// Made by gaetan senn
// Login   <senn_g@epitech.net>
//
// Started on  Wed Jun 20 13:55:18 2012 gaetan senn

//

#include "FactoryZappyCommand.hh"

#include "AClient.hh"
#include "ClientIA.hh"

Factory		*Factory::_instance = NULL;

Factory::Factory()
  : _creatorpt(), _target(NULL)
{
  /* ABOUT SEND FUNCTION ZAPPY COMMAND */
  _creatorpt[CommandZappy::SEND][CommandZappy::avance] =
    &Factory::avanceSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::droite] =
    &Factory::droiteSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::gauche] =
    &Factory::gaucheSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::voir] =
    &Factory::voirSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::inventaire] =
    &Factory::inventaireSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::prend_objet] =
    &Factory::prend_objetSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::pose_objet] =
    &Factory::pose_objetSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::expluse] =
    &Factory::expluseSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::broadcast_texte] =
    &Factory::broadcast_texteSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::incantation] =
    &Factory::incantationSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::fork] =
    &Factory::forkSend;
  _creatorpt[CommandZappy::SEND][CommandZappy::connect_nbr] =
    &Factory::connect_nbrSend;

  /* ABOUT RECEIVED FUNCTION ZAPPY COMMAND */
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::avance] =
    &Factory::avanceReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::droite] =
    &Factory::droiteReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::gauche] =
    &Factory::gaucheReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::voir] =
    &Factory::voirReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::inventaire] =
    &Factory::inventaireReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::prend_objet] =
    &Factory::prend_objetReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::pose_objet] =
    &Factory::pose_objetReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::expluse] =
    &Factory::expluseReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::broadcast_texte] =
    &Factory::broadcast_texteReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::incantation] =
    &Factory::incantationReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::fork] =
    &Factory::forkReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::connect_nbr] =
    &Factory::connect_nbrReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::mort] =
    &Factory::mortReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::message] =
    &Factory::messageReceived;
  _creatorpt[CommandZappy::RECEIVED][CommandZappy::level] =
    &Factory::levelReceived;
}

Factory::~Factory()
{
}

Factory		*Factory::getInstance(void)
{
  if (!_instance)
    _instance = new Factory();
  return (_instance);
}

void		Factory::killInstance(void)
{
  if (_instance)
    delete _instance;
  _instance = NULL;
}

Factory::action		Factory::createCommandSend(CommandZappy::eType type,
						   AClient *target)
{
  if (!_target && target)
    _target = target;
  if (_creatorpt[CommandZappy::SEND].find(type) ==
      _creatorpt[CommandZappy::SEND].end())
    return (NULL);
  return (_creatorpt[CommandZappy::SEND][type]);
}

Factory::action		Factory::createCommandReceived(CommandZappy::eType
						       type, AClient *target)
{
  if (!_target && target)
    _target = target;
  if (_creatorpt[CommandZappy::RECEIVED].find(type) ==
      _creatorpt[CommandZappy::SEND].end())
    return (NULL);
  return (_creatorpt[CommandZappy::RECEIVED][type]);
}

bool			Factory::findCommand(std::queue<Token> *q)
{
  std::map<CommandZappy::eWay, std::map< CommandZappy::eType,
					  action> >::iterator	it;
   std::map< CommandZappy::eType, action>::iterator	it2;

   if (q->size() == 1 && q->front().getData() == "ko")
     return (false);
   for (it = _creatorpt.begin(); it != _creatorpt.end();++it)
     {
       if (it->first == CommandZappy::RECEIVED)
	 {
	   for (it2 = it->second.begin();it2 != it->second.end();++it2)
	     {
	       if ((this->*it2->second)(q))
		 return (true);
	     }
	 }
     }
   return (false);
 }

 /* FUNCTION SEND */

bool			Factory::avanceSend(void *)
{
  this->_target->addCommand(command_avance);
  return (true);
}

bool			Factory::droiteSend(void *)
{
  this->_target->addCommand(command_droite);
  return (true);
}

bool			Factory::gaucheSend(void *)
{
  this->_target->addCommand(command_gauche);
  return (true);
}

bool			Factory::voirSend(void *)
{
  this->_target->addCommand(command_voir);
  return (true);
}

bool			Factory::inventaireSend(void *)
{
  this->_target->addCommand(command_inventaire);
  return (true);
}

bool			Factory::prend_objetSend(void *data)
{
  std::string		command(command_prend_objet);

  if (data)
    {
      command += *(static_cast<std::string *>(data));
      command += delimitor;
      delete (static_cast<std::string *>(data));
    }
  this->_target->addCommand(command);
  return (true);
}

bool			Factory::pose_objetSend(void *data)
{
  std::string		command(command_pose_objet);

  if (data)
    {
      command += *(static_cast<std::string *>(data));
      command += delimitor;
      delete (static_cast<std::string *>(data));
    }
  this->_target->addCommand(command);
  return (true);
}

bool			Factory::expluseSend(void *)
{
  this->_target->addCommand(command_expluse);
  return (true);
}

bool			Factory::broadcast_texteSend(void *data)
{
  std::string		command(command_broadcast_texte);

  if (data)
    {
      command += *(static_cast<std::string *>(data));
      command += delimitor;
      delete (static_cast<std::string *>(data));
    }
  this->_target->addCommand(command);
  return (true);
}

bool			Factory::incantationSend(void *)
{
  this->_target->addCommand(command_incantation);
  return (true);
}

bool			Factory::forkSend(void *)
{
  this->_target->addCommand(command_fork);
  return (true);
}

bool			Factory::connect_nbrSend(void *)
{
  this->_target->addCommand(command_connect_nbr);
  return (true);
}

 /* RECEIVED FUNCTION */

bool			Factory::addIdentifiedCommand(CommandZappy::eType type,
						      std::queue<Token> const &q)
{
  _target->addIdentifiedCommand(type, q);
  return (true);
}

bool			Factory::addIdentifiedCommandMessage(std::queue<Token> const &q)
{
  _target->addIdentifiedCommandMessage(q);
  return (true);
}

bool			Factory::addIdentifiedCommandLevel(std::queue<Token> const &q)
{
  _target->addIdentifiedCommandLevel(q);
  return (true);
}


bool			Factory::avanceReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING &&
      q->front().getData() == "ok")
    return (addIdentifiedCommand(CommandZappy::avance, *q));
  return (false);
}

bool			Factory::droiteReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING &&
      q->front().getData() == "ok")
    return (addIdentifiedCommand(CommandZappy::droite, *q));
  return (false);
}

bool			Factory::gaucheReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING &&
      q->front().getData() == "ok")
    return (addIdentifiedCommand(CommandZappy::gauche, *q));
  return (false);
}

bool			Factory::getObjectsvoir(std::queue<Token> *q)
{
  while (!q->empty())
    {
      Token			t = q->front();
      q->pop();
      if (t.getType() == Token::COMMA)
	return (true);
      if (t.getType() != Token::BLANK)
	return (false);
      t = q->front();
      q->pop();
      if (t.getType() != Token::STRING)
	return (false);
      t = q->front();
      if (t.getType() == Token::COMMA || t.getType() == Token::CLOSE_BRAKET)
	return (true);
    }
  return (true);
}

bool			Factory::voirReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  std::queue<Token>	bk = *q;
  Token			t = bk.front();

  bk.pop();
  if (t.getType() != Token::OPEN_BRAKET)
    return (false);
  while (!bk.empty())
    {
      if (!getObjectsvoir(&bk))
	return (false);
      if (bk.front().getType() == Token::CLOSE_BRAKET)
	return (addIdentifiedCommand(CommandZappy::voir, *q));
    }
  return (addIdentifiedCommand(CommandZappy::voir, *q));
}

bool			Factory::getObjectsinventaire(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  while (!q->empty())
    {
      Token			t = q->front();
      q->pop();
      if (t.getType() != Token::STRING)
	return (false);
      t = q->front();
      q->pop();
      if (t.getType() != Token::BLANK)
	return (false);
      t = q->front();
      q->pop();
      if (t.getType() != Token::UNSIGNED_INT)
	return (false);
      t = q->front();
      if (t.getType() == Token::CLOSE_BRAKET)
	return (true);
      q->pop();
    }
  return (true);
}

bool			Factory::inventaireReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  std::queue<Token>	bk = *q;
  Token			t = bk.front();

  bk.pop();
  if (t.getType() != Token::OPEN_BRAKET)
    return (false);
  while (!bk.empty())
    {
      if (!getObjectsinventaire(&bk))
	return (false);
      if (bk.front().getType() == Token::CLOSE_BRAKET)
	return (addIdentifiedCommand(CommandZappy::inventaire, *q));
    }
  return (addIdentifiedCommand(CommandZappy::inventaire, *q));
}

bool			Factory::prend_objetReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING)
    if (q->front().getData() == "ok" || q->front().getData() == "ko")
      return (addIdentifiedCommand(CommandZappy::prend_objet, *q));
  return(false);
}

bool			Factory::pose_objetReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING)
    if (q->front().getData() == "ok" || q->front().getData() == "ko")
      return (addIdentifiedCommand(CommandZappy::pose_objet, *q));
  return(false);
}

bool			Factory::expluseReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING)
    if (q->front().getData() == "ok" || q->front().getData() == "ko")
      return (addIdentifiedCommand(CommandZappy::expluse, *q));
  return(false);
}

bool			Factory::broadcast_texteReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING)
    if (q->front().getData() == "ok")
      return (addIdentifiedCommand(CommandZappy::broadcast_texte, *q));
  return(false);
}

bool			Factory::incantationReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if ((q->size() == 1 && q->front().getType() == Token::STRING &&
       q->front().getData() == "ko") ||
      (q->size() == 5 && q->front().getType() == Token::STRING &&
       q->front().getData() == "elevation"))
    return (addIdentifiedCommand(CommandZappy::incantation, *q));
  return (false);
}

bool			Factory::forkReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING &&
      q->front().getData() == "ok")
    return (addIdentifiedCommand(CommandZappy::fork, *q));
  return(false);
}

bool			Factory::connect_nbrReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::UNSIGNED_INT)
    {
      std::stringstream	ss;
      unsigned int	res;

      ss << q->front().getData();
      ss >> res;
      dynamic_cast<ClientIA *>(_target)->setFreePLayerSpace(res);
      return (addIdentifiedCommand(CommandZappy::connect_nbr, *q));
    }
  return (false);
}

bool			Factory::mortReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  if (q->size() == 1 && q->front().getType() == Token::STRING &&
      q->front().getData() == "mort")
    {
      throw ExceptionMort(" IA Dead", " mortReceived");
      return (addIdentifiedCommand(CommandZappy::mort, *q));
    }
  return(false);
}

bool			Factory::messageReceived(void *r)
{
  std::queue<Token>	*q;

  q = static_cast<std::queue<Token> *>(r);
  std::queue<Token>	bk = *q;

  if (bk.front().getType() == Token::STRING &&
      bk.front().getData() == "message")
    {
      bk.pop();
      if (bk.front().getType() != Token::BLANK)
	return (false);
      bk.pop();
      if (bk.front().getType() != Token::UNSIGNED_INT)
	return (false);
      bk.pop();
      if (bk.front().getType() != Token::COMMA)
	return (false);
      return (addIdentifiedCommandMessage(*q));
    }
  return (false);
}

bool			Factory::levelReceived(void *r)
{
    std::queue<Token>	*q;
    unsigned int	 value;

    std::cout << "\033[32m Command received \033[0m" << std::endl;

    q = static_cast<std::queue<Token> *>(r);
    std::queue<Token>	bk = *q;
    if (bk.front().getData() == "ko")
      return (addIdentifiedCommandLevel(*q));
    if (bk.front().getType() == Token::STRING &&
	bk.front().getData() == "niveau")
      {
	bk.pop();
	if (bk.front().getType() != Token::BLANK)
	  return (false);
	bk.pop();
	if (bk.front().getData() != "actuel")
	  return (false);
	bk.pop();
	if (bk.front().getType() != Token::BLANK)
	  return (false);
	bk.pop();
	if (bk.front().getData() != ":")
	  return (false);
	bk.pop();
	if (bk.front().getType() != Token::BLANK)
	  return (false);
	bk.pop();
	if (bk.front().getType() != Token::UNSIGNED_INT)
	  return (false);
	std::stringstream	ss;
	ss << bk.front().getData();
	ss >> value;
	return (addIdentifiedCommandLevel(*q));
      }
    return (false);
}
