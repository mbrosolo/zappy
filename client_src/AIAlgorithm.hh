//
// AIAlgorithm.hh for zappy in /home/gressi_b/Epitech/B4/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Thu Jun 28 14:21:06 2012 gressi_b
// Last update Sun Jul 15 16:50:42 2012 tosoni_t
//

#ifndef	__AI_ALGORITHM_HH__
#define	__AI_ALGORITHM_HH__

/**
 * @author Banjamin Gressier
 * @file AIAlgorithm.hh
 */

#include "../lib/graph/Graph.hpp"
#include "AInterpret.hpp"

/*!
 * @class ClientIA
 * @brief Client IA class
 */
class ClientIA;
/*!
 * @class InterpretCommand
 * @brief Interpret Command class
 */
template<typename T> class InterpretCommand;
/*!
 * @class AInterpret
 * @brief structure of clientIA / InterpretCommand typedef
 */
typedef AInterpret<ClientIA, InterpretCommand<ClientIA> > interpreter_t;

/*!
 * @brief IA namespace
 */
namespace ai
{
  struct Data; /*!< Data structure */

  /*!
   * @brief Enum of labels
   */
  enum eLabel
    {
      YES,
      NO,
      NO_MATTER
    };

  /*!
   * @brief Graph of data / label
   */
  typedef gr::TGraph<Data, eLabel>	Graph;
  /*!
   * @brief graph vertex
   */
  typedef Graph::Vertex			Vertex;
  /*!
   * @brief graph edge
   */
  typedef Graph::Edge			Edge;
  /*!
   * @brief function pointer on function_t
   */
  typedef Vertex*	(*function_t)(Vertex*, interpreter_t &, ClientIA&);

  /*!
   * @brief Data structure
   */
  struct Data
  {
    function_t	f;  /*!< function_t  */

    /*!
     * @brief Data constructor
     */
    Data(function_t _f = 0) : f(_f) {}
    /*!
     * @brief Data dtor
     */
    ~Data() {}

    /*!
     * @brief operator == of data
     */
    bool
    operator==(Data const& d)
    {
      return (this->f == d.f);
    }
  };

  /*!
   * @brief Vertex start
   */
  Vertex*	start(Vertex*, interpreter_t &, ClientIA&);			// 0
  /*!
   * @brief Check if food is on given quare
   */
  Vertex*	isFoodOnSquare(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Take food on square
   */
  Vertex*	takeFood(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Search food near square
   */
  Vertex*	searchFood(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Goes to the closest food
   */
  Vertex*	headForClosestFood(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Random movement
   */
  Vertex*	randomlyMove(Vertex*, interpreter_t &, ClientIA&);		// 5
  /*!
   * @brief Check if client received broadcast
   */
  Vertex*	checkBroadcast(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check if client is on same team
   */
  Vertex*	checkSameTeam(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check client level
   */
  Vertex*	checkLevel(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check client data
   */
  Vertex*	checkData(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check if stones and players are enough
   */
  Vertex*	setStoneAndPlayer(Vertex*, interpreter_t &, ClientIA&);		// 10

  /*!
   * @brief Sends Stone and player number
   */
  Vertex*	sendStoneAndPlayerNumber(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check kind of stones
   */
  Vertex*	checkStoneInfo(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Add a stone to inventory
   */
  Vertex*	incrementStoneNumber(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check player data
   */
  Vertex*	checkPlayerInfo(Vertex*, interpreter_t &, ClientIA&);		// 15
  /*!
   * @brief Add a player to a team
   */
  Vertex*	incrementPlayerNumber(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check if client can gather
   */
  Vertex*	checkGather(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Give direction to client
   */
  Vertex*	giveDirection(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Throw all stones on current square
   */
  Vertex*	layAllStone(Vertex*, interpreter_t &, ClientIA&);		// 20
  /*!
   * @brief Check number stones on the ground 
   */
  Vertex*	checkIfTooManyLaidStone(Vertex*, interpreter_t &, ClientIA&);


  /*!
   * @brief LvlUp Client
   */
  Vertex*	incantation(Vertex*, interpreter_t &, ClientIA&);

  /*!
   * @brief Scatter leveled up clients
   */
  Vertex*	scatter(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check if it need to do incantation
   */
  Vertex*	checkIfNeedLevelUp(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check if number of stones is ok to lvl up
   */
  Vertex*	hasEnoughStoneToLevelUp(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check if nunmber of players is ok to lvl up
   */
  Vertex*	hasEnoughPlayerToLevelUp(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check if enough players are on the same square to lvl up
   */
  Vertex*	isThereEnoughPlayerOnCase(Vertex*, interpreter_t &, ClientIA&);	// 30
  /*!
   * @brief Check if stone is in a square
   */
  Vertex*	checkIfStoneInCase(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Take stone and add to inventory
   */
  Vertex*	takeStone(Vertex*, interpreter_t &, ClientIA&);

  /*!
   * @brief Check squares around the client
   */
  Vertex*	checkSight(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Move forward
   */
  Vertex*	goAhead(Vertex*, interpreter_t &, ClientIA&);			// 35
  /*!
   * @brief Send broadcast
   */
  Vertex*	broadcast(Vertex*, interpreter_t &, ClientIA&);
  /*!
   * @brief Check stones and player number
   */
  Vertex*	checkStoneAndPlayerNumber(Vertex*, interpreter_t &, ClientIA&);

  /*!
   * @brief Check number of clients connected 
   */
  Vertex*	checkNbConnect(Vertex*, interpreter_t &, ClientIA&);

  /*!
   * @brief Fork
   */
  Vertex*	doFork(Vertex*, interpreter_t &, ClientIA&);
}

#endif
