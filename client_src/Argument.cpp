//
// Argument.cpp for zappy in /home/gressi_b/Epitech/B4/zappy/client_src
//
// Made by gressi_b
// Login   <gressi_b@epitech.net>
//
// Started on  Tue Jun 19 18:09:32 2012 gressi_b
// Last update Sun Jul 15 23:22:42 2012 thomas tosoni
//

#include <sstream>
#include <cstdlib>

#include "Argument.hh"

// Argument --------------------------------------------------------------------

Argument::Argument()
  : BaseError<std::string>(), _teamName(""), _port(0), _hostName("localhost")
{
}

// Member functions ------------------------------------------------------------

bool
Argument::_isOption(std::string const& str)
{
  return (str.size() > 1 && str[0] == '-');
}

bool
Argument::_checkArgs(std::string const& progname)
{
  if (this->_teamName == "")
    {
      this->_error = progname + ": team name missing";
      return false;
    }
  if (this->_port == 0)
    {
      this->_error = progname + ": invalid port number";
      return false;
    }
  return true;
}

bool
Argument::parse(int ac, char **av)
{
  std::stringstream	ss;
  int			c;

  while ((c = getopt(ac, av, "n:p:h:")) != -1)
    {
       if (optarg && Argument::_isOption(optarg))
	 {
	   ss << av[0] << ": option requires an argument -- '" << c << '\'';
	   this->_error = ss.str();
	   return (false);
	 }
      if (c == 'n')
	this->_teamName = optarg;
      else if (c == 'p')
	this->_port = atoi(optarg);
      else if (c == 'h')
	this->_hostName = optarg;
      else
	return (false);
    }
  return (this->_checkArgs(av[0]));

}

#if 0
static int	check_args(char const *progname, t_args *args)
{
  if (args->team_name == NULL)
    {
      std::cerr << "Null team error" << std::endl;
      return (1);
    }
  if (args->team_name[0] == '\0')
    {
      std::cerr << "Team name error" << std::endl;
      return (1);
    }
  if (args->port == 0)
    {
      std::cerr << "Port error" << std::endl;
      return (1);
    }
  return (0);
}

int		parse_args(int ac, char **av, t_args *args)
{
  int		c;

  args->team_name = DEFAULT_TEAM_NAME;
  args->port = DEFAULT_PORT;
  args->hostname = DEFAULT_HOSTNAME;
  while ((c = getopt(ac, av, "n:p:h:")) != -1)
    {
      if (IS_OPT(optarg))
	{
	  std::cerr << "Optarg error" << std::endl;
	  return (1);
	}
      if (c == 'n')
	args->team_name = optarg;
      else if (c == 'p')
	args->port = atoi(optarg);
      else if (c == 'h')
	args->hostname = optarg;
      else
	return (1);
    }
  return (check_args(av[0], args));
}
#endif

// Getters ---------------------------------------------------------------------

std::string const&
Argument::getTeamName() const
{
  return this->_teamName;
}

std::string const&
Argument::getHostName() const
{
  return this->_hostName;
}

int
Argument::getPort() const
{
  return this->_port;
}
