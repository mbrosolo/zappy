#!/bin/bash
## export_git.sh for nibbler in /home/d-eima_g/work/nibbler
##
## Made by guillaume d-eimar-de-jabrun
## Login   <d-eima_g@epitech.net>
##
## Started on  Mon Feb 27 15:28:58 2012 guillaume d-eimar-de-jabrun
## Last update Thu Mar  8 14:39:37 2012 guillaume d-eimar-de-jabrun
##

if [[ $# != 2 ]]
then
    echo "Usage : $0 depot_local_svn message"
    exit 0
fi
if [[ ! -e $1/.svn ]]
then
    echo "$1/ n'est pas relie a un depot svn"
    exit 1
fi

cd $1
svn rm * --force -q
cd - >/dev/null
git checkout-index -a -f --prefix=$1/
cd $1

rm .gitignore export_git.sh
#rm -r example

svn add $(find . -wholename "*.svn" -prune -o -print) -q

svn ci -m "$2"
