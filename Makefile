##
## Makefile for a in /home/bourdi_b/
##
## Made by benjamin bourdin
## Login   <bourdi_b@epitech.net>
##
## Started on  Tue Jan 17 17:28:57 2012 benjamin bourdin
## Last update Sun Jul 15 18:44:41 2012 benjamin bourdin
##

REP_SERVER=		./server_src/
REP_CLIENT=		./client_src/
REP_GRAPH=		./graph_src/
REP_LIB=		./lib/

all:		    	lib client serveur graph

serveur:
			$(MAKE) -Wl,-rpath -Wl,lib/ -C $(REP_SERVER)

client:
			$(MAKE) -Wl,-rpath -Wl,lib/ -C $(REP_CLIENT)

graph:
			$(MAKE) -Wl,-rpath -Wl,lib/ -C $(REP_GRAPH)

lib:
			$(MAKE) -C $(REP_LIB)

lib.clean:
			$(MAKE) clean -C $(REP_LIB)

lib.fclean:
			$(MAKE) fclean -C $(REP_LIB)

lib.re:
			$(MAKE) re -C $(REP_LIB)

clean:
			$(MAKE) $@ -C $(REP_CLIENT)
			$(MAKE) $@ -C $(REP_SERVER)
			$(MAKE) $@ -C $(REP_GRAPH)

fclean:
			$(MAKE) $@ -C $(REP_CLIENT)
			$(MAKE) $@ -C $(REP_SERVER)
			$(MAKE) $@ -C $(REP_GRAPH)

re:			lib.re fclean all

doc:
			@doxygen doxygen.conf

.PHONY:			all clean fclean re serveur client lib doc graph
